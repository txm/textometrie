---
ititle: Documentation TXM
layout: page
lang: fr
ref: documentation
---

## Sommaire
{: .no_toc}

1. TOC
{:toc}

## Documentation du logiciel TXM pour poste Windows, Mac OS X ou Linux

### Manuel de TXM
{: id="txm-manual"}
{: .no_toc}
{: style="margin-bottom: 20px"}

#### Manuel de TXM 0.8 - en cours de rédaction

- [à lire en ligne](https://pages.textometrie.org/txm-manual) (HTML)
- [à imprimer](https://gitlab.huma-num.fr/txm/txm-manual/-/raw/master/txm-manual.pdf) (PDF)

#### Manuel de TXM 0.7

- [à lire en ligne]({{"/files/documentation/manual/0.7.9/fr/manual1.xhtml" | absolute_url}}) (HTML)
- [à imprimer]({{"/files/documentation/Manuel%20de%20TXM%200.7%20FR.pdf" | absolute_url}}) (PDF), [version anglaise]({{"/files/documentation/TXM%20Manual%200.7.pdf" | absolute_url}}) (PDF)

### Tutoriels vidéo
{: .no_toc}

#### 1) Atelier initiation à TXM 0.6 de Bénédicte Pincemin du 27 septembre 2012

* [Sommaire des vidéos de l'atelier](https://txm.gitpages.huma-num.fr/textometrie/html/enregistrement_atelier_initiation_TXM_fr.html) [^1](screencast)

#### 2) Atelier de formation Import de corpus dans TXM 0.8 d'Alexey Lavrentev en mars 2020

   Dans le cadre du cours *Édition numérique de sources textuelles* (LAF4204) de l'ENS de Lyon.

* Sommaire (screencast) :

  1. [Import TXM 1. Introduction](https://www.youtube.com/watch?v=pX_qwr_NxOs&list=PL_weW0aPormgLdNb0fSyS5lwaXU3it6WN&index=1&pp=iAQB) (10:55)
  1. [Import TXM 2. Presse-Papier et TXT+CSV](https://www.youtube.com/watch?v=TE4hxNOl2eA&list=PL_weW0aPormgLdNb0fSyS5lwaXU3it6WN&index=2&pp=iAQB) (23:22)
  1. [Import TXM 3. XML et module XML TEI Zero + CSV simple](https://www.youtube.com/watch?v=aqXAmJnJIRI&list=PL_weW0aPormgLdNb0fSyS5lwaXU3it6WN&index=3&pp=iAQB) (23:12)
  1. [Import TXM 4. TEI Oxgarage et module XML TEI Zero+CSV avancé (CSS et XSLT)](https://www.youtube.com/watch?v=unzbuwpfq00&list=PL_weW0aPormgLdNb0fSyS5lwaXU3it6WN&index=4&pp=iAQB) (18:54)

Télécharger les [**documents supports**](https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/supports/Cours%20%C3%89dition%20num%C3%A9rique%20de%20sources%20textuelles%20(LAF4204)%20-%20ENS%20de%20Lyon) pour les exercices.

[^1]: N'hésitez pas à régler les paramètres de qualité vidéo dans Youtube - utiliser la résolution 720p HD si votre connexion le permet.

### Documentation de la communauté
{: .no_toc}

- en anglais : [Explore, play, analyse your corpus with TXM: A short introduction of TXM](http://dhd-blog.org/?p=3384) de José Calvo et Silvia Gutiérrez, 17 Avril 2014.
- en allemand : [TXM Kurzreferenz](https://www.penflip.com/c.schoech/txm-kurzreferenz), C. Schoech,  9 Novembre 2014.

### Introduction très rapide aux concepts clés de TXM en 90 minutes (en anglais)
{: .no_toc}

- [PDF]({{"/files/documentation/Tutorial%20A%20Gentle%20Introduction%20To%20TXM%20Key%20Concepts%20In%20One%20And%20An%20Half%20Hour.pdf" | absolute_url}}) (en Anglais)

### Plaquettes de présentation de TXM

- en [français]({{"/files/documentation/TXM%20leaflet%20FR.pdf" | absolute_url}})
- en [anglais]({{"/files/documentation/TXM%20Leaftlet%20EN.pdf" | absolute_url}})

### Tutoriels

- [Tutoriel de construction d'éditions synoptiques à partir de PDF]({{"/files/documentation/tutorials/pdf-to-synoptic-edition" | absolute_url}})
- [Tutoriel d’importation de corpus TEI avec le module XML-TEI Zero + CSV]({{"/files/documentation/tutorials/import-xtz-simple-joubert" | absolute_url}})
- [Tutoriel d’importation de transcriptions d’enregistrements au format Word]({{"/files/documentation/tutorials/import-transcription-word" | absolute_url}})
- [Import d'articles d'Europresse dans TXM avec le module XML/w + CSV](https://groupes.renater.fr/wiki/txm-users/public/tutoriel_europresse)
- [Tutoriel R](http://zoonek2.free.fr/UNIX/48_R/all.html) (en anglais)
- Autres tutoriels disponibles sur la [page wiki de la liste txm-users](https://groupes.renater.fr/wiki/txm-users/public/tutoriels)

### Questions Fréquemment Posées

- [faq sur le wiki txm-users](https://groupes.renater.fr/wiki/txm-users/public/faq)

### Participer
{: .no_toc}

- signaler les [bugs](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel)
- demander de [nouvelles fonctionnalités](https://groupes.renater.fr/wiki/txm-users/public/demande_de_fonctionnalites)

### Documentation utilisateur complémentaire
{: .no_toc}

- [Guide de Transcription d'entretiens Transcriber pour TXM]({{"/files/documentation/Guide%20de%20Transcription%20d_entretiens%20Transcriber-TXM%200.3_FR.pdf" | absolute_url}})
- [Mémo pour le langage d'interrogation CQL]({{"/files/documentation/memo_cql.pdf" | absolute_url}}) ([.odt]({{"/files/documentation/memo_cql.odt" | absolute_url}}))
- [Manuel de Weblex]({{"/files/documentation/references/weblex.pdf" | absolute_url}}) : Weblex est le logiciel de Textométrie développé à Lyon avant TXM ; il n'est plus en ligne, mais sa documentation peut rester instructive, par exemple le Chapitre 9 "Manuel de référence des Expressions CQP".

### Diagnostic
{: .no_toc}

- [Diagnostic de TXM en ligne de commande](https://groupes.renater.fr/wiki/txm-users/public/tutoriel_ligne_de_commande)

## Documentation pour l'utilisateur du logiciel portail TXM

[Le tutoriel de la BFM](http://txm.ish-lyon.cnrs.fr/bfm/files/Tutoriel_TXM_BFM_V1.pdf) (Base de Français Médiéval) est la principale documentation utilisateur qu'on ait actuellement pour utiliser la version portail de TXM. Il est téléchargeable depuis la page d'accueil du [portail BFM](http://txm.bfm-corpus.org/).

Par ailleurs, peuvent être utiles :
{: style="margin-bottom: 5px"}

- [Bugs connus de la version actuelle](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_web/txmweb_0.4)
- [Demandes de fonctionnalités et évolutions envisageables ou envisagées](https://groupes.renater.fr/wiki/txm-users/public/demande_de_fonctionnalites_du_portail)

## Documentation pour l'administrateur du logiciel portail TXM

- [Prise en main rapide d'un portail TXM (Huma-Num)]({{"/files/documentation/Portail%20TXM/Manuel%20du%20portail%20TXM%200.6.3%20-%20prise%20en%20main%20rapide.pdf" | absolute_url}})
- [Installation du logiciel portail TXM](https://groupes.renater.fr/wiki/txm-info/public/install_a_txm_portal)
- Manuel d'administration de portail TXM
  - [Manuel d'administration du portail TXM 0.6.3]({{"/files/documentation/Portail%20TXM/Manuel%20du%20portail%20TXM%200.6.3%20-%20admin.pdf" | absolute_url}})
  - [Manuel d'administration du portail TXM 0.6.1]({{"/files/documentation/Portail%20TXM/Manuel%20du%20portail%20TXM%200.6.1%20-%20admin.pdf" | absolute_url}})
- [Diagnostics et redémarrage d'un portail TXM](https://groupes.renater.fr/wiki/txm-info/private/diagnostic_portail_txm)
- [Mise à jour du logiciel d'un portail TXM](https://groupes.renater.fr/wiki/txm-info/public/update_txm_portal_software)
- [Liste des portails TXM publics](https://groupes.renater.fr/wiki/txm-users/public/references_portails)

## Documentation pour les développeurs de la plateforme TXM

Le [wiki des développeurs de TXM](https://groupes.renater.fr/wiki/txm-info) est le site de référence pour le développement de la plateforme TXM. Contactez nous pour participer.

On y trouve notamment :
{: style="margin-bottom: 5px"}

- comment [installer l'environnement de développement](https://groupes.renater.fr/wiki/txm-info/public/preparation_env_dev_manuel)
- comment [accéder aux sources](https://groupes.renater.fr/wiki/txm-info/public/preparation_env_dev_manuel#recuperer_les_projets_txm_depuis_svn)
- la [Javadoc]({{"/Javadocs" | absolute_url}})
- la [documentation du package R textometry](https://cran.r-project.org/web/packages/textometry/textometry.pdf)
- des tutoriels concernant le développement dans la Toolbox et dans la version pour poste, l'architecture des modules d'importation, la procédure de traduction de l'interface graphique utilisateur, la procédure de construction des setups pour une release, etc.
- la [roadmap](https://groupes.renater.fr/wiki/txm-info/#chantiers) des versions successives de TXM
- quelques éléments de conception de composants à venir
- la liste des développeurs participant au projet
- une liste de technologies open-sources clés qui sont ou peuvent être utilisées dans TXM
- des liens vers les documentations de référence des composants de TXM

Toute la documentation disponible aussi [dans la documentation](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/).

## Composants de la plateforme TXM

- Moteur de recherche CQP
    - [CQP Manual](http://corpora.dslo.unibo.it/TCORIS/cqpman.pdf) (1999) [intéressant comme introduction, ne rend pas compte de tous les ajouts récents à CQP]
    - [CQP Tutorial](http://cwb.sourceforge.net/files/CQP_Tutorial.pdf) (2019) [intéressant pour parcourir toutes les fonctionnalités disponibles] 
    - [CQP Corpus Administrator's Manual]({{"/files/documentation/cqp-technical-manual.pdf" | absolute_url}}) (1994) [intéressant pour l'architecture, ne rend pas compte de tous les ajouts récents à CQP] 
    - [Corpus Encoding Tutorial](http://cwb.sourceforge.net/files/CWB_Encoding_Tutorial.pdf) (2002) [reprend la section 3.2 du CQP Corpus Administrator's Manual]
    - [documentation du projet Open CWB](http://cwb.sourceforge.net/documentation.php) [parcours introductif technique rapide]
    - [CQI Tutorial](http://cwb.sourceforge.net/files/cqi_tutorial.pdf) (2000) [API de référence à CQP] 
- Moteur statistique R
    - [Introduction à R et au tidyverse](https://juba.github.io/tidyverse)
    - [An Introduction to R](http://cran.r-project.org/doc/manuals/R-intro.html) [très pédagogique, pour la syntaxe]
    - François Husson, Sébastien Lê, Jérôme Pagès (2009) Analyse de données avec R. Presses Universitaires de Rennes [introduction pédagogique à l'AFC s'appuyant sur des exemples et travaux pratiques avec FactomineR, le composant utilisé par TXM]
    - Richard A. Becker, John M. Chambers, Allan R. Wilks (1988) *The New S Language, A programming environment for data analysis and graphics*, Pacific Grove, Wadsworth & Brooks/Cole [la référence, dite "the blue book", par les auteurs du langage, très pédagogique]
    - Brian S. Everitt & Torsten Hothorn (2006) *A Handbook of Statistical Analyses Using R*, Boca Raton, Chapman & Hall/CRC [par et pour des statisticiens, très clair, orienté modèles statistiques]
    - Pierre-André Cornillon, Arnaud Guyader, François Husson, Nicolas Jégou, Julie Josse, Maela Kloareg, Éric Matzner-Løber, Laurent Rouvière (2008), *Statistiques avec R*, Rennes, Presses Universitaires de Rennes [type matériel de cours ; une première partie d'introduction au langage, une seconde partie de mise en oeuvre, sous forme de fiches, sur des problèmes statistiques classiques]
    - R. Harald Baayen (2008) *[Analyzing Linguistic Data, A practical introduction to statistics](http://www.sfs.uni-tuebingen.de/~hbaayen/publications/baayenCUPstats.pdf)*, Cambridge, Cambridge University Press. [appliqué à des données linguistiques de type très différents (psycho-linguistique, classification de langues, productivité morphologique, etc.) ; pas orienté maîtrise du langage et programmation]
    - Gries (2009) *Quantitative Corpus Linguistics With R: A Practical Introduction*
    - [site officiel de R](http://www.r-project.org)
    - [Writing R Extensions](http://cran.r-project.org/doc/manuals/R-exts.html) [pour tout ce qui a trait à la conception, le test, la documentation, le packaging d'une librairie, comme textometrieR]
- Moteur de script Groovy
    - [Une introduction à Groovy](https://ericreboisson.developpez.com/tutoriel/java/groovy)
    - [Site officiel du langage Groovy](https://groovy-lang.org)
    - Dierk König, Andrew Glover, Paul King, Guillaume Laforge et al., *Groovy in action*, Greenwich : Manning, 2007.
    - Barclay, Kenneth A. and Savage, W. J., *Groovy programming: an introduction for Java developers*, Morgan Kaufmann Publishers, 2007.
    - Venkat Subramaniam, *Programming Groovy : dynamic productivity for the Java developer*, Daniel H. Steinberg ed., Raleigh, N.C. : Pragmatic Bookshelf, 2008.

## Textométrie : encodage des ressources textuelles

- Habert Benoît, Fabre Cécile, Issac Fabrice. (1998) De l'écrit au numérique : constituer, normaliser, exploiter les corpus électroniques. Paris, InterÉditions/Masson, Informatiques
- Habert Benoît. (2005) Instruments et ressources électroniques pour le français, Paris, Orphys 
- XML & TEI
    - [A gentle introduction to XML](http://www.tei-c.org/release/doc/tei-p5-doc/en/html/SG.html)
    - [Qu’est-ce que la Text Encoding Initiative ?](https://books.openedition.org/oep/1237?lang=fr)
    - [Éléments structurants fondamentaux](http://www.tei-c.org/release/doc/tei-p5-doc/fr/html/DS.html)
    - [L'entête TEI](http://www.tei-c.org/release/doc/tei-p5-doc/fr/html/HD.html)
    - [Documentation de tous les élements](http://www.tei-c.org/release/doc/tei-p5-doc/fr/html/TD.html)
    - [Éléments disponibles pour tous les documents TEI](http://www.tei-c.org/release/doc/tei-p5-doc/fr/html/CO.html)
    - [L'infrastructure de la TEI](http://www.tei-c.org/release/doc/tei-p5-doc/fr/html/ST.html)
    - [Introduction à ODD](https://tei-c.org/guidelines/customization/getting-started-with-p5-odds) [ce qu'il faut retenir de la TEI aujourd'hui : une capacité d'adaptation contrôlée]
        - [page ODD du wiki TEI](https://wiki.tei-c.org/index.php?title=ODD)
    - [Interface de paramétrisation Roma](http://www.tei-c.org/Roma)
    - [Manuel de Roma](http://www.tei-c.org/Guidelines/Customization/use_roma.xml)
- TAL (Traitement Automatique de la Langue naturelle)
    - [Revue TAL](http://www.atala.org/revuetal)
    - [TreeTagger](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger)
        - Helmut, Schmid (1994) - [Probabilistic Part-of-Speech Tagging Using Decision Trees](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tree-tagger1.pdf)
 
## Documentation complémentaire

Voir aussi la page de [documentation complémentaire](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/).