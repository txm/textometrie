---
ititle: Tutoriel d'installation de TreeTagger dans TXM
layout: page
lang: fr
ref: install-treetagger
---

# Tutoriel d'installation de TreeTagger dans TXM [^1]
{: style="text-align: center; padding-bottom: 30px" .no_toc}

Pour automatiser la lemmatisation et l'étiquetage
morphosyntaxique de votre corpus lors de son importation dans TXM,
veuillez suivre l'un des deux tutoriels **A** ou **B** ci-dessous selon votre version de TXM.

## A. Installation de TreeTagger dans TXM 0.8.0 et versions suivantes

À partir de TXM 0.8.0, deux extensions dédiées à TreeTagger installent
automatiquement le logiciel TreeTagger et les modèles français et anglais :
{: style="margin-bottom: 5px"}

1. Lancer la commande "Fichier &gt; Ajouter une extension"
2. Sélectionner les lignes "TreeTagger software" et "TreeTagger models"
   pour installer le logiciel TreeTagger et les modèles français et
   anglais
3. Valider les étapes suivantes
4. Après le redémarrage de TXM, TreeTagger est opérationnel
5. Pour installer d'autres modèles de langues, suivre l'[étape 4](#step-4) de
   l'installation manuelle ci-dessous
6.  Fin

## B. Installation "manuelle" de TreeTagger dans TXM 0.7.9 et versions précédentes

Ce tutoriel va vous guider pour:
{: style="margin-bottom: 5px"}

1. Télécharger le logiciel TreeTagger et un ou plusieurs de ses modèles
    linguistiques
2. Indiquer à TXM où se trouve le logiciel TreeTagger et les modèles
    linguistiques.

### B.1 Télécharger les fichiers depuis le web et les préparer

En étant connecté à Internet :
{: style="margin-bottom: 5px"}

1. Télécharger l'archive du logiciel TreeTagger à partir du site de TreeTagger :
    -   [Windows (32bit et
        64bit)](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tree-tagger-windows-3.2.zip)
    -   [Mac OS
        X](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tree-tagger-MacOSX-3.2.tar.gz)
    -   [Linux
        64bit](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tree-tagger-linux-3.2.tar.gz)
    -   [Linux
        32bit](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tree-tagger-linux-3.2-old.tar.gz)
2. Extraire le contenu de l'archive compressée (bin, cmd, doc, FILES, LICENSE and README) dans un dossier
   nommé '**treetagger**' situé dans votre dossier d'applications [^2].
   En fonction de votre système d'exploitation, dans :

    |  Windows | **`C:\Programmes\treetagger`**
    |  Windows XP | **`C:\Program Files\treetagger`**
    |  Mac OS X | **`/Applications/treetagger`**
    |  Linux | **`/usr/lib/treetagger`**

   **Vérification** : Une fois extrait, ce dossier doit contenir les
   dossiers et fichiers suivants : bin, cmd, doc, FILES, LICENSE et README.

   **Remarque**: Cette manière d'installer TreeTagger est spécifique à TXM.
   Vous n'avez vraiment qu'à extraire le contenu de l'archive TreeTagger.
   Vous n'avez pas besoin de suivre les instructions supplémentaires contenues dans
   un fichier INSTALL.txt se trouvant dans l'archive.
3. Créer un dossier '**treetagger-models**' dans votre dossier 'TXM-0.8.1' [^3]
    qui contiendra les modèles de langues de TreeTagger.
4. {: id="step-4"}Télécharger à partir du site de TreeTagger le modèle (fichier
   compressé '\*.gz') de chaque langue dont vous souhaitez une
   lemmatisation :
    -   français \[fr\]: modèle
        [french.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/french.par.gz)
        ([jeu d'étiquettes
        français](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/french-tagset.html))
    -   français parlé \[frp\]: modèle
        [perceo](https://cnrtl.fr/corpus/perceo) à récupérer sur le site
        du CNRTL
    -   ancien français \[fro\]: modèle
        [fro.par](https://gitlab.huma-num.fr/bfm/bfm-language-models/-/raw/main/TreeTagger/fro.par) model ([jeu
        d'étiquettes](http://bfm.ens-lyon.fr/IMG/pdf/Cattex2009_2.0.pdf))
    -   anglais<sub>1</sub> \[en\]: modèle
        [english.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/english.par.gz)
        construit à partir de la Penn treebank ([jeu d'étiquettes Penn
        treebank](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/Penn-Treebank-Tagset.pdf))
    -   anglais<sub>2</sub> \[en\]: modèle
        [english-bnc.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/english-bnc.par.gz)
        construit à partir du British National Corpus ([jeu d'étiquettes
        BNC](https://www.natcorp.ox.ac.uk/docs/c5spec.html))
    -   allemand \[de\]: modèle
        [german.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/german.par.gz)
        ([jeu
        d'étiquettes](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/stts_guide.pdf))
    -   italien \[it\]: modèle
        [italian.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/italian.par.gz)
        ([jeu
        d'étiquettes](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/italian-tagset.txt))
    -   espagnol<sub>1</sub> \[es\]: modèle
        [spanish.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/spanish.par.gz)
        ([jeu
        d'étiquettes](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/spanish-tagset.txt))
    -   espagnol<sub>2</sub> \[es\]: modèle
        [spanish-ancora.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/spanish-ancora.par.gz)
        construit à partir du corpus Ancora ([jeu
        d'étiquettes](https://clic.ub.edu/corpus/webfm_send/18))
    -   russe \[ru\]: modèle
        [russian.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/russian.par.gz)
        ([jeu
        d'étiquettes](https://corpus.leeds.ac.uk/mocky/ru-table.tab))
    -   latin<sub>1</sub> \[la\]: modèle
        [latin.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/latin.par.gz)
        construit par Gabriele Brandolini ([jeu
        d'étiquettes](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/Lamap-Tagset.pdf))
    -   latin<sub>2</sub> \[la\]: modèle
        [latinIT.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/latinIT.par.gz)
        construit à partir de l'Index Thomisticus Treebank ([jeu
        d'étiquettes](https://itreebank.marginalia.it/doc/Tagset_IT.pdf))
    -   grec \[el\]: modèle
        [greek.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/greek.par.gz)
        ([jeu
        d'étiquettes](https://nlp.ilsp.gr/nlp/tagset_examples/tagset_en))
    -   grec ancien \[grc\]: modèle
        [ancient-greek.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/ancient-greek.par.gz)
        ([jeu
        d'étiquettes](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tagsetdocs.txt))
    -   Autres langues : voir la liste de tous les modèles de langue disponibles dans la section '[Parameter files](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger)' du site Web de TreeTagger
5. Décompresser chaque fichier compressé de modèle dans votre dossier
   'treetagger-models'.
   Sous Windows, si vous n'avez pas de logiciel
   extracteur-décompresseur compatible avec les fichiers '\*.gz', nous
   vous recommandons le logiciel ouvert [7-zip](https://www.7-zip.org).
6. Renommer chaque fichier de modèle en utilisant les codes de langues
   [ISO 639-1](https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1)
   à deux lettres. Par exemple :
    -   'french.par' en 'fr.par' pour le fichier modèle français
    -   'english.par' en 'en.par' pour le fichier modèle anglais
    -   etc. 

   **Sous Windows et Mac OS X** : Par défaut, ces systèmes masquent à
   l'utilisateur les extensions de fichiers dont il gère le type. Dans
   ce cas, on peut se trouver dans une situation où l'on pense avoir
   renommé un fichier 'fr.bin' en 'fr.par' alors que le nom complet
   réel du fichier reste 'fr.par.bin'.

   Dans ce cas il faut accéder à l'affichage complet des noms de
   fichiers puis les renommer :
    - Sous Windows :
        1. Pour afficher les noms complets des fichiers avec leur
               extension, vous pouvez suivre ce tutoriel :
               [Afficher-les-extensions-et-les-fichiers-caches-sous-windows](https://www.commentcamarche.net/faq/825-afficher-les-extensions-et-les-fichiers-caches-sous-windows#solution-simple)
        2. Vous pouvez alors renommer le nom complet.
    - Sous Mac OS X :
        1. Faire un clic droit sur l'icone du fichier (Ctrl-clic avec
               la souris ou bien cliquer à deux doigts sur le trackpad)
        2. Lancer la commande 'Lire les informations'
        3. Éditer le champ 'nom et extension' : supprimer l'extension
               '.bin'.
        4. Fermer la fenêtre d'informations.
            
    **Vérification** : Le dossier 'treetagger-models' doit contenir le fichier
    'fr.par' qui fait environ 17 Mo, et éventuellement les fichiers
    d'autres modèles de langues (à titre indicatif, 'en.par' : 14 Mo ,
    'de.par' : 37 Mo , etc.).

### B.2 Dans TXM

#### 1. Régler les préférences TreeTagger

1.  Sélectionner l'entrée 'Édition / Préférences' du menu principal
2.  Aller à la page 'TXM / Avancé / TAL / TreeTagger' (voir la figure 1)
3.  Renseigner le champ 'Chemin vers le dossier d'installation' : cliquer sur 'Parcourir...', puis sélectionner
        votre dossier 'treetagger' (voir étape 2.) et terminer par 'OK'
4.  Renseigner le champ 'Chemin vers le dossier de modèles linguistiques' : cliquer sur 'Parcourir...', puis sélectionner
        votre dossier 'treetagger-models' et terminer par 'OK'
5.  Terminer par 'OK' pour enregistrer ces réglages

<figure markdown="1">
{: style="margin-top: 1px"}
![]({{"/img/preferences-treetagger-fr.png" | absolute_url}} "Figure 1 : Préférences TreeTagger dans TXM")
<figcaption style="text-align: center;">
 Figure 1 : Préférences TreeTagger dans TXM
 </figcaption>
</figure>

####  2. Vérifier votre installation


1.  Voir l'entrée de FAQ [Comment vérifier que TreeTagger est bien paramétré dans TXM&nbsp;?](https://groupes.renater.fr/wiki/txm-users/public/faq#comment_verifier_que_treetagger_est_bien_parametre_dans_txm)

En cas de problème, vous trouverez de l'aide supplémentaire dans la
[FAQ](https://groupes.renater.fr/wiki/txm-users/public/faq#treetagger_ne_fonctionne_pas_comment_bien_regler_treetagger_pour_txm).

Si vous ne parvenez pas à aller jusqu'au bout de cette procédure
d'installation, veuillez nous contacter via la liste de diffusion des
utilisateurs de TXM (txm-users AT groupes.renater.fr) après vous être [inscrit](https://groupes.renater.fr/sympa/subscribe/txm-users) à la
liste de diffusion, ou bien <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contactez l'équipe TXM par mail</a>.

------------------------------------------------------------------------
Notes:

[^1]: La licence de diffusion de TreeTagger n'autorise pas la livraison de TreeTagger embarqué (ou inclus) dans un logiciel à usage commercial. Comme la licence de TXM n'interdit à personne d'avoir une activité commerciale avec TXM, nous respectons les souhaits de l'auteur de TreeTagger en n'incluant pas ce logiciel dans la distribution de TXM. Pour plus d'informations, voir le [site web de TreeTagger](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger)

[^2]: Si vous n'avez pas les droits d'accès pour créer le dossier dans le dossier des applications, vous pouvez les créer dans votre dossier personnel.

[^3]: Le dossier 'TXM-0.8.1' se trouve dans votre dossier personnel.

