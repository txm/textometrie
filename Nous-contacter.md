---
ititle: Contact
layout: page
lang: fr
ref: nous-contacter
---

# Nous contacter

- Par mail : <a href="mailto:{{ "textometrie@groupes.renater.fr" | encode_email }}" title="textometrie AT groupes.renater.fr">textometrie AT groupes.renater.fr</a>
- Site web des utilisateurs de TXM : <https://groupes.renater.fr/wiki/txm-users>
- Site web des développeurs de TXM : <https://groupes.renater.fr/wiki/txm-info>
- Site web du projet : <http://www.textometrie.org>
- Adresse postale :  
  Serge Heiden - groupe Cactus  
  Laboratoire IHRIM - ENS de Lyon  
  15 parvis René Descartes - BP7000  
  69342 Lyon Cedex 07  
  FRANCE  
- Téléphone : +33 (0)6 22 00 38 83






