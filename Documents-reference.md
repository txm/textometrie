---
ititle: Documents référence
layout: page
lang: fr
ref: documents-référence
---

## Monographies générales sur la méthode

Lebart L., Salem A. (1994) - Statistique Textuelle. Dunod, Paris, 342 p.<br/>
[[consulter](http://books.google.com/books?id=_kNrGwAACAAJ&hl=fr&cd=1&source=gbs_ViewAPI) – [emprunter](http://www.sudoc.abes.fr/DB=2.1/LNG=FR/SRCH?IKT=12&TRM=003289990)] [^1] – [version auteur disponible en ligne : [à Paris 3](http://lexicometrica.univ-paris3.fr/livre/st94/st94-tdm.html), [sur le site de Dtm-Vic](http://www.dtmvic.com/ST.html) ]

**Sommaire**

- [Préface, Avant propos, Introduction](http://www.dtmvic.com/doc/ST.chap_0.pdf)
- [Chapitre 1](http://www.dtmvic.com/doc/ST.chap_1.pdf) : Domaines et problèmes
- [Chapitre 2](http://www.dtmvic.com/doc/ST.chap_2.pdf) : Les unités de la statistique textuelle
- [Chapitre 3 ](http://www.dtmvic.com/doc/ST.chap_3.pdf) : L'analyse des correspondances
- [Chapitre 4](http://www.dtmvic.com/doc/ST.chap_4.pdf) : La classification automatique des formes et des textes
- [Chapitre 5](http://www.dtmvic.com/doc/ST.chap_5.pdf) : Typologies, visualisations
- [Chapitre 6](http://www.dtmvic.com/doc/ST.chap_6.pdf) : Éléments caractéristiques, réponses ou  textes modaux
- [Chapitre 7](http://www.dtmvic.com/doc/ST.chap_7.pdf) : Partitions longitudinales, contiguïté
- [Chapitre 8](http://www.dtmvic.com/doc/ST.chap_8.pdf) : Analyse discriminante textuelle
- [Annexes et fin](http://www.dtmvic.com/doc/ST_annexes_et_Fin.pdf) : Description sommaire de quatre logiciels, Esquisse des algorithmes et  structures de données, Glossaire, Références bibliographiques, Index des auteurs.

Lebart L., Salem A., Berry L. (1998) - Exploring Textual Data. Kluwer academic pub., Boston, 222 p.<br/>
[adaptation anglaise largement actualisée de l’ouvrage « Statistique Textuelle » – [consulter](http://books.google.com/books?id=24nvLSkVcJsC&printsec=frontcover&hl=fr&cd=1&source=gbs_ViewAPI) – [emprunter](http://www.sudoc.abes.fr/DB=2.1/LNG=FR/SRCH?IKT=12&TRM=049397265)]
  

## Articles de référence de modèles textométriques

Guilbaud G-Th. (1980) - Zipf et les fréquences, Mots N° 1, p. 97-126<br/>
[[consulter en ligne sur Persée](http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1980_num_1_1_1007)]

Lafon P. (1980) - Sur la variabilité de la fréquence des formes dans un corpus, Mots N°1 , p. 127-165<br/>
[modèle des spécificités – [consulter en ligne sur Persée](http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1980_num_1_1_1008)]

Tournier M. (1980) - D’où viennent les fréquences de vocabulaire ?, Mots N°1, p 189-212.<br/>
[lexique, usages, vocabulaire et probabilités – [consulter en ligne sur Persée](http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1980_num_1_1_1010)] [^2]

Lafon P. (1981) - Statistiques des localisations des formes d’un texte, Mots, N° 2, mars 1981, p. 157-188<br/>
[modèle des rafales – [consulter en ligne sur Persée](http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1981_num_2_1_1026)] [^2]

Lafon P. (1981) - Analyse lexicométrique et recherche des cooccurrences, Mots N°3 , p. 95-148<br/>
[modèle des cooccurrences – [consulter en ligne sur Persée](http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1981_num_3_1_1041)] [^2]

Lafon P., Salem A. (1983) - L’inventaire des segments répétés d’un texte, Mots N°6, p. 161-177<br/>
[modèle des segments répétés – [consulter en ligne sur Persée](http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1983_num_6_1_1101)] [^2]

Salem A. (1988) - Approches du temps lexical. Statistique textuelle et séries chronologiques, Mots, 17, octobre 1988, p. 105-143<br/>
[modèle des séries chronologiques – [consulter en ligne sur Persée](http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1988_num_17_1_1401)] [^2]
  
## Actes de la conférence JADT

Version électronique des [Actes des conférences JADT](http://lexicometrica.univ-paris3.fr/jadt/) sur le site Lexicometrica.

## Autres références

Brunet, Etienne (1986) - Actes du Colloque international CNRS "Méthodes quantitatives et informatiques dans l’étude des textes" (en hommage à Charles Muller), Genève, Editions Slatkine, XIV- 948 pages

Brunet, Etienne (2006) - "Le corpus conçu comme une boule",
Corpus en Lettres et Sciences sociales : des documents numériques à l’interprétation, Actes du XVIIe Colloque d’Albi Langages et Signification, Albi, 10-14 juillet 2006, Carine Duteil-Mougel & Baptiste Foulquié (éds), ISBN 2-907955-12-18, pp. 69-78<br/>[[accès en ligne dans Texto!](http://www.revue-texto.net/1996-2007/Parutions/Livres-E/Albi-2006/Brunet.pdf), ISSN 1773-0120, juin 2006, vol. XI, n°2]

Brunet, Etienne (2009) - Comptes d’auteurs. Tome I, Etudes statistiques, de Rabelais à Gracq, Textes édités par Damon Mayaffre, préface d’Henri Béhar, Paris, Champion, 2009, 395 pages, ISBN 978-2-7453-2019-3 (premier tome d’une série de trois, rééditant des articles choisis d’Etienne Brunet)
[[emprunter](https://www.sudoc.fr/140701605)]

Brunet, Etienne (2011) - Ce qui compte : méthodes statistiques, Textes édités par Céline Poudat, préface de Ludovic Lebart, Paris, Champion, 2011, 371 pages, ISBN 978-2-7453-2225-8 (Écrits choisis, tome II).
[[emprunter](https://www.sudoc.fr/15306045X)]

Brunet, Etienne (2016) - Tous comptes faits : écrits choisis, tome III, Questions linguistiques, Textes édités par Bénédicte  Pincemin, préface de François Rastier, Paris, Champion, 2016, 417 pages, ISBN 978-2-7453-3553-1 (Écrits choisis, tome III).
[[emprunter](https://www.sudoc.fr/195788648), [édition numérique](https://www.sudoc.fr/258949716)]

Fénelon J.P. (1981) - Qu’est-ce-que l’analyse des données ?, Lefonen, Paris. Réédité en 1999 par SEISAM, Paris<br/>
[fiches pédagogiques – [consulter](http://books.google.com/books?id=SBpwGQAACAAJ) – [emprunter (édition originale)](http://www.sudoc.abes.fr/DB=2.1/LNG=FR/SRCH?IKT=12&TRM=022282033)  ([2e édition](http://www.sudoc.abes.fr/DB=2.1/LNG=EN/SRCH?IKT=12&TRM=053617657))]

Geffroy Annie, Lafon Pierre, Tournier Maurice (1974) - L’indexation minimale, Plaidoyer pour une non-lemmatisation, Colloque sur l’analyse des corpus linguistiques : "Problèmes et méthodes de l’indexation minimale", Strasbourg 21-23 mai 1973

Husson François, Lê Sébastien & Pagès Jérôme (2009) - Analyse de données avec R, Presse Universitaires de Rennes (ISBN 978-2-7535-0938-2, 15 euros). 2e édition revue et augmentée en 2016, ISBN 978-2-7535-4869-5. [[emprunter](https://www.sudoc.fr/191940313)]

Lafon Pierre (1984) - Dépouillements et statistiques en lexicométrie, Paris : Slatkine-Champion. Avec la préface "De la lemmatisation" de Charles Muller, pp.I-XII

Lebart, Ludovic, Pincemin, Bénédicte et Poudat Céline (2019) - Analyse des données textuelles, Québec, Presses de l'université du Québec, 2019, 472 pages, ISBN 978-2-7605-5052-0.
[[emprunter](https://www.sudoc.fr/241546060), [édition numérique](https://www.sudoc.fr/238571777)]

Luong Xuan (éd.) (2003) - La distance intertextuelle, Corpus, 2<br/>
[[consulter en ligne sur Revues.org](https://journals.openedition.org/corpus/52?lang=fr)]

Muller Charles (1973) - Initiation aux méthodes de la statistique linguistique, Champion, coll. Unichamp, 1992 (réimpression de l’édition Hachette de 1973)

Muller C. (1977) - Principes et méthodes de statistique lexicale, Hachette, Paris

Née, Émilie (dir.) (2017) - Méthodes et outils informatiques pour l'analyse des discours, Rennes, Presses universitaires de Rennes, 2017, 248 pages, ISBN 978-2-7535-5499-3.
[[emprunter](https://www.sudoc.fr/204082358), [édition numérique](https://www.sudoc.fr/268106770)]

Poudat, Céline, et Landragin, Frédéric (2017) - Explorer un corpus textuel : méthodes, pratiques, outils, Louvain-la-Neuve : De Boeck Supérieur, 2017, 239 pages, ISBN 978-2-8073-0563-2
[[emprunter](https://www.sudoc.fr/199556539), [édition numérique](https://www.sudoc.fr/24268078X)]

Tournier Maurice (1985a) - « Sur quoi pouvons-nous compter ? Réponse à Charles Muller », Verbum, hommage à Hélène Nais, pp. 481-492

Viprey J.-M. (2002) - Analyses textuelles et hypertextuelles des Fleurs du mal, Champion, Paris

Volle M. (1980) - Analyse des données, Economica, Paris

## Manuels de logiciels liés au projet ANR Textométrie

-  [DTM](http://www.dtmvic.com/06_TutorialF.html)
-  [Hyperbase](ftp://ancilla.unice.fr/manuel.pdf)
-  [Lexico 3](http://lexi-co.com/ressources/manuel-3.41.pdf)
-  [SATO](http://www.ling.uqam.ca/sato/satoman-fr.html)
-  [Trameur](http://tal.univ-paris3.fr/trameur/leMetierLexicometrique.pdf)
-  [Weblex](http://textometrie.ens-lyon.fr/IMG/pdf/weblex.pdf)
-  [Xaira](http://projects.oucs.ox.ac.uk/xaira/Doc/oldrefman.xml?ID=body.1_div.2)
  
## Quelques thèses

Beaudouin Valérie (2002) – Mètre et rythmes du vers classique. Corneille et Racine, Paris : Champion, coll. « Lettres numériques »

Bourion E. (2001). [L’aide à l’interprétation des textes électroniques](http://www.revue-texto.net/1996-2007/Corpus/Publications/Bourion/Bourion_Aide.html). Thèse de doctorat, Sciences du langage, Université de Nancy II

Martinez William (2003) - Contribution à une méthodologie de l’analyse des cooccurrences lexicales multiples dans les corpus textuels, Thèse de troisième cycle, Université de Paris 3 Sorbonne Nouvelle, décembre 2003

Zimina-Poirot Maria (2004) - [Approches quantitatives de l’extraction de ressources traductionnelles à partir de corpus parallèles](http://www.cavi.univ-paris3.fr/ilpga/ed/student/stmz/ED268-PagePersoMZ_fichiers/stmz/page8.htm), Thèse pour le Doctorat en Sciences du langage, Université de la Sorbonne nouvelle - Paris 3, soutenue le 26 novembre 2004, 328 p.


## Notes

[^1]: Cliquer sur le lien « Où trouver ce document ? » de la page du SUDOC pour lister les bibliothèques où emprunter l'ouvrage en France.
[^2]: Non implémenté dans la plateforme TXM au 30 Avril 2010.

