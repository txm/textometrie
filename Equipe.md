---
ititle: Équipe TXM
layout: page
lang: fr
ref: équipe
---

## L’équipe TXM

| **Prénom** | **Nom** | **Rôle** | **Téléphone** | **e-mail** | **jabber** |
| Matthieu  | DECORDE                                                        | Développeur logiciel principal | +33.(0)4.37.37.62.16 | matthieu *POINT* decorde *AT* ens-lyon.fr | mdecorde *AT* jabber.ens-lyon.fr |
| Serge     | [HEIDEN](http://ihrim.ens-lyon.fr/auteur/heiden-serge)         | Responsable du projet TXM, Textométrie & Philologie numérique, Développement, Diffusion, Valorisation | +33.(0)6.22.00.38.83 | slh *AT* ens-lyon.fr | sheiden *AT* jabber.ens-lyon.fr |
| Sébastien | JACQUOT                                                        | Développeur logiciel | +33.(0)3.81.66.54.22 | sebastien *POINT* jacquot *AT* univ-fcomte.fr | |
| Alexei    | [LAVRENTIEV](http://ihrim.ens-lyon.fr/auteur/lavrentev-alexey) | Philologie numérique, encodage et traitement de corpus XML-TEI | +33.(0)4.37.37.63.10 | alexei *POINT* lavrentev *AT* ens-lyon.fr | |
| Bénédicte | [PINCEMIN](http://ihrim.ens-lyon.fr/auteur/pincemin-benedicte) | Utilisatrice pilote, Textométrie | +33.(0)4.37.37.66.42 | benedicte *POINT* pincemin *AT* ens-lyon.fr | |

<br/>

![]({{ "/img/equipe-top.jpg" | absolute_url }})

De gauche à droite : Serge Heiden, Matthieu Decorde, Bénédicte Pincemin, Alexis Lavrentiev & Sébastien Jacquot

