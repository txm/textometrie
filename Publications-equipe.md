---
ititle: Publications équipe TXM
layout: page
lang: fr
ref: publications-équipe
---

# Publications de l'équipe

{% bibliography --file Publications de l’équipe %}

