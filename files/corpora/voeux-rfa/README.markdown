VOEUXRFA corpus
======

Contributed by Sascha Diwersy, Universität zu Köln

The corpus is composed of the Christmas and the New Year's addresses
delivered by the Presidents and the Chancellors of the Federal Republic
of Germany since 1987.

The texts are published in the Bulletin edited by the Presse- und
Informationsamt der Bundesregierung (Press and Information Office of the
Federal Government of Germany) and have been retrieved from the
electronic archive accessible at
[http://www.bundesregierung.de/Webs/Breg/DE/Service/Bulletin/\_node.html](http://www.bundesregierung.de/Webs/Breg/DE/Service/Bulletin/_node.html)
.

The corpus is licensed under a Creative Commons
Attribution-NonCommercial-NoDerivs 3.0 Unported License.

The sample texts have undergone orthographic normalization. They have
been tokenized using a script implemented by Stefanie Dipper (University
of Bochum) and distributed at
[http://www.linguistics.ruhr-uni-bochum.de/\~dipper/tokenizer.html](http://www.linguistics.ruhr-uni-bochum.de/~dipper/tokenizer.html)
. The subsequent PoS tagging, lemmatization, morphological tagging and
syntactic parsing of the texts have been carried out by means of the NLP
chain mate-tools (available at
[https://code.google.com/p/mate-tools](https://code.google.com/p/mate-tools)),
v. 3.3.

Encoded word properties
------

* word: word form
* lemma: lemma associated with the word
* pos: PoS tag associated with the word [1](#sdfootnote1sym)
* feat: set of morphological features associated with the word
* nhead: sentence internal token counter associated with the head in the dependency relation implicating the word
* depfunc: label of the dependency relation implicating the word [2](#sdfootnote2sym)
* headform: word form of the head in the dependency relation implicating the word
* headlemma: lemma of the head in the dependency relation implicating the word
* headpos: PoS tag associated with the head in the dependency relation implicating the word
* headfeat: set of morphological features associated with the head in the dependency relation implicating the word

Descriptors associated with the structural unit <text\>
------

* id: unique identifier of the text in the corpus
* type: type of address (as a distinction between the Christmas address delivered by the Federal President [VoeuPRE_RFA] and the New Year's address delivered by the Federal Chancellor [VoeuCHA_RFA])
* year: year of the address
* date: date on which the address was delivered (format: YYYY-MM-DD)
* speaker: name of the speaker
* speakersig: signature associated with the speaker's name
* num: running number of the text in the corpus

[1](#sdfootnote1anc) For a description of the tagset see the TIGER
annotation scheme for morphology available at
[http://www.ims.uni-stuttgart.de/forschung/ressourcen/korpora/TIGERCorpus/annotation/index.html](http://www.ims.uni-stuttgart.de/forschung/ressourcen/korpora/TIGERCorpus/annotation/index.html).

[2](#sdfootnote2anc) For a description of the tagset see the TIGER
annotation scheme for syntax available at
[http://www.ims.uni-stuttgart.de/forschung/ressourcen/korpora/TIGERCorpus/annotation/index.html](http://www.ims.uni-stuttgart.de/forschung/ressourcen/korpora/TIGERCorpus/annotation/index.html).
