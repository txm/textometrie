---
ititle: Corpus TDM80J TXM
layout: page
lang: fr
ref: corpus-tdm80j
---

# TDM80J

Le corpus TDM80J est composé du texte « Le tour du monde en quatre vingt jours » de Jules Verne (1873), soit 71.927 mots pour 1 texte.

Il s'agit de l'édition J. Hetzel et Compagnie, 1873, transcrite par [Wikisource](https://fr.wikisource.org/wiki/Page:Verne_-_Le_Tour_du_monde_en_quatre-vingts_jours.djvu/3) et encodée en *[XML-TEI P5](https://tei-c.org/release/doc/tei-p5-doc/fr/html/index.html)* par Serge Heiden pour le projet Textométrie.

Le corpus contient une édition synoptique affichant côte-à-côte l’édition TEI du texte, incluant les images des illustrations du livre, et les images du fac-similé du livre du site Wikisource.

- **[corpus binaire](https://gitlab.huma-num.fr/txm/txm-ressources/-/blob/master/corpora/TDM80J/bin/TDM80J-2020-07-20.txm)** : À charger dans TXM avec « Fichier > Charger > Un corpus binaire (.txm)... ». Le texte a été lemmatisé et étiqueté morpho-syntaxiquement par TreeTagger ;
- **[sources](https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/TDM80J/src/tdm80j)** : à importer dans TXM avec « Fichier > Importer > XML-TEI Zero + CSV ».

La transcription Wikisource est dans le domaine public. Le texte source XML-TEI P5 et le corpus binaire TXM sont sous licence [Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0)](http://creativecommons.org/licenses/by-nc-sa/3.0).


