MPT
===

Corpus of French National Assembly debates on the "Mariage pour tous" law of 2013 from the [mariagepourtousInXML](https://github.com/nlegrand/mariagepourtousInXML) GitHub project adapted for TXM 0.7.8 XTZ+CSV import module.

* mpt-src.zip: source files
* mpt.txm: binary corpus to load into TXM

