P1S8 4 avril 2014 transcription et enregistrement d'un cours de physique en Lycée
===

Ce corpus exemple illustre les 2 niveaux d'importation possibles de transcriptions d'enregistrements dans TXM :

* des transcriptions au format traitement de text (.odt, .doc, etc.), éditables avec LibreOffice Writer, MS Word
* des transcriptions au format .trs, éditables avec le logiciel Transcriber

Licences
---

Transcriptions et enregistrements : Copyright © 2013-2014 Andrée Tiberghien, Marie-Pierre Chopin, Laurent Lima, Laurent Talbot, Abdelkarim Zaid

Documents annexes : Copyright © 2013-2014 ENS de Lyon

Les transcriptions, les enregistrements et les documents annexes sont diffusés sous licence Creative Commons BY-NC-SA : [http://creativecommons.org/licenses/by-nc-sa/3.0/fr](http://creativecommons.org/licenses/by-nc-sa/3.0/fr)

Les différentes transcriptions (la même transcription dans divers formats) ont été produites par Serge Heiden à partir des sources RTF d'Andrée Tiberghien pour le tutoriel de transformation de transcriptions d'enregistrements au format ODT
vers le format TRS pour import dans TXM (voir ci-dessous) pour le [projet Textométrie](http://textometrie.ens-lyon.fr).
Les différents fichiers média ont été préparés par Justine Lascar à partir des médias originaux d'Andrée Tiberghien.

Références
---

La vidéo à l'origine de cette transcription et les études qui ont été menées
à partir d'elle sont décrites dans un numéro spécial de la revue « éducation & didactique » :

Tiberghien Andrée et al., « Partager un corpus vidéo dans la recherche en éducation :  
analyses et regards pluriels dans le cadre du projet ViSA », Education & didactique 3/ 2012 (vol.6), p. 9-17  
URL : [www.cairn.info/revue-education-et-didactique-2012-3-page-9.htm](http://www.cairn.info/revue-education-et-didactique-2012-3-page-9.htm).  
DOI : 10.4000/educationdidactique.1686

Contenu de l'archive
---

Cette [archive](https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/P1S8/src) contient :

A) différentes versions de la même transcription source « P1S8 4 avril 2014 »

* ~~P1S8 30 avril 2014.rtf : fichier original à partir duquel tous les autres ont été produits~~
* ~~P1S8 30 avril 2014.odt : fichier éditable avec LibreOffice Writer~~
* ~~P1S8 30 avril 2014.doc : fichier éditable avec MS Word~~
* ~~P1S8 30 avril 2014.pdf : fichier imprimable~~
* P1S8 30 avril 2014-trimmed.trs : fichier source au format Transcriber produit par la macro 'TextTranscription2TRS', à importer dans TXM avec le module 'XML Transcriber+CSV'
* trans-14.dtd : un fichier annexe nécessaire à l'importation du fichier .trs

L'enregistrement du cours à partir duquel a été établie la transcription :

* P1S8 30 avril 2014-trimmed.mp4 : vidéo anonymisée, seule la partie située entre 0h30m26s et 0h31m39s est visualisable
* ~~P1S8 30 avril 2014-trimmed.mp3~~ : audio anonymisé, seule la partie située entre 0h30m26s et 0h31m39s est audible

B) le fichier journal 'TextTranscription2TRSMacroP1S8Log.txt'
qui est un exemple détaillé des messages d'exécution de la macro
TextTranscription2TRS appliquée à un répertoire contenant le fichier
'P1S8 30 avril 2014.odt'.  
Remarque : la macro peut transformer les transcriptions aux formats : rtf, odt et doc.

C) le corpus binaire TXM résultant constitué de cette unique transcription lemmatisée par TreeTagger :

* [P1S8-2021-11-16.txm](https://gitlab.huma-num.fr/txm/txm-ressources/-/blob/master/corpora/P1S8/bin/P1S8-2021-11-16.txm) : à charger directement dans TXM

Usage
---

La transcription est conforme aux conventions de transcription de
la chaine d'importation de transcriptions au format traitement de texte
dans le logiciel TXM décrite dans le "[Tutoriel import de transcriptions entretiens TXT-ODT-RTF-DOC dans TXM.pdf](https://gitlab.huma-num.fr/txm/textometrie/-/raw/master/files/documentation/Tutoriel%20import%20de%20transcriptions%20entretiens%20TXT-ODT-RTF-DOC%20dans%20TXM.pdf?inline=false)".  
Elle peut donc servir d'exemple pour l'application de ce tutoriel.  
La vidéo et le son correspondant à cette transcription sont jouables
depuis TXM en installant l'extension "Media Player" de niveau BETA.

