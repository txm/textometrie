---
ititle: Corpus voeux TXM
layout: page
lang: fr
ref: corpus-voeux-fr
---

# VOEUX

Corpus de 54 discours de présidents français pour le Nouvel An (1959-2009)

<table>
{% assign files = site.static_files | sort_natural:'name' %}
{% for file in files %}
    {% if file.path contains 'corpora/voeux/' %}
        <tr>
	  <td>
	    <a href="{{ file.path | relative_url }}">{{ file.name }}</a>
	  </td>
	  <td>
	    {% include translated_date.html date=file.modified_time format="%A %e %B %Y %k:%M" lang="fr" %}
	  </td>
	</tr>
    {% endif %}
{% endfor %}
</table>

© 2010 Jean-Marc Leblanc

Le corpus VOEUX de Jean-Marc Leblanc est sous licence [Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported](http://creativecommons.org/licenses/by-nc-nd/3.0). Pour lire cette licence, visitez le site Web de Creative Commons ou envoyez une lettre à Creative Commons, 171 Second Street, Suite 300, San Francisco, Californie, 94105, États-Unis.

Cette version du corpus est compatible avec la plate-forme TXM et est fournie par le [projet de recherche Textométrie](http://textometrie.ens-lyon.fr).

# Téléchargements

* [Corpus binaire pour TXM 0.7.9 -> 0.8.2](https://gitlab.huma-num.fr/txm/txm-ressources/-/blob/master/corpora/VOEUX/bin/VOEUX.txm).
* [Sources] (https://gitlab.huma-num.fr/txm/txm-ressources/-/blob/master/corpora/VOEUX/src)
