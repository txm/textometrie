---
ititle: TXM voeux corpus
layout: page
lang: en
ref: corpus-voeux-fr
---

# VOEUX

Corpus of 54 New Year’s Day speeches of French presidents (1959-2009)

<table>
{% assign files = site.static_files | sort_natural:'name' %}
{% for file in files %}
    {% if file.path contains 'corpora/voeux/' %}
        <tr>
	  <td>
	    <a href="{{ file.path | relative_url }}">{{ file.name }}</a>
	  </td>
	  <td>
	    {% include translated_date.html date=file.modified_time format="%A %e %B %Y %k:%M" lang="en" %}
	  </td>
	</tr>
    {% endif %}
{% endfor %}
</table>

© 2010 Jean-Marc Leblanc

The VOEUX corpus by Jean-Marc Leblanc is licensed under a [Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License](http://creativecommons.org/licenses/by-nc-nd/3.0). To view a copy of this license, visit the Creative Commons website or send a letter to Creative Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA.

This version of the corpus is compatible with the TXM platform and is provided by the [Textométrie research project](http://textometrie.ens-lyon.fr).

# Downloads

* [Binairy corpus for TXM 0.7.9 -> 0.8.2](https://gitlab.huma-num.fr/txm/txm-ressources/-/blob/master/corpora/VOEUX/bin/VOEUX.txm).
* [Sources] (https://gitlab.huma-num.fr/txm/txm-ressources/-/blob/master/corpora/VOEUX/src)
