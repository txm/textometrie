Leviathan, or, The matter, forme, and power of a common wealth, ecclesiasticall and civil by Thomas Hobbes, 1588-1679 
======

Text Creation Partnership (TCP), 2003-03 EEBO-TCP Phase 1, DLPS: A43998 digital edition

Fitted for import into TXM by Textometrie.org project

* **LEVIATHAN-nofacs.txm**: TXM binary corpus, without synoptic edition including facsimile images
* **leviathan-src.zip**: source archive including the XML TEI P5 version of the text, prepared for import with the XTZ+CSV import module

