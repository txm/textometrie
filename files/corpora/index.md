---
ititle: Corpus TXM exemples
layout: page
lang: fr
ref: corpus-dir
---

# Corpus exemples

Accédez à un répertoire de corpus, téléchargez le fichier binaire (.txm), puis appelez la commande 'Fichier > Charger' depuis TXM pour le charger.

Pour certains corpus, les sources sont également fournies (.zip). Dans ce cas, vous pouvez également importer le corpus depuis les sources et régler l'import selon vos préférences.

## Textes écrits

### Français

- [discours](https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/discours): corpus de divers discours des présidents français, publié par Damon Mayaffre.
- [fleurs-du-mal](https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/fleurs-du-mal): les Fleurs du mal de Charles Baudelaire, édition de Jean-Marie Viprey.
- [mpt](https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/mpt): corpus des débats à l'Assemblée nationale sur la loi "Mariage pour tous" de 2013, du [projet mariagepourtousInXML](https://github.com/nlegrand/mariagepourtousInXML).
- [quete-du-graal-tei](https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/quete-du-graal-tei): Queste del Saint Graal, édition de Christiane-Marchello Nizia et Alexei Lavrentiev, d'après 'Lyon, Palais des Arts 77 (ms K) (fol. 160a-224d)' et 'Paris, BNF n. acq. fr. 1119 (ms. Z)' ca. 1225-1230, manuscrits en ancien français.
- [tdm80j](https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/TDM80J): Le tour du monde en quatre-vingts jours (Around the World in Eighty Days), Jules Verne, 1873, édition de J. Hetzel et Cie. Édition synoptique avec images en fac-similé de Wikisource.
- [txm-odt-manual](https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/txm-odt-manual): Manuel utilisateur de TXM sous forme de corpus TXM.
- [voeux](https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/VOEUX): corpus de 63 voeux présidentiels français pour le Nouvel An (1959-2021), publié par Jean-Marc Leblanc.
- voeux-fr: Voir voeux.

### Anglais

- [brown](https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/brown): corpus de 500 textes écrits en anglais américain de 1961, publié par WN Francis et H. Kucera (cette version est basée sur la version XMLtTEI du [projet NLTK](https://github.com/nltk/nltk_data)).
- [leviathan]({{"/files/corpora/leviathan" | absolute_url}}): Leviathan de Thomas Hobbes, 1588-1679. Exemple de texte XML-TEI P5 du projet EEBO-TCP Phase 1.

### Allemand

- [voeux-rfa]({{"/files/corpora/voeux-rfa" | absolute_url}}): corpus de discours de Noël et de Nouvel An prononcés par les présidents et les chanceliers de la République fédérale d'Allemagne depuis 1987, contribution de Sascha Diwersy, Université de Cologne.

## Transcriptions d’enregistrements (synchronisées)

- [p1s8-course-transcription](https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/P1S8): transcription des paroles et enregistrement audio/vidéo d'un cours de physique de lycée (en français).<br/> Voir Tiberghien Andrée et al., *Partager un corpus vidéo dans la recherche en éducation: analyses et pluriels dans le cadre du projet ViSA*, éducation & didactique 3/2012 (vol.6) [[en ligne sur openedition.org](https://journals.openedition.org/educationdidactique/1686)].<br/> Pour visualiser la vidéo à partir de concordances (nécessite l’extension Media Player).

## Corpus parallèles (multilingue)

- [uno-tmx-sample](https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/uno-tmx-sample): échantillon de résolutions de l'Assemblée générale des Nations Unies: Un [corpus parallèle en six langues](http://www.uncorpora.org) (anglais, arabe, chinois, espagnol, français et russe), Alexandre Rafalovitch, Robert Dale. 2009. *Résolutions de l'Assemblée générale des Nations Unies: Un corpus parallèle en six langues.* Dans Proceedings of the MT Summit XII, pages 292 à 299, Ottawa, Canada, août.<br/> À importer avec le module d'import XML-TMX.

## Corpus annotés

- [CORPUS110CYL067](https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/CORPUS110CYL067): un texte annoté syntaxiquement du corpus [MASC](http://www.anc.org/data/masc). Pour s'exercer aux requêtes TIGER Search (voir [Validation de l'importation TIGER-XML](https://groupes.renater.fr/wiki/txm-info/public/import_tiger#recette_alpha)) (nécessite l'extension TIGER Search).

Certains corpus sont également téléchargeables depuis le [portail de démonstration TXM](http://portal.textometrie.org/demo/?locale=fr).

