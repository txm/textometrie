---
ititle: Bibliothèque XSLT
layout: page
lang: fr
ref: xsl-library
---

# Bibliothèque de transformations XSLT pour l'import dans TXM
{: .no_toc}

Collection de feuilles de style XSLT (1.0 ou 2.0) pouvant être utilisées pour préparer divers types de documents XML en vue de leur importation dans TXM. Placez-les dans le sous-dossier `xsl/<étape>` approprié lorsque vous utilisez le module d'importation XML-TEI Zero + CSV (XTZ) ou sélectionnez les avec l'option "Front XSLT" de l'interface de paramètres du module d'import XML/w+CSV (XML/w).

Les filtres sont généralement nommés selon le modèle suivant :
> txm-filter-[format d'entrée]-[module d'import]\(-[option])?

[pour télécharger un fichier ci-dessous, faites un clic droit sur son lien puis lancez "Enregistrer la cible du lien sous..."]

## Sommaire
{: .no_toc}

1. TOC
{:toc}

## Feuilles de style à utiliser avec le module d'import XTZ

### Étape 1-split-merge

Placer ces xsl dans le répertoire `xsl/1-split-merge` de votre répertoire de sources (sous-répertoire `1-split-merge` du répertoire `xsl`).

[en raison d'un bug, cette étape de traitement ne fonctionne pas correctement dans TXM 0.7.8 et 0.7.9. Dans ce cas, ces feuilles de style doivent être appliquées avant l'import à l'aide de la macro ExecXSL ou d'un autre processeur XSLT 2.0]

- **[txm-rename-files-no-dots.xsl](txm-rename-files-no-dots.xsl)** : cette feuille de style est conçue pour que le module d'import XTZ remplace les points par des soulignés dans les noms de fichiers source (un bug dans TXM 0.7.8 empêchait les fichiers contenant des points dans leur nom d’être importés, ce bug a été résolu depuis TXM 0.7.9).
- **[txm-split-teicorpus](txm-split-teicorpus.xsl)** : cette feuille de style peut être utilisée pour scinder un fichier contenant un teiCorpus en autant de fichiers textes séparés que d'éléments TEI enfants.

### Étape 2-front

Placer ces xsl dans le répertoire `xsl/2-front` de votre répertoire de sources.

- **[txm-front-teiHeader2textAtt.xsl](txm-front-teiHeader2textAtt.xsl)** : cette feuille de style copie des métadonnées se trouvant dans le `teiHeader` dans des attributs de l'élément `text` de chaque fichier source.
- **[txm-front-teitxm2xmlw.xsl](txm-front-teitxm2xmlw.xsl)** : cette feuille de style peut être utilisée pour importer des fichiers au format XML-TEI TXM avec les modules XTZ ou XML/w. Ceci peut servir notamment à importer :
  - un corpus déjà tokenisé et annoté en dehors de TXM ;
  - un corpus déjà importé dans TXM, par n'importe quel module d'import et dont la représentation pivot est éventuellement retravaillée, tout en préservant l'encodage de ses mots. \\
Les fichiers au format pivot XML-TEI TXM d'un corpus se trouvent dans le répertoire `txm/NOM-DU-CORPUS` de l'archive de son export binaire (voir la commande 'Exporter > Corpus en format binaire...').

### Étape 3-posttok

Placer ces xsl dans le répertoire `xsl/3-posttok` de votre répertoire de sources.

- **[txm-posttok-addRef.xsl](txm-posttok-addRef.xsl)** : cette feuille de style peut être utilisée pour personnaliser les références dans les concordances de TXM. Pour cela, elle ajoute aux éléments `w` un attribut `@ref` qui est utilisé par TXM pour afficher la référence par défaut dans les concordances.
- **[txm-posttok-unbreakWords.xsl](txm-posttok-unbreakWords.xsl)** : cette feuille de style peut être personnalisée pour recoller des mots (dont les éléments sont séparés par un saut de ligne ou un saut de page, par exemple) après tokenisation
- **[txm-posttok-structure2wordAtt.xsl](txm-posttok-structure2wordAtt.xsl)** : cette feuille de style projette dans des attributs de `w` le niveau d'imbrication de certains éléments ancêtres. \\
Par exemple, selon qu'un `w` est imbriqué dans :
- aucun élément `q`
- 1 seul élément `q` (`//q//`)
- 2 éléments `q` (`//q//q//`)
- ou 3 éléments `q` (`//q//q//q//`) \\
les `w` reçoivent un nouvel attribut `q` à la valeur (resp.) :
- `0`
- `1`
- `2`
- ou `3` \\
Listez dans le paramètre `elementsToProject` les noms des éléments dont il faut calculer l'imbrication, séparés par le caractère `|`. Dans l'exemple ci-dessus le paramètre `elementsToProject` vaut `q`.

### Étape 4-edition

Placer ces xsl dans le répertoire `xsl/4-edition` de votre répertoire de sources.

- **[1-default-html.xsl](1-default-html.xsl)** : Cette feuille de style permet de créer des éditions alternatives à l'édition par défaut du module XTZ. Elle fonctionne en tandem avec la feuille *2-default-pager.xsl*.  Elle transforme chaque élément TEI en un `span` HTML ayant un attribut `@class`.
- **[2-default-pager.xsl](2-default-pager.xsl)** : Cette feuille de style fonctionne en tandem avec la feuille *1-default-html.xsl*. Elle crée autant de fichiers HTML que de pages, pour chaque texte.

## Feuilles de style de base pour filtrer les sources XML

- **[ filter-keep-only-select.xsl](filter-keep-only-select.xsl)** : cette feuille de style peut être personnalisée pour ignorer tout le texte et les balises à l'exception du contenu et des ancêtres d'un élément particulier (`select` par défaut). Positionnez la valeur du paramètre `tagToKeep` au nom de l'élément à sélectionner.
- **[filter-out-p.xsl](filter-out-p.xsl)** : cette feuille de style peut être personnalisée pour ignorer un élément XML particulier (`p` par défaut) et son contenu.
- **[filter-out-sp.xsl](filter-out-sp.xsl)** : cette feuille de style peut être personnalisée pour ignorer un élément XML, et son contenu, ayant un attribut à une valeur particulière (élément `sp` ayant l'attribut `@who` à la valeur `enqueteur` par défaut).

## Feuilles de style de base pour l’adaptation de sources XML TEI P5

- **[txm-filter-teip5-teibfm.xsl](txm-filter-teip5-teibfm.xsl)** : cette feuille de style peut être personnalisée pour importer toute source encodée en TEI P5 avec le module d'import XML TEI BFM. Notez que ce module est expérimental et peut échouer si les documents ne suivent pas les principes d'encodage de la BFM.
- **[txm-filter-teip5-xmlw-preserve.xsl](txm-filter-teip5-xmlw-preserve.xsl)** : cette feuille de style peut être personnalisée pour importer toute source encodée en TEI P5 avec le module d'import XML/w. Par défaut, elle ignore les éléments `teiHeader` et `facsimile` et leur contenu et préserve tous les autres éléments.
- **[txm-filter-teip5-xmlw-simplify.xsl](txm-filter-teip5-xmlw-simplify.xsl)** : cette feuille de style peut être personnalisée pour importer toute source encodée en TEI P5 avec le module d'import XML/w. Par défaut, elle ignore les éléments `teiHeader`, `facsimile` et `note` ainsi que leur contenu, et ignore toutes les balises du corps du texte, à l'exception de `b`, `body`, `div`, `front`, `lb`, `p`, `pb`, `s`, `TEI`, `text` et `w`.

## Feuilles de style de personnalisation des éditions

- **[txm-edition-page-split.xsl](txm-edition-page-split.xsl)** : feuille de style créant autant de fichiers HTML que de pages, pour chaque texte.

## Feuilles de style supplémentaires pour des corpus particuliers

### Perseus

- **[p4top5_perseus.xsl](p4top5_perseus.xsl)** : cette feuille de style est nécessaire pour convertir les fichiers TEI P4 du projet [Perseus](http://www.perseus.tufts.edu/hopper) en TEI P5 avant tout processus d'importation.
avec le module XML/w.
- **[txm-filter-teiperseus-xmlw.xsl](txm-filter-teiperseus-xmlw.xsl)** : feuille de style pour préparer l'import de textes de Perseus avec le module XML/w (après conversion en TEI P5).
- **[txm-filter-perseustreebank-xmlw.xsl](txm-filter-perseustreebank-xmlw.xsl)** : feuille de style pour préparer l'import de textes du [corpus Treebank](https://github.com/PerseusDL/treebank_data) de Perseus 

### Textgrid

- **[txm-filter-teicorpustextgrid-xmlw.xsl](txm-filter-teicorpustextgrid-xmlw.xsl)** : feuille de style pour préparer l'import de textes Textgrid de DARIAH-DE au format TEI avec le module XML/w.
- **[txm-edition-xmltxm-textgrid.xsl](txm-edition-xmltxm-textgrid.xsl)** : feuille de style pour personnaliser les éditions TXM des [textes Textgrid](https://textgrid.de/en/digitale-bibliothek) de [DARIAH-DE](https://de.dariah.eu/en).
- **[txm-filter-teitextgrid-xmlw-posttok.xsl](txm-filter-teitextgrid-xmlw-posttok.xsl)** : feuille de style ajustant les propriétés de mots de la version tokenisées des textes Textgrid de DARIAH-DE.

### Corpus Akkadien

- **[txm-filter-corpusakkadien-xmlw_syllabes-cuneiform.xsl](txm-filter-corpusakkadien-xmlw_syllabes-cuneiform.xsl)** : feuille de style pour préparer l'import de textes d'un corpus de tablettes Akkadiennes avec le module XML/w. Voir le [wiki du projet](https://groupes.renater.fr/wiki/txm-users/public/umr_proclac_corpus_akkadien#etape_3_facultative_mise_a_jour_de_l_edition_translitteree_affichage_des_sauts_de_lignes_et_de_traits_d_union_entre_les_syllabes) pour plus de détails.
- **[txm-edition-xtz-cuneiform.xsl](txm-edition-xtz-cuneiform.xsl)** : feuille de style pour produire les éditions TXM des transcriptions de tablettes Akkadiennes en cunéiforme. Voir le [wiki du projet](https://groupes.renater.fr/wiki/txm-users/public/umr_proclac_corpus_akkadien#etape_3_facultative_mise_a_jour_de_l_edition_translitteree_affichage_des_sauts_de_lignes_et_de_traits_d_union_entre_les_syllabes) pour plus de détails[^1]. À utiliser avec le module XTZ.
- **[txm-edition-xtz-corpusakkadien-translit.xsl](txm-edition-xtz-corpusakkadien-translit.xsl)** : feuille de style pour personnaliser les éditions TXM des transcriptions de tablettes Akkadiennes en cunéiforme translittéré. Voir le [wiki du projet](https://groupes.renater.fr/wiki/txm-users/public/umr_proclac_corpus_akkadien#etape_3_facultative_mise_a_jour_de_l_edition_translitteree_affichage_des_sauts_de_lignes_et_de_traits_d_union_entre_les_syllabes) pour plus de détails[^1]. À utiliser avec le module XTZ.

### Queste del Saint Graal

- **[txm-filter-qgraal_cm-xmlw.xsl](txm-filter-qgraal_cm-xmlw.xsl)** : feuille de style pour préparer l'import de fichiers source de la [Queste del Saint Graal](http://txm.bfm-corpus.org/?command=documentation&path=/GRAAL) au format diffracté avec le module XML/w.

### RNC

- **[txm-filter-rnc-xmlw.xsl](txm-filter-rnc-xmlw.xsl)** : feuille de style pour préparer l'import de textes du [Corpus National Russe](http://www.ruscorpora.ru/old/en) avec le module XML/w.

### BROWN

- **[txm-filter-teibrown-xmlw.xsl](txm-filter-teibrown-xmlw.xsl)** : feuille de style pour préparer l'import de textes du [corpus BROWN](https://txm.gitpages.huma-num.fr/textometrie/files/corpora/brown/) au format TEI avec le module XML/w.

### BVH

- **[txm-filter-teibvh-xmlw.xsl](txm-filter-teibvh-xmlw.xsl)** : feuille de style pour préparer l'import de textes du [projet BVH](http://www.bvh.univ-tours.fr) au format TEI avec le module XML/w.
- **[txm-filter-teibvh-xmlw-posttok.xsl](txm-filter-teibvh-xmlw-posttok.xsl)** : feuille de style corrigeant les erreurs de tokenisation et ajustant les propriétés de mots de la version tokenisée des textes du corpus BVH au format TEI, pour l'import avec le module XML/w.

### Frantext

- **[txm-filter-teifrantext-teibfm.xsl](txm-filter-teifrantext-teibfm.xsl)** : feuille de style pour préparer l'import de [textes de Frantext au format TEI](https://www.cnrtl.fr/corpus/frantext) avec le module XML-TEI BFM. Notez que ce module est expérimental et peut échouer si les documents ne suivent pas les principes d'encodage de la BFM.
- **[txm-filter-teifrantext-xmlw.xsl](txm-filter-teifrantext-xmlw.xsl)** : feuille de style pour préparer l'import de textes de Frantext au format TEI avec le module XML/w.

### XCES-IDS

- **[txm-front-idsHeader2textAtt.xsl](txm-front-idsHeader2textAtt.xsl)** : feuille de style projetant des métadonnées contenues dans l'élément `idsHeader` sur des attributs de l'élément `text` (schéma IDS-XCES du [Leibniz-Institut für Deutsche Sprache](http://www1.ids-mannheim.de/index.php) de Mannheim).
- **[txm-split-xces-ids-corpus2text.xsl](txm-split-xces-ids-corpus2text.xsl)** : feuille de style qui transforme un fichier du corpus XCES-IDS en autant de fichiers que de textes pour l'import avec le module XTZ.

Pour toute question relative à la bibliothèque XSLT de TXM, merci d'envoyez un mail à <a href="mailto:{{ "textometrie@groupes.renater.fr" | encode_email }}" title="textometrie AT groupes.renater.fr">textometrie AT groupes.renater.fr</a>.

[^1]: Voir aussi un exemple d'édition du [corpus OBLCUNEIF](http://portal.textometrie.org/demo/?command=edition&path=/OBLCUNEIF&textid=TXM_cuneif_Ha_S_AbB_2_4&editions=translit,cuneiform,facs&pageid=3) en ligne.

## Liste de toutes les feuilles de style CSS livrées avec TXM 0.8.2

<table>
  <tr><th scope="col">Nom du fichier</th><th scope="col">Date de dernière modification</th></tr>
{% assign files = site.static_files | sort_natural:'name' %}
{% for file in files %}

  {% assign pathParts = file.path | split: "/" %}
  {% assign length = pathParts.size | minus: 2 %}
  {% assign path = "" %}
  {% for c in (0..length) %}
    {% capture path %}{{ path }}/{{pathParts[c]}}{% endcapture %}
  {% endfor %}

    {% if path == '//files/library/xsl' %}
        <tr>
	        <td>
	          <a href="{{ file.path | relative_url }}">{{ file.name }}</a>
	        </td>
	        <td>
	          {% include translated_date.html date=file.modified_time format="%A %e %B %Y %k:%M" lang="fr" %}
	        </td>
	      </tr>
    {% endif %}
{% endfor %}
</table>

