---
ititle: TXM CSS library
layout: page
lang: en
ref: css-dir
---

# TXM CSS Stylesheets Library

[to download a file below, right click on its link and call "Save link target as..."]

<table>
{% assign files = site.static_files | sort_natural:'name' %}
{% for file in files %}
    {% if file.path contains 'library/css' %}
        <tr>
	  <td>
	    <a href="{{ file.path | relative_url }}">{{ file.name }}</a>
	  </td>
	  <td>
	    {% include translated_date.html date=file.modified_time format="%A %e %B %Y %k:%M" lang="en" %}
	  </td>
	</tr>
    {% endif %}
{% endfor %}
</table>

