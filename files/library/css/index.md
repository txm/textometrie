---
ititle: Bibliothèque CSS
layout: page
lang: fr
ref: css-dir
---

# Bibliothèque de feuilles de style CSS pour TXM

[pour télécharger un fichier ci-dessous, cliquer à droite sur son lien et lancer "Enregistrer la cible du lien sous..."]

<table>
{% assign files = site.static_files | sort_natural:'name' %}
{% for file in files %}
    {% if file.path contains 'library/css' %}
        <tr>
	  <td>
	    <a href="{{ file.path | relative_url }}">{{ file.name }}</a>
	  </td>
	  <td>
	    {% include translated_date.html date=file.modified_time format="%A %e %B %Y %k:%M" lang="fr" %}
	  </td>
	</tr>
    {% endif %}
{% endfor %}
</table>

