---
ititle: Matériel de cours TXM
layout: page
lang: fr
ref: support-dir
---

# Matériel de cours
{: style="text-align: center; padding-bottom: 30px" .no_toc}


<table>
{% assign files = site.static_files | sort_natural:'name' %}
{% for file in files %}
    {% if file.path contains 'course materials' %}
        <tr>
	  <td>
	    <a href="{{ file.path | relative_url }}">{{ file.name }}</a>
	  </td>
	  <td>
	    {% include translated_date.html date=file.modified_time format="%A %e %B %Y %k:%M" lang="fr" %}
	  </td>
	</tr>
    {% endif %}
{% endfor %}
</table>

