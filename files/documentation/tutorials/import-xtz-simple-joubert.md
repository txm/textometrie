---
ititle: Tutoriel d'importation de corpus TEI avec le module XML-TEI Zero + CSV (corpus Joubert, encodage simple)
layout: page
lang: fr
ref: tutorials-dir
---

<style>

.txm-keyboard-string {
    font-family:Consolas,'Andale Mono','Lucida Console',monospace;
    background-color: #f9f2f4;
    padding: 2px 4px;
}

</style>

# Tutoriel d'importation de corpus TEI avec le module XML-TEI Zero + CSV<br/>(corpus Joubert, encodage simple)
{: style="text-align: center;"}

<center>
Alexei Lavrentiev ᵃ<br/>
Serge Heiden ᵃ ᵉ<br/><br/>

Septembre 2024<br/>
</center>

[[CC BY](https://creativecommons.org/licenses/by/4.0/deed.fr)]
{: style="text-align: center;"}

Ce tutoriel va vous aider à créer un corpus TXM à partir d'un document [XML](https://fr.wikipedia.org/wiki/Extensible_Markup_Language) encodé en [TEI](https://tei-c.org) exploitant :

* les sauts de pages et les sauts de lignes (balises `<pb/>` resp. `<lb/>`)
  * l'attribut `@facs` de la balise `<pb/>` est notamment utilisé pour construire une édition synoptique affichant les images du fac-similé du document source hébergées dans Gallica
* le contenu des notes (balises `<note>`).

**Résumé des opérations**

Les opérations vont être les suivantes :

1. **Télécharger** le document XML-TEI source depuis l'entrepôt de ressources de TXM
1. **Importer** directement dans TXM avec le module XML-TEI Zero + CSV
1. **Observer** les caractéristiques du corpus produit
1. **Affiner** les paramètres d'import pour :
   1. créer une édition synoptique affichant le fac-similé du document source Gallica
   1. ne pas indexer le contenu des notes (tout en les affichant dans l'édition)
   1. personnaliser les références des concordances

> Remarque : tous les outils utilisés dans ce tutoriel sont capables de traiter des dossiers contenant plusieurs fichiers à traiter à la fois. Donc bien que ce tutoriel présente la construction d'un corpus composé d'un seul texte issu d'un fichier XML-TEI, il peut être généralisé à la construction d'un corpus contenant plusieurs textes encodés en XML-TEI.


## Télécharger le fichier .xml d'un extrait des «&nbsp;Erreurs populaires&nbsp;» depuis l'entrepôt de ressources de TXM

![Erreurs populaires](https://upload.wikimedia.org/wikipedia/commons/9/91/Erreurs_populaires%2C_by_Joubert._Wellcome_L0015667.jpg){: style="float: left; margin-right: 1em;" width="250"}

*Erreurs populaires* est un ouvrage destiné à désavouer les clichés répandus dans la société de la fin du XVIe siècle au sujet de la médecine et de la santé.

![Laurent Joubert](https://upload.wikimedia.org/wikipedia/commons/5/5c/Laurent_Joubert_%281529-1583%29.jpg){: style="float: left; margin-right: 1em;" width="250"}

Son auteur, [Laurent Joubert](https://fr.wikipedia.org/wiki/Laurent_Joubert) (1529-1583), fut professeur de médecine à l'université de Montpellier et premier médecin du roi Henri III de Navarre (futur Henri IV de France). Ses *Erreurs populaires* ont suscité une vive polémique en raison des sujets délicats qui y étaient abordés. Ce texte fait l'objet d'une [édition numérique](https://txm-ihrim.huma-num.fr/txm/?command=Documentation&path=/JOUBERT) en cours d'établissement au [laboratoire IHRIM](https://ihrim.ens-lyon.fr).

Ce tutoriel utilise un extrait de trois pages du deuxième chapitre du premier livre de l'ouvrage. Sa taille limitée permet d'expérimenter l'import en quelques secondes sans encombrer l'espace disque.

Cet extrait est un document XML-TEI valide composé d'un entête TEI minimal et du corps du texte doté de balises de sauts de pages et de lignes, de divisions en chapitres et de plusieurs types de notes.

**Télécharger l'extrait**

* dans un navigateur, aller à l'[entrepôt des ressources TXM](https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/joubert1579_1-02-simple) hébergeant ce document XML-TEI.

* enregistrer le fichier `joubert1579_1-02-simple.xml` sur votre disque dur


## Préparer le dossier source pour l'import avec le module XML-TEI Zero + CSV de TXM

* créer un dossier des sources du corpus (dont le nom sera utilisé pour créer le corpus dans TXM), par exemple `JOUBERT-TEST`
* copier le fichier source .xml dans le dossier source


## Importer une première version du corpus dans TXM

Nous allons créer un corpus composé du seul texte `joubert1579_1-02-simple.xml`.

* dans TXM, lancer la commande 'Fichier > Importer > Corpus > XML-TEI Zero + CSV'

![Import XML-TEI Zero + CSV](../images/tuto-import-xtz-1-ouverture.png)
Lancement du [module d'import XML-TEI Zero + CSV](https://txm.gitpages.huma-num.fr/txm-manual/importer-un-corpus-dans-txm.html#module-xml-tei-zerocsv-dit-aussi-xtzcsv-ou-xtz-import-de-xml-tei-g%C3%A9n%C3%A9rique-.xml).

  * dans le formulaire de paramètres :
    * cliquer sur le lien 'Sélectionner le dossier des fichiers sources et nommer le corpus'
      * pointer vers le dossier source : `JOUBERT-TEST`
      * nommer le corpus (un nom basé sur celui du dossier source est proposé automatiquement, c'est celui que nous utiliserons dans la suite de ce tutoriel)
      * cliquer sur le bouton <button>Finish</button>
    * cliquer sur le bouton <button>Lancer l'import du corpus</button>
    * ne pas fermer le formulaire de paramètres car il sera ré-utilisé plus tard

🠲 le module d'import crée le nouveau corpus JOUBERT-TEST.


## Observer comment l'édition du texte est construite

* dans TXM
  * ouvrir l'édition du premier texte du corpus (il n'y en a qu'un) en double-cliquant sur l'icone du corpus JOUBERT-TEST dans la vue Corpus
    * TXM affiche la page de garde mentionnant le nom du fichier
    * aller à la première page de l'ouvrage en cliquant sur le bouton [>] (Page suivante) situé en bas de la fenêtre
  * observer l'interprétation par défaut de l'encodage XML-TEI pour construire l'édition :
    * la délimitation et la numérotation des pages correspond aux balises `<pb/>` du document XML-TEI
    * les sauts de lignes correspondent aux balises `<lb/>`
    * le titre du chapitre (encodé par la balise `<head>`) est centré et affiché en gras
    * les appels de notes correspondent aux balises `<note>` et leur contenu s'affiche en pied de page

## Observer comment les mots ont été indexés

* dans TXM
  * calculer les `Propriétés` du corpus à partir de son menu contextuel (clic droit sur l'icone dans la vue Corpus)
    * le nombre de mots est 717
    * noter que `note` figure bien dans la liste des structures
  * vérifier la présence de mots particuliers en lançant une `Concordance` sur le corpus
     * saisir le mot <span class="txm-keyboard-string">fichier</span> dans le champ de requête et lancer la recherche
      * constater qu'aucune occurrence n'est trouvée ⇨ Alors que ce mot figure dans l'en-tête `<teiHeader>` il n'est pas trouvé. Cela montre que le texte libre se trouvant dans le `<teiHeader>` n'a pas été indexé.
    * saisir le mot <span class="txm-keyboard-string">Découpage</span> dans le champ de recherche et lancer à nouveau la recherche
      * on trouve une occurrence située dans la troisième note de la page 3 ⇨ Le contenu des notes a donc bien été indexé - comme si c'était des notes d'auteur (alors que ce sont des notes de l'éditeur)

## Observer comment les concordances sont affichées

* dans TXM
  * constater que seul le nom du fichier s'affiche dans la colonne des références de la concordance (colonne de gauche)


## Personnaliser le corpus en affinant les paramètres d'import dans le formulaire

* accéder à la fenêtre du formulaire d'import ou bien rouvrir le formulaire d'import et sélectionner à nouveau le dossier des sources si nécessaire

![Formulaire de paramètres du module d'import XML-TEI Zero + CSV](../images/tuto-import-xtz-2-formulaire.png)
Formulaire de paramètres du module d'import XML-TEI Zero + CSV

  * dans la section 'Éditions'
    * cocher l'option "Construire l'édition 'facs' "
    * dans le champ 'Édition par défaut', saisir <span class="txm-keyboard-string">default,facs</span>
> Remarque : la mention de la balise `note`dans le champ 'Balises de Notes' provoque l'affichage des notes en pied de page dans l'édition TXM
  * dans la section 'Plans textuels', saisir <span class="txm-keyboard-string">note</span> dans le champ 'Hors texte à éditer' (leur contenu ne sera plus indexé mais restera affiché dans les éditions)
  * dans la section 'Commandes', personnaliser les références des concordances
    * saisir <span class="txm-keyboard-string">%s, %s %s, %s %s</span> dans le champ 'Patron' du paramètre 'Références par défaut'
    * saisir <span class="txm-keyboard-string">text_id,div_type,div_n,div_type1,div_n1</span> dans le champ 'Properties'
> Remarque : une infobulle explicative s'affiche lorsqu'on place la souris sur ces champs. La référence que l'on veut construire consiste en nom du fichier (text_id) suivi du type et du numéro de la division de niveau 1 (div_type,div_n - livre 1 dans notre document) et se termine par le type et le numéro de la division de niveau 2 (div_type1,div_n1 - chapitre 2 dans notre document)
  * cliquer sur le bouton <button>Lancer l'import du corpus</button>, puis sur <button>OK</button> pour confirmer le remplacement du corpus.


## Vérifier l'édition synoptique

* dans TXM, lancer la commande 'Édition' sur le corpus mis à jour
* aller à la première page de l'édition. Elle devrait apparaître comme ceci :

![Édition synoptique](../images/tuto-import-xtz-3-synoptique.png)
Édition synoptique de la première page de l'édition. L'image de fac-similé est téléchargée depuis le site de [Gallica](https://gallica.bnf.fr/ark:/12148/bpt6k1090863x) (il faut être connecté à Internet pour la visualiser).

## Vérifier la non indexation des notes

* lancer la commande `Propriétés`
  * le nombre de mots est passé à 659
  * la structure note n'apparaît plus dans la liste
* lancer la commande `Concordances` et rechercher le mot <span class="txm-keyboard-string">Découpage</span>
  * la requête ne retourne aucun résultat

## Vérifier le contenu des références de concordance

* saisir le mot <span class="txm-keyboard-string">est</span> dans le champ de requête
  * la colonne des références affiche `joubert1579_1-02-simple, livre 1, chapitre 2`

## Pour aller plus loin dans la personnalisation du corpus

Grâce à l'utilisation de feuilles de style CSS et XSLT il est possible de

* **mettre en évidence** (par une couleur ou une police de caractères) des zones de textes balisées
* indiquer les **coupures de mots** en fin de ligne
* moduler l'affichage ou l'indexation des **notes** en fonction de leur type
* indiquer les **numéros** de pages ou de lignes dans les références de concordances
* ajuster la segmentation automatique des **mots**

Cela fera l'objet d'un autre tutoriel.

___
Notes

a : auteur.<br/>
e : éditeur.