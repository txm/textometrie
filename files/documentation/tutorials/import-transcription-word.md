---
ititle: Tutoriel d'importation de transcription d'enregistrement au format Word
layout: page
lang: fr
ref: tutorials-dir
---

<style>

.txm-keyboard-string {
    font-family:Consolas,'Andale Mono','Lucida Console',monospace;
    background-color: #f9f2f4;
    padding: 2px 4px;
}

</style>

# Tutoriel d'importation de transcriptions d'enregistrements au format Word
{: .no_toc style="text-align: center;"}

<center>
Serge Heiden ᵃ,ᵉ<br/><br/>
Novembre 2024<br/>
</center>

[[CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr)]
{: style="text-align: center;"}

Ce tutoriel va vous aider à créer un corpus TXM à partir d'une transcription d'enregistrement au format .docx en utilisant des conventions de transcription simples permettant d'encoder :
* des sections thématiques
* des tours de paroles
* des marques de synchronisation avec la vidéo source (bullets)
* des commentaires

Cette transcription sera associée à un fichier vidéo au format `.mp4` qui pourra être joué dans TXM au moment précis où des mots de la transcription sont prononcés.

**Résumé des opérations**

Les opérations vont être les suivantes :

1. **Télécharger** la transcription et l'enregistrement
2. **Convertir** la transcription du format .docx au format .trs pour être importable dans TXM
3. **Préparer** le dossier de fichiers source
4. **Importer** le corpus dans TXM
4. **Exploiter** rapidement le corpus à travers un lexique, une concordance et la lecture de l'enregistrement dans TXM

Remarques :
* ce tutoriel s'appuie sur une seule transcription mais on peut l'appliquer à un corpus de plusieurs transcriptions ;
* l'enregistrement source est une vidéo mais le tutoriel fonctionne également avec un enregistrement audio (par exemple au format `.mp3`).

## Sommaire
{: .no_toc}

1. TOC
{:toc}

## Télécharger la transcription «&nbsp;P1S8-30-avril-2014&nbsp;» d'un extrait de cours de physique en Lycée depuis l'entrepôt de ressources de TXM

![Copie d'écran de la vidéo du cours](../images/p1s8.png){: style="float: left; margin-right: 1em;" width="350"}

La séance de cours de physique *P1S8*, enregistrée le 8 octobre 2010, fait partie d'un ensemble de cours de physique enregistrés en vidéo tout au long d'une année scolaire en vue de réaliser des recherches en éducation scolaire sur les pratiques d’enseignement-apprentissage en classe.

Les objectifs et la méthode employée par ce projet de recherche sont décrits dans la publication suivante :

    Andrée Tiberghien, Marie-Pierre Chopin, Laurent Lima, Laurent Talbot, Abdelkarim Zaid,
    «&nbsp;Partager un corpus vidéo dans la recherche en éducation : analyses et regards pluriels dans le cadre du projet ViSA&nbsp;»,
    Education & didactique 3/ 2012 (vol.6), p. 9-17.
    DOI : 10.4000/educationdidactique.1686
    URL : https://journals.openedition.org/educationdidactique/1686

Ce tutoriel utilise la version du 30 avril 2014 de la transcription d'un extrait du cours P1S8 appelée «&nbsp;P1S8-30-avril-2014.docx&nbsp;». Sa taille limitée permet d'expérimenter les conversions et l'import en quelques secondes sans encombrer l'espace disque.

**Télécharger l'extrait**

* dans un navigateur, ouvrir l'adresse : [https://gitlab.huma-num.fr/txm/txm-ressources/-/raw/master/corpora/P1S8/src/P1S8-30-avril-2014/P1S8-30-avril-2014.docx](https://gitlab.huma-num.fr/txm/txm-ressources/-/raw/master/corpora/P1S8/src/P1S8-30-avril-2014/P1S8-30-avril-2014.docx)
* enregistrer dans le fichier`P1S8-30-avril-2014.docx`

## Découvrir les conventions de transcription en parcourant le document P1S8-30-avril-2014

Les conventions utilisées dans la transcription sont assez simples et sont compatibles avec le modèle de [transcriptions du logiciel Transcriber](https://shs.hal.science/halshs-01341955/file/Guide%20de%20Transcription%20d_entretiens%20Transcriber-TXM%200.3_FR.pdf) qui est utilisé comme modèle par TXM.

* ouvrir le document `P1S8-30-avril-2014.docx` dans Microsoft Word ou LibreOffice Writer.

### Structure générale d'une transcription

En parcourant le document, vous pouvez constater que la transcription est composée successivement :

1. d'un **préambule** contenant n'importe quel texte introductif à la transcription
2. <span markdown="1">suivi d'une première ligne de synchronisation marquant le **début de l'enregistrement**[^1]&nbsp;: `(00:00:00.0)`</span>
3. suivi du **corps de la transcription** constitué de :
   - limites de sections
   - de tours de parole
4. <span markdown="1">et terminée par une dernière ligne de synchronisation marquant la **fin de l'enregistrement**[^2]&nbsp;: `(00:01:13.02)`</span>

#### Usage de styles Word

On remarque que cette transcription utilise des styles italique, gras, centrage et Titre 1. Ces styles ne font pas partie des conventions de la transcription et sont donc ignorés par TXM. Ne pas hésiter à en utiliser pour améliorer la lisibilité de la transcription, y compris des couleurs.

### Préambule de la transcription

`P1S8 4 avril 2014`<br>
`Séance 8 du vendredi 8 octobre 2010`<br>
`(36 élèves)`<br>
`Transcription publiée le 4 avril 2014`<br>
`Cette transcription est diffusée sous licence`<br>
`Creative Commons BY-NC-SA`<br>
`Copyright © 2013 - Andrée Tiberghien, Marie-Pierre Chopin, Laurent Lima, Laurent Talbot, Abdelkarim Zaid`

Tout ce qui précède la première marque de synchronisation ne fait pas partie du corps de la transcription, mais sera affiché en préambule dans la première page de l'édition de la transcription.

Cette partie n'a pas à suivre les règles du corps de la transcription (sections, tours de parole, etc.) et ne sera pas indexée par TXM. On peut donc y mettre n'importe quel texte (texte introductif, métadonnées sur les locuteurs ou la situation, informations de durée, conditions, etc.).

### Marques de synchronisation

Exemple :

`(00:00:16.58)`

La transcription peut contenir des marques de synchronisation sous la forme «&nbsp;(hh:mm:ss.MM)&nbsp;». Comme l'exemple `(0:00:16.58)` qui correspond à 16 secondes et demie du début de l'enregistrement. On place ces marques soit seules entre les lignes de tours soit au sein de la transcription de tours de paroles. Une bonne pratique est d'en placer soit en début soit en fin de tour.

TXM affectera un temps de début et de fin à chaque tour de parole, éventuellement en interpolant à partir de marques existantes en cas de manque de certaines marques de synchronisation dans la transcription.

Si vous ne mettez que les seules marques de synchronisation obligatoires de début et de fin d'enregistrement dans votre transcription, TXM calculera de façon approximative les marques de synchronisation de chaque début de tour (il utilisera pour cela le nombre total de tours pour la répartition, mais pas le nombre de mots de chaque tour).

L'ajout de marques de synchronisation dans la transcription permet de situer temporellement des occurrences de mots dans les références de concordances de TXM et améliore la précision de lecture de l'enregistrement depuis TXM.

### Tours de parole

Exemple :

`P	(00:00:0.0) si vous mettez alors il faudrait faire une obscurité parfaite vraiment parfaite euh un CD sous la lumière d'un néon vous ne verrez pas toutes les couleurs de l'arc en ciel (00:00:13.06) puisque le CD il fait quoi il est où là dedans (montre au tableau sur le prisme)`

La saisie d'un tour de parole :

- commence par un code de locuteur en début de ligne, ici `P` (code simplifié pour le **P**rofesseur).
- immédiatement suivi d'un caractère \<Tabulation\> (au clavier touche «&nbsp;→\|&nbsp;» ou «&nbsp;tab&nbsp;»), invisible ici
- puis de la transcription de l’énoncé :`si vous mettez alors il faudrait`...
- ce tour contient également deux marques de synchronisation (optionnelles) :
  - `(00:00:0.0)` au début
  - `(00:00:13.06)` au milieu
- ainsi qu'un commentaire :`(montre au tableau sur le prisme)`

L'identifiant du locuteur est composé d'un seul mot commençant par une lettre et systématiquement passé en majuscules pour uniformiser tous les identifiants.

**Astuces pour vérifier le bon formatage des tours de parole** :

- Pour vérifier que les tabulations entre les codes de locuteur et les transcriptions d’énoncés sont bien placées dans Writer/Word : vous pouvez transformer le texte de la transcription en tableau (sélectionner le texte des tours de parole et utiliser la commande 'Tableau > Convertir > Texte en tableau') et vérifier que l’on n'obtient bien que deux colonnes :
  
  - la première colonne ne contient que des codes de locuteurs (avec éventuellement des marques de synchronisation entre parenthèses)
  - la deuxième colonne ne contient que ce qui est dit ou fait.
- On peut également vérifier les tabulations doublées (fautives) en faisant une recherche de tabulations doublées : ‘\t\t’ dans le module de recherche de Writer (‘^t^t’ dans le module de recherche de Word), les remplacer alors par une seule '\t' dans Writer (ou ‘^t’ dans Word).

Les tours de parole permettent d'attribuer un énoncé à un locuteur.

Ceci permet par exemple :

- de focaliser une recherche sur un locuteur précis ou un type de locuteur
- d'afficher le code du locuteur ou ses propriétés dans les références de concordances de TXM
- de construire des sous-corpus par locuteur (eg «&nbsp;le discours du professeur&nbsp;» de code 'P').

### Commentaires

Exemples :

`(montre au tableau sur le prisme)`<br>
`...`<br>
`(inaud.)`<br>
`...`<br>
`(un élève lève le doigt on suppose hors caméra)`

Les commentaires sont écrits entre parenthèses ou entre crochets. Le fait qu'ils soient dans un certain style, par exemple en italiques, n'est pas différencié par TXM ; Une bonne pratique consiste à mettre entre parenthèses tout ce qui n’est pas de la parole.

Les commentaires ne sont pas indexés par TXM mais sont affichés dans les éditions des transcriptions.

### Limites de sections

Exemple :

`[theme="Thème 4 : Spectres d'émission et lumière émise par une étoile" organisation="class"]`

Une transcription peut être divisée en sections, caractérisées par des propriétés.

Une section commence par une ligne au format suivant :

[propriété1="*une valeur*" propriété2="*une autre valeur*" etc.]

Le nombre et les noms de propriétés «&nbsp;propriété1&nbsp;» et «&nbsp;propriété2&nbsp;» de la section sont libres mais ne doivent pas contenir d'accents ni d'espace ou de ponctuation.

**Astuce pour nommer des propriétés** : si besoin on peut remplacer les espaces dans les noms composés par des soulignés (_).

Les valeurs de propriétés doivent être entre placées entre guillemets droits anglais `"…"`.

Les valeurs «&nbsp;une valeur&nbsp;» et «&nbsp;une autre valeur&nbsp;» de ces propriétés sont actives jusqu'à la section suivante.

Les groupes propriété="valeur" sont séparés par un espace.

Une nouvelle section ferme la section qui la précède.

**Astuce pour gérer les valeurs de propriétés vides** : si jamais une propriété de section n'a pas de valeur à prendre, on peut lui mettre la valeur conventionnelle “none”. Cela sera plus pratique pour traiter l'absence de valeur dans TXM.

Attention : il ne faut rien saisir d'autre dans cette ligne, pas de commentaire ni de marque de synchronisation.

Les sections peuvent servir à construire des sous-corpus à partir de leurs propriétés lors de l'analyse du corpus. Par exemple le sous-corpus des moments organisés en classes (organisation="class") en opposition aux moments organisés en travaux pratiques.

## Convertir la transcription du format Word .docx vers le format Transcriber .trs

Nous allons convertir la transcription .docx en .trs pour qu'elle soit importable dans TXM.

Pour cela nous allons utiliser l'outil TXM **TextTranscription2TRS** qui peut convertir plusieurs fichiers situés dans un dossier d'entrée.

* créer un nouveau dossier `docx`
* y déposer le fichier `P1S8-30-avril-2014.docx`
* dans TXM, lancer 'Utilitaires > transcription > Text Transcription 2 TRS'
  * dans la boite de dialogue des paramètres :
    * dans le champ du paramètre **odtDir** pointer vers le dossier `docx`
    * puis cliquer sur le bouton <button>Exécuter</button> pour lancer l'utilitaire de conversion

🠲 La console de TXM doit afficher des lignes comme ci-dessous lors de la conversion&nbsp;:

`Exécution du script Groovy TextTranscription2TRSMacro.groovy…`<br>
`out: /home/sheiden/Documents/txm/documentation/tutoriels/transcriptions/docx/out`<br>
`odtDir: /home/sheiden/Documents/txm/documentation/tutoriels/transcriptions/docx`<br>
`DEBUG: false`<br>
`* Processing /home/sheiden/Documents/txm/documentation/tutoriels/transcriptions/docx directory`<br>
`** Processing /home/sheiden/Documents/txm/documentation/tutoriels/transcriptions/docx/P1S8-30-avril-2014.docx file`<br>
`*** DOC -> TXT`<br>
`Reading .docx directly`<br>
`*** TXT -> TRS`<br>
`PROCESS GROUPS...`<br>
`*** TRS -> FIXED TRS`<br>
`Nfix: 3 [Sync[attributes={time=0.0}; value=[]], Sync[attributes={time=0.0}; value=[]], Sync[attributes={time=13.0}; value=[]]]`<br>
`start=0.0 end=13.0 delta=4.333333333333333`<br>
`Effectué en 848 ms.`

Glose :
* le fichier .trs résultat de la conversion sera enregistré dans le répertoire `/home/sheiden/Documents/txm/documentation/tutoriels/transcriptions/docx/out`
* le fichier `P1S8-30-avril-2014.docx` a été converti
il y a eu quelques réglages de synchronisations (TRS -> FIXED TRS)
* le fichier `P1S8-30-avril-2014.trs` résultat de la conversion a été déposé dans le sous-dossier `docx/out`

Notre transcription `P1S8-30-avril-2014.trs` est maintenant prête à être importée dans TXM.

## Préparer le dossier source pour l'import avec le module XML Transcriber + CSV de TXM

### Transcription
{: .no_toc :}

Préparation de la transcription :

* créer un nouveau dossier qui contiendra les fichiers sources du corpus (dont le nom sera utilisé pour créer le corpus dans TXM), par exemple `P1S8`
* copier le fichier source `P1S8-30-avril-2014.trs` dans le dossier source `P1S8`

### Fichier vidéo
{: .no_toc :}

Préparation de l'enregistrement :

* téléchargement de la vidéo .mp4
  * dans un navigateur, enregistrer la cible du lien : [https://gitlab.huma-num.fr/txm/txm-ressources/-/raw/master/corpora/P1S8/src/P1S8-30-avril-2014/P1S8-30-avril-2014.mp4](https://gitlab.huma-num.fr/txm/txm-ressources/-/raw/master/corpora/P1S8/src/P1S8-30-avril-2014/P1S8-30-avril-2014.mp4)
  * enregistrer dans le fichier `P1S8-30-avril-2014.mp4`
* créer un nouveau sous-dossier dans le dossier des sources `P1S8/media`
* déplacer le fichier `P1S8-30-avril-2014.mp4` dans le dossier `P1S8/media`

Remarque : il est important que le fichier de transcription et le fichier d'enregistrement aient le même nom `P1S8-30-avril-2014` (modulo l'extension) pour que TXM puisse faire le lien entre les deux.

## Importer le corpus dans TXM

Nous allons créer un corpus composé de la seule transcription `P1S8-30-avril-2014.trs`.

* dans TXM, lancer la commande 'Fichier > Importer > Corpus > XML&nbsp;Transcriber&nbsp;+&nbsp;CSV'

![XML Transcriber + CSV](../images/tuto-import-transcription-import-trs.png){: style="padding: 20px;" width="600"}<br>
Lancement du module d'import [XML Transcriber + CSV](https://txm.gitpages.huma-num.fr/txm-manual/importer-un-corpus-dans-txm.html#module-xml-transcribercsv-import-de-xml-selon-la-dtd-transcriber-.trs).

* dans le formulaire de paramètres :
  * cliquer sur le lien 'Sélectionner le dossier des fichiers sources et nommer le corpus'
    * pointer vers le dossier source de votre disque dur :`P1S8`
    * nommer le corpus (un nom basé sur celui du dossier source est proposé automatiquement, c'est celui que nous utiliserons dans la suite de ce tutoriel)
    * cliquer sur le bouton<button>Finish</button>
  * cliquer sur le bouton <button>Lancer l'import du corpus</button>

🠲 le module d'import crée un nouveau corpus P1S8 prêt à être utilisé.

## Installer l'extension 'Media Player'

Jusqu'à TXM 0.8.3 (inclus), pour pouvoir lire la vidéo du cours depuis TXM, si vous ne l'avez pas déjà fait, vous devez d'abord installer l'extension 'Media Player'. Ceci n'est à faire qu'une seule fois par installation de TXM.

* dans TXM, lancer 'Fichier > Ajouter une extension'
* dans la boite de dialogue 'Install' qui s'ouvre
![Extension Media Player](../images/tuto-import-transcription-mediaplayer-extension-choice.png){: style="padding: 20px;" width="550"}
  * sélectionner l'extension 'Media Player'
  * puis cliquer sur le bouton <button>Finish</button>
* dans la nouvelle boite de dialogue 'Install' qui s'ouvre
  * cliquer sur le bouton <button>Next</button>
  * puis le bouton <button>Finish</button>
* dans la boite de dialogue 'Trust' qui s'ouvre
![Trust extension](../images/tuto-import-transcription-mediaplayer-trust.png){: style="padding: 20px;" width="550"}
  * sélectionner la ligne `Unsigned|n/a ...`
  * puis cliquer sur le bouton <button>Trust Selected</button>
* dans la boite de dialogue 'Software Updates' qui s'ouvre
![Restart](../images/tuto-import-transcription-mediaplayer-restart.png){: style="padding: 20px;" width="500"}
  * cliquer sur le bouton <button>Restart Now</button>

TXM se relance et l'extension est prête à être utilisée.

## Exploitation du corpus

### Lecture de l'enregistrement depuis une concordance

* dans TXM
  * calculer le Lexique du corpus P1S8
   ![Lexique](../images/tuto-import-transcription-lexique.png){: style="padding: 20px;" width="550"}
  * calculer la concordance du mot "néon" en double-cliquant sur le mot dans le Lexique (vers la 20ième ligne)
  ![Concordance de "néon"](../images/tuto-import-transcription-concordance-neon.png){: style="padding: 20px;" width="550"}
  * lancer la lecture de l'enregistrement au moment de l'énoncé de la deuxième ligne de la concordance à partir de son menu contextuel :<br>
  `P1S8-30-avril-2014`<br>
  `si vous faites taper la lumière d'un`<br>
  `néon`<br>
  `sur un CD vous ne verrez pas toutes les couleurs de l'`
    * ouvrir le menu par un clic droit sur la deuxième ligne
 ![Menu contextuel "néon"](../images/tuto-import-transcription-menu-neon.png){: style="padding: 20px;" width="550"}
     * lancer "Jouer le média"
     * la fenêtre du lecteur vidéo s'ouvre à côté de la concordance et joue la vidéo au moment de l'énoncé<br>
![Lecture vidéo synoptique avec la concordance](../images/tuto-import-transcription-lecture-concordance.png){: style="padding: 20px;" width="900"}

### Lecture de l'enregistrement depuis l'édition

* dans TXM
  * ouvrir l'édition du corpus P1S8 en double-cliquant sur son icone dans la vue Corpus
  * aller au début de la transcription à la page 2
  * chaque bouton "Lecture" [![bouton Lecture](https://upload.wikimedia.org/wikipedia/commons/7/7b/Octicons-playback-play.svg){: width="10"}] de l'édition permet de lancer la lecture de l'enregistrement à ce moment de la transcription
  ![Édition de la transcription P1S8-30-avril-2014](../images/tuto-import-transcription-edition.png){: style="padding: 20px;" width="450"}
  * lancer la lecture de l'enregistrement à partir du début de l'énoncé `puisque le CD il fait quoi ...` dans le premier tour de parole du professeur (P) en cliquant sur le bouton "Lecture" [![Édition de la transcription P1S8-30-avril-2014](https://upload.wikimedia.org/wikipedia/commons/7/7b/Octicons-playback-play.svg){: width="10"}] situé au début de ce passage
  * la fenêtre du lecteur vidéo s'ouvre à côté de l'édition et joue la vidéo au moment de l'énoncé
 ![Lecture vidéo synoptique avec l'édition](../images/tuto-import-transcription-lecture-edition.png){: style="padding: 20px;" width="900"}

## Conclusion

Nous venons de voir comment importer une transcription au format .docx dans TXM et comment lire l'enregistrement vidéo source correspondant grâce à quelques codes de synchronisation introduits dans la transcription. Ainsi équipée, la transcription se comporte alors comme un index (verbal et non verbal - gestes, silences, etc.) de l'enregistrement qui constitue la source de référence de tout travail d'analyse basé sur des enregistrements d'entretiens au delà des imprécisions et erreurs de transcription. Et la transcription elle-même est équipée des tous les éléments de structuration qui permettrons d'orienter les analyses textométrique finement au niveau de chaque locuteur.

Ce tutoriel n'aborde pas la question du plan textuel des "enquêteurs" correspondant aux discours de certains locuteurs à exclure des analyses, car le corpus exemple ne s'y prête pas.
Ce plan sert, en général, à ignorer les questions et les commentaires des enquêteurs pour ne travailler que sur le discours des répondants à une enquête.
Si votre travail en a besoin son fonctionnement est décrit à travers la documentation du paramètre 'Indexer les tours d'enquêteur' et de la métadonnée 'interviewer-id-regex' de la documentation de référence du module d'import [XML Transcriber + CSV](https://txm.gitpages.huma-num.fr/txm-manual/importer-un-corpus-dans-txm.html#module-xml-transcribercsv-import-de-xml-selon-la-dtd-transcriber-.trs).

Ce corpus est par ailleurs vraiment trop petit pour faire l'objet d'une exploration textométrique plus avant. L'analyse d'un corpus de transcriptions complet sera l'objet d'un autre tutoriel qui abordera notamment :
* le réglage des références de concordance pour afficher le nom du locuteur et le moment de l'énoncé
* la recherche de mots et leur décompte au sein des seuls tours de parole d'un locuteur ou d'un type de locuteur donné
* la création de sous-corpus et le contraste par locuteurs ou par type de locuteurs
* etc.

---
Notes

a : auteur.<br/>
e : éditeur.<br/>
[^1]: même si on ne souhaite pas insérer de marques de synchronisation dans la transcription, il faut insérer une première marque de temps factice `(00:00:00.0)` pour délimiter le corps de la transcription du préambule.<br/>
[^2]: la dernière synchronisation est également obligatoire pour marquer la fin du corps de la transcription. Si on ne souhaite pas insérer de marques de temps dans la transcription on peut utiliser une marque factice, par exemple une heure d'enregistrement `(01:00:00.0)`.

