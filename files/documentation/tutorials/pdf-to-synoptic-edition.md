---
ititle: Tutoriel de construction d'éditions synoptiques à partir de PDF
layout: page
lang: fr
ref: tutorials-dir
---

# Tutoriel de construction d'édition synoptique à partir de PDF
{: style="text-align: center;"}

<center>
Serge Heiden<br/>

septembre 2023<br/>

CC BY-NC-SA

</center>

Ce tutoriel va vous aider à construire une édition synoptique dans TXM mettant côte à côte les pages de l'édition du texte d'un fichier PDF avec les images de pages correspondantes.
{: text-justify: auto}

**Résumé des opérations**

Les opérations vont être les suivantes :

1. si vous utilisez un TXM plus ancien que TXM 0.8.3, [**Préparer** votre TXM avec la librairie PDFBox et les dernières versions des outils utilisés](#préparer-txm-avec-la-librairie-pdfbox-et-les-dernières-versions-des-outils-utilisés) 
1. **Télécharger** depuis Gallica le fichier .pdf exemple du « Recueil d'anciens petits ouvrages politiques et sacrés » ; composés par l'abbé Beugin (J.-B.)
1. extraire le **texte** du fichier PDF dans un fichier .txt en insérant une marque à chaque saut de page
1. produire des fichiers **images** correspondants à chaque page du fichier PDF
1. encoder le fichier .txt en **.xml** pour pouvoir profiter des imports XML produisant des éditions synoptiques
1. réaliser l'**import** XML-TEI Zero + CSV du fichier .xml et des images de pages pour obtenir l'édition synoptique

Remarque : tous les outils utilisés dans ce tutoriel sont capables de traiter des dossiers contenant plusieurs fichiers à traiter à la fois. Donc bien que ce tutoriel présente la construction d'un corpus contenant un seul texte issu d'un fichier PDF, il peut être généralisé à la construction d'un corpus contenant plusieurs textes issus de plusieurs fichiers PDFs.

## Télécharger le fichier .pdf du « Recueil d'anciens petits ouvrages politiques et sacrés » depuis Gallica

Le « Recueil d'anciens petits ouvrages politiques et sacrés » ; composés par l'abbé Beugin (J.-B.) est un petit texte de 13 pages allégeant les manipulations réalisées dans ce tutoriel et prenant peu de place sur votre disque dur.

* dans un navigateur, ouvrir l'adresse : [https://gallica.bnf.fr/ark:/12148/bpt6k5538973h](https://gallica.bnf.fr/ark:/12148/bpt6k5538973h)

* dans le menu de droite de la page qui s'ouvre, cliquer sur le bouton <button>Téléchargement / impression</button>

* dans la boite de dialogue :
  * cocher la case 'En cochant cette case, je reconnais avoir pris connaissance des conditions d'utilisation et je les accepte.'
  * puis cliquer sur le bouton <button>Télécharger</button>

🠲 le navigateur dépose le fichier `Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h.pdf` sur votre disque dur.

## Extraire le texte du fichier PDF

Le contenu du texte à importer dans TXM sera le texte brut d'OCR stocké dans le .pdf. Son extraction est assez rapide.

* dans TXM, lancer la commande 'Utilitaires > pdf >  PDF&nbsp;2&nbsp;TXT'
  * dans la boite de dialogue des paramètres :
    * dans le champ du paramètre **input_file** pointer vers le fichier .pdf
    * puis cliquer sur le bouton <button>Exécuter</button> pour lancer l'utilitaire

🠲 l'utilitaire crée un fichier `Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h.txt` à côté du .pdf.

## Produire les fichiers d'images de pages

Nous allons produire autant d'images de pages qu'il y a de pages dans le .pdf. Ces images seront alignées avec les pages de l'édition du texte dans TXM. Leur production prend plus de temps.

* dans TXM, lancer la commande 'Utilitaires > pdf >  PDF&nbsp;2&nbsp;Images'
  * dans la boite de dialogue des paramètres :
    * dans le champ du paramètre **input_file** pointer vers le fichier .pdf
    * puis cliquer sur le bouton <button>Exécuter</button> pour lancer l'utilitaire

🠲 l'utilitaire crée 15 fichiers d'images `Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-<##>.png` numérotés de 01 à 15 à côté du .pdf.

## Encoder le fichier .txt en .xml

Pour pouvoir paginer l'édition du texte, nous avons besoin du format .xml. En effet, dans du texte brut il n'y a pas de sauts de page.

* dans TXM, lancer la commande 'Utilitaires > txt >  TXT&nbsp;2&nbsp;XML'
  * dans la boite de dialogue des paramètres :
    * dans le champ du paramètre **inputDirectory** pointer vers le répertoire contenant le fichier .pdf
    * puis cliquer sur le bouton <button>Exécuter</button> pour lancer l'utilitaire

🠲 l'utilitaire crée un fichier `Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h.xml` dans un sous-dossier 'out' du dossier contenant le .pdf.

## Convertir les symboles de sauts de pages en balise TEI `<pb/>`

Le module d'import XML-TEI Zero + CSV que nous allons utiliser interprète les balises `<pb/>` pour déterminer les pages de l'édition du texte.

* dans TXM, lancer la commande 'Utilitaires > txt >  SearchReplaceInDirectory'
  * dans la boite de dialogue des paramètres :
    * dans le champ du paramètre **inputDirectory** pointer vers le répertoire 'out' contenant le fichier .xml
    * dans le champ du paramètre **extension** saisir '`\.xml`'
    * dans le champ du paramètre **find** saisir '`%%PB%%`'
    * dans le champ du paramètre **replaceWith** saisir '`<pb/>`'
    * puis cliquer sur le bouton <button>Exécuter</button> pour lancer l'utilitaire

**Remarque sur la mise en correspondance des pages de l'édition avec les images de pages du PDF source**

La balise `<pb/>` permettrait, par le biais d'un attribut `@facs`[^1], d'associer explicitement une page d'édition à un fichier image de page du PDF source, mais dans le cas du module d'import XML-TEI Zero + CSV cela n'est pas nécessaire : à défaut d'attribut `@facs`, le module va associer les images aux pages dans l'ordre lexicographique de leur nom de fichier. Le fichier image `Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-01.png` sera associé à la première page de l'édition, le fichier image `Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-02.png` sera associé à la deuxième page, etc.

## Organiser les fichiers source pour l'import avec le module XML-TEI Zero + CSV de TXM

* créer un dossier des sources du corpus (dont le nom sera utilisé pour créer le corpus dans TXM, par exemple `recueil-J-B`)
* copier le fichier source .xml dans le dossier source
* créer un sous-dossier `images` dans le dossier source
* créer un sous-dossier `Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h` dans le dossier `images`
* copier tous les fichiers .png dans le sous-dossier `Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h` du dossier `images`
* votre dossier des sources doit avoir la forme suivante :

<div markdown="0">
   <pre style="font-family: monospace; max-width: 1000;">
recueil-J-B/
├── images
│   └── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h
│       ├── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-01.png
│       ├── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-02.png
│       ├── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-03.png
│       ├── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-04.png
│       ├── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-05.png
│       ├── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-06.png
│       ├── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-07.png
│       ├── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-08.png
│       ├── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-09.png
│       ├── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-10.png
│       ├── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-11.png
│       ├── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-12.png
│       ├── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-13.png
│       ├── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-14.png
│       └── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h-15.png
└── Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h.xml</pre>
</div>

## Importer le corpus dans TXM

Nous allons créer un corpus composé du seul texte `Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h`.

* dans TXM, lancer la commande 'Fichier > Importer > Corpus > XML-TEI Zero + CSV'
  * dans le formulaire de paramètres :
    * cliquer sur le lien 'Sélectionner le dossier des fichiers sources et nommer le corpus'
      * pointer vers le dossier source
      * nommer le corpus
      * cliquer sur le bouton <button>Finish</button>
    * dans la section 'Éditions'
      * cocher l'option 'Construire l'édition facs'
      * dans le champ du paramètre 'Dossier des images', pointer vers le dossier 'images' du dossier source
    * cliquer sur le bouton <button>Lancer l'import du corpus</button>

🠲 le module d'import crée le nouveau corpus.

## Vérifier l'édition synoptique

* dans TXM, lancer la commande 'Édition' sur le nouveau corpus
* aller à la cinquième page de l'édition. Elle devrait apparaître comme ceci :

![Page 5 de l'édition TXM du texte Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h (qui correspond à la page 4 de l'édition originale)](../edition-page-4.png "Page 5 de l'édition TXM du texte Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h (qui correspond à la page 4 de l'édition originale)"){: width="1000"; max-width="1000" }
*Fig 1. Page 5 de l'édition TXM du texte Recueil_d'anciens_petits_..._bpt6k5538973h (qui correspond à la page 4 de l'édition originale).*

* en naviguant dans l'édition, vous pouvez vérifier que les premiers mots de pages correspondent bien entre les deux vues (quand l'OCR ne s'est pas trompé)

À partir de là, on voit que l'on peut améliorer l'édition du texte en réalisant les opérations supplémentaires suivantes sur le fichier source .xml :
* corriger et centrer les numéros de page ;
* insérer des balises '`<p>`...`</p>`' au fil des pages pour aérer l'édition et mieux correspondre au formatage de l'édition d'origine ;
* éventuellement forcer quelques sauts de lignes en insérant des '`<lb/>` pour aérer l'édition et mieux correspondre au formatage de l'édition d'origine quand les balises '`<p>`...`</p>`' ne suffisent pas ;
* insérer des balises '`<emph>`...`</emph>`' pour marquer les italiques ;
* insérer quelques symboles de séparation, par exemple '`⸻`' et '`~`' ;
* pour affiner le lexique, délimiter certains passages par une balise, par exemple '`<seg>...</seg>`', pour pouvoir les placer dans le plan 'hors-texte-à-éditer' à l'import pour ne pas indexer leur contenu tout en les affichant dans l'édition, par exemple :
  * le contenu des trois pages liminaires
    * les deux premières pages de Gallica
    * la page de titre
  * les numéros de pages
  * les symboles de séparation
  * le colophon à la fin "De l'Imprimerie de J. MORONVAL, rue des Ppêtres-St.- Severin, N°. 4."
* et bien sûr corriger l'OCR...

On pourrait alors obtenir quelque chose comme ceci :

![Page 5 de l'édition TXM corrigée du texte Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h (qui correspond à la page 4 de l'édition originale)](../edition-page-4-corrected.png "Page 5 de l'édition TXM corrigée du texte Recueil_d'anciens_petits_ouvrages_politiques_[...]Beugin_J_bpt6k5538973h (qui correspond à la page 4 de l'édition originale)"){: width="1000"; max-width="1000" }
*Fig 2. Page 5 de l'édition TXM corrigée du texte Recueil_d'anciens_petits_..._bpt6k5538973h (qui correspond à la page 4 de l'édition originale).*

Cela fera l'objet d'un autre tutoriel.

## Préparer TXM avec la librairie PDFBox et les dernières versions des outils utilisés

### 1. Installer la librairie Apache PDFBox dans TXM

* **télécharger** la librairie 'pdfbox-app-3.0.0.jar' depuis l'adresse [https://dlcdn.apache.org/pdfbox/3.0.0/pdfbox-app-3.0.0.jar](https://dlcdn.apache.org/pdfbox/3.0.0/pdfbox-app-3.0.0.jar)
* dans TXM, lancer la commande 'Utilitaires > **Add Library**'
  * dans la boite de dialogue des paramètres :
    * dans le champ du paramètre **jarFiles** pointer vers le fichier 'pdfbox-app-3.0.0.jar' téléchargé
    * puis cliquer sur le bouton <button>Exécuter</button> pour lancer l'utilitaire

🠲 La console de TXM doit afficher le message suivant pour confirmer la bonne installation&nbsp;:

<code>
New library: <...>/TXM-0.8.3/scripts/groovy/lib/pdfbox-app-3.0.0.jar
</code>

* **relancer** TXM pour la prise en compte de la nouvelle librairie

### 2. Installer les dernières versions des outils PDF2TXT et PDF2Images

* **télécharger** les dernières versions des macros :
  * **PDF2TXT** : depuis l'adresse [https://gitlab.huma-num.fr/txm/txm-ressources/-/raw/master/macros/org/txm/macro/pdf/PDF2TXTMacro.groovy](https://gitlab.huma-num.fr/txm/txm-ressources/-/raw/master/macros/org/txm/macro/pdf/PDF2TXTMacro.groovy)
  * **PDF2Images** : depuis l'adresse [https://gitlab.huma-num.fr/txm/txm-ressources/-/raw/master/macros/org/txm/macro/pdf/PDF2ImagesMacro.groovy](https://gitlab.huma-num.fr/txm/txm-ressources/-/raw/master/macros/org/txm/macro/pdf/PDF2ImagesMacro.groovy)
* dans TXM, lancer la commande 'Utilitaires > **Add Macro**'
  * dans la boite de dialogue des paramètres :
    * dans le champ du paramètre **groovyFiles** pointer vers les deux fichiers .groovy téléchargés (utiliser "Màj-Click" ou "Ctrl-Click" pour la sélection multiple de fichiers)
    * puis cliquer sur le bouton <button>Exécuter</button> pour lancer l'utilitaire

🠲 La console de TXM doit afficher le message suivant pour confirmer la bonne installation&nbsp;:

<code>
Update macro: <...>/TXM-0.8.3/scripts/groovy/user/org/txm/macro/pdf/PDF2ImagesMacro.groovy
</code>
<br>
<code>
Update macro: <...>/TXM-0.8.3/scripts/groovy/user/org/txm/macro/pdf/PDF2TXTMacro.groovy
</code>

___
Notes

[^1]: Voir la section [4.9.5 Module XML-TEI Zero+CSV / 4.9.5.2 Éditions / Pagination](https://txm.gitpages.huma-num.fr/txm-manual/importer-un-corpus-dans-txm.html#%C3%A9ditions-1) du manuel de TXM.
