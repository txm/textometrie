---
ititle: FAQ - Réponses aux Questions les plus Fréquentes
layout: page
lang: fr
ref: documentation-dir
---

# FAQ - Réponses aux<br/>Questions les plus Fréquentes
{: .no_toc style="text-align: center;"}

[ ![FREQUENTLY ASKED QUESTIONS - English translation of this page](../images/uk_flag.png){: width="24px"} FREQUENTLY ASKED QUESTIONS **(English translation of this page)**](https://translate.google.fr/translate?hl=fr&sl=auto&tl=en&u=https://txm.gitpages.huma-num.fr/textometrie/files/documentation/faq/)

{% include important.html content="**Vous êtes invité(e) à contribuer à ces pages** au
bénéfice de la communauté. Pour les éditer il suffit d’être connecté
avec un identifiant correspondant à votre adresse mail d’abonnement à la
[liste de diffusion 'txm-users'](https://groupes.renater.fr/sympa//info/txm-users)." %}

{% include note.html content="Si vous ne trouvez pas ici de réponse à votre question (ni dans
le [manuel de TXM](https://txm.gitpages.huma-num.fr/txm-manual/)),
vous pouvez aussi consulter les [archives de la liste de discussion par mail 'txm-users'](https://groupes.renater.fr/sympa/arc/txm-users),
puis si besoin poser votre question sur la liste (envoyer un mail à txm-users *à* groupes.renater.fr, après vous [être inscrit](https://groupes.renater.fr/sympa/subscribe/txm-users))." %}

Certaines questions, rendues caduques par l’évolution de TXM, ont été
déplacées vers les [archives de la FAQ](https://groupes.renater.fr/wiki/txm-users/public/faq_archives) ou bien simplement raturées sur place.

# Sommaire
{: style="text-align: center" .no_toc}

* TOC
{:toc}

# Documentation de TXM

### Comment faire des retours sur le manuel de référence de TXM ?

On peut faire des [propositions de modification du
manuel](https://groupes.renater.fr/wiki/txm-users/public/txm_manual_writing).

# Traitement des langues

### TXM peut-il traiter d’autres langues que le français ?

Oui, le logiciel TXM peut analyser toute langue dont l’alphabet est pris en
charge par le standard d’encodage des caractères
[Unicode](https://home.unicode.org) (c’est à dire tous les alphabets connus).
Voir :

- la liste des [alphabets
  Unicode](http://en.wikipedia.org/wiki/Script_%28Unicode%29#Table_of_scripts_in_Unicode-)
  ;
- la liste des [langues prises en charge par chaque alphabet
  Unicode](https://simple.wikipedia.org/wiki/List_of_languages_by_writing_system).

**Délimitation des unités lexicales**

La textométrie analyse la répartition d’unités lexicales dans des
textes. Elle a donc besoin d’une définition des unités lexicales
présentes dans le corpus. La statistique sur corpus étant une technique
relativement robuste (elle peut donner des résultats intéressants malgré
un certain taux d’erreurs ou d’imprécisions dans les données initiales),
on peut se contenter en première approche d’une définition
opérationnelle des unités lexicales basée essentiellement sur l’analyse
des catégories de caractères des textes, en l’absence d’une description
linguistique de qualité.

Certains formats importés par TXM (XML/w + CSV, XML-TEI BFM, CNR + CSV,
XML-TXM) permettent de lui transmettre un pré-découpage en unités
lexicales : c’est a priori ce qu’il faut privilégier quand on dispose
d’un corpus déjà annoté au plan lexical. Quand un tel découpage n’est
pas fourni, TXM utilise un outil automatique robuste qui a été conçu
pour le français mais qui peut quelquefois convenir en première
approximation à d’autres langues alphabétiques : l’algorithme se base
sur des classes de caractères (délimiteurs, ex. le blanc, le point,
l’apostrophe, vs non délimiteurs, ex. les lettres). En pratique, si vous
n’avez pas de pré-analyse lexicale de votre corpus, vous pouvez tester
l’intérêt du découpage intégré à TXM en effectuant un import et en
regardant la liste des unités lexicales obtenues par la commande
Lexique, une fois le corpus importé.

Par défaut, TXM va automatiquement identifier dans les sources
textuelles la délimitation des unités lexicales en fonction de leur
alphabet en s’appuyant sur les classes de caractères Unicode (comme les
classes de caractères séparateurs, de ponctuations, etc.). Bien que pour
l’ensemble des langues utilisant l’alphabet latin (c’est-à-dire
l’alphabet Unicode “Latin”) cet algorithme permette de calculer la
majorité des mots simples et des ponctuations, il y a pour chaque langue
des cas dont on peut tenir compte en plus (par exemple en français, les
figements comme “parce que” ou bien “aujourd’hui”, les clitiques comme
“…-je”, etc.). Par défaut, TXM ne réalise pas de traitement
supplémentaire à celui des classes Unicode comme il ne construit pas non
plus d’entités nommées (expressions de noms de personnes, de monnaies,
de lieux, de dates) voire de syntagmes comme les noms composés.

**Délimitation des phrases orthographiques**

Par défaut, TXM va automatiquement identifier dans les sources
textuelles la délimitation des phrases orthographiques en fonction de
leur alphabet en s’appuyant sur les classes de caractères Unicode. Bien
que pour l’ensemble des langues utilisant l’aphabet latin cet algorithme
permette d’obtenir une bonne approximation des phrases, il y a pour
chaque langue des cas dont il faut tenir compte en plus (l’utilisation
du point “.” dans l’écriture d’abbréviation comme “M.”, certaines
expressions mathématiques, etc.). Par défaut, TXM ne réalise pas de
traitement supplémentaire à celui des classes Unicode.

**Analyse morphosyntaxique et lemmatisation**

Si vous avez installé le logiciel d’analyse morphosyntaxique et de
lemmatisation Tree Tagger (en appliquant les [instructions
d'installation de TreeTagger dans
TXM](https://txm.gitpages.huma-num.fr/textometrie/InstallTreeTagger)), et indiqué
le chemin vers ce programme et les modèles de langue utiles à TXM, TXM
peut appeler Tree Tagger (TT) à la volée, lors de l’import, suite à la
phase de découpage (si le corpus comporte déjà une délimitation des
unités lexicales alors : pour les modules XML-TXM et CNR + CSV, TT n’est
pas appelé ; pour les modules XML/w et XML-TEI BFM, TT ajoute les
informations aux unités lexicales). TT calcule automatiquement la
catégorie morphosyntaxique et le lemme de chaque mot, c’est une analyse
clairement liée à la langue des textes. Si on souhaite l’utiliser pour
un corpus il faut donc s’assurer que l’on dispose bien du bon modèle de
langue (identifié par TXM selon le nom du fichier en deux caractères
selon la norme ISO, ex. “fr.par” pour le français, “it.par” pour
l’italien, etc.) et que le module d’import est informé de la langue du
corpus (à indiquer dans les paramètres au lancement de l’import, sinon
par défaut la langue est celle de l’interface de TXM).

Liste des langues prises en charge par TT en 2013 (pour voir la liste
complète et à jour des langues prises en charge [site de
TreeTagger](http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/)) :

- français - fr :
  <http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/french-par-linux-3.2-utf8.bin.gz>
- ancien français - fro : <http://bfm.ens-lyon.fr/IMG/zip/fro.zip>
- latin classique - la :
  <http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/latin-par-linux-3.2.bin.gz>
- anglais - en :
  <http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/english-par-linux-3.2.bin.gz>
- allemand - de :
  <http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/german-par-linux-3.2-utf8.bin.gz>
- italien - it :
  <http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/italian-par-linux-3.2-utf8.bin.gz>
- espagnol - sp :
  <http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/spanish-par-linux-3.2-utf8.bin.gz>
- galicien - gl :
  <http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/galician-par-linux-3.2.bin.gz>
- portugais - pt : <http://gramatica.usc.es/~gamallo>
- russe - ru : <http://corpus.leeds.ac.uk/mocky>
- estonien - et :
  <http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/estonian-par-linux-3.2.bin.gz>
- bulgare - bg :
  <http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/bulgarian-par-linux-3.1.bin.gz>
- swahili - sw :
  <http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/swahili-par-linux-3.2.bin.gz>
- néerlandais - nl :
  <http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/dutch-par-linux-3.2-utf8.bin.gz>
- slovaque - sk :
  <http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/slovak-par-linux-3.2-utf8.bin.gz>
- chinois - zh : <http://corpus.leeds.ac.uk/tools/zh>

D’autres logiciels d’étiquetage morphosyntaxique et de lemmatisation
sont utilisables de façon expérimentale avec l’aide de TXM (Cordial,
TNT, MElt, etc.).

### TXM peut il traiter des corpus de textes Arabes ?

Oui, TXM importe correctement les textes Arabes encodés en UTF-8. Les
éditions et résultats s’affichent alors correctement (de droite à gauche
avec liaison entre les caractères).

Avec quelques **limitations** :

- les unités lexicales ne sont segmentées qu’au niveau des espaces. Le
  découpage de la surface textuelle ne correspond donc pas
  systématiquement aux mots des textes, mais il semble malgré tout utile
  pour une première approche d’analyse. Nous sommes à la recherche de
  composants open-source de segmentation lexicale efficaces pour cette
  langue, ou au moins de règles de segmentation élémentaires permettant
  d’améliorer la segmentation actuelle. Si vous avez la possibilité de
  faire segmenter les textes par d’autres outils, nous vous recommandons
  alors d’utiliser le module d’importation XML/w qui tient compte des
  segmentations déjà disponibles au sein des textes ;
- il n’y a donc évidemment pas de lemmatisation ni d’étiquetage
  morpho-syntaxique. Nous sommes à la recherche de composants
  open-source efficaces pour réaliser ces opérations pour cette langue ;
- les contextes gauche et droit des concordances sont inversés ;
- les pages d’éditions sont justifiées à gauche, alors qu’elles
  devraient être justifiées à droite.

**Recommandation** : appliquer le [logiciel
Stanza](https://stanfordnlp.github.io/stanza/) sur les textes avant de
les importer dans TXM au format XML/w. Dans ce cas les outils
s’appliquerons sur les mots identifiés, lemmatisés et catégorisés par
Stanza.

#### Références de travaux utilisant TXM pour travailler sur des textes en langue arabe
{: .no_toc }

Alnassan, A.  (2017).  L'enseignement de l'arabe L2 entre l'héritage et la nouveauté en didactique des langues vivantes. <span style="font-style: italic;">Bellaterra Journal Of Teaching &amp; Learning Language &amp; Literature</span>, <span style="font-style: italic;">10</span>(1), 20 - 41. doi:10.5565/rev/jtl3.665. <a href="https://halshs.archives-ouvertes.fr/halshs-01519376">Snapshot</a>, <a href="https://halshs.archives-ouvertes.fr/halshs-01519376/document">Full Text PDF</a>.

### TreeTagger ne fonctionne pas. Comment bien régler TreeTagger pour TXM&nbsp;?

{% include note.html content="Depuis la mise en place des extensions TreeTagger pour TXM cette entrée a perdu de son intérêt dans la mesure où les extensions réalisent tous les réglages automatiquement désormais. Cependant, elle peut garder son intérêt pour des configurations spécifiques." %}

1.  Vérifier que les chemins renseignés dans la page de préférences `TXM > Avancé > TAL > TreeTagger` sont corrects&nbsp;:
  - Chemin du répertoire d‘installation du logiciel : Le dossier
      renseigné doit contenir les sous-dossiers “bin”, “cmd”, “doc,
      ”lib“. Si le dossier ne contient qu’un sous-dossier “TreeTagger”
      par exemple, il y a des chances que le logiciel de gestion
      d’archives ait créé ce dossier lors de l’extraction. Dans ce cas,
      il faut indiquer le chemin jusqu’à ce sous-dossier “TreeTagger”
      inclus.
  - Chemin du répertoire des modèles linguistiques : Ce
      dossier doit contenir les fichiers “\*.par”
2. Vérifier que le fichier modèle est compatible avec votre version de TreeTagger (un
      ancien modèle de langue peut ne plus être utilisable avec un
      TreeTagger récent) ou n’est tout simplement pas un modèle
      TreeTagger. Il faut reprendre attentivement la procédure
      d’[installation de
      TreeTagger](https://txm.gitpages.huma-num.fr/textometrie/InstallTreeTagger)
      pour la partie qui concerne les modèles de langue, en particulier
      mettre à jour les fichiers modèles et vérifier que les modèles
      sont bien dézippés avant d’être renommés.
3.  Vérifier que la version du logiciel TreeTagger installé correspond
    bien à celle de l’architecture de votre système. Ce bug est présent
    en ce moment car seule une version 64bit est disponible sur le site
    officiel de TreeTagger. Pour avoir une version 32bit, veuillez
    suivre ce lien [Installation de TreeTagger pour
    TXM](https://txm.gitpages.huma-num.fr/textometrie/InstallTreeTagger)

Si cela ne suffit pas, des messages d‘erreur sont affichés dans la
console de TXM à différents moments pour vous permettre de diagnostiquer
si TreeTagger a été correctement appliqué ou non&nbsp;:
- Lors d’une requête :
  - **"Dernière erreur CQP : \`\`frpos'' is neither a positional structural attribute nor a label reference"** signifie que la propriété
“frpos” n’est pas présente dans le corpus.
    - Soit TreeTagger n’a pas
    été lancé lors de l’import
    - Soit TreeTagger a été lancé mais pas avec
    le modèle “fr” (français) mais “en” (anglais) par exemple.
    - Sinon voir la liste des propriétés de mot disponibles en affichant les
“informations” du corpus
- Lors de l’import entre l’étape “– ANNOTATE”
et “– COMPILING”
  - “**Could not find TreeTagger binaries in …**” : TXM
n’a pas pu trouver le fichier exécutable de TreeTagger à partir du
chemin renseigné dans les préférences. Il peut y avoir plusieurs
raisons&nbsp;:
    - Le dossier du logiciel indiqué dans la page de
préférences `TXM > Avancé > TAL > TreeTagger` n’est pas le bon (voir
ci-dessus)
    - Vous n’avez pas les droits d’accès au dossier indiqué dans
la page de préférences `TXM > Avancé > TAL > TreeTagger` ou pour
exécuter le fichier qui s’y trouve
  - “**Skipping ANNOTATE: Incorrect modelfile path: …xx.par**” : TXM n’a pas trouvé le modèle “xx.par” dans
le dossier des modèles TreeTagger. Il peut y avoir plusieurs
raisons&nbsp;:
    - Le dossier des modèles indiqué dans la page de
préférences `TXM > Avancé > TAL > TreeTagger` n’est pas le bon (voir
ci-dessus)
    - Le fichier modèle n’existe pas dans le dossier des modèles
indiqué dans les préférences.
    - Le fichier modèle est bien présent mais
pas avec la bonne extension. Ce cas arrive fréquemment sous Windows ou
Mac OS X - malgré le renommage habituel des fichiers modèles demandé par
la procédure d’installation de TreeTagger pour TXM - quand *le système
masque par défaut à l’utilisateur les extensions de fichiers* dont il
connaît (ou pense connaître) le type. On peut par exemple voir un
fichier nommé “fr.par” alors que son nom ’réel’ (pour le système et pour
TXM) est “fr.par.bin”.  
Dans ce cas il faut accéder aux extensions réelles des fichiers et
renommer les noms ‘complets’ des fichiers selon chaque système :
      - sous Windows : pour voir et renommer le nom complet des fichiers (sans masquage de l'extension pour les types de fichiers "connus") vous pouvez suivre ce tutoriel : <http://www.commentcamarche.net/faq/825-afficher-les-extensions-et-les-fichiers-caches-sous-windows#solution-simple>
      - sous Mac OS X : pour vérifier l'extension d'un fichier modèle faire un clic droit sur son icone et aller dans « Lire les informations » au champ « nom et extension ». Si l’extension est "xx.par.bin", supprimer le ".bin".
  - "**ERROR: not a parameter file: ...xx.par!**" : le fichier modèle "xx.par" n'est pas un fichier modèle de TreeTagger. Si par exemple, le modèle a été mal extrait ou si il a été seulement renommé (sans être extrait).

### Je n’arrive pas à installer le modèle linguistique XXX pour TreeTagger, je peux le télécharger mais pas le dézipper, quel que soit le logiciel utilisé

Il se peut que le navigateur qui a fait le téléchargement a déjà dézippé
le fichier dans la foulée, ça dépend de votre configuration. Si c‘est le
cas, il ne vous reste alors plus qu’à le renommer ’xx.par’ dans le bon
répertoire et relancer l‘import en précisant la langue ’xx’ dans les
options.

Cette question répond aussi au problème rencontré : j’obtiens à chaque
fois le message “ne peut pas être ouvert comme une archive” ou “accès
refusé”.

Prenons l’exemple du fichier modèle pour l’anglais.

Une fois téléchargé, il doit apparaitre dans un répertoire sous la forme
d‘un fichier nommé **english-par-linux-3.2-utf8.bin.gz** La taille de ce
fichier est exactement 3 357 948 octets (soit 3,4 Mo à la louche).
Normalement, 7 Zip adore les fichiers dont le nom se termine par ’.gz‘.
Ce n’est pas à proprement parler une archive, mais c’est un fichier
compressé et 7 Zip sait quoi en faire (le décompresser). Pour qu’il dise
:
- “ne peut pas être ouvert comme une archive”, le fichier a peut-être
déjà été dé-compressé donc il n’est peut-être pas nécessaire d’essayer
de le décompresser à nouveau (le message d’erreur peut être trompeur).
Pour le vérifier, la taille du fichier une fois décompressé est
exactement de 14 412 183 octets (soit 14,4 Mo à la louche). Si le
fichier que vous essayez de manipuler a cette taille, il n’est plus
nécessaire de le décompresser, il faut passer à l’étape suivante. Les
raisons pour lesquelles le fichier aurait déjà été décompressé sont
multiples et dépendent des systèmes d’exploitation (il a été décompressé
par le navigateur web lors du téléchargement - typique sur Mac, il a été
décompressé lors d’une manipulation précédente sans que ce soit
explicité par un message - arrive sous Windows parce que le navigateur
de fichiers “connait” certains types de fichiers compressés, etc.).
- “accès refusé”, vous n’avez pas les droits pour accéder au fichier à
décompresser ou pour créer un fichier qui soit la version décompressée.
Dans ce cas il faut déplacer le fichier
’english-par-linux-3.2-utf8.bin.gz’ en lieu connu, par exemple dans
votre répertoire personnel. Et le décompresser là puis le déplacer dans
le répertoire ‘models’ ou autre.

### Dans TXM, comment lancer l’annotation de son corpus avec TreeTagger&nbsp;?

Il faut d’abord installer TreeTagger puis, lors du paramétrage d’un
module d’import, laisser le paramètre “annoter” coché pour que
TreeTagger se lance avec le modèle de la langue principale du corpus.

La description de la procédure d’installation de TreeTagger est
accessible à partir du menu de TXM `Aide > Documentation > Installer TreeTagger`, qui
correspond à la page web
<https://txm.gitpages.huma-num.fr/textometrie/InstallTreeTagger>.

### Je suis déçu par la qualité de l’étiquetage de TreeTagger sur mon corpus d’entretiens, pourtant j’avais spécialement choisi un modèle pour le français oral&nbsp;?

La qualité de l’étiquetage est notamment tributaire du découpage adopté
pour les mots, dit “tokenization”, qui ne correspond pas
systématiquement à celle opérée par défaut par TXM, et des conventions
de transcription.

**Pour ce qui concerne le modèle Treetagger pour le français parlé**
(celui indiqué sur le [site de
TreeTagger](http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/)
auquel nous renvoyons dans la [page d'aide
TXM](https://txm.gitpages.huma-num.fr/textometrie/InstallTreeTagger), construit et
[documenté](https://www.ortolang.fr/market/corpora/perceo) par le projet PERCEO) :

le corpus qui a servi pour l’entraînement ne comportait pas de mots en
majuscules (sauf pour les noms propres), aucune ponctuation (excepté
dans les multi-transcriptions) et était basé sur l’orthographe standard.
Du coup, si les transcriptions à étiqueter n’utilisent pas les mêmes
conventions de transcription, il est possible que la qualité de
l’étiquetage s’en ressente fortement. Il est alors préconisé de tester
avec la version de base de Treetagger (le modèle de français courant,
indiqué sur la [page d'aide
TXM](https://txm.gitpages.huma-num.fr/textometrie/InstallTreeTagger)) et de
comparer les résultats afin de voir si ceux-ci sont meilleurs ou moins
bons.

### Je n’arrive pas à changer la langue de l’interface

Il peut être nécessaire de fermer TXM et de le redémarrer pour que le
changement de langue soit effectif.

# Installation et mises à jour

## Préparation de l’installation

### Systèmes d’exploitation et architectures supportés par TXM

Cette entrée liste les systèmes d’exploitation et architectures pour
lesquels nous vérifions le bon fonctionnement de TXM (supporté), ou
connus pour exécuter TXM correctement bien que nous ne fassions pas de
vérification systématique (connu pour fonctionner).

Merci de nous indiquer les systèmes pour lesquels TXM fonctionne et que
nous n’avons pas encore listé ici, et merci pour les tests que vous
pourriez faire du bon fonctionnement de TXM sur tous ces systèmes et
architectures, en particulier lors de la sortie d’une nouvelle version
de TXM ou d’un nouveau système d’exploitation.

#### TXM version pour poste
{: .no_toc }

Pour TXM 0.8.3.

##### Supportés
{: .no_toc }

L’équipe TXM a testé le bon fonctionnement sur ces systèmes. La
correction de bug est réalisée en priorité pour ces systèmes.

- Windows 7 à 10 - 64 bit
- Mac OS X 10.15 à 13.0 - 64 bit ou M1
- Ubuntu 20.04 à 22.04 - 64 bit

##### Non supportés
{: .no_toc }

TXM n’est pas adapté pour les machines équipées des systèmes suivant :

- ChromeOS - toutes versions - ChromeBook toutes marques
- Android - toutes versions - Tablette, smartphones…
- iOS - toute versions - Ipad, IPhone…

##### Fonctionnent
{: .no_toc }

Des utilisateurs ont rapporté le bon fonctionnement sur ces systèmes.

- Windows 10 - 64 bit
- Windows 8 - 64 bit
- Windows Vista - 64 bit
- Windows XP - 32 bit
- Mac OS X 10.11 - 64 bit
- Mac OS X 10.10 - 64 bit
- Mac OS X 10.9 - 64 bit
- Linux Debian Jessie 8 - 64 bit
- ChromeOS 123.0.6312.132 - 64 bit
  - Debian 12.5 (Bookworm), Linux penguin 6.6.17-01102-gd3cec3c11146
    x86_64 GNU/Linux
    - Lenovo Chromebook 3, processeur Intel Celeron N4020 (IdeaPad 3 CB
      14IGL05)

#### TXM version portail
{: .no_toc }

Pour TXM portail 0.6.2.

##### Serveur
{: .no_toc }

- Tomcat 6 (supporté)
- Tomcat 7 (fonctionne)
- Tomcat 5 (fonctionne)
- Glassfish (fonctionne)

##### Client
{: .no_toc }

- Firefox 26.0 Linux (supporté)
- Firefox 25.0 Linux (fonctionne)
- Firefox 21.0 Linux (fonctionne)
- Chrome (fonctionne)
- Safari (fonctionne)
- Internet Explorer (fonctionne)
- Edge (fonctionne)

### Peut-on installer TXM sur d’autres versions de systèmes d’exploitation&nbsp;?

TXM peut s’installer sur d’autres versions de systèmes d’exploitation.

Indications selon la version de TXM,

#### TXM 0.7.8
{: .no_toc }

**Ubuntu 14.04** :

- la version de R embarqué dans TXM 0.7.8 n’est pas compatible avec les
  version précédentes d’Ubuntu 16.04
- indications pour contourner ce problème :
  1.  installer TXM avec dpkg et en utilisant l’option –force-all
  2.  installer R et les packages nécessaires pour TXM
  3.  configurer le chemin de R dans les préférences de TXM

Pour Ubuntu 12.04 la procédure est similaire.

**Windows** :

- nous n’avons pas de retours concernant les versions XP, Vista et 8 de
  Windows mais TXM devrait pouvoir s’installer

**Mac OS X** :

- la version de R embarqué dans TXM n’est pas compatible avec les
  versions de Mac OS X inférieures à 10.9 (exclu)
- indications pour contourner ce problème :
  1.  installer TXM
  2.  installer R et les packages nécessaires pour TXM
  3.  configurer le chemin de R dans les préférences de TXM

### Dois-je installer un TXM 32 bits ou 64 bits&nbsp;?

#### Windows
{: .no_toc }

Il faut installer le TXM qui correspond à l’architecture de votre
machine. En général, les machines 64 bits sont des machines récentes,
donc si vous n’êtes pas féru d’informatique et que votre machine a 4
ans, il y a de très grandes chances que vous soyez en 32 bits (il faut
quand même savoir que les machines 64 bits existent depuis plus de 10
ans, et c’est environ depuis 2011 qu’on n’achète plus trop de machines
32 bits).

Pour connaitre l’architecture 32 ou 64 bits de votre système Windows :
[voir la documentation
Microsoft](https://support.microsoft.com/en-us/help/15056/windows-7-32-64-bit-faq)

#### Mac OS X
{: .no_toc }

La question ne se pose pas, une seule version de TXM est proposée (64
bits), car elle convient aussi à des machines 32 bits (dans la limite
des systèmes compatibles, voir section [TXM tourne-t-il sur (tous les)
Mac ?](#txm-tourne-t-il-sur-tous-les-mac).

#### Linux/Ubuntu
{: .no_toc }

Il faut installer le TXM qui correspond à l’architecture de votre
machine.

Pour connaitre l’architecture 32 ou 64 bits de votre système Linux :

- Sous Ubuntu (testé avec un Ubuntu 12.04 LTS), dans le menu tout en haut
à droite (petite roue dentée), `Paramètres système… > Système : Détails > Type d’OS` (= 32 ou 64 bits)
- Pour tous les Linux dans un terminal, lancer la commande ‘lscpu’ :
  - si la ligne ‘Architecture’ donne la valeur ‘i686’ -\> 32 bits
  - sinon si la valeur est ‘x86_64’ -\> 64 bits
  - sinon \<aucune idée\>

#### Autre façon de faire si Java est déjà installé
{: .no_toc }

<del> Prendre une des versions de TXM pour votre système.</del>

<del>Lors de son installation TXM devrait détecter l’architecture de votre
machine et vous prévenir s’il y a incompatibilité.</del>

<del>Attention : si la version de Java installée n’est pas la bonne (32 ou 64
bits), cette détection peut ne pas fonctionner correctement voire TXM
lui-même ne pas fonctionner non plus.</del>

<del>Si TXM échoue à détecter l‘architecture de votre machine, vous pouvez
utiliser cet exécutable <http://txm.sourceforge.net/DumpProperties.jar>
(sous Linux, vous pouvez avoir à cocher au préalable l’option “autoriser
l’exécution du fichier comme un programme” de l’onglet des ’permissions’
de la boite de dialogue des propriétés du fichier).  
‘DumpProperties.jar’ affichera la liste des variables d‘environnement de
la machine Java installée. Si la ligne de la variable ’os.arch’ vaut
‘x86_64’, il faut installer la version 64 bits de TXM, sinon il faut
installer la version 32 bits. </del>

### Comment désinstaller TXM&nbsp;?

La désinstallation dépend du système d’exploitation :

- Windows : depuis le menu
      `Démarrer > répertoire TXM > uninstall`
- Mac OS X : supprimer le répertoire
      /Applications/TXM
- Linux :
  - depuis TXM 0.8.0, utiliser la ligne de commande (Ctrl-Alt-T)
~~~ bash
    sudo apt remove txm-0.8.0
~~~
    (changer le numéro en fonction de la version)  
    Ou bien pour supprimer toutes les versions :
~~~ bash
sudo apt remove 'txm*'
~~~
  - jusqu’à TXM 0.7.9, utiliser la ligne de commande
~~~ bash
sudo apt remove txm
~~~

Pour une désinstallation complète et retirer tous les corpus, il faut
supprimer le répertoire de travail de TXM dont l’emplacement dépend du
système d’exploitation :

- Windows :
      `C:\Users\<login>\TXM`
- Mac OS X :
      `/Users/<login>/TXM`
- Linux :
  - depuis TXM 0.8.0 :
        `/home/<login>/TXM-0.8.0`

    (changer le numéro en fonction de la version)
  - jusqu’à TXM 0.7.9 :
        `/home/<login>/TXM`

### Comment vérifier la version et l’architecture de Java sur ma machine&nbsp;?

{% include note.html content="Cette entrée concerne les versions antérieures à TXM 0.7.7" %}

<del>Pour cela il faut :

1.  ouvrir un terminal
    - Windows : il faut taper “cmd” quand on ouvre le menu Démarrer
    - Linux : il faut ouvrir le programme “Terminal”
    - Mac OS X : il faut ouvrir le programme “terminal” dans les
      accessoires (ou avec le spotlight)
2.  entrer la commande “java -version”
3.  interpréter les messages affichés. Exemple sur Ubuntu :
~~~ text
        java version "1.7.0_65"
        OpenJDK Runtime Environment (IcedTea 2.5.3) (7u71-2.5.3-0ubuntu0.14.04.1)
        OpenJDK 64-Bit Server VM (build 24.65-b04, mixed mode)
~~~
  - la première ligne indique que la version de Java est "1.7.0_65"
  - la troisième ligne indique que l'architecture est "64-Bit"
       

En complément, pour Windows, quand on a installé plusieurs fois Java sur
la machine, il peut y avoir des conflits entre les installations de
Java. Un cas fréquent est l’installation de Java d ’architectures
différentes. Ce qui produit les deux répertoires suivants :

- `C:\Program files (x86)\Java` (ou `C:\Programmes (x86)\java`) - version
  32-bit
- `C:\Program files\Java` (ou `C:\Programmes\java`) - version 64-bit

Il faut alors désinstaller celle qui ne correspond pas à l’architecture
de la machine (32-bit ou 64-bit). </del>

### Quel ordinateur faut il pour faire tourner TXM&nbsp;?

Caractéristiques types d’un ordinateur portable compatible :

- un écran pas trop petit, de taille supérieure à 11“, pas la peine de
  prendre un tank de 17” non plus.
- TXM ne tourne pas sur toutes les machines à écran tactile, ça dépend
  du processeur. Les machines à processeurs Intel fonctionnent
- TXM tourne avec 500Mo, mais il faut 4Go de ram minimum sur la machine
  si on veut pouvoir travailler avec plusieurs applications ouvertes
- éviter une tablette hybride qui n’aurait que 32 ou 64 Go de stockage,
  même en SSD

### Dois-je prendre des précautions particulières pour installer TXM et Iramuteq sur la même machine&nbsp;?

TXM et Iramuteq utilisant tous les deux le module statistique R, il faut
s’assurer que chacun des deux programmes dispose bien de la
configuration dont il a besoin.

#### Pour windows
{: .no_toc }

A priori pas de difficulté particulière, TXM installant et utilisant son
propre R (installé dans PROGRAMFILES/TXM/R).

#### Pour Mac
{: .no_toc }

Actuellement (v0.7), TXM installe R 2.14 s’il ne trouve pas d’autre
version. Iramuteq fonctionne avec R 2.15.2. On propose donc de procéder
ainsi :

- désinstaller TXM s’il est déjà installé ; dans ce cas vérifier qu’il
  ne reste pas un fichier “.Rprofile” dans votre répertoire utilisateur,
  sinon le supprimer (En ligne de commande du Terminal ça donne : rm
  ~/.Rprofile ) (C’est un reliquat d’installation de TXM qui va
  disparaître dans les prochaines version de TXM.) cf. [échange sur la
  liste
  txm-users](https://groupes.renater.fr/sympa/arc/txm-users/2013-02/msg00012.html)
- suivre les instructions de la [page d'installation
  d'Iramuteq](http://www.iramuteq.org/telechargement) pour installer
  d’abord R 2.15.2 puis Iramuteq (on trouve des indications plus
  détaillées pour l’installation de R sur le site de R, c’est pour la
  version courante de R il faut donc adapter cela à la 2.15.2 :
  <http://cran.r-project.org/bin/macosx/>)
- installer TXM

#### Pour Linux
{: .no_toc }

Lors de l’installation de TXM c’est le R des dépôts qui est installé
(via un apt-get). Pour les versions d’ubuntu antérieures à 12.10, la
version de R est antérieure à la version 2.15.1 qui est requise pour
Iramuteq 0.6 alpha 3 (la version conseillée est la 2.15.2, la 2.15.1
passe).

**Cas 1 : ubuntu antérieur à 12.10**

Le plus simple est d’ajouter à ses dépôts le dépôt de R, cf.
instructions <http://cran.r-project.org/bin/linux/ubuntu/>. Par exemple
pour un ubuntu 12.04 :

- Ouvrir un terminal
- taper : sudo gedit /etc/apt/sources.list
- rajouter la ligne suivante à la fin du fichier :

~~~ text
    deb http://cran.rstudio.com/bin/linux/ubuntu precise/
~~~

- Sauvegarder
- toujours dans le terminal
- taper : sudo apt-key adv –keyserver keyserver.ubuntu.com –recv-keys
  E084DAB9
- taper : sudo apt-get update
- taper : sudo apt-get install r-base

{% include note.html content="voir comment faire pour installer spécifiquement la version
2.15.2 de R. Si on lance une mise à jour générale du système, c’est une
version plus récente qui peut être installée (non vérifiée pour
Iramuteq)." %}

Afin de pouvoir compiler le paquet rgl de R lors de son installation,
vous devez installer quelques librairies de développement. Sous
debian/ubuntu, utilisez la commande suivante :

    sudo apt-get build-dep r-cran-rgl

Ensuite les deux logiciels peuvent être installés selon leurs procédures
respectives, peu importe lequel en premier. Si l’un des logiciels est
déjà installé il n’y a pas besoin de le désinstaller.

Si aucun des deux logiciels n’est encore installé, le plus simple est
d’installer d’abord Iramuteq <http://www.iramuteq.org/telechargement>
puis TXM.

**Cas 2 : ubuntu 12.10 ou supérieur**

Rien de particulier à faire : les deux logiciels peuvent être installés
selon leurs procédures respectives, peu importe lequel en premier.

Si aucun des deux logiciels n’est encore installé, le plus simple est
d’installer d’abord Iramuteq <http://www.iramuteq.org/telechargement>
puis TXM.

**Cas 3 : cas particulier des utilisateurs avancés de R**

Pour les utilisateurs avancés de R qui ne voudraient pas modifier
automatiquement l’installation de R lors de l’installation de TXM, mais
simplement ajouter à la main les paquets requis, alors se reporter aux
[indications pour l'installation de R pour
TXM](http://txm.sourceforge.net/wiki/index.php/Build_the_toolbox_or_the_application#Install_R_yourself).

### Vais-je perdre mes corpus à l’installation de la nouvelle version de TXM&nbsp;?

Depuis TXM 0.7.1, les corpus pré-existants sont transférés
automatiquement dans le nouveau TXM installé.

Remarque : l’installation de TXM ne modifie pas les sources des corpus
qui auraient été importés ou chargés. Vous pouvez toujours (ré-)importer
vos sources dans un nouveau TXM.

### Comment garder les préférences de TXM après installation&nbsp;?

Depuis la version 0.7 de TXM, il est possible de sauvegarder et
recharger les préférences dans et depuis un fichier au format Java
“\*.properties). Procédure : - Lancer TXM - Ouvrir les préférences et
aller dans ”TXM”

1.  Cliquer sur le bouton “Exporter les préférences” et choisir le
    fichier de sauvegarde (1).
2.  Fermer TXM
3.  Installer TXM
4.  Relancer TXM
5.  Ouvrir les préférences et aller dans “TXM”
6.  Cliquer sur le bouton “Importer les préférences” et sélectionner le
    fichier de sauvegarde (1)
7.  Fermer les préférences

# Diagnostic de l’installation et dysfonctionnements<br>

### Comment vérifier que TXM s’est installé correctement&nbsp;?

Quelques indices :

#### TXM 0.8.0 et versions supérieures
{: .no_toc }

1\. Lorsque TXM a fini de démarrer, on doit lire les lignes suivantes
dans la console (zone en bas de la fenêtre) :

    Démarrage de TXM 0.8.0.2586 (2019-08-30 14h42)…
    TXM est prêt.

(Le numéro de la version peut, bien entendu, être différent)

2\. Au moins deux petits cubes figurant des corpus (VOEUX et GRAAL) sont
affichés dans la zone gauche de la fenêtre de TXM.

Dans la version TXM 0.8.0 (mais pas 0.8.1), lorsqu’on sélectionne un de
ces cubes, sa taille s’affiche dans la barre de statut (en bas de la
fenêtre), comme ceci par exemple :

    VOEUX T = 61197

#### TXM 0.7.9
{: .no_toc }

1\. Lorsque TXM a fini de démarrer, on doit lire les lignes suivantes
dans la console (zone en bas de la fenêtre) :

    Démarrage...
    Moteur de recherche lancé en mode mémoire.
    Moteur statistique lancé.connecté.
    Chargement des sous-corpus et des partitions...Terminé.
    Prêt.

2\. Au moins deux petits cubes figurant des corpus (VOEUX et GRAAL) sont
affichés dans la zone gauche de la fenêtre de TXM. Lorsqu’on en
sélectionne un, sa taille s’affiche dans la barre de statut (en bas de
la fenêtre), comme ceci par exemple :

    VOEUX, T = 61102

### Comment vérifier que TreeTagger est bien paramétré dans TXM&nbsp;?

#### Pour un environnement en français
{: .no_toc }

##### TXM 0.8.0
{: .no_toc }

1\. Copier le texte suivant (ou n’importe quel message qui s’affiche
dans la console) :

    Démarrage de TXM 0.8.0.2586 (2019-08-30 14h42)…
    TXM est prêt.

2\. Dans TXM :

1.  éventuellement, choisir dans la page de préférences `Édition > Préférences > TXM > Utilisateur > Import` la langue ‘fr’
    d’étiquetage dans le paramètre “Langue par défaut”
    - cela est nécessaire si l’interface du système d’exploitation
      utilise une langue différente du français
2.  lancer la commande Fichier > Importer > Presse-Papier

3\. Vérifier que dans la console les dernières lignes affichées sont :

    pAttrs : [id, lbn, frpos, frlemma]
    sAttrs : [s:+n, txmcorpus:+lang, text:+id+base+project]
    -- EDITION - Building edition
    001 .
    Import terminé en 14 sec (14746 ms).
    Import terminé.

{% include note.html content="La première ligne ci-dessus doit bien contenir **frpos** et
**frlemma**. En revanche l’indication de durée après “Import terminé”
peut bien sûr être différente." %}

##### TXM 0.7.9
{: .no_toc }

1\. Copier le texte suivant (ou n’importe quel message qui s’affiche
dans la console) :

    Démarrage...
    Moteur de recherche lancé en mode mémoire.
    Moteur statistique lancé.connecté.
    Chargement des sous-corpus et des partitions...Terminé.
    Prêt.

2\. Dans TXM :

1.  éventuellement, choisir dans la page de préférences `Outils > Préférences > TXM > Utilisateur > Import` la langue ‘fr’
    d’étiquetage dans le paramètre “Default language” (Langue par
    défaut)
    - cela est nécessaire si l’interface du système d’exploitation
      utilise une langue différente du français
2.  lancer la commande Fichier > Importer > Presse-Papier

3\. Vérifier que dans la console les dernières lignes affichées sont :

    pAttrs : [id, lbid, frpos, frlemma]
    sAttrs : [text:+name+id+type+path+base+project, s:+n, p:+id, txmcorpus:+lang]
    -- EDITION - Building edition
    .
    Importation terminée : 3 sec (3966 ms)
    Moteur de recherche lancé en mode mémoire.
    Moteur statistique lancé.connecté.
    Chargement des sous-corpus et des partitions...Terminé.
    TXM est prêt.

{% include note.html content="La première ligne ci-dessus doit bien contenir **frpos** et
**frlemma**. En revanche l’indication de durée après “Import terminé”
peut bien sûr être différente." %}

#### Pour un environnement dans n’importe quelle langue
{: .no_toc }

*Solution 1 :*

La procédure ci-dessus peut être transposée : TreeTagger doit disposer
d’un modèle de langue correspondant à la langue de l’environnement, le
texte copié doit être dans cette langue, et dans la console après import
les “fr” de frlemma et frpos sont remplacés par les deux lettres du code
de la langue, par exemple pour un environnement anglophone :

    pAttrs : [id, lbid, frpos, frlemma]

devient

    pAttrs : [id, lbid, enpos, enlemma]

*Solution 2 :*

Vous choisissez et paramétrez la langue que vous allez tester (appelons
la ‘XX’), en unifiant les trois réglages suivants :

1.  modèle de langue pour TreeTagger : télécharger et installer un
    modèle de langue TreeTagger pour la langue sur laquelle vous
    souhaitez faire le test, si ce n‘est pas déjà fait ; le nom du
    fichier est le code de la langue sur deux caractères, comme expliqué
    dans les [instructions pour l'installation de
    TreeTagger](https://txm.gitpages.huma-num.fr/textometrie/InstallTreeTagger)
    (ex. ’en.par’, ‘fr.par’ - dans notre cas ‘XX.par’).
2.  langue choisie pour l‘import presse-papier : saisir le code en deux
    caractères, ex. ’en’, ‘fr’, ‘it’… - ‘XX’ dans notre cas) dans le
    champ de préférence suivant : `Outils > Préférences > TXM > Utilisateur > Import > Clipboard`, Default language (en interface
    anglophone : `Tools > Parameters > TXM > User > Import >
    Clipboard`, Default language).
3.  corpus de test : choisir un (bout de) texte dans la même langue (XX
    donc), que vous copiez puis importez via la commande `Fichier > Importer > Presse-Papier` (en interface anglophone : `File > Import > Clipboard`).

Dans la console après import, vous chercherez alors la ligne :

    pAttrs : [id, lbid, XXpos, XXlemma]

où XX représente le code en deux caractères de la langue choisie.

### Au premier lancement, TXM ne démarre pas, pourquoi&nbsp;?

Il y a deux raisons majeures au fait que TXM ne se lance pas :

**Cas 1** : le logiciel TXM installé correspond à une architecture
différente de celle de la machine

Par exemple typiquement, vous avez un ordinateur 32-bit, et vous avez
téléchargé l’installeur TXM qui vous était proposé par défaut pour votre
système d’exploitation. Comme la majorité des ordinateurs sont
maintenant 64-bit et qu’il n’est pas possible de détecter
automatiquement l’architecture au moment du téléchargement, la version
proposée par défaut est 64-bit. Or cette version n’est pas la bonne pour
votre ordinateur.

Dans ce cas il faut naviguer sur le site de téléchargement de TXM pour
choisir la version qui convient à votre système d’exploitation et à
votre architecture :

- aller dans la partie `Files > Software > TXM` :
  <http://sourceforge.net/projects/txm/files/software/TXM/>
- cliquer sur le dossier de la dernière version stable de TXM
- choisir dans la liste des installeurs celui qui indique à la fois
  votre système d’exploitation et votre architecture, par exemple
  TXM_0.7.5_Win32.exe est l’installeur de TXM 0.7.5 pour windows 32-bit.

**Cas 2** : l’ordinateur n’a pas assez de mémoire pour exécuter TXM

Dans ce cas, deux possibilités :

- a\) ajouter de la mémoire à votre ordinateur ou utiliser un ordinateur
  avec plus de mémoire
- b\) ou bien faire demander moins de mémoire par TXM au démarrage. Voir
  la section suivante :

#### Modifier la taille de la mémoire demandée par TXM au démarrage, en modifiant le fichier de paramètres “launcher.ini” (“TXM.ini” pour TXM 0.8.0 et versions antérieures-)
{: .no_toc }

Suivant la version de TXM et le système d’exploitation, le fichier
launcher.ini se trouve à l’endroit suivant :

**Pour TXM 0.8.1 :**

- Sous Windows :
  - `C:\Users\[USER]\.TXM-0.8.1\launcher.ini`
  - ou bien `C:\Users\[USER]\AppData\Roaming\.TXM\launcherTXM.ini`
- Sous Mac OS X : `/Users/[USER]/.TXM-0.8.1/launcher.ini`
- Sous Linux : `/home/[USER]/.TXM-0.8.1/launcher.ini`

**Pour TXM 0.7.9 et versions antérieures :**

- Sous Windows :
  - `C:\Users\[USER]\TXM\.txm\TXM.ini`
  - ou bien `C:\Users\[USER]\AppData\Roaming\TXM\.txm\TXM.ini`
- Sous Windows XP : `C:\Documents and Settings\[USER]\Application Data\.txm`
- Sous Mac OS X : `/Users/[USER]/TXM/.txm/TXM.ini`
- Sous Linux : `/home/[USER]/TXM/.txm/TXM.ini`

Ouvrir le fichier avec un [éditeur de
texte](#quels-logiciels-utiliser-pour-éditer-des-documents-au-format-texte-brut-txt)

et remplacer les lignes suivantes :
~~~ text
    .txm
    -vmargs
    -Xms128m
    -Xmx512m
~~~
par (pour avoir entre 500Mo et 1Go de mémoire)
~~~ text
    .txm
    -vmargs
    -Xms512m
    -Xmx1024m
~~~
ou bien (pour avoir entre 5Go et 10Go de mémoire)
~~~ text
    .txm
    -vmargs
    -Xms5g
    -Xmx10g
~~~
<del>**Cas 3** : la version de TXM utilisée correspond bien à celle de
la machine, mais c’est la version du Java installé qui ne correspond pas
à celle de TXM. Ceci ne concerne que les versions antérieures à TXM
0.7.7[^2].

Par exemple typiquement sous Windows, votre machine est 64-bit, mais
vous utilisez un Java 32-bit : cela est possible car un Java 32-bit peut
tourner sur une machine 64-bit. Dans ce cas le Java 32-bit ne sera pas
compatible avec un TXM 64-bit.

Les configurations compatibles sont les suivantes :

- machine 64-bit -\> TXM 64-bit + Java 64-bit
- machine 32-bit -\> TXM 32-bit + Java 32-bit</del>

### J’ai installé Java sur ma machine mais TXM ne démarre pas et m’indique une erreur à propos de Java&nbsp;?

Ceci ne concerne que les versions antérieures à TXM 0.7.7[^3].

<del>Suivant la situation, il peut y avoir différents messages
d’erreur :

- “No Java virtual machine found…”
- “Java was started but returned exit code=13…”  

![java-error.png](https://groupes.renater.fr/wiki/txm-users/_detail/public/java-error.png)

- Aucun message

Le Java que vous avez installé ne correspond pas à l’architecture de
votre TXM (32-bit ou 64-bit).

Les configurations compatibles sont les suivantes :

- machine 64-bit -\> TXM 64-bit + Java 64-bit
- machine 32-bit -\> TXM 32-bit + Java 32-bit

En particulier sous Windows, le site de téléchargement de Java peut vous
installer la mauvaise architecture par défaut.

Il faut alors télécharger Java en choisissant manuellement
l’architecture (32-bit ou 64-bit) correspondant à votre TXM depuis ce
site puis l’installer : <http://www.java.com/fr/download/manual.jsp>

Voir les instructions détaillées d’installation pour Windows ici :
<http://www.java.com/en/download/help/windows_offline_download.xml>

\[Remarque concernant la [sécurité de la version 7 du Java
Oracle](http://www.developpez.com/actu/50844/Faille-de-securite-critique-dans-Java-7-activement-exploitee-pouvant-etre-utilisee-pour-installer-des-malwares)
: pour cette version il est recommandé de ne pas activer l’exécution des
Applets Java ainsi que le téléchargement et l’exécution Java JNLP dans
vos navigateurs web. TXM n’est pas concerné par cette recommandation
dans la mesure où il n’utilise pas ces mécanismes.\] </del>

### J’ai vérifié que l’architecture de Java est la bonne mais TXM ne se lance toujours pas&nbsp;?

Ceci ne concerne que les versions antérieures à TXM 0.7.7[^4].

<del> **En dernier recours**, si vous avez déjà appliqué l’intégralité
des instructions des entrées de FAQ suivantes **sans succès** :</del>

<del>
- [Au premier lancement, TXM ne démarre pas, pourquoi
  ?](#au-premier-lancement-txm-ne-d%C3%A9marre-pas-pourquoi)
-  [J'ai installé Java sur ma machine mais TXM ne démarre pas et
  m'indique une erreur à propos de Java
  ?](#jai-install%C3%A9-java-sur-ma-machine-mais-txm-ne-d%C3%A9marre-pas-et-mindique-une-erreur-%C3%A0-propos-de-java)
</del>

<del>Il est possible de déposer manuellement une machine virtuelle Java (JRE
en faisant attention à l’architecture 32-bit ou 64-bit) dans le dossier
d’installation de TXM (c’est ici que TXM cherchera en premier une JRE).</del>

<del>Marche à suivre :</del>

<del>
1.  Sous Windows, si vous avez installé une JRE sans option
    particulière, alors il faut copier le dossier : “C:\Program
    Files\Java\jre7”
2.  Dans le dossier d’installation de TXM (sans option particulière)
    dans : “C:\Program Files\TXM\TXM”
3.  Et enfin dans le dossier d’installation de TXM, il faut renommer le
    dossier “jre7” en “jre”
</del>

<del>
{% include important.html content="N’hésitez pas à vous faire accompagner pour cette
manipulation très technique." %}
</del>

# Mises à jour<br>

### Ma mise à jour ne se télécharge pas

1.  Lancer TXM et tenter la mise à jour classique (s’assurer d’être connecté à Internet avant de lancer TXM) ;
2.  Tenter de télécharger la mise à jour manuellement aux adresses :
  Mises à jour : [TXM 0.8.2](https://gitlab.huma-num.fr/txm/txm-software/-/archive/master/txm-software-master.zip?path=main/0.8.2/ext/stable), [TXM 0.8.3](https://gitlab.huma-num.fr/txm/txm-software/-/archive/master/txm-software-master.zip?path=main/0.8.3/ext/stable), [TXM 0.8.4](https://gitlab.huma-num.fr/txm/txm-software/-/archive/master/txm-software-master.zip?path=main/0.8.4/ext/stable)
  Extensions : [TXM 0.8.2](https://gitlab.huma-num.fr/txm/txm-software/-/archive/master/txm-software-master.zip?path=dist/0.8.2/ext/stable), [TXM 0.8.3](https://gitlab.huma-num.fr/txm/txm-software/-/archive/master/txm-software-master.zip?path=dist/0.8.3/ext/stable), [TXM 0.8.4](https://gitlab.huma-num.fr/txm/txm-software/-/archive/master/txm-software-master.zip?path=dist/0.8.4/ext/stable)
3.  Si le téléchargement a échoué, récupérer “txm-software-master.zip” par un autre moyen (clé USB, partage réseau…) ;
4.  Dans TXM, sélectionner l’entrée de menu `Fichier > ajouter une extension tierce` ;
5.  Sélectionner le bouton “Ajouter…”, puis le bouton “Archive…” de la fenêtre ouverte ;
6.  Dans la fenêtre de sélection de fichier ouverte, sélectionner le fichier “txm-software-master.zip” ;
7.  Si rien ne s’affiche, s’assurer que “Grouper les éléments par catégorie” n’est pas sélectionné ;
8.  Cliquer sur le bouton “Sélectionner tout”, puis le bouton “Finish”, “puis le bouton ”Next\>” ;
9.  Accepter les licences, puis terminer l’installation avec le bouton “Finish” ;
10. Valider les étapes suivantes puis finaliser l’installation en redémarrant TXM.

### TXM n’arrive pas à télécharger une mise à jour ou une extension, pourtant je suis bien connecté à Internet

**Solution 1**

Télécharger les fichiers de l'extension en dehors de TXM puis installer l'extension dans TXM à l'aide des fichiers téléchargés.

Procédure pour installer l'extension 'Media Player' dans TXM 0.8.3 (adapter l'adresse de téléchargement pour une autre version) :

1. Télécharger et extraire depuis l'archive .zip les fichiers de l'extension depuis l'entrepôt `https://gitlab.huma-num.fr/txm/txm-software/-/archive/master/txm-software-master.zip?path=dist/0.8.3/ext/stable`
2. Dans TXM, lancer la commande `Fichier > Ajouter une extension tierce`
3. Dans le formulaire qui s'ouvre, indiquer dans le champ du haut 'Work with...' le chemin vers le dossier "stable" extrait de l'archive (il faut descendre dans `dist/0.8.3/ext`) :<br/>
![available software form](../images/available-software-form.png){: width="800px"}
4. Sélectionner la ligne "MediaPlayer" : <br/>
![select media player](../images/select-media-player.png){: width="800px"}
5. Lancer l'installation avec le bouton "[OK]"

**Solution 2**

Il se peut que les nouveaux réglages d’accès à l’Internet de TXM[^1]
soient trop restrictifs pour votre connexion (par exemple un WiFi de
qualité moyenne) et que le message d’erreur suivant s’affiche :

    Unable to read repository at ...
    Timeout while reading input stream.
    The following system properties can be used to adjust the readTimeout, retryAttempts, and closeTimeout
        org.eclipse.ecf.provider.filetransfer.retrieve.readTimeout=<default:1000>
        org.eclipse.ecf.provider.filetransfer.retrieve.retryAttempts=<default:30>
        org.eclipse.ecf.provider.filetransfer.retrieve.closeTimeout=<default:1000>

Pour relâcher un peu de contraintes sur ces réglages, vous pouvez :

1.  S’assurer d’être connecté à Internet avant de lancer TXM ;
2.  Éditer le fichier \$HOME/TXM/.txm/TXM.ini (\$HOME votre répertoire
    utilisateur différent pour chaque système d’exploitation) ;
3.  Retirer les lignes suivantes :
        -Dorg.eclipse.ecf.provider.filetransfer.retrieve.connectTimeout=3000
        -Dorg.eclipse.ecf.provider.filetransfer.retrieve.retryAttempts=2
4.  Redémarrer TXM.

# Windows<br>

### L’installateur de TXM ne se lance pas et affiche une erreur

En cas de message d’erreur :

    Nsis Error » : installer integrity check has failed. Common causes in clude incomplete dowledand damaged media

Il faut vérifier si la taille de l’installateur est bien celle indiquée
sur la page de téléchargement ([version
0.7.5](http://sourceforge.net/projects/txm/files/software/TXM/0.7.5/)).
Si elle est différente alors, il faut re-télécharger l’installateur.

### Connexion au moteur de recherche : le moteur de recherche n’arrive pas à se lancer&nbsp;?

Depuis la version 0.7, par défaut, TXM lance le moteur de recherche en
mode mémoire. Ce mode présente l’avantage d’être plus rapide que le mode
réseau, le mode des versions antérieures de TXM.

En mode réseau, si le moteur ne peut pas se lancer alors le message
suivant apparaît dans la console :

- “Failed to load CQP lib with exception”

Les raisons de l’échec sont alors affichées dans la suite des messages
de la console. Veuillez nous contacter sur la liste de diffusion dans ce
cas.

En attendant, que l’on puisse vous aider, vous pouvez utiliser le mode
réseau en l’activant par la manipulation suivante :

- Ouvrir les préférences de TXM
- Aller dans `TXM > Avancé > Moteur de recherche`
- Cocher “Utiliser le protocole réseau” (EN : “Use network protocol”)

Si le moteur réseau n’arrive nan lui non plus à se lancer alors dans la
console vous obtenez l’un des messages suivants :

- FR : “Échec de la connexion au moteur de recherche…” ou “La connexion
  au moteur de recherche a échoué…”
- EN : “Error while connecting to search engine”

En mode réseau, il faut tout d’abord vérifier que les préférences du
moteur de recherche soient bonnes :

- “Chemin vers le fichier exécutable ‘cqpserver’” doit pointer :
  - sous Linux vers : “/usr/lib/TXM/cwb/bin/cqpserver”
  - sous Windows vers : “\$INSTALLDIR/cwb/bin/cqpserver” ; \$INSTALLDIR
    vaut généralement “C:\Program Files\TXM”
  - sous Mac OS X vers : “/Applications/TXM/cwb/bin/cqpserver”
- “Chemin vers le répertoire de Registre” doit pointer vers
  \$HOME/TXM/registry
  - sous Linux \$HOME vaut : “/home/\<user\>/TXM”
  - sous Windows \$HOME vaut : “C:\Documents and settings\\user\>\TXM”
    sous XP, “C:\Utilisateurs\\user\>\TXM” sous Vista et Seven
  - sous Mac OS X \$HOME vaut : “/Users/\<user\>/TXM”
- “Chemin vers le fichier d’initialisation” :
  - sous Linux vers : “/usr/lib/TXM/cwb/cqpserver.init”
  - sous Windows vers : “\$INSTALLDIR/cwb/cqpserver.init” ; \$INSTALLDIR
    vaut généralement “C:\Program Files\TXM”
  - sous Mac OS X vers : “/Applications/TXM/cwb/cqpserver.init”

Si après redémarrage, le serveur ne fonctionne toujours pas. Alors il
est fort probable que le programme “cqpserver” n’arrive pas à se lancer.
Dans les messages d’erreur de la console, se trouve la ligne de commande
de lancement de cqpserver. Il faut la copier dans un Terminal et appuyer
sur la touche Entrée. Si le programme se lance correctement, essayez de
relancer TXM. Sinon il y a plusieurs problèmes connus :

- \[LINUX\] cqpserver n’arrive pas a trouver la librairie
  “libreadline.so”. Il faut :
  - Ouvrir un Terminal
  - Executer la commande suivante : sudo ln -s /lib/libreadline.so.6
    /lib/libreadline.so
- \[LINUX\] cqpserver ne se lance pas du tout avec un message “fichier
  binaire impossible à lancer”. Le binaire n’est pas prévu pour votre
  architecture. Il faut installer le TXM approprié à votre architecture

Relancer TXM. Si le serveur ne fonctionne toujours pas. Il faut nous
contacter sur la liste de diffusion.

### Sous windows, TXM peut-il être installé avec un compte qui n’a pas les droits administrateur&nbsp;?

L’installation de TXM doit être faite avec un compte qui a des droits
d’administrateur. C’est en général le cas des comptes utilisés sur les
machines à usage personnel ou familial. Le plus simple, en l’état actuel
de TXM, est ensuite d’utiliser TXM avec le même compte.

La question se rencontre surtout sur des ordinateurs partagés gérés par
un service, par exemple les machines d’une salle de cours. Dans ce cas,
on peut faire en sorte que le programme TXM soit disponible pour tous
les utilisateurs de l’ordinateur en copiant le dossier \$HOMEDIR/Menu
Démarrer/Programmes/TXM dans \$ALLUSERHOMEDIR/Menu
Démarrer/Programmes/TXM (\$HOMEDIR représente le chemin vers le
répertoire du compte administrateur qui a installé TXM, et
\$ALLUSERHOMEDIR le chemin vers le répertoire utilisateur “All users” ;
les chemins peuvent être un peu différents selon la version de windows).

### TXM ne se lance pas et affiche un message d’erreur “Windows ne peut pas ouvrir ce fichier…”

Windows 7 n’arrive pas a exécuter le script de lancement de TXM
C:\Program files\TXM\TXM.vbs". Il faut indiquer à Windows avec quel
programme il doit ouvrir ce fichier.

Pour cela il faut :

- Aller dans le dossier d’installation de TXM : C:\Program files\TXM
- Faire clic droit sur le fichier C:\Program files\TXM\TXM.vbs"
- Sélectionner “Ouvrir avec…” ou “Ouvrir” si le premier est absent
- une fenêtre “Ouvrir avec” pour choisir quel programme associer au type
  de fichier “.vbs”. Il faut alors :
  - sélectionner dans la liste des programmes affichés : “Microsoft
    Windows Based Script Host”
  - si “Microsoft Windows Based Script Host” est absent, il faut cliquer
    sur “Parcourir” et sélectionner le programme CScript.exe qui se
    trouve dans le dossier “C:\Windows\system32” (Windows XP)

### Sous Windows, je n’arrive pas à lancer TXM

La façon de lancer TXM dépend de la version de Windows :

- **Windows XP**, **Windows Vista** et **Windows 7** : ouvrir le menu
  Démarrer en bas à gauche `“Tous les programmes” ou “Programmes” > TXM > TXM`
- **Windows 8** sans menu Démarrer
  1.  touche Windows pour aller à l’écran d’accueil Windows
  2.  faire un clic droit de la souris : un bandeau s’affiche alors en
      bas de l’écran
  3.  dans le bandeau, sélectionner “Toutes les applications” : un
      nouvel écran s’affiche présentant toutes les icones des
      applications installées dont celle de TXM
  4.  cliquer sur l’icone de TXM
- **Windows 8** avec menu Démarrer et **Windows 10** : ouvrir le menu
  Démarrer en bas à gauche `“Toutes les applications installées” > TXM > TXM`

### Sous Windows 8, je ne trouve pas TXM sur l’écran d’accueil

L’installeur Windows de TXM ne fait pas encore en sorte que TXM soit
affiché sur l’écran d’accueil de Windows 8 après l’installation.

En attendant que cela se fasse automatiquement, voici la procédure
manuelle à suivre pour que l’installation soit complète :

1.  Aller sur l’écran d’accueil Windows (touche Windows)
2.  Faire un clic droit de la souris : un bandeau s’affiche alors en bas
    de l’écran
3.  Dans le bandeau, sélectionner “Toutes les applications” : un nouvel
    écran s’affiche présentant toutes les icones des applications
    installées dont celle de TXM.
4.  Faire un clic droit sur l’icone de TXM pour refaire apparaître le
    bandeau
5.  Dans le bandeau, sélectionner “Épingler à l’écran d’accueil”

Voilà c’est fini, TXM devrait à présent se trouver sur l’écran d’accueil
de Windows en glissant vers la droite.

### Sous Windows, je ne trouve pas TXM dans le menu Démarrer

TXM ne s’installe dans les menus de Windows que pour l’utilisateur qui
installe TXM. Concrètement, si vous installez TXM à l’aide d’un compte
admin différent de votre compte, alors TXM ne sera visible que dans les
menus du compte admin.

Dans ce cas, voici deux moyens de lancer TXM depuis votre compte
utilisateur :

- Créer un raccourcis vers le fichier de lancement de TXM :
  \$PROGRAMFILES\TXM\TXM.bat
- Voir la section [F. Rendre disponible TXM dans le menu Démarrer pour
  les autres
  utilisateurs](https://groupes.renater.fr/wiki/txm-users/public/installer_txm_dans_une_salle_de_cours_windows#f_rendre_disponible_txm_dans_le_menu_demarrer_pour_les_autres_utilisateurs)

### Sous Windows, TXM 0.7 (et versions ultérieures) aucune requête CQL ne fonctionne sur aucun corpus et message “VOEUX is not ready”

Il s’agit sûrement d’un bug non-résolu ([bug
\#871](http://forge.cbp.ens-lyon.fr/redmine/issues/871)) depuis que le
moteur CQP a été intégré à TXM sous forme de librairie qui empêche le
moteur de recherche (CQP) de fonctionner quand le chemin du dossier
utilisateur contient des caractères accentués ou spéciaux. Le bug se
manifeste de la façon suivante :

- un moteur de recherche qui démarre sans problème
- une concordance affiche l’erreur suivante : “Couldn’t open directory
  C:\Users\Gaëlle\TXM\registry (continuing)” (l’idenfiant utilisateur
  contient un caractère accentué) ou bien “Couldn‘t open directory
  C:\Documents and Settings\Gaëlle\TXM\registry (continuing)”
- ou bien
  la sélection de l’icone d’un corpus provoque l’affichage du message de
  status ’VOEUX is not ready’
- ou bien un module d’import échoue avec
  le message ”Error: The registry file was not created: C:\Users\\”

Il y a 3 façons différentes de contourner le problème :

- A\) utiliser un nom d‘utilisateur sans accents pour installer et
  utiliser TXM
- B) déplacer le dossier de travail utilisateur de TXM
  en dehors du répertoire utilisateur et recharger/importer à nouveau
  les corpus :
- Ouvrir les préférences de TXM à la page : `TXM > Avancé`
- Dans un explorateur de fichiers, déplacer le répertoire
  indiqué par le champ “Dossier utilisateur TXM” dans un nouveau
  répertoire “C:\gaelle_txm” (par exemple, l’important est qu’il n’y ait
  pas de caractères accentués ni d’espaces dans le chemin)
- Procédure
  sous Windows 7 :
- ouvrir l’Explorateur de fichiers Windows \*
  naviguer jusque dans `C:\Users\Gaëlle`
  - clic droit sur le dossier 'TXM',  puis commande 'Couper' dans le menu
  - naviguer jusque dans 'C:'
  - créer un nouveau répertoire 'gaelle_txm'
  - naviguer jusque dans 'C:\gaelle_txm'
  - clic droit, puis commande 'Coller' -> le dossier se déplace de `C:\Users\Gaëlle' vers 'C:\gaelle_txm`
- Mettre à jour la préférence `TXM > Avancé > Dossier utilisateur TXM` à `C:\gaelle_txm\TXM`
- Mettre à jour la préférence `TXM > Avancé > Moteur de Corpus > Chemin vers le répertoire de Registre` à `C:\gaelle_txm\TXM\registry` (si la préférence est présente dans votre TXM)
- Relancer TXM
  - **Re-charger ou ré-importer les corpus**
- C) changer le mode de communication entre TXM et le moteur de recherche CQP (cette solution a pour inconvénient que TXM est plus lent à récupérer les résultats de CQP)
  - Ouvrir les préférences de TXM à la page : `TXM > Avancé > Moteur de recherche`
  - Sélectionner "Utiliser le protocole réseau"
  - Redémarrer les moteurs avec `Fichier > Redémarrer les moteurs`

La méthode A) est la plus simple si on peut changer de login, la méthode
B) est la plus sûre si on souhaite garder son login et la méthode C) est
la plus simple mais va ralentir votre TXM.

### Sous Windows, TXM 0.7 (et versions antérieures) n’arrive pas à lancer le moteur de statistique R

{% include note.html content="Depuis TXM 0.7.7, TXM utilise son propre Java, il n’est donc pas concerné par cette entrée de FAQ" %}

Suite à la mise à jour de Java (version Windows 1.7.21 Oracle du 16
Avril 2013), les versions 0.7 de TXM et antérieures ne peuvent plus
lancer le moteur de statistique R.

Pour résoudre ce bug, 2 solutions sont disponibles :

- Installer TXM à la racine d’un disque dur, ou dans un chemin sans
  espace. Par exemple “C:\TXM”.
- OU Déplacer et changer le chemin d’accès du moteur de statistique R
  pour qu’il ne contienne pas d’espace. Par exemple “C:/R-TXM”. Cette
  solution est moins pratique car TXM ne désinstallera pas R en cas de
  mise à jour de TXM s’il n’est plus dans le répertoire d’installation
  de TXM. Il vaut donc mieux privilégier la 1ere solution.

Si le problème persiste après réinstallation, veuillez contacter la
liste de diffusion.

### Sous Windows, TXM 0.7.7 n’arrive pas à lancer le moteur de statistique R

Vous obtenez le message d’erreur suivant :

    The Statistics Engine program path is set but we couldn't find it :
    C:\TXM\R\bin\x64\Rterm.exe

Nous l’avons rencontré dans deux situations différentes :

- un anti-virus bloque l’accès à l’exécution ou à la communication
  avec R. Une façon de résoudre le problème a été de désinstaller ou de
  reconfigurer des anti-virus redondants ou en conflits, par exemple
  Windows Defender ;
- le compte utilisateur Windows exécutant TXM n’a arrive pas à accéder
  ou n’a pas les droits d’exécution sur le fichier Rterm.exe. Une façon
  de vérifier cela est de lancer avec le même compte utilisateur
  directement Rterm.exe à partir d’une console cmd.exe. Si Rterm.exe
  s’exécute normalement, le problème vient d’ailleurs. Si vous obtenez
  des messages d’erreur comme ci-dessous :
  
  ~~~ text 
      PS C:\Program Files> "C:\Program Files\TXM\R\bin\x64\Rterm.exe"
      Warning message:
      In normalizePath(path.expand(path), winslash, mustWork) :
        path[1]="C:/Program Files/TXM/R/library": Access denied

      R version 3.1.2 (2014-10-31) -- "Pumpkin Helmet"
      Copyright (C) 2014 The R Foundation for Statistical Computing
      Platform: x86_64-w64-mingw32/x64 (64-bit)

      R is free software and comes with ABSOLUTELY NO WARRANTY.
      You are welcome to redistribute it under certain conditions.
      Type 'license()' or 'licence()' for distribution details.

      R is a collaborative project with many contributors.
      Type 'contributors()' for more information and
      'citation()' on how to cite R or R packages in publications.

      Type 'demo()' for some demos, 'help()' for on-line help, or
      'help.start()' for an HTML browser interface to help.
      Type 'q()' to quit R.

      Warning message:
      package "methods" in options("defaultPackages") was not found
      During startup - Warning messages:
      1: package 'datasets' in options("defaultPackages") was not found
      2: package 'utils' in options("defaultPackages") was not found
      3: package 'grDevices' in options("defaultPackages") was not found
      4: package 'graphics' in options("defaultPackages") was not found
      5: package 'stats' in options("defaultPackages") was not found
      6: package 'methods' in options("defaultPackages") was not found
      >
~~~

vous n’avez pas suffisamment de droits d’accès au répertoire C:/Program
Files/TXM/R/library. Dans ce cas donner vous plus de droits d’accès sur
ce répertoire, par exemple en suivant les procédures de la page [File
and Folder
Permissions](https://msdn.microsoft.com/en-us/library/bb727008.aspx).

### Sous Windows, TXM 0.7.9 le moteur de statistique R est démarré mais les commandes statistiques ne fonctionnent pas

Bien que R semble démarrer correctement, les commandes de statistiques
ne fonctionnent pas. Si votre répertoire utilisateur `C:/Users/<mon répertoire>`
contient des accents, R n’arrive pas à finaliser son
initialisation.

Pour résoudre ce bug, il faut alors déplacer le répertoire de travail de
TXM en suivant les instructions de résolution de la section [Sous
Windows, TXM 0.7 (et versions ultérieures) aucune requête CQL ne
fonctionne sur aucun corpus et message "VOEUX is not
ready"](#sous-windows-txm-07-et-versions-ultérieures-aucune-requête-cql-ne-fonctionne-sur-aucun-corpus-et-message-voeux-is-not-ready).

# Mac<br>

### TXM tourne-t-il sur (tous les) Mac&nbsp;?

Depuis la version 0.6, TXM dispose d’un installeur pour Mac, il
s’installe comme un logiciel Mac courant (la procédure est documentée
dans le [manuel TXM
0.7](http://textometrie.ens-lyon.fr/files/documentation/Manuel%20de%20TXM%200.7%20FR.pdf)).

#### Prérequis matériel et système
{: .no_toc }

Pour connaître la version du système de son Mac, on peut afficher les
Informations Système à partir du menu Pomme `À propos de ce Mac”.

À partir de TXM 0.7.7, il n’est plus nécessaire d’installer Java sur son
Mac au préalable (TXM s’installe avec son propre Java).

Avant TXM 0.7.7, il faut installer Java sur son Mac avant l’installation
de TXM :

- Télécharger Java : [page de téléchargement
  officielle](http://www.java.com/fr/download/manual.jsp)
- Suivre les instructions suivantes : [instructions
  officielles](http://www.java.com/fr/download/help/mac_install.xml)

##### Mac OS X 10.5 (Leopard) et antérieur
{: .no_toc }

TXM ne fonctionne que sur certains Mac OS X 10.5 (Leopard) :

- il faut que le processeur soit de type “Intel” : cela exclut notamment
  les PowerPC. Pour connaître son processeur, consulter menu Pomme `“A
  propos de ce mac”, dans la ligne “Processeur” s’il apparaît la mention
  “intel” c’est bon pour TXM, sinon ce n’est pas bon (incompatibilité
  matérielle).
- si vous avez bien un processeur intel, il faut également mettre à jour
  Java à la version 1.6 (cf. [instructions du
  constructeur](http://support.apple.com/kb/DL1359?viewlocale=fr_FR&locale=fr_FR)).

TXM ne fonctionne pas sur les versions antérieures à Mac OS X 10.5.

##### Mac OS X 10.6 (Snow Leopard-)
{: .no_toc }

TXM fonctionne sur les Mac à partir de la version Mac OS X 10.6 (Snow
Leopard).

##### Mac OS X 10.7 (Lion) et ultérieur
{: .no_toc }

Si vous utilisez un Mac de version 10.7 (Lion) ou ultérieure, il faut
modifier les paramètres de “sécurité” pour pouvoir installer
correctement TXM (si on ne fait pas cette modification, le problème ne
se manifeste pas au moment de l’installation, mais au premier
lancement). Pour ce faire, il faut aller dans `Préférences systèmes > Personnel > Sécurité` et autoriser les applications téléchargées depuis
Internet.

(Il peut être nécessaire de déverrouiller au préalable les préférences
en cliquant sur l’icone de cadenas en bas à gauche)

Plus d’informations ici :
<http://irz.fr/provient-developpeur-non-identifie/>

et ici : <https://support.apple.com/fr-fr/HT202491>

### L’installeur de TXM ouvre une fenêtre pour installer Java mais l’installation n’aboutit pas même après avoir installé Java

{% include note.html content="Depuis TXM 0.7.7, il n’est plus nécessaire d’installer
Java. Cette entrée de FAQ n’est donc plus pertinente pour cette version
et les versions ultérieures." %}

<del>Depuis Mac OS X 10.10, l’installation de Java classique
n’installe pas tous les outils Java dont les versions précédentes de TXM
0.7.7 avaient besoin pour se lancer.

Résolution : Pour installer Java pour une version précédente de TXM
0.7.7, lors de l’ouverture de la fenêtre indiquant qu’il faut installer
Java, au lieu de cliquer sur le bouton “OK”, il faut cliquer sur le
bouton “Plus d’informations” qui ouvre une page Web permettant
d’installer une version plus ancienne de Java :
<https://support.apple.com/kb/DL1572?viewlocale=en_US&locale=en_US>.

Après avoir installé cette nouvelle version Java, TXM pourra s’installer
et se lancer correctement. </del>

### Comment voir les messages d’installation de l’installeur Mac OS X&nbsp;?

Voici la procédure pour afficher les messages d’installation de TXM sur
Mac :

1.  Lancer l’installeur Mac OS X (TXM_X.Y.Z_MacOSX.pkg)
2.  Ouvrir la fenêtre des messages :
    - à l’aide du raccourci **CMD - L** (ou Pomme-L)
    - ou à l’aide du menu “Fenêtre / Historique de Programme
      d’installation” (“Window / Installer log” en anglais)
3.  Sélectionner “Afficher tout l’historique” (“Show all logs” en
    anglais) dans la liste déroulante de la fenêtre qui s’est ouverte
    sous celle de l’installeur
4.  Une fois l’installation terminée ou interrompue, les messages
    d’installation peuvent être sauvegardés dans un fichier texte à
    l’aide du bouton “Enregistrer” à droite
5.  On peut alors envoyer ce fichier à un correspondant

Remarque : La fenêtre de messages d’installation peut être ouverte à
n’importe quel moment de l’installation, aucun message ne sera perdu.

### Installation Mac OS X le moteur de statistique R ne démarre pas

Pour ce message : “Impossible de démarrer RServe :
org.txm.stat.engine.r.RWorkspaceException: Le chemin du programme du
serveur statistique a été renseigné mais sa recherche a échoué :
XXXXXXXXXX. StatEngine is not ready. Canceling command.”

Il peut y avoir plusieurs raisons :

- a\) Si le R embarqué dans TXM 0.7.8 (depuis novembre 2017) ne
  fonctionne pas correctement. Dans ce cas, en attendant la résolution
  du bug dans un nouvel installateur de TXM, il faut :
  - installer sur le Mac un R indépendant de TXM
  - installer dans ce R les packages utilisés par TXM en exécutant les
    lignes de code R de la section ci-dessous (faire un copier/coller
    dans la console R)
  - indiquer dans les préférences de TXM `TXM > Avancé > Moteur de statistique > Chemin vers l’exécutable de R` le chemin vers le R
    installé sur le Mac. Par exemple
    /Applications/R.app/Contents/Macos/R
  - relancer TXM
- b\) Si on met à jour R indépendamment de l’installation de TXM
  (versions antérieures à 0.7.8 novembre 2017), par exemple pour un
  usage de R indépendant de TXM, les packages R installés par TXM sont
  supprimés. Il faut les réinstaller en exécutant les lignes de code R
  de la section ci-dessous
- c\) Lors du premier lancement de TXM, le firewall de Mac OS X lance
  une alerte pour ajouter une exception pour RServe, il faut accepter
  sinon TXM ne pourra pas communiquer avec le moteur de statistique.
- d\) Si lors de l’installation de TXM (versions antérieures à TXM
  0.7.5), Java a été installé en même temps, un bug dans l’installeur a
  empêché TXM d’installer les librairies de R nécessaires. Si c’est le
  cas, il suffit de réinstaller TXM.

### Installation de TXM sur Mac OS X impossible ou message d’erreur “there is no package called ‘textometrieR’”

Dans les versions précédentes de TXM, le fichier “~/.Rprofile” était
créé pour charger au démarrage de TXM les package R dont il allait avoir
besoin.

À cette époque, lorsque l’installation de TXM était interrompue et que
les packages R n’avaient pas pu être installés, les tentatives suivantes
d’installation échouaient et provoquaient ce message d’erreur.

Certaines machines sur lesquelles l’installation de TXM ne fonctionne
pas peuvent être dans ce cas de figure.

Dans cette situation, pour pouvoir installer TXM à nouveau, il faut tout
simplement supprimer le fichier `/Users/<mon identifiant>/.Rprofile`.

**A.** Cela peut se faire par le biais du Finder, sachant que les
fichiers dont le nom commence par un point ‘.’, comme celui-ci, ne sont
pas affichés par défaut (on les appelle les fichiers cachés). Pour
visualiser les fichiers cachés dans le Finder, il existe plusieurs
moyens :

- chercher le fichier “.Rprofile”
  - faire “CMD - F”
  - dans la fenêtre ouverte, sélectionner le type de recherche “Fichiers
    invisibles” (à trouver dans la liste déroulante, catégorie
    “Autres…”) et sélectionner le critère “éléments invisibles”
  - dans le champ de rechercher mettre “.Rprofile”  
    ![capture_d_ecran_2014-10-16_a_17.44.42.png](https://groupes.renater.fr/wiki/txm-users/public/capture_d_ecran_2014-10-16_a_17.44.42.png)
- utiliser Onyx pour configurer l’affichage du Finder
- lancer la commande “defaults write com.apple.finder AppleShowAllFiles
  1” dans le Terminal

**B.** Cela peut également se faire par une ligne de commande dans une
fenêtre de Terminal :

      rm ~/.Rprofile

**C.** Si on ne souhaite pas supprimer ce fichier, on peut se contenter
de supprimer la ligne suivante dans le fichier “~/.Rprofile” :

      library(textometrieR)

Il faut ensuite bien sûr ré-installer TXM.

### Le menu principal ne s’affiche pas sous Mac OS X 10.9 (Maverick-)

Sous OS X 10.9 (Maverick), le menu principale de Finder est affiché (et
inopérant) à la place de celui de TXM.

Pour afficher le menu de TXM, il faut cliquer sur une autre application
que TXM puis cliquer à nouveau sur TXM.

# Linux<br>

### L’installation avec la Logithèque Ubuntu ne semble pas aboutir&nbsp;?

Sous un Ubuntu récent, quand on double-clique sur le fichier
d‘installation de TXM (.deb), cela lance la Logithèque. L’installation
avec la Logithèque ne fonctionne qu’**à partir de la version 0.7 de
TXM** (pour les versions antérieures il faut installer GDebi puis lancer
l’installation depuis GDebi).

De plus, en l’état actuel, la logithèque Ubuntu n’a pas l’information
comme quoi TXM est installé ou non. Cela a au moins deux conséquences :

- si TXM est déjà installé sur la machine, on n’a aucun message
  d’avertissement au moment d’une nouvelle installation de TXM (le
  nouveau TXM est toujours installé, et il remplace l’ancien la cas
  échéant) ;
- à la fin de l’installation (qd les petites flèches en haut de la
  fenêtre ont fini de tourner), la Logithèque continue d’afficher un
  bouton “Installer” (au lieu d’un bouton “Supprimer”). On peut
  cependant quitter la logithèque et lancer TXM, qui est prêt à
  fonctionner.

Autrement dit, l’installation aboutit bien (pour une version 0.7 ou
supérieure de TXM), même si cela n’apparaît pas clairement. (Mais au cas
où, l’installation avec GDebi reste possible en cas de souci avec la
logithèque.)

### L’installation de TXM sur Ubuntu a échoué et maintenant je n’arrive plus à rien installer !

Le gestionnaire de paquet essaye de réinstaller TXM en boucle, mais
cette installation n’aboutit pas. On va lui indiquer de ne plus le faire
avec la commande suivante qui va desinstaller TXM :
~~~ bash
     dpkg --purge txm
~~~
Si alors même la désinstallation de TXM ne fonctionne pas, il suffira de
supprimer les fichiers “txm.\*” du dossier “/var/lib/dpkg/info”
(commande = rm -rf /var/lib/dpkg/info/txm.\*“) et de relancer la
commande ”dpkg –purge txm” (ce dossier contient les scripts
d’installations et desinstallation de TXM)

### Comment savoir quelles versions de TXM sont installées sur ma machine&nbsp;?

Dans un terminal (ouvert par Ctrl-Alt-T), lancer :
~~~ bash
     dpkg-query -l '*txm*'
~~~
Exemple de résultat :
~~~ text
    Souhait=inconnU/Installé/suppRimé/Purgé/H=à garder
    | État=Non/Installé/fichier-Config/dépaqUeté/échec-conFig/H=semi-installé/W=attend-traitement-déclenchements
    |/ Err?=(aucune)/besoin Réinstallation (État,Err: majuscule=mauvais)
    ||/ Nom                      Version           Architecture      Description
    +++-========================-=================-=================-=====================================================
    ii  txm                      0.7.9             all               Analyse textométrique de corpus textuels
    ii  txm-0.8.0                0.8.0             all               Analyse textométrique de corpus textuels
    ii  txm-0.8.1                0.8.1             amd64             Analyse textométrique de corpus textuels
    ii  txm-0.8.1beta1           0.8.1beta1        amd64             Analyse textométrique de corpus textuels
~~~

Glose : 3 versions sont installées : TXM 0.7.9, TXM 0.8.0, TXM 0.8.1 (ce
dernier en deux variantes)

### Comment supprimer toutes les variantes d’une version de TXM sur ma machine&nbsp;?

a\) D‘abord vérifier quelles actions de désinstallation seront
effectuées : sudo apt autoremove –purge –dry-run ’txm-0.8.0\*’ Glose :
on s’intéresse aux variantes de TXM 0.8.0

Exemple de résultat :
~~~ text
    Lecture des listes de paquets... Fait
    Construction de l'arbre des dépendances       
    Lecture des informations d'état... Fait
    Note : sélection de txm-0.8.0 pour l'expression rationnelle « txm-0.8.0* »
    Les paquets suivants seront ENLEVÉS :
      txm-0.8.0*
    0 mis à jour, 0 nouvellement installés, 1 à enlever et 45 non mis à jour.
    Purg txm-0.8.0 [0.8.0]
~~~
b\) Si les opérations de désinstallation sont les bonnes, les exécuter
effectivement :

     sudo apt autoremove --purge 'txm-0.8.0*'

### Que faire si l’installation sur Linux Debian ne trouve pas le package XXX&nbsp;?

***NOTE :** Cette solution semble difficilement praticable dans Debian
Buster à cause du nombre de paquets obsolètes requis par TXM, eux-mêmes
dépendants de paquets obsolètes. L‘utilisation de la commande ’‘dpkg
–force-all -i’’, mentionnée plus bas, est risquée. Il semble préférable
d’installer TXM sous Linux avec Wine.*

Les dépôts Debian ne contiennent pas le package XXX.

Pour installer TXM dans ce cas, il faut d‘abord rajouter le dépôt YYY
contenant le package XXX dans la liste de dépôts de l’apt (système de
gestion des dépôts et des dépendances entre packages) : - éditer le
fichier /etc/apt/sources.list - ajouter la ligne du dépôt ’YYY’ (par
exemple ‘deb <http://cran.rstudio.com/bin/linux/ubuntu> precise/’ pour
un dépôt Ubuntu de R)

1.  créer un fichier portant le nom du paquet dans le répertoire
    /etc/apt/preferences.d (voir le [Manuel de l'administrateur
    Debian](https://www.debian.org/doc/manuals/debian-handbook/sect.apt-get.fr.html#sect.apt.priorities-)
2.  exécuter : apt-get update
3.  relancer l’installation de TXM

### Installer TXM sous Linux avec Wine

Il est possible d‘installer la version de TXM pour Windows sur Linux en
utilisant [Wine](https://www.winehq.org/). Pour ce faire : 

* [Installer Wine](https://wiki.winehq.org/Download) (voir la page correspondant à
votre distribution) ;
* Télécharger l’installeur Windows de TXM ;
* Dans un terminal, se placer dans le répertoire dans lequel vous avez
enregistré l’installeur, puis entrer `wine TXM_<version téléchargée>.exe` ; 
* Une fois que le programme est installé, deux
possiblités pour le lancer :
  * Toujours dans le terminal, se placer
dans le répertoire `~/.wine/drive_c/<chemin d’installation de TXM>`,
puis entrer ’‘wine TXM.bat’’ ;
  * Sans nécessairement changer de répertoire, entrer dans le terminal la commande ''wine explorer'' (qui ouvre un explorateur de fichiers), naviguer jusqu'au répertoire de TXM à partir de "Poste de travail"->"(C:)", puis double-cliquer sur le fichier "TXM.bat" (pas sur le fichier "TXM.exe"!).

Pour un meilleur rendu des polices, vous pouvez :

- [Installer Winetricks](https://wiki.winehq.org/Winetricks) ;
- Lancer Winetricks sans arguments ;
- Sélectionner “Select default wineprefix”, “Change settings”,
  “fontsmooth=rgb”.
- Pour quitter Winetricks, il faut cliquer plusieurs fois sur “Annuler”
  pour revenir en arrière jusqu’à ce que la fenêtre se ferme.

### Comment installer TXM 0.6 sur un Linux Ubuntu de version supérieure à 11.04&nbsp;?

La logithèque Ubuntu ne parvient pas à exécuter le script de post
installation (pour une raison inconnue). On ne peut donc pas installer
TXM 0.6 avec cet outil.

Pour installer TXM dans ce cas, il faut utiliser le logiciel ‘GDebi’
disponible dans Ubuntu :

1.  installer GDebi
2.  puis, clic-droit sur l‘icone du fichier d’installation de TXM,
    sélectionner ’Ouvrir avec GDebi’

A partir de la version 0.7 TXM peut être installé avec la logithèque.

### Comment installer TXM 0.7.5 sur un Linux Ubuntu 14.04 ou 14.10

Lors de l’installation de TXM, si l’option “install needed R packages”
est cochée dans l’assistant, TXM essaie de télécharger et de compiler
des paquets R, mais échoue. En effet, certains paquets Debian
nécessaires pour cette compilation sont absents par défaut. S’ils ne
sont pas présents, toute installation ou mise à jour de n’importe quel
logiciel (y compris TXM) échouera systématiquement, car le système
essayera à chaque fois de terminer d’abord l’installation de TXM.

\(1\) Il faut installer quelques paquets Debian (présents dans les
dépôts), *avant* d’installer TXM:

~~~ bash
    sudo apt-get install r-base r-base-dev r-cran-rcppeigen r-cran-rsclient
~~~

Une fois ceci fait, on peut installer TXM normalement et, si tout va
bien, ignorer les points suivants.

\(2\) Si on a déjà essayé d’installer TXM sans avoir préalablement
installé ces paquets, et que l’installation de TXM échoue, il faut
*totalement* supprimer TXM et R:

~~~ bash
    sudo apt-get remove --purge r-base txm
~~~

(si ça ne suffit pas, par exemple si on essayé d’installer plein de
paquets pour essayer de débloquer l’installation, il faut en plus
enlever tous les paquets liés à R: *sudo apt-get remove –purge r-base\*
r-cran-\** ) ensuite, revenir en (1)

\(3\) Si TXM ne fonctionne qu’en mode administrateur (la commande *sudo
TXM* fonctionne, mais pas juste *TXM* ):

~~~ bash
    sudo chmod -R 777 /usr/lib/TXM
~~~

\(4\) Si une fenêtre “TXMLaunch: Can’t find STAMP file
/usr/lib/TXM/STAMP” s’ouvre quand on essaye de lancer TXM:

~~~ bash
    touch /usr/lib/TXM/STAMP
~~~

### Comment installer TXM 0.7.9 sur un Ubuntu 18.04

{% include warning.html content="La première commande proposée est potentiellement
dangereuse pour la stabilité du système : voir **man dpkg**, rubrique
**OPTIONS --force**. En attendant une meilleure solution, il paraît plus
sûr d’[installer TXM sur Linux avec
Wine](#installer-txm-sous-linux-avec-wine)." %}

- Télécharger l’installeur
  <http://textometrie.ens-lyon.fr/files/software/TXM/0.7.9>
- dans le terminal:
~~~ bash
      sudo dpkg -i --force-all TXM_0.7.9_Linux64_installer.deb
~~~
- puis
~~~ bash
      sudo ln /lib/x86_64-linux-gnu/libreadline.so.7 /lib/x86_64-linux-gnu/libreadline.so.6
~~~
  (pour que R fonctionne)

# Lancement de TXM<br>

### Je clique sur l’icone de TXM, mais TXM ne se lance pas (ou se lance bizarrement), pourtant tout marchait bien jusqu’à présent&nbsp;?

Chaque environnement a une manière bien précise de lancer TXM, qui est
documentée dans le [manuel
utilisateur](http://txm.sourceforge.net/doc/manual/manual1.xhtml).

#### Sous Windows
{: .no_toc }

On lance TXM depuis le menu Démarrer. Voir aussi si besoin dans cette
FAQ :

- [Sous Windows 8, je ne trouve pas TXM sur l'écran
  d'accueil](#sous-windows-8-je-ne-trouve-pas-txm-sur-lécran-daccueil)
- [Sous Windows, je ne trouve pas TXM dans le menu
  Démarrer](#sous-windows-je-ne-trouve-pas-txm-dans-le-menu-démarrer)

#### Sous Mac
{: .no_toc }

On lance TXM à partir du Finder, avec l’exécutable qui est dans le
dossier Applications : avec le Finder naviguer jusqu’au répertoire
“Applications/TXM” et double-cliquer sur l’icone de l’application TXM
(ce qui lance le fichier /Applications/TXM/TXM.app). Noter qu’on ne peut
pas toujours lancer TXM à partir d’une icone située sur le bureau ou à
partir de la barre d’applications située en bas de l’écran.

#### Sous Linux (Ubuntu-)
{: .no_toc }

On lance TXM à partir du lanceur Unity (commencer à saisir ‘txm’ dans le
dashboard, l‘outil de recherche situé en haut du lanceur, puis
’Return’), ou bien à partir d’une ligne de commande “TXM &” dans un
terminal.  
Une fois TXM lancé, on peut l’ancrer dans la barre d’applications Unity
avec un clic-droit sur l’icone temporaire de TXM dans le lanceur, puis
demander à la conserver. Une fois conservée, le clic sur l’icone dans le
lanceur permet d’appeler TXM directement.

**Réglage de l’appel de TXM à partir d’un lanceur** : Sous Ubuntu 16.04,
ouvrir le fichier de lanceur “~/.local/share/applications/txm.desktop”
dans un éditeur de texte (comme gedit) et modifier la ligne commençant
par “Exec=…” pour qu’elle devienne “Exec=/usr/bin/TXM”, fermer et
relancer TXM à partir de l’icone du lanceur.

### TXM ne se lance plus, que faire&nbsp;?

Une invocation précédente de TXM peut ne pas avoir réussi à terminer le
moteur statistique en quittant, ce qui empêche un nouveau TXM de se
lancer (avec un nouveau moteur statistique).

Sous Windows, deux actions connues peuvent produire cette situation :

- en cas de changement de langue ;
- en cas de plantage de TXM.

Pour résoudre ce problème :

A\)

1.  vérifier la présence d‘un processus en cours d’exécution dénommé
    ’Rserve’ sur votre machine (par exemple avec le gestionnaire de
    processus sous Windows) ;
2.  arrêter ce processus avec le gestionnaire de processus ;
3.  lancer TXM.

Si TXM ne se lance toujours pas :

B\)

1.  redémarrer la machine
2.  lancer TXM

Si TXM ne se lance toujours pas :

C\)

1.  réinstaller TXM
2.  lancer TXM

Si TXM ne se lance toujours pas :

D\)

1.  nous contacter sur la liste de diffusion pour résoudre le problème.

Si TXM ne se lance toujours pas :

E\)

1.  passer nous voir au laboratoire avec votre machine (et des biscuits)

### TXM se bloque au démarrage ou lors de la fermeture des préférences

Depuis la version 0.7.5, TXM vérifie la présence de mises à jour à son
lancement. Si votre machine se connecte à Internet par un proxy, la
recherche de mises à jour peut provoquer des blocages au démarrage de
TXM ou lors de la modification de certaines préférences de TXM. Si c’est
le cas, il faut alors indiquer la configuration de votre proxy à TXM, en
suivant les instructions de la section [Réglages de l'accès au réseau
par proxy](http://txm.sourceforge.net/doc/manual/manual18.xhtml#toc32-)
du manuel.

#### Au premier lancement, TXM bloque à l’écran de démarrage/splash screen
{: .no_toc }

Si le blocage s’effectue dès le lancement de TXM :

- attendre que TXM se lance (cela peut prendre 5 à 10 minutes). Pour
  éviter l’attente, une solution peut être de débrancher temporairement
  votre câble réseau, TXM devrait alors se lancer quasi-instantanément.
- une fois TXM lancé, indiquer la configuration de votre proxy à TXM, en
  suivant les instructions de la section [Réglages de l'accès au réseau
  par proxy](http://txm.sourceforge.net/doc/manual/manual18.xhtml#toc32-)
  du manuel
- redémarrer TXM
- le blocage ne devrait plus se reproduire

# Import<br>

### Comment importer un corpus dans TXM&nbsp;?

Le plus simple est de sélectionner puis de copier (dans le
presse-papier) le texte à importer dans l‘outil de votre choix
(traitement de texte, navigateur, etc.), puis depuis TXM lancer la
commande `Fichier > Importer > **Presse-papier**`. Une nouvelle icone
de corpus apparaît dans la vue des corpus située à gauche de l‘interface
qui permet de lui appliquer les outils de TXM. Remarque : dans l’import
par le presse-papier, la langue utilisée pour la lemmatisation à la
volée est réglée par la préférence `Outils > Préférences > TXM > Utilisateur > Import : Default language`.

Pour importer un ou plusieurs fichiers dans un format spécifique, voici
les différentes options disponibles.

#### A. Textes écrits

##### formats TXT et XML
{: .no_toc }

TXT correspond au format texte brut ou “texte au kilomètre”, composé
seulement de caractères, d’espaces et de sauts de lignes. C’est le
format textuel le plus simple.

XML correspond au format texte structuré à l’aide de balises xml, selon
une syntaxe stricte.

Chaque format dispose de son propre module d‘import :
- TXT : déposer
les fichiers source au format TXT dans un répertoire et appliquer le
module d’import `Fichier > Importer > TXT+CSV` sur le répertoire.

- XML : déposer les fichiers source au format XML dans un répertoire et
  appliquer le module d‘import `Fichier > Importer > XML/w+CSV`
  sur le répertoire.

Comme pour tous les modules d‘import, vous pouvez de façon optionnelle
associer des propriétés à chaque texte (auteur, titre, genre, date…) en
déposant un fichier ’metadata.csv’ au format CSV dans le répertoire des
sources (voir la documentation de ces modules pour le format précis de
ce fichier).

Pour vous aider à choisir le format source le plus adapté à votre
travail, nous vous invitons à suivre la séquence du tutoriel [Support -
Atelier preparation de corpus et import dans
TXM.pdf'](https://sourceforge.net/projects/txm/files/course%20materials/Support%20-%20Atelier%20preparation%20de%20corpus%20et%20import%20dans%20TXM.pdf/download)
qui utilise des fichiers texte exemples fournis dans l’archive support
[PreparationEtImportDansTXM.zip](https://sourceforge.net/projects/txm/files/course%20materials/PreparationEtImportDansTXM.zip/download).

Cette séquence vous montrera comment choisir au mieux le niveau de
représentation de vos textes sources en fonction des services dont vous
voulez bénéficier au sein de la plateforme TXM. TXM est conçu pour vous
aider à importer progressivement vos corpus, d’une représentation
minimaliste à la plus évoluée, pour vous aider à gérer au mieux le coût
de préparation de vos sources en fonction du temps dont vous disposez :

1.  format texte brut (TXT)
2.  format texte brut (TXT) avec métadonnées de textes (CSV)
3.  format xml (XML)
4.  format xml (XML) avec métadonnées de textes (CSV)
5.  format xml (XML) avec structures xml supplémentaires (paragraphes)
6.  format xml (XML) avec structures xml ayant des propriétés
7.  format xml (XML) avec pré-encodage de certains mots
8.  format xml (XML) avec pré-encodage de certains mots ayant des
    propriétés

Voir également le [Tutoriel pour importer des textes bruts (ou 'plain
text') dans
TXM](https://groupes.renater.fr/wiki/txm-users/public/tutoriel_import_txt_csv).

Pour les conversions depuis de nombreux formats de textes vers TXT et
XML, nous vous recommandons d’utiliser le service de conversion
[OxGarage](http://www.tei-c.org/oxgarage).

##### sources XML-TEI
{: .no_toc }

Voir les [tutoriels dédiés aux représentations
XML-TEI](https://groupes.renater.fr/wiki/txm-users/public/tutoriels_import_xml-tei).

##### traitements de texte .DOCX, .ODT
{: .no_toc }

Déposer les fichiers dans un répertoire et lui appliquer le module
d’import
[ODT/DOC/RTF+CSV](http://textometrie.ens-lyon.fr/html/doc/manual/0.7.9/fr/manual26.xhtml#toc112).

Ou bien convertir d’abord les fichiers en fichiers en XML-TEI avec le
service en ligne [OxGarage](http://www.tei-c.org/oxgarage), puis
importer les fichiers au format XML avec le module XTZ+CSV.

Ou bien convertir d’abord les fichiers en fichiers au format TXT avec la
[macro
Text2TXT](https://groupes.renater.fr/wiki/txm-users/public/macros#text2txt),
puis importer les fichiers au format TXT avec le module TXT+CSV.

##### Europresse
{: .no_toc }

Voir le [tutoriel d'importation de corpus d'articles exportés du portail
EUROPRESS dans
TXM](https://groupes.renater.fr/wiki/txm-users/public/tutoriel_europresse).

##### Factiva
{: .no_toc }

Déposer les fichiers exportés au format XML dans un répertoire et lui
appliquer le module d‘import ’XML Factiva’.

Déposer les fichiers exportés au format TXT (mail) dans un répertoire et
lui appliquer le module d‘import ’Factiva TXT’.

##### Hyperbase
{: .no_toc }

Déposer le fichier dans un répertoire et lui appliquer le module
d‘import ’Hyperbase’.

##### Iramuteq, Alceste
{: .no_toc }

Déposer le fichier dans un répertoire et lui appliquer le module
d‘import ’Alceste’.

##### Taltac
{: .no_toc }

Convertir d’abord le fichier au format Taltac en un fichier au format
XML avec la [macro Taltac2XML](https://groupes.renater.fr/wiki/txm-users/public/macros#taltac2xml), puis importer
le fichier au format XML avec le module XTZ+CSV.

##### Cordial
{: .no_toc }

Déposer les fichiers au format CNR dans un répertoire et lui appliquer
le module d‘import ’CNR+CSV’.

##### PDF
{: .no_toc }

Voir la question [peut on importer des documents au format PDF dans TXM
?](https://groupes.renater.fr/wiki/txm-users#peut_on_importer_des_documents_au_format_pdf_dans_txm-)

##### pages web (HTML-)
{: .no_toc }

Voir la question [comment faire pour importer des pages web dans TXM
?](https://groupes.renater.fr/wiki/txm-users#comment_faire_pour_importer_des_pages_web_dans_txm-)

Pour l’aspiration de pages web, nous vous recommandons d’utiliser le
logiciel [gromoteur](http://gromoteur.ilpga.fr).

##### EPub
{: .no_toc }

TXM n’importe pas actuellement de fichiers EPub directement.

Nous vous recommandons d’utiliser le logiciel
[Calibre](https://calibre-ebook.com) pour en extraire une représentation
HTML puis d’utiliser le logiciel [Tidy](http://www.html-tidy.org/) pour
obtenir du XHTML à importer avec le module d’import XML/w+CSV. Vous
pouvez également extraire une représentation .TXT et utiliser le module
d’import TXT+CSV.

Remarque : le logiciel
[\<oXygen/\>](http://www.oxygenxml.com/xml_editor/epub.html) permet
également de manipuler des fichiers EPub.

#### B. Transcriptions d’enregistrements

##### Transcriber
{: .no_toc }

Déposer les fichiers au format .TRS (saisis avec le logiciel
Transcriber) dans un répertoire et lui appliquer le module d‘import ’XML
Transcriber+CSV’.

##### .ODT, .DOC, .RTF, .TXT
{: .no_toc }

Vous pouvez également saisir vos transcriptions directement dans un
traitement de texte (Word ou équivalent) en respectant des conventions
de transcription élémentaires puis les importer dans TXM avec le module
d‘import ’XML Transcriber+CSV’ après conversion automatique. Voir le
[Tutoriel d’importation de transcriptions d’enregistrements au format Word](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/tutorials/import-transcription-word/).

##### ELAN, CLAN, Praat
{: .no_toc }

Nous vous recommandons de convertir les fichiers source vers le format
Transcriber (ou .TRS) à l‘aide du [convertisseur
TEI_CORPO](http://ct3.ortolang.fr/teiconvert) puis d’utiliser le module
d’import ’XML Transcriber+CSV’.

#### C. Corpus multilingues alignés

##### TMX
{: .no_toc }

Déposer les fichiers au format TMX dans un répertoire et lui appliquer
le module d‘import ’XML-TMX’.

On peut tester avec le corpus exemple
‘[uno-tmx-sample-src.zip](https://sourceforge.net/projects/txm/files/corpora/uno-tmx-sample/uno-tmx-sample-src.zip/download)’.

#### D. Données textuelles tabulées

On peut importer des données textuelles enregistrées dans des tableaux
MS Excel ou LibreOffice Calc en les convertissant en XML au préalable,
puis en utilisant les modules d’import basés sur le format XML (comme
XML/w+CSV ou XTZ+CSV par exemple).

Lors de la conversion, on choisira les colonnes qui deviendront des
métadonnées de texte et les colonnes qui deviendront des sections de
texte.

##### CSV
{: .no_toc }

Convertir les fichiers .csv en .xml avec la [macro
CSV2XML](https://groupes.renater.fr/wiki/txm-users/public/macros#csv2xml).

##### Excel
{: .no_toc }

Convertir les fichiers .xlsx en .xml avec la macro [macro
Excel2XML](https://groupes.renater.fr/wiki/txm-users/public/macros#excel2xml_exceldir2xml) ou ExcelDir2XML pour
une conversion par lots.
https://groupes.renater.fr/wiki/txm-users/public/macros#excel2xml_exceldir2xml
Remarque : il est conseillé d’utiliser la macro Excel2XML plutôt que
CSV2XML car le format .xlsx est plus précis (moins de problèmes
d’encodage de caractères, de caractère séparateur de colonnes, etc.) et
Excel2XML offre plus de services.

### Après import, certains caractères de mes textes sont illisibles. Que se passe-t-il&nbsp;?

Le système d’encodage des caractères utilisé dans les fichiers d’entrée
(ou fichiers sources) ne correspond pas à celui utilisé par TXM pour
décoder les textes au moment de l’import.

Pour régler le problème : dans le formulaire de paramètres d’import à la
section “Encodage des caractères”, il faut sélectionner le bon système
d’encodage des caractères[^5].

Le système d’encodage des caractères UTF-8 est en train de se
généraliser sur tous les systèmes d’exploitation, mais certains
encodages continuent à être utilisés par certains systèmes
d’exploitation :

- si vous êtes sous Windows : essayer le système “windows-1252”
- si vous êtes sous Mac OS X : essayer le système “MacRoman”
  - des variantes pour différentes régions géographiques peuvent
    utiliser d’autres systèmes : comme MacCyrillic ou MacCentralEurope
    par exemple

TXM connaît 169 systèmes d’encodage des caractères différents. Consulter
la [liste exhautive des systèmes d'encodage des caractères connus de
TXM](https://groupes.renater.fr/wiki/txm-users/public/character_encodings_list).

Les macros suivantes sont utiles pour diagnostiquer et remédier aux
difficultés de système d‘encodage des caractères utilisés dans vos
fichiers sources :
- CharList : index hiérarchique des caractères
utilisés par un fichier (caractères et leur fréquence dans le texte), on
voit tout de suite les caractères incorrects
- CharListDirectory : même
chose mais pour tous les fichiers d’un répertoire \*
ListCharacterEncodings : affiche dans la console la liste utilisée
habituellement dans le menu du paramètre ’encodage des caractères’ du
formulaire d’import (à copier/coller) dans Calc pour chercher dedans

- ChangeEncoding : convertit l’encodage des caractères de tous les
  fichiers d’un répertoire. Il faut indiquer l’encodage d’entrée et
  l’encodage de sortie

Remarque pénultième : il arrive que tous les fichiers d’un répertoire de
sources n’utilisent pas tous un système unique d’encodage des
caractères. Cela peut venir de l’historique des traitements subis par
ces fichiers. Dans ce cas il faut redresser certains fichiers vers le
système d’encodage unique qui sera utilisé par tous les fichiers.

Remarque finale : il arrive que le système d’encodage varie au sein d’un
même fichier source (l’encodage peut varier au caractère près). Suivant
l’ampleur de l’hétérogénéité il peut être difficile de récupérer le
contenu du texte.

### Est il possible d’importer n’importe quel corpus de textes encodés en TEI même s’ils ne sont pas conformes à l’usage TEI du projet BFM&nbsp;?

{% include note.html content="Voir aussi le [Tutoriel d’importation de corpus TEI avec le module XML-TEI Zero + CSV
(corpus Joubert, encodage simple)](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/tutorials/import-xtz-simple-joubert/)." %}

Oui, pour cela il peut être intéressant - mais ce n’est pas
obligatoire - d’adapter le XML-TEI pour que TXM puisse importer les
éléments utiles aux outils d’analyse et de lecture.

Cette adaptation peut se faire en dehors de TXM ou bien à la volée au moment
de l’import du corpus à l’aide de feuilles de transformation XSL comme
celles fournies dans la bibliothèque XSL de TXM :
<https://txm.gitpages.huma-num.fr/textometrie/files/library/xsl/>.

Le module le plus pratique pour importer n’importe quel type de sources
XML-TEI dans TXM est le module **XTZ+CSV** (ie XML-TEI Zero + CSV). Ses
capacités d’adaptation de sources à la volée et d’interprétation de
divers éléments TEI pour la production des éditions de textes sont
documentées dans le manuel de TXM :
<https://txm.gitpages.huma-num.fr/txm-manual/importer-un-corpus-dans-txm.html#module-xml-tei-zerocsv-dit-aussi-xtzcsv-ou-xtz-import-de-xml-tei-g%C3%A9n%C3%A9rique-.xml>.

Si la voie des XSL n’est pas suffisante, tous les modules d’import de
TXM étant écrits en langage de script Groovy, les utilisateurs de TXM
peuvent les modifier directement eux-mêmes pour adapter la façon dont
TXM importe un corpus donné. Il s’agit d’un langage de scripts, comme
VisualBasic pour les macros de MS Word. Il est documenté dans le manuel
de TXM à la section [Utilitaires et macros](https://txm.gitpages.huma-num.fr/txm-manual/utilitaires-et-macros.html).

#### Pourquoi faut il adapter l’encodage XML-TEI pour TXM&nbsp;?

Comme la TEI n’est pas un format mais un ensemble de recommandations, il
y a tellement de façons différentes d’encoder des données particulières
qu’aucun logiciel ne peut tout interpréter directement (voir plus bas la
mention d’outils XML généralistes).

Souvent les projets se focalisent sur la restitution du texte par la
production d’éditions (HTML souvent) à partir du XML-TEI pour offrir des
services de lecture et de navigation.

S’il y a une possibilité de recherche de mots en plein texte, celle-ci
se limite souvent aux formes graphiques de surface sans distinction
entre les différents plans textuels (corps de texte, titres de sections,
contenu de notes, d’index, de sommaire, etc.) et sans tenir compte
d’encodages internes aux mots (comme des choix de segmentation, de
correction, le commencement sur une page et la terminaison sur une
autre, etc.).

Dans le cas de TXM, les traitements réalisés sont beaucoup plus riches
qu’une simple édition : on peut préciser la délimitation des textes et
quelles sont leurs métadonnées (pour pouvoir les comparer entre eux ou
attribuer une expression au bon auteur), comment calculer les unités
lexicales (les mots) et leurs propriétés, les phrases orthographiques
qui les contiennent, la structure logique des textes, les informations
de référencement comme la pagination, ce dont il ne faut pas tenir
compte dans les sources (le hors-texte), etc. D’un certain point de vue,
aujourd’hui la TEI n’est pas assez précise pour TXM…

Bref, ce qu’on promet surtout avec la TEI aujourd’hui, c’est la
pérennité du travail effectué pour l’avenir (éviter de recréer des
représentations de textes selon des conventions ad hoc locales -
incompatibles avec les précédentes et les suivantes - en permanence).
Pour ce qui est de traiter les textes avec des outils, il faut s’assurer
de la compatibilité des conventions avec eux AVANT d’encoder, de sorte à
avoir des données numériques compatibles avec les outils de traitement
utilisés en bout de chaîne.

#### Existe-t’il des outils pour traiter n’importe quel XML (et TEI)&nbsp;?

TXM est prioritairement orienté textes, mots et structures logiques dans
une optique d’analyse qualitative et quantitative. Si vous souhaitez
faire d’autres types de traitements, vous pouvez utiliser des outils
plus généraux et neutres vis-à-vis de la sémantique TEI (ou de tout
schéma XML particulier) comme les bases XML natives (eXist -
<http://exist.sourceforge.net> ou baseX - <http://basex.org> par
exemple). Vous devrez alors construire dans ces outils toute la logique
de la sémantique des éléments TEI que vous souhaitez encoder puis
utiliser dans les outils pour l’exploitation de vos textes - de la même
façon que TXM implémente la sémantique d’éléments nécessaire à ses
traitements.

#### Quels types d’encodage TEI ont déjà été importés dans TXM&nbsp;?

Du côté du projet TXM, nous discutons avec chaque projet de production
et d’exploitation de corpus, et documentons ce que peut faire la
plateforme avec des ressources XML et TEI avant de les adapter pour une
exploitation particulière.

C’est pourquoi nous avons commencé par réaliser :

- a\) un premier module d’importation capable d’interpréter les
  principes d’encodage XML-TEI des sources du projet BFM que nous
  connaissons bien et qui sont publics et accessibles à
  <http://bfm.ens-lyon.fr/article.php3?id_article=158> ;
- b\) un module d’importation d’XML tout venant, XML/w+CSV, donc capable
  d’importer n’importe quel XML-TEI.

Depuis, nous avons développé le module XTZ+CSV pour pouvoir le
paramétrer en fonction d’autres principes d’encodage XML-TEI à l’aide de
feuilles de style XSL de prétraitement.

À ce jour, les modules d’import XML-TEI BFM et XML/w+CSV sont déjà
utilisés pour importer les sources XML-TEI des projets suivants :

- Perseus Digital Library : <http://www.perseus.tufts.edu/hopper>
- TextGrid : <http://www.textgrid.de/en>
- NLTK - Brown Corpus (TEI XML Version) :
  <http://nltk.googlecode.com/svn/trunk/nltk_data/index.xml>
- Frantext (libre) : <http://www.cnrtl.fr/corpus/frantext>
- Base de Français Médiéval (BFM) : <http://bfm.ens-lyon.fr>
- BVH Epistemon : <http://www.bvh.univ-tours.fr/Epistemon>
- Bouvard&Pécuchet : <http://dossiers-flaubert.ish-lyon.cnrs.fr>
- PUC, MRSH de Caen - Revues.org :
  <http://www.unicaen.fr/recherche/mrsh/document_numerique/outils>
  ([revue DISCOURS](http://discours.revues.org))
- TXM (le format pivot interne de TXM) :
  <https://groupes.renater.fr/wiki/txm-info/public/xml_tei_txm>

Désormais, l’essentiel des sources XML-TEI sont importées par le module
XTZ+CSV à l’aide de feuilles d’adapation XSL déclenchées à différentes
étapes du processus d’import.

{% include note.html content="toutes les sources importées dans TXM convergent vers le
format pivot ‘XML-TXM’ qui est une extension du format TEI définie pour
le traitement par la plateforme, quel que soit le type et le format
d’origine des sources (texte écrit - texte brut ou XML, transcription
d’enregistrement - traitement de texte ou Transcriber, corpus
parallèle - TMX, etc.)." %}

#### Conclusion
{: .no_toc }

Du côté de la TEI, nous devons mieux documenter ce qui est possible de
faire avec les logiciels sur les corpus encodés en XML-TEI. Mais l’idéal
est quand même de partir d’un minimum d’expression de besoins de
traitements concrets au final avant même de définir les conventions
d’encodage des sources textuelles d’un projet.

### Comment importer les métadonnées de textes dans l’importation XML-TXM&nbsp;?

Dans TXM 0.5, qui implémente la version 1 de ce format, les métadonnées
sont transmises sous la forme d’attributs de la balise “\<text\>”
correspondant aux unités documentaires.

Les noms d’attributs sont en minuscules et sans accents. Exemple :
~~~ xml
    <text id="qgraal" auteur="anonyme" titre="Queste del saint Graal" datecompo="1225-01-01">
    ...
    </text>
~~~

### Quel est le format du fichier 'metadata'&nbsp;?

Le fichier de métadonnées de textes “metadata.*” est utilisé par tous
les modules d’import dont le nom finit par “+CSV”.

Il doit contenir au minimum la colonne “id” (en minuscule, en première
colonne) : cette colonne contient les noms des fichiers contenant les
textes, sans leur extension.

Les intitulés des colonnes (qui deviendront les noms des propriétés
associées aux textes) doivent respecter les contraintes suivantes (liées
au langage CQL) :

- pas d’espace,
- pas de caractères accentués,
- pas de majuscule,
- pas de chiffre en premier ni en dernier caractère,
- d’une manière générale, éviter les ponctuations et les caractères non
  alphanumériques (à l’exception du tiret en caractère médian).

Le fichier metadata.* doit être placé à coté des fichiers source, dans
le même répertoire.

À partir de TXM 0.8.3, il est recommandé d'utiliser l'un des formats de tableur `.xlsx` (MS Excel) ou bien `.ods` (LO Calc) pour le fichier metadata.

Ces formats ont l'avantage d'éviter tous les problèmes de paramétrage des fichiers `.csv`.

Pour TXM 0.6 et ultérieur, le fichier “metadata.csv” doit être sauvegardé à partir
d’Excel ou de Calc avec les paramètres suivants :

- encodage : UTF-8 (ou Unicode)
- séparateur de colonne : ,
- séparateur de texte : ”

Dans certaines version d’Excel (Mac OS X), il n’est pas possible de
régler ces paramètres au moment de la sauvegarde. Il faut alors utiliser
Calc de la suite LibreOffice (ou OpenOffice) pour pouvoir le faire ou
bien modifier le format du fichier metadata.csv pris en charge dans les
paramètres TXM (Menu `Outils > Préférences > TXM > Utilisateur > Import`).

Pour TXM 0.5, le format du fichier metadata.csv varie en fonction du
module d’import :

- modules TXT + CSV et CNR + CSV:
  - encodage : UTF-8
  - séparateur de colonne : tabulation
  - séparateur de texte : aucun
- module Transcriber :
  - encodage : UTF-8 (ou Unicode)
  - séparateur de colonne : ,
  - séparateur de texte : ”

### Je n’arrive pas à éditer mon fichier metadata.csv avec Calc

En général on édite le tableau de métadonnées avec le logiciel tableur
Calc en le sauvegardant dans le format de fichier natif de ce logiciel
appelé ‘.ods’ (Open Document Spreadsheet), et on l’exporte ensuite dans
un format CSV pour l’utilisation avec TXM[^6].

Cependant, il est possible d‘éditer le fichier ’metadata.csv’
directement avec Calc mais il faut prendre certaines précautions. Par
exemple, si dans Calc on visualise le fichier de la façon suivante :

![](http://i.imgur.com/7dm0sze.jpg)

Cela veut dire que Calc n’a pas compris où se trouvaient les colonnes
dans le fichier CSV.

Le tableau dans Calc se visualise de la façon suivante :

                     colonne 1     colonne 2       etc.
                         A      |      B     |     C      |      D       etc.
    ligne 1      1 ] cellule A1 | cellule B1 | cellule C1 | cellule D1 |        etc.
    ligne 2      2 ] cellule A2 | cellule B2 | cellule C2 | cellule D2 |        etc.
    etc.

Dans l’illustration on voit ceci :

                       A
    1 ]    "id,"auteur","type","date"   |
    2 ] "chant_un","Noel","vers","1983" |
    etc.

C’est à dire un tableau à une seule colonne.

Il y a deux moyens de rétablir les colonnes du tableau CSV :

- A\) Utiliser une commande de Calc
  1.  sélectionner la colonne
  2.  lancer la commande `Données > Texte en colonnes…`
  3.  choisir les bons paramètres de caractère séparateur de colonnes et
      de caractères de texte (si nécessaire)  
      -\> le contenu de la colonne est interprété et de nouvelles
      colonnes la remplacent
- B\) Fermer le fichier et ouvrir à nouveau le fichier CSV mais avec les
  bons paramètres d’importation : caractère séparateur de colonnes et
  caractères de texte (si nécessaire)  
  -\> les colonnes devraient apparaître directement

Le résultat se visualise comme ceci :

           A     |    B   |  C   |  D
    1 ]    id    | auteur | type | date |
    2 ] chant_un |  Noel  | vers | 1983 |
    etc.

### Y a-t-il une limitation de taille pour les corpus en un seul fichier source&nbsp;?

Il y a une limitation de taille de fichier source à importer d’une
traite dans TXM qui dépend de la taille mémoire de la machine et de
réglages initiaux de TXM. Il y a deux façons de contourner ce problème :

**1) indiquer un modèle mémoire Java plus grand au lancement de TXM en
modifiant le fichier de paramètres “TXM.ini”**

Suivant le système d’exploitation, le fichier TXM.ini se trouve à
l’endroit suivant :

- Sous Windows :
  - `C:\Users\[USER]\TXM\.txm\TXM.ini`
  - ou bien `C:\Users\[USER]\AppData\Roaming\TXM\.txm\TXM.ini`
- Sous Windows XP : `C:\Documents and Settings\[USER]\Application Data\.txm`
- Sous Mac OS X : `/Users/[USER]/TXM/.txm/TXM.ini`
- Sous Linux : `/home/[USER]/TXM/.txm/TXM.ini`

Ouvrir le fichier avec un [éditeur de
texte](#quels-logiciels-utiliser-pour-éditer-des-documents-au-format-texte-brut-txt)
et remplacer les lignes suivantes :
~~~ text
    .txm
    -vmargs
    -Xms128m
    -Xmx512m
~~~
par (pour avoir entre 500Mo et 1Go de mémoire)
~~~ text
    .txm
    -vmargs
    -Xms512m
    -Xmx1024m
~~~
ou bien (pour avoir entre 5Go et 10Go de mémoire)
~~~ text
    .txm
    -vmargs
    -Xms5g
    -Xmx10g
~~~
Remarque : en cas d’impossibilité de lancement de TXM avec un message
d’erreur de type “Could not create java virtual machine”, la mémoire
demandée est trop importante par rapport à la mémoire réelle, il faut
baisser la taille de la mémoire demandée en modifiant les valeurs des
options -Xms et -Xmx.

**2) découper le fichier unique du corpus en plusieurs (de moins de 5Mo
par exemple), dans le même répertoire**

L’unité d’importation de corpus de TXM est actuellement un répertoire,
et non un fichier. Donc la façon standard de faire est de créer autant
de fichiers que nécessaire dans le même répertoire qui sera à l’origine
de l’importation. Remarque : cette stratégie permet notamment d’associer
facilement des métadonnées de textes à chaque fichier au moyen d’un
simple fichier CSV, et la géométrie de corpus par défaut de TXM est
adaptée à cette stratégie.

Remarque : en important le corpus en plusieurs fichiers cela provoquera
la création d’autant d’éditions de textes que de fichiers (au lieu d’une
seule édition de texte pour un corpus contenant un seul fichier texte).

### Que signifie le message d’erreur “Error: The registry file was not created”

Cela signifie que l’outil “cwb-encode” du moteur de recherche n’a pas
réussi à indexer le corpus que le module d’import a préparé.

Pour savoir plus précisément pour quelle raison “cwb-encode” a échoué,
il faut activer l’affichage des messages d’erreur de TXM en allant dans
la page de préférence `TXM > Avancé`. Il faut :

- Sélectionner le niveau de log SEVERE
- cocher “Afficher la stacktrace”
- cocher “Ajouter des commentaires techniques”

Une erreur courante sous Windows levée par cwb-encode est “Too many open
files”. Cette erreur apparaît lorsqu’un corpus atteint un certain nombre
de structures et propriétés. Voir le ticket
[\#873](http://forge.cbp.ens-lyon.fr/redmine/issues/873).

### Comment faire pour importer des pages web dans TXM&nbsp;?

TXM ne dispose pas de module d’import HTML pour l’instant.

Cela dit, pour importer des pages web dans TXM vous pouvez transformer
ces pages en XML au préalable puis utiliser le module d’import
XML/w+CSV.

Pour transformer les pages web en XML, vous pouvez utiliser l‘outil
[Tidy](http://www.w3.org/People/Raggett/tidy) de la façon suivante :
tidy -o page_web.xml -clean -bare -asxml -utf8 –doctype omit
–numeric-entities yes page_web.html Légende :
- -o page_web.xml :
sauvegarder le résultat de la transformation dans le fichier
’page_web.xml’

- -clean : substituer certaines balises HTML de formatage (FONT…)
- -bare : supprimer certains caractères typographiques annexes (quote,
  dash…)
- -asxml : produire un résultat au format XHTML (soit XML)
- -utf8 : utiliser l’encodage des caractères UTF-8 en entrée et en
  sortie (pour que certains caractères ne soient pas encodés en entités
  XML numériques)
- –doctype omit : ne pas ajouter de préambule de déclaration de la DTD
- –numeric-entities yes : transformer toutes les entités XML en entités
  numériques (pour que l’omission de la déclaration de la DTD soit
  possible)
- page_web.html : appliquer la commande sur ce fichier

### Peut on importer des documents au format PDF dans TXM&nbsp;?

{% include note.html content="Voir aussi le [Tutoriel de construction d’édition synoptique à partir de PDF](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/tutorials/pdf-to-synoptic-edition/)." %}

TXM ne peut pas importer des documents au format PDF directement et il
n’est pas prévu à court terme de pouvoir le faire car ce format est
particulièrement difficile à interpréter par un logiciel afin d’y
trouver du texte **de façon fiable**[^7].

Il faut donc au préalable convertir ces documents vers un format
compatible avec TXM.

À notre connaissance, le meilleur outil open-source pour réaliser ce
travail est le logiciel de gestion de bibliothèque numérique
[Calibre](http://calibre-ebook.com) (voir cependant les avertissements
de la section ‘[Conversion de documents
PDF](http://manual.calibre-ebook.com/conversion.html#convert-pdf-documents)’
du manuel utilisateur de Calibre) qui à partir d’un (voire plusieurs à
la fois) fichier(s) PDF pourra produire des fichiers au format EPUB
(pour les liseuses électroniques), ou bien HTML (pour la mise en ligne),
LibreOffice ODT (pour l’édition et l’import dans TXM), etc[^8].

Pour certains types de fichiers PDF, le logiciel
[pdftotext](https://en.wikipedia.org/wiki/Pdftotext), disponible
notamment sous Linux, peut fournir de bons résultats.

En dernier recours, le logiciel vous permettant de lire les fichiers PDF
(comme Adobe Acrobat Reader) peut parfois vous aider à extraire leur
contenu textuel par un simple copier/coller vers un autre logiciel, si
le fichier PDF autorise cette opération :

- a\) ouvrir le fichier PDF
- b\) sélectionner tout le contenu textuel : raccourcis clavier Ctrl-A
  (ou Commande-A sur Mac)
- c\) copier la sélection dans le presse-papier : Ctrl-C (ou Commande-C
  sur Mac)
- d\) coller le contenu du presse-papier :
  - dans un traitement de texte, comme Libre Office Writer ou MS Word,
    pour manipuler le texte sous un autre format : Ctrl-V (ou Commande-V
    sur Mac)
  - ou dans un [éditeur de
    texte](#quels-logiciels-utiliser-pour-éditer-des-documents-au-format-texte-brut-txt)
    pour manipuler le texte au format texte brut (TXT) : Ctrl-V (ou
    Commande-V sur Mac)
  - ou encore directement dans TXM, dans le cas d‘un corpus composé d’un
    seul texte, avec la commande d’import ’Fichier / Importer /
    Presse-papier’

Si les documents PDF ne contiennent que des images de pages[^9], c’est à
dire qu’il n’y a pas de contenu textuel à base de caractères mais des
images de texte (ceci est valable également pour une collection de
fichiers images de pages issues de scanners ou de smartphones - par
exemple au format JPEG, PNG ou TIFF), on peut s’orienter vers un
logiciel de reconnaissance automatique des caractères du texte au sein
des images de pages (Optical Character Recognition - OCR) comme [ABBYY
FineReader](http://france.abbyy.com/finereader) ou
[OmniPage](http://www.nuance.fr/for-individuals/by-product/omnipage/index.htm).
Toutes les Universités et laboratoires de recherche de France disposent
d’une ou plusieurs licences d’utilisation de ces logiciels. Les
résultats de ces outils sont enregistrables au format MS Word,
facilement convertible au format LibreOffice ODT pour l’import dans TXM.

### Comment faire un import Hyperbase&nbsp;?

1\) Préparation

Dans un dossier, mettre le fichier source à l’ancien format d’hyperbase
(documenté dans le manuel Hyperbase) et lui seul.

2\) Lancement de l’import

Si l’utilisateur est sous Linux :

- l’encodage par défaut des fichiers sources Hyperbase est CP1252, il
  faut le renseigner dans les paramètres optionnels

Puis

- renseigner le chemin du dossier
- si le corpus est d’une langue différente de celle du système, mettre
  le code ISO de la langue souhaitée (fr, de, it, en…)

### Peut on importer dans TXM un export de résultats de recherche Frantext&nbsp;?

Oui, il suffit d‘utiliser le module d’import ’XML-TEI Frantext’.

Avec qlqs réserves :

- l‘entête TEI de chaque extrait n’est pas conforme TEI, en particulier
  à cause de la balise `<auteur>` qui devrait être `<author>`
- la balise `<br/>` devrait être `<lb/>`
- les mots étoilés sont réencodés en `<w type="caps">…</w>`
- les `<seg>` sont réencodés en `<w>…</w>`

{% include warning.html content="Attention, également, d’être vigilant envers tout calcul
quantitatif dans ce type de “corpus” à cause des répétitions de parties
de contextes d’occurrences" %}

### Quels logiciels utiliser pour éditer des documents au format texte brut (TXT)&nbsp;?

Les logiciels de traitement de texte (Libre Office Writer ou MS Word)
peuvent facilement sauvegarder un document au format texte brut (TXT).
Par contre ils sont peu adaptés à l’édition régulière de texte brut,
notamment à cause d’un certain nombre d’automatismes aidant à la saisie
de texte (dont certains peuvent être désactivés pour aider à l’édition
de texte brut : comme le remplacement automatique d’apostrophes ou de
guillemets, l’ajout d’espaces insécables devant les doubles
ponctuations, etc.). Les logiciels **éditeur de texte** \[brut\] sont
plus rudimentaires mais plus proches du format correspondant à une
séquence de caractères d’écriture. Certains éditeurs de texte offrent
par ailleurs de l’aide à l’édition de documents au format XML qui est
une forme de texte brut plus évoluée (comme par exemple la coloration
des balises et attributs XML).

#### Quel que soit le système d’exploitation
{: .no_toc }

TXM inclut depuis sa version 0.7 un **éditeur de texte intégré** offrant toutes les fonctionnalités d'édition de base :

- il offre exactement la même interface utilisateur pour les trois systèmes d’exploitation ;
- on le lance avec la commande `Fichier > Éditer un fichier...` ;
- une fois ouvert sur un fichier du disque dur la barre d'outils est augmentée de ses outils de base (copier/coller, chercher/remplacer, aller à la ligne...) ;
- on peut obtenir la liste des commandes disponibles par le biais de la liste des raccourcis clavier par le menu `Aide > Raccourcis clavier` ou par le raccourci clavier `Ctrl-Màj-L` ;
- on peut régler ses préférences (notamment d'affichage) par l'entrée 'Preferences...' du menu contextuel (clic droit dans l'éditeur).

Voici par ailleurs une liste de logiciels éditeur de texte indépendants
populaires pour chaque système d’exploitation :

#### Windows
{: .no_toc }

- Notepad++ : <http://notepad-plus.sourceforge.net> (open-source)
- UltraEdit : <http://www.ultraedit.com>
- Emacs : <http://savannah.gnu.org/projects/emacs> (open-source,
  puissant mais complexe)
- Vi : <http://www.vim.org> (open-source, puissant mais complexe)

#### Mac OS X
{: .no_toc }

- TextEdit : installé par défaut (open-source)
- Fraise :
  <http://fr.wikipedia.org/wiki/Fraise_%28%C3%A9diteur_de_texte%29>
- Aquamacs : <http://aquamacs.org> (open-source)

#### Linux
{: .no_toc }

- Gedit : installé par défaut (open-source)
- VSCodium : <https://vscodium.com> (open-source)
- SciTE : <http://www.scintilla.org/SciTE.html> (open-source)
- Emacs : <http://savannah.gnu.org/projects/emacs> (open-source,
  puissant mais complexe)
- Vi : <http://www.vim.org> (open-source, puissant mais complexe)

Voir d’autres possibilités dans la liste des éditeurs recensés dans le
wiki de la TEI (en anglais) : <http://wiki.tei-c.org/index.php/Editors>

# Sous-corpus et partitions<br>

### Quelle différence y a-t-il entre les sous-corpus et les partitions&nbsp;?

On peut dire qu’une partition est un ensemble de sous-corpus dont la
somme est le corpus dans son ensemble. Par exemple, quand on fait une
partition d’un corpus en siècles, on crée autant de sous-corpus que de
siècles.

Les partitions sont surtout exploitées par les outils qui expriment un
contraste : spécificités, analyse factorielle…

Par contre, le fait de faire un sous-corpus dans TXM correspond au
besoin de ne s’intéresser qu’à un texte ou un chapitre particulier ou au
discours d’une seule personne ou au seul contenu des notes ou aux seuls
verbes ou aux seuls pronoms de première personne etc. On peut appliquer
aux sous-corpus les mêmes outils que ceux appliqués aux corpus.

Dans le cas particulier du calcul des spécificités d’un sous-corpus (par
rapport au corpus dans son ensemble), on peut dire que le calcul
s’applique à une partition (temporaire) composée de deux sous-corpus :
le sous-corpus en question et son complémentaire par rapport au corpus
entier.

### Comment sont ordonnées les parties d’une partition&nbsp;?

Lorsque l’on crée une partition selon le mode “simple”,

- en v 0.5 l’ordre des parties est arbitraire, sauf pour l’affichage des
  résultats des spécificités où il est alphabétique sur le nom des
  parties (mais donc a priori non concordant avec l’ordre des colonnes
  de la table de sécificités exportée, ni avec l’abcisse du diagramme en
  bâtons) ;
- en v 0.6 l’ordre des parties dépend du type de la métadonnée : en
  général l’ordre est l’ordre alphabétique sur le nom de la partie, et
  si l’on a une métadonnée de type “date” l’ordre est chronologique.

Lorsque la partition est créée en mode “assisté” ou “avancé”, l’ordre
des parties est celui adopté pour les définir, et non un tri sur le nom
donné aux parties.

### Peut-on choisir l’ordre des parties d’une partition&nbsp;?

Lorsque la partition est créée en mode “assisté” ou “avancé”, il suffit
de définir les parties dans l’ordre dans lequel on veut qu’elles soient
ensuite présentées.

S’il s’agit d’une partition définie en mode “simple”, mais que l’on
n’est pas satisfait de l’ordre obtenu, on peut recommencer la
construction de la partition en passant par le mode assisté et en
définissantles parties dans l’ordre dans lequel on voudrait qu’elles
soient. Par exemple, si l’on travaille sur un corpus de discours
présidentiels de la Ve république, et que l’on veut que les présidents
soient présentés dans l’ordre chronologique de leur présidence et non
dans l’ordre alphabétique de leur nom, on crée une partition en mode
assisté, sur la métadonnée donnant le locuteur, et on définit la
première partie avec (seulement) le locuteur De Gaulle, la deuxième
partie avec le locuteur Pompidou, etc.

### Je veux faire une partition sur un sous-corpus, mais c’est bizarre, certaines parties semblent avoir disparu, ou être vides, ou la partition n’est pas créée&nbsp;?

La commande “Créer une Partition” peut être appliquée à un sous-corpus,
mais elle produit un résultat incorrect si la structure utilisée pour
définir la partition est **au-dessus de**[^10] la structure utilisée
pour définir le sous-corpus.

Soit un corpus d’entretiens (structure “text”) portant une information
sur la tranche d’âge de la personne interviewée (propriété “agecla”,
avec des valeurs de la forme “20-32”, “32-38”, etc.), ainsi que des
informations sur les thématiques propres à chaque partie des entretiens,
définies grâce à la structure “div” : 

Dans ce corpus, on pourra sans difficulté :

- partitionner un sous-corpus d’entretiens réalisés auprès d’un
  interviewé ayant entre 38 et 50 ans, afin de faire contraster les
  entretiens sur le genre : cette opération ne pose aucune difficulté
  puisque les structures sont de **même niveau** (« text ») ;
- partitionner un sous-corpus d’entretiens réalisés auprès d’un
  interviewé ayant entre 38 et 50 ans, afin de faire contraster les
  différentes parties des entretiens selon leur thématique : cette
  opération ne posera aucune difficulté puisque la structure ayant servi
  à définir le sous-corpus (“text”) **contient** la structure servant à
  définir la partition (“div”).

En revanche, vous devrez utiliser le mode avancé pour créer une
partition si la structure utilisée pour la définir est **supérieure** à
la structure utilisée pour définir le sous-corpus. Il faudra donc
utiliser le mode avancé dans le cas où l’on souhaiterait, par exemple,
partitionner un sous-corpus constitué des parties d’entretien consacrées
à la thématique “métier” en vue de faire contraster les entretiens
suivant la tranche d’âge de l’interviewé. En effet, dans ce cas, la
structure utilisée pour définir le sous-corpus (“div”) est inférieure à
la structure servant à définir la partition (“text”).

Dans le mode avancé, l’utilisateur aura ainsi à entrer les équations
suivantes pour partitionner son corpus :

- partie 1 : \[\_.text_agecla=“20-32” & \_.div_topic=“métier”\] expand
  to div
- partie 2 : \[\_.text_agecla=“32-38” & \_.div_topic=“métier”\] expand
  to div  
  etc.

Rq. 1 : Cette partition est construite directement sur le corpus entier
mais en fait ne couvre que le sous-corpus “métier” (qui n’est pas créé
et n’apparaît pas au niveau de l’interface). On fait une exception en
construisant une partition qui ne couvre pas tout le domaine sur lequel
elle porte (corpus entier), son domaine effectif (métier) reste
implicite.

Rq. 2 : Le “expand to div” permet de construire les parties comme un
ensemble de div (et nom comme un ensemble de mots), et donc de garder la
séquence des mots à l’intérieur des div (pour les requêtes sur des
expressions).

# Le langage d’interrogation CQL et le moteur de recherche CQP<br>

### Quelle est la liste des opérateurs d’expression régulière des chaînes de caractères dans CQL&nbsp;?

#### Métacaractères
{: .no_toc }

- `.` matche n‘importe quel caractère
- `\` neutralise l‘opérateur situé à droite
- `|` alternance
- `()` regroupement
- `[…]` classe de caractères entre crochets (eg “`[aeiouy]`” pour une voyelle)
- `[^…]` ensemble complémentaire de la classe de caractères entre
  crochets, le caractère `^` joue le rôle de négation (eg “`[^aeiouy]`”
  pour un caractère qui n’est pas une voyelle)

#### Quantifieurs
{: .no_toc }

- `?` matche 0 ou 1 fois
- `*` matche 0 fois ou plus
- `+` matche 1 fois ou plus
- `{n}` matche n fois
- `{n,}` matche au moins n fois
- `{n,m}` matche entre n et m fois

#### Codes de caractères
{: .no_toc }

- `\x{CC}` caractère de valeur CC (exprimée en hexadécimal) (eg `\x{E9}` pour `é`)
- `\xCC` caractère de valeur CC (exprimée en hexadécimal)

#### Classes de caractères
{: .no_toc }

- `\d` un chiffre
- `\D` pas un chiffre
- `\w` un caractère de “mot”
- `\W` pas un caractère de “mot”
- `\s` un caractère d‘espace
- `\S` pas un caractère d‘espace
- `\h` un caractère d‘espace horizontal (eg “Espace, Tabulation…”)
- `\H` pas un caractère d‘espace horizontal
- `\v` un caractère d‘espace vertical (eg “Form Feed”…)
- `\V` pas un caractère d‘espace vertical
- `\p{Classe}` un caractère de la classe Unicode
  “Classe” (eg “`\p{Lu}`” pour un caractère majuscule)
- `\P{Classe}` pas un caractère de la classe Unicode “Classe”
- `[[:ClassePOSIX:]]` un caractère de la classe ClassePOSIX (eg
  `[[:upper:]]` pour un caractère majuscule)
- `\p{…}` classes de caractères Unicode
  - `\p{L}` lettre
  - `\p{Ll}` caractère minuscule
  - `\p{Lu}` caractère majuscule
  - `\p{Lt}` caractère de titre
  - `\p{Pd}` caractère de tiret (eg “-”, “—” …)
  - `\p{P}` caractère de ponctuation (eg “,”, “.” …)
  - `\p{Ps}` caractère de ponctuation ouvrante (eg “(”)
  - `\p{Pe}` caractère de ponctuation fermante (eg “)”)
  - `\p{Sm}` caractère de symbole mathématique (eg “~”)
  - etc. La liste complète des classes Unicode disponibles dans CQL est indiquée dans la section “The general category property for \p and \P” de la
page de documentation <https://www.pcre.org/current/doc/html/pcre2pattern.html> de la bibliothèque PCRE utilisée par CQP.
- classes de caractères POSIX ([standard](https://fr.wikipedia.org/wiki/POSIX) plus ancien qu'Unicode)
  - `alpha` n‘importe quel caractère alphabétique (usage : `[[:alpha:]]`)
  - `alnum` n‘importe quel caractère alphanumérique
  - `ascii` n‘importe quel caractère du code ASCII
  - `blank` espace ou tabulation
  - `cntrl` n‘importe quel caractère de contrôle
  - `digit` n‘importe quel chiffre décimal
  - `graph` n‘importe quel caractère imprimable, sans l’espace
  - `lower` n‘importe quel caractère minuscule
  - `print` n‘importe quel caractère imprimable, incluant l’espace
  - `punct` n‘importe quel caractère de ponctuation
  - `space` n‘importe quel caractère d’espace
  - `upper` n‘importe quel caractère majuscule
  - `word` n‘importe quel caractère de mot
  - `xdigit` n’importe quel chiffre hexadécimal

#### Références de caractères mémorisés
{: .no_toc }

- `\2` contenu du premier groupe de parenthèses mémorisé (suppose la
  présence de “..`(`..`)`..” auparavant dans l‘expression)
- `\3`
  contenu du deuxième groupe de parenthèses mémorisé
- etc.
- `\g{nom}` contenu du groupe de parenthèses mémorisé nommé “nom”
  (suppose la présence de “..`(?<nom>`..`)`..” auparavant dans
  l’expression)

Exemples :

- `([[:lower:]])([[:lower:]]).*m.*\3\2`
- `(.*)\2`
- `(?<groupe1>.*)\g{groupe1}`

### Quelle est la liste des opérateurs du langage CQL&nbsp;?

Voir la [liste de référence de la syntaxe
CQL](https://docs.google.com/document/d/1rz39LixYl6uegx35kIj6JLYbMPEOsy2ycg4JuCBZ68Y/edit?hl=fr)
en cours d’édition sur GoogleDocs.

### Quelles sont les limites de CQP&nbsp;?

- Nombre maximum de mots par corpus
  - Limite théorique : 2 milliards de mots (2 147 483 648 mots
    exactement)
  - Limites expérimentales :
    - CQP version 32-bit : ~200 à ~500 million de mots
    - CQP version 64-bit : jusqu’à 2 milliards de mots (mais les
      requêtes sont lentes)
    - TXM 0.5 : Nous n’avons pas encore effectué de tests sur de telles
      tailles de corpus, notamment parce que nous n’avons pas encore
      optimisé l’architecture vis-à-vis des très grands corpus (aucun de
      nos partenaires n’utilise de tels corpus). L’architecture actuelle
      de TXM empêche probablement de traiter des corpus de cette taille
      dans des délais raisonnables. Des corpus de plusieurs millions de
      mots sont malgré tout déjà régulièrement traités avec TXM 0.5.
- Autres limites :
  - maximum 1024 types de structures différents par corpus
  - maximum 1024 caractères dans le nom d’une propriété ou d’une
    structure
  - maximum 4096 caractères par requête CQL

\[Notamment d’après “Stefan Evert, Inside the IMS Corpus Workbench,
2008, <http://cwb.sf.net>”\]

### Combien d’espace disque prennent les index du moteur CQP&nbsp;?

Volume disque utilisé par un corpus de 100 millions de mots :

- pour des sources au format texte brut de ~400 à ~600 Mo, les index
  sont de ~800 Mo (non compressés).  
  Remarque : CQP n’a plus besoin des fichiers sources pour répondre aux
  requêtes.
- les index sont de volumes respectifs (par type d’index) :
  - mots, lemmes : ~320 à ~360 Mo (volume d’une propriété à valeurs
    ouvertes)
  - pos : ~100 à ~150 Mo (volume d’une propriété à valeurs fermées)
  - propriété booléenne : ~50 Mo

\[D’après “Stefan Evert, Inside the IMS Corpus Workbench, 2008,
<http://cwb.sf.net>”\]

### Dans une équation CQL, je voudrais pouvoir tenir compte du contexte englobant

1\) Dans une équation CQL, on peut situer les occurrences par rapport à
la frontière du début ou de la fin d’une structure donnée. Par exemple,

- la délimitation du début d’une structure div est notée `<div>`, et
- la délimitation de la fin d’une structure div est notée `</div>`.

On peut alors écrire des requêtes CQL comme :

- `<div> []` : le premier mot qui commence une div
- `[] <q> []` : le mot juste avant une structure q, et le premier mot
  qui commence la structure q
- `[] </s>` : le dernier mot d’une structure s.

2\) Le langage CQL permet de prendre en compte les valeurs des
propriétés associées à l’occurrence (par exemple sa catégorie
grammaticale ou son lemme s’ils ont été définis à l’import), mais aussi
les valeurs des propriétés des structures englobant l’occurrence (par
exemple les métadonnées associées au texte).

Soit par exemple un corpus formés d’entretiens (structure “text”)
portant une information sur la tranche d’âge de la personne interviewée
(propriété “agecla”, avec des valeurs de la forme “20-32”, “32-38”,
etc.). On peut chercher les occurrences du mot “accord” telles qu’elles
soient dans un entretien d’une personne dans la classe d’âge 38-44 ans :

~~~ bash
    [word="accord" & _.text_age="38-44"]
~~~

La syntaxe pour désigner une propriété d’une structure englobante, c’est
donc les caractères “souligné” puis “point” (\_.), suivis du nom de la
structure, suivi d’un souligné (\_), suivi du nom de la propriété dans
cette structure.

L’exemple précédent est un cas d’école pour illustrer de façon simple la
syntaxe CQL : pour le besoin décrit, on aurait en pratique plutôt
commencé par faire un sous-corpus des interviews des 38-44 ans, et lancé
dessus la requête simple

~~~ bash
    accord
~~~

Mais il y a des cas où la possibilité d’exprimer la contrainte vraiment
au niveau de l’occurrence est nécessaire. Par exemple, dans un corpus
divisé en parties (structure “div”) portant chacune un thème (propriété
“topic”), on localisera les enchaînements entre une partie thématisée
“métier” et une partie thématisée “ressources” avec l’équation suivante
:

~~~ bash
    [_.div_topic="métier"]</div><div>[_.div_topic="ressources"]
~~~

*glose* :

- *je cherche deux mots (il y a deux paires de crochets),*
- *`[_.div_topic=“métier”]` ce premier mot est contenu dans une partie
  thématisée “métier”,*
- *`</div>` c’est le mot qui termine cette partie (c’est le dernier
  avant la frontière de fin de partie),*
- *`<div>` ouvre ensuite une nouvelle partie,*
- *`[_.div_topic=“ressources”]` le deuxième mot est contenu dans une
  partie thématisée “ressources”.*

# Calculs textométriques<br>

### Comment puis-je rechercher une occurrence dans l’édition&nbsp;?

Il y a moyen de naviguer d’une occurrence à la suivante d’un mot donné,
ou plus généralement d’un motif CQL donné, en passant par la concordance
:

- faire la concordance du mot ou motif voulu.
- double-cliquer sur une des lignes de la concordance : cela ouvre un
  onglet avec l’édition du texte positionnée sur l’occurrence concernée,
  mise en évidence par surlignage (et les autres occurrences
  positionnées aux alentours sont également surlignées avec une couleur
  atténuée).
- dédoubler l’affichage pour avoir à la fois sous les yeux les lignes de
  concordance (formant une sorte de sommaire des occurrences
  disponibles) et l’édition (dans laquelle on veut naviguer).

Pour cela, utiliser le gestionnaire de fenêtres (cf. manuel) : cliquer
gauche sur le nom de l’onglet, maintenir cliqué et déplacer le curseur
vers la limite basse de la zone d’affichage des résultats ; relâcher le
bouton de la souris quand, arrivé près du bord, on voit le curseur qui
se transforme en une épaisse flèche noire orthogonale au bord, et le
contour d’une nouvelle zone qui se dessine. La fenêtre des résultats est
alors divisée en deux zones, au-dessus les lignes de concordance, en
dessous la page d’édition, et quand on double-clique sur une ligne de
concordance l’édition se positionne au niveau de l’occurrence
correspondante. La frontière séparant les deux zones peut être déplacée
: on l’attrape en cliquant dessus et en maintenant le bouton appuyé
pendant le déplacement, puis en le relâchant lorsque la frontière est à
la position souhaitée. On peut ainsi accorder une plus large place à
l’édition par exemple.

On peut préférer faire la même chose mais avec la concordance en marge
gauche de l’édition (avec une zone d’affichage étroite montrant juste
les références de localisation), en divisant verticalement la zone
d’affichage des résultats. Pour cela, on déplace l’onglet de l’édition
vers la frontière droite de la fenêtre au lieu de la frontière du bas.
Il peut être commode aussi de choisir les informations utiles pour
composer les références de localisation, par un clic gauche dans les
lignes de concordance et en choisissant la fonction “Définir le patron
des références”.

### Comment, au sein de l’édition, puis-je naviguer dans les structures internes du texte (chapitres, sections…)&nbsp;?

Les boutons de navigation dans l’édition permettent de passer à la page
ou au texte suivant ou précédent. Si l’on veut naviguer plus finement,
on peut s’aider de la concordance, cf. question précédente. En
particulier, pour se positionner au début de telle ou telle structure,
on peut faire la concordance sur le premier mot des structures
recherchées ; un double-clic sur les lignes de la concordance permet de
naviguer d’un début de structure à l’autre.

Exemples d’application :

1\) les textes du corpus sont composés de parties (codées par la
structure “div”). Pour trouver tous les débuts de partie, la requête
pour la concordance peut être de la forme :

~~~ bash
    <div>[]
~~~

*glose de la requête : je cherche le premier mot (quel qu’il soit) juste
après le début d’une structure “div”.*

2\) les textes du corpus sont composés de parties (codées par la
structure “div”) rapportées chacune à un thème (codé par la proriété
“topic”). On peut voir toutes les parties d’un thème donné (ici
“métier”) avec la requête suivante :

~~~ bash
    <div>[_.div_topic="métier"]
~~~

*glose de la requête : je cherche le premier mot juste après le début
d’une structure “div”, et la propriété “topic” de la structure “div”
dans laquelle l’occurrence est incluse vaut “métier”.* Les occurrences
trouvées sont donc tous les premiers mots de parties thématisées
“métier”.

### Comment fusionner des lignes dans un index&nbsp;?

Lorsque l’index a été calculé sur une partition, on peut le convertir en
table lexicale (sélectionner l’index dans l’arborescence des corpus et
résultats, et lui appliquer la commande “Table lexicale”).L’interface de
la table lexicale permet de fusionner des lignes : les fréquences des
lignes regroupées sont bien additionnées.

### Comment faire un index sans les mots-outils ou grammaticaux&nbsp;?

Dans un corpus **français** étiqueté avec TreeTagger, vous pouvez par
exemple utiliser l’expression CQL suivante :

~~~ bash
    [frpos="NOM|NAM|ADJ|VER.*"  & frlemma!=".*être.*|avoir|faire|pouvoir|devoir|vouloir|falloir|aller|dire|savoir"]
~~~

Dans un corpus **anglais** étiqueté avec TreeTagger, vous pouvez par
exemple utiliser l’expression CQL suivante :

~~~ bash
    [enpos="N.*|JJ.*|V.*"  & enlemma!="be|have|do|say|go|make|other" & word!="."]
~~~

Dans un corpus **espagnol** étiqueté avec TreeTagger, vous pouvez par
exemple utiliser l’expression CQL suivante :

~~~ bash
    [((espos="N.*" & espos!="NEG") | espos="ADJ" | espos="VL.*") & eslemma!="ser|hacer|tener|saber|decir|ir|ir\|.*" & word!='…|“|”|–']
~~~

### Quelle différence y a-t-il entre les options ‘cumulatif’ et ‘densité’ de la commande progression&nbsp;?

La différence est dans la perception de la densité :

- cumulatif montre une marche par occurrence -\> le plus précis, ça
  s’appelle l’histogramme cumulatif ;
- densité montre l’histogramme lissé des occurrences -\> moins précis,
  mais plus intuitif.

### A quoi correspond l’indice de spécificité&nbsp;?

Le calcul de spécificités se lance sur une partition : le corpus est
divisé en plusieurs parties. On veut évaluer la sur-utilisation ou la
sous-utilisation de mots ou de motifs dans chacune des parties.

La **fréquence** de ces mots ou motifs serait un premier indicateur
intéressant, mais il est trop sensible à la différence de taille entre
parties. Une règle de trois, qui rapporte la fréquence à la taille de la
partie et nous donnerait une **fréquence relative**, semble être une
bonne solution. On montre cependant, en modélisant mathématiquement la
répartition des mots entre les différentes parties, qu’on a un
indicateur plus juste avec le calcul des **spécificités** [(Lafon
1980)](http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1980_num_1_1_1008),
utilisant une loi hypergéométrique. C’est le calcul implémenté dans TXM.
Il permet de mieux rendre compte des écarts de fréquence (entre mots
rares et mots très courants) et des écarts de taille de parties.

Le calcul de la spécificité d’un mot dans une partie repose sur les
quatre grandeurs suivantes :

- la fréquence *f* d’apparition du mot au sein de la partie ;
- la fréquence totale *F* d’apparition du mot dans l’ensemble du corpus
  ;
- la taille *t* de la partie (nombre total de mots dans la partie) ;
- la taille *T* du corpus complet (nombre total de mots).

Il fournit une probabilité. Un indice de spécificité de « +3 » signifie
que le mot avait moins d’une chance sur mille (3 → 1 suivi de 3 zéros =
1 000) d’apparaître dans la partie avec une fréquence *f* aussi élevée
(indice positif → surreprésentation). Autrement dit, si la distribution
du mot était aléatoire (selon une loi hypergéométrique), dans 99,9% des
cas on aurait observé une fréquence *f* plus faible ; on a donc une
fréquence *f* plus élevée de façon statistiquement significative avec un
seuil de risque de 0,1%.

Et conventionnellement, un indice de spécificité négatif pointe de la
même façon une sous-représentation. Ainsi, un indice de spécificité de «
-6 » signifie que le mot avait moins d’une chance sur un million (6 → 1
suivi de 6 zéros = 1 000 000) d’apparaître dans la partie avec une
fréquence *f* aussi faible (indice négatif → sous-représentation). En
termes de pourcentage, cela revient à dire que si la distribution du mot
était aléatoire (selon une loi hypergéométrique), dans 99,9999% des cas
on aurait eu une fréquence *f* plus forte, soit une significativité
statistique avec un seuil de risque de 0,0001%.

Lorsque l’indice atteint une valeur telle qu’il dépasse les capacités de
la machine, on le représente dans les tableaux conventionnellement par
la valeur 1000, cf. question suivante.

### J’obtiens des spécificités de 1000 : est-ce normal&nbsp;?

Le module actuellement utilisé pour le calcul des spécificités rencontre
assez vite la limite des capacités de représentation de la machine, pour
les valeurs de spécificité les plus hautes (ce qui correspond à des
probabilités très faibles, très proches de zéro). **Lorsque le calcul
sort des limites de la machine, alors on a choisi d’indiquer
conventionnellement “1000” comme valeur de spécificité** -valeur
nettement au-dessus de celles qu’on arrive à obtenir par le calcul, pour
éviter tout risque de confusion.

=\> Donc : les valeurs de spécificité de 1000 ne correspondent pas à une
probabilité de 1 sur 1 suivi de 1000 zéros, mais à un indice supérieur à
tous ceux qui ont été calculés, mais indéterminé pour des raisons de
limites du calcul.

Les mots dotés d’un indice de 1000 sont interprétables et utilisables :
pour une partie donnée, les mots d’indice 1000 sont les mots les plus
spécifiques de la partie. On considère aussi comme mots spécifiques de
la partie les mots jusqu’à l’indice 3 environ. Attention, les mots
d’indice 1000 ne sont pas hiérarchisés entre eux, ils sont “ex-aequo”
dans l’état actuel de précision du calcul, il ne faut pas interpréter
l’ordre dans lequel ils sont affichés comme un ordre de spécificité
(c’est un calcul plus précis qui permettrait de les hiérarchiser).

L’indice conventionnel de 1000 est particulièrement gênant quand on
trace des diagrammes en bâtons des spécificités d’un ou plusieurs mots
donnés (commande contextuelle, obtenue par clic droit sur une sélection
de ligne(s) dans le tableau des spécificités). C’est pourquoi, depuis la
version 0.7 de TXM, une option du calcul des spécificités (menu
Outils/Préférences/TXM/Utilisateur/Spécificités) permet de **choisir la
valeur plafond** des spécificités : on peut ainsi choisir une valeur
plus proche -légèrement au-dessus- des spécificités maximales obtenues
par le calcul, pour ne pas “écraser” le diagramme en bâtons. Si
certaines spécificités calculées dépassent cette valeur plafond, alors
elles sont ramenées à ce plafond, et se confondent alors avec les
indices ayant dépassé les limites du calcul : il ne peut pas y avoir
d’indice supérieur au plafond fixé.

Pour utiliser ce paramètre, on conseille donc de fixer d’abord le
plafond très haut (ex. 1000), d’observer tous les indices obtenus (en
triant les colonnes les unes après les autres), de noter la plus haute
valeur d’indice (par ex. 15), et de choisir un plafond qq points
au-dessus (par ex. 20). Il faut bien sûr veiller à distinguer et
indiquer l’interprétation particulière qui doit être faite de cet indice
plafonné dans la communication faite des résultats.

### Peut-on calculer des spécificités sur des motifs&nbsp;?

Il y a deux manières de comprendre cette question, mais dans les deux
cas on utilisera pour y répondre la possibilité de calculer les
spécificités non pas directement pour l’ensemble des mots du corpus
(commande Spécificités lancée sur une partition), mais via
l’enchaînement *Index (sur partition) -\> Table lexicale -\>
Spécificités*, ce qui permet de définir souplement la table sur laquelle
lancer le calcul de spécificités.

**Premier cas de figure** : je veux obtenir les spécificités de toutes
les réalisations d’un motif, par exemple de toutes les formes de type
NOM+ADJ ou ADJ+NOM.

- calculer un index sur la partition , et mettre en requête l’équation
  CQL qui correspond au motif dont on veut étudier les différentes
  réalisations ; penser à utiliser le paramètre “propriétés” pour
  choisir la manière de décompter les réalisations (en graphies, en
  lemmes, en catégories grammaticales…)
- transformer l’index en table lexicale (clic droit sur l’icone de
  l’index dans la vue corpus, en marge gauche de la fenêtre) ; on a
  alors le choix entre les options suivantes (décider en fonction du
  sens voulu pour l’analyse) :
  - “Utiliser toutes les occurrences” : les marges de la table lexicale
    seront calculées avec tous les mots du corpus, autrement dit chaque
    spécificité sera calculée en prenant comme contraste le corpus dans
    son ensemble ;
  - “Occurrences d’index” : les marges ne seront calculées qu’à partir
    des occurrences correspondant au motif, autrement dit le corpus est
    “réduit” à ce qui concerne le motif, la spécificité contraste ce qui
    se passe pour une réalisation du motif par rapport aux autres
    réalisations attestées (et seulement par rapport à elles).
- calculer les spécificités sur cette table lexicale.

**Deuxième cas de figure** : je veux obtenir la spécificité d’un élément
donné (mot, expression composée, construction…).

- calculer un index sur la partition, et mettre en requête l’équation
  CQL qui correspond au motif dont on veut calculer la spécificité ;
- transformer l’index en table lexicale (clic droit sur l’icone de
  l’index dans la vue corpus, en marge gauche de la fenêtre), et dans la
  boîte de dialogue qui s’affiche choisir l’option “Utiliser toutes les
  occurrences” ;
- la table lexicale s’affiche
- fusionner toutes les lignes de la table lexicale sauf la ligne
  \#RESTE#
- calculer les spécificités sur la table lexicale (qui contient 2
  lignes) : l’indice de spécificité voulu est celui de la ligne
  correspondant au motif (l’indice pour le \#RESTE# n’a a priori pas
  d’intérêt).

*Remarque* : cette procédure s’appuie sur une propriété importante du
calcul des spécificités : la spécificité d’un mot dans une partie est
déterminée par quatre paramètres : la fréquence *f* du mot dans la
partie, la fréquence totale *F* du mot dans le corpus, la taille *t* de
la partie, la taille *T* du corpus. Par conséquent, l’indice de
spécificité ne dépend pas du détail des fréquences des autres mots, qui
peuvent être aussi bien représentés de façon cumulée sur une seule ligne
(la ligne \#RESTE#) si l’on ne s’intéresse pas spécialement à eux.
L’indice de spécificité du mot dans le tableau en deux lignes est
exactement le même que l’indice du mot dans un tableau avec en lignes
tous les mots du corpus.

### Quel est le calcul utilisé pour les cooccurrences&nbsp;?

Le calcul des cooccurrences dans TXM est basé sur le calcul des
spécificités.

Pour le mot ou motif indiqué en requête (le “pivot” des cooccurrences),
et pour la définition de contexte choisie via les paramètres (contexte
en nombre de mots ou en structure, ex. phrase(s) ou paragraphe), on
construit l’ensemble de tous les contextes des occurrences du pivot. Cet
ensemble définit une partie du corpus.

On calcule alors les spécificités de cette partie (constituée de tous
les contextes), par rapport à l’ensemble du corpus : le calcul met en
évidence les mots qui sont statistiquement sur-représentés dans la
partie, c’est-à-dire -vue la manière dont a été constituée la partie-
les mots qui sont sur-représenté au voisinage du pivot, qui semblent
statistiquement attirés par ce pivot.

Les indices de cooccurrence s’interprètent donc exactement comme les
indices de spécificité : un indice de 5 par exemple signifie qu’il y a
moins d’une chance sur 100 000 que le cooccurrent ait une fréquence
aussi élevée dans les contextes du pivot (indice de 5 -\> 1 suivi de 5
zéros -\> 100 000), si la répartition des mots était aléatoire.

**Cas particulier**

En l’état actuel de la fonctionnalité, les contextes de cooccurrences
situés à cheval entre deux textes ne sont pas pris en compte comme
contextes du pivot dans le calcul. Par exemple, le pivot est le 3e mot
du texte, et la fenêtre est de 10 mots de part et d’autre du pivot : le
contexte gauche déborde du texte, tout le contexte est alors considéré
comme ne faisant pas partie des voisinages du pivot. En règle générale
le cas se présente peu ou pas du tout, et l’ordre de grandeur des
indices est conservé, -par contre le détail du décompte n’est pas exact,
il peut manquer quelques occurrences dans les co-fréquences.

Les cas où cela peut devenir gênant sont le cas où le corpus est composé
de textes (très) courts (relativement à la taille des empans), ou encore
le cas où le pivot a une affinité avec le début ou la fin du texte. En
pratique, on peut vérifier avec une concordance que le chevauchement de
la fenêtre ne se présente pas trop souvent. Préférer si besoin (et si
possible) un contexte en structure : par exemple, si les phrases ont été
codées dans le corpus, “contexte = phrase” au lieu de “contexte = 10
mots avant et 10 mots après”, car on peut s’assurer ainsi que le
contexte ne chevauche jamais une frontière textuelle.

### Comment lancer une AFC&nbsp;?

Dans TXM, l’analyse factorielle s’applique pour le moment à une table
croisant les parties d’une partition et des mots (ou des réalisations de
motifs CQL). Il y a deux voies principales pour lancer l’AFC, selon
qu’on veut focaliser ou non le choix des mots.

1\) Première voie : pas de focalisation particulière sur certains mots

L’AFC est appliquée sur le Tableau Lexical Entier (éventuellement
seuillé en fréquence ou en nombre de lignes), comme on le fait par
exemple dans Lexico 3. Dans TXM, on fait cela en sélectionnant une
partition et en appelant directement le calcul d’AFC (par le menu
contextuel clic droit, ou par le menu principal “Outils”, ou par l’icône
correspondante dans la barre de fonctions).

2\) Deuxième voie : choix des mots à prendre en compte dans le calcul

Il s’agit d’appliquer l’AFC à une table croisant les parties avec une
sélection de mots, comme on le fait par exemple dans Hyperbase avec la
fonction liste. Le calcul de l’AFC requiert alors l’enchaînement de
trois fonctionnalités : Index, puis Table lexicale, puis AFC.

Sélectionner la partition et demander la fonction Index. La table est
vide initialement, on la remplit en choisissant une propriété (veut-on
une table de graphies, de lemmes, de pos…) et en définissant un filtre
sélectionnant les occurrences à considérer. On n’a pas la possibilité
d’ajouter des lignes peu à peu (le lancement d’une nouvelle requête
efface et remplace les lignes précédentes), mais on peut obtenir le même
résultat en construisant une requête plus longue : au lieu d’entrer
successivement “équation CQL 1” puis “équation CQL 2” puis “équation CQL
3” etc., on entre directement :

~~~ bash
    (équation CQL 1) | (équation CQL 2) | (équation CQL 3) etc.
~~~

Les parenthèses délimitent chaque sous-sélection ; le trait vertical
(caractère “pipe”) signifie un opérateur OU (non exclusif : il suffit
qu’une occurrence corresponde à l’une des équations CQL 1 ou 2 ou 3 pour
qu’elle soit sélectionnée). Si les sous-sélections se trouvent
sélectionner certaines occurrences en commun, le calcul les dédoublonne
(chaque occurrence du corpus n’est comptée qu’une fois).

Une fois obtenues les lignes voulues, on doit convertir l’index en table
lexicale pour pouvoir lui appliquer le calcul d’AFC ou de spécificités.
Pour cela, on utilise le menu contextuel (clic droit sur l’index
correspondant dans l’arborescence des corpus, à gauche) ou l’icône de la
barre en haut de la fenêtre pour lancer sur cet index la fonctionnalité
table lexicale, qui effectue la conversion. L’interface de la table
lexicale permet si on le souhaite d’affiner la table obtenue par
l’index. Des paramètres permettent de retailler le tableau :

- en fusionnant ou supprimant des colonnes (parties)
- en fusionnant ou supprimant des lignes (mots)
- en filtrant les lignes en éliminant des basses fréquences : soit par
  un seuil sur une fréquence minimale, soit en indiquant le nombre de
  lignes qu’on veut garder (par exemple les 300 mots les plus
  fréquents).

Une fois le tableau souhaité obtenu, on appelle l’AFC sur la table
lexicale, en sélectionnant la table lexicale dans l’arborescence Corpus
(à gauche de la fenêtre) et sélectionnant AFC dans le menu (contextuel
ou principal) ou en cliquant sur l’icône AFC dans la barre d’outil.

# Statistiques & R<br>

### Sous Mac, quand je veux afficher un graphique, TXM se ferme sans prévenir ou affiche un message d’erreur

Message :

~~~ text
    Premature end of file.

    ---

    org.apache.batik.dom.util.SAXIOException: Premature end of file.
        at org.apache.batik.dom.util.SAXDocumentFactory.createDocumentSAXDocumentFactory.java:437)
        at org.apache.batik.dom.util.SAXDocumentFactory.createDocumentSAXDocumentFactory.java:439)
            ...
~~~

Sous Mac OS X 10.9 et ultérieur, certains fichiers utilisés par R pour
produire des visualisations graphiques (Informations sur une partition,
histogramme de Spécificités, plans factoriels) ne sont plus installés
par défaut. Il faut alors installer manuellement le logiciel ‘xQuartz’ à
l’aide de l’installeur téléchargeable sur le site officiel
<http://xquartz.macosforge.org>. Il faudra se reconnecter à son compte
personnel d’ordinateur pour que les changements prennent effets.

### Sous Ubuntu 14.04 mis à niveau depuis Ubuntu 12.04, l’AFC ne fonctionne pas

Lors du calcul de l’AFC, le message suivant apparait dans la console :

    Calcul de l'analyse factorielle des correspondances
    ** Impossible de charger la librairie 'ca'org.txm.stat.engine.r.RException: ** Erreur R : "Error : le package ‘FactoMineR’ a été compilé avant R 3.0.0 : veuillez le réinstaller, s'il-vous-plait
    " 
     lors de l'évaluation de :library(FactoMineR)

La mise à niveau d’Ubuntu 12.04 vers 14.04 a rendu les packages R
pré-existants obsolètes. Pour être en mesure d’utiliser ces packages à
nouveau, le plus simple est de les désinstaller puis de les réinstaller
en suivant les instructions suivantes :

- A\) dans un terminal, supprimer tous les packages R installés avec la
  commande
      sudo rm -r /usr/local/lib/R/site-library/*
- B.1) réinstaller TXM qui réinstallera automatiquement tous les
  packages dont il a besoin  
  **ou bien**  
  B.2) installer manuellement tous les packages R nécessaires au
  fonctionnement de TXM (voir les instructions suivantes),  
  dans les 2 cas l’opération est assez longue car R va devoir
  réinstaller les nombreux packages R dépendants.  
  Installation manuelle des packages R nécessaires au fonctionnement de
  TXM :
  - dans un terminal, lancer R avec la commande
  
  ~~~ bash
        R
  ~~~

  - installer les packages nécessaires au fonctionnement de TXM avec les
    commandes R suivantes :
    
 ~~~ r
       install.packages("lme4", dependencies=TRUE, repos="http://cran.rstudio.com");
        install.packages("pbkrtest", dependencies=TRUE, repos="http://cran.rstudio.com");
        install.packages("alr4", dependencies=TRUE, repos="http://cran.rstudio.com");
        install.packages("MASS", dependencies=TRUE, repos="http://cran.rstudio.com");
        install.packages("nnet", dependencies=TRUE, repos="http://cran.rstudio.com");
        install.packages("car", dependencies=TRUE, repos="http://cran.rstudio.com");
        install.packages("ellipse", dependencies=TRUE, repos="http://cran.rstudio.com");
        install.packages("leaps", dependencies=TRUE, repos="http://cran.rstudio.com");
        install.packages("lattice", dependencies=TRUE, repos="http://cran.rstudio.com");
        install.packages("cluster", dependencies=TRUE, repos="http://cran.rstudio.com");
        install.packages("scatterplot3d", dependencies=TRUE, repos="http://cran.rstudio.com");
        install.packages("FactoMineR", dependencies=TRUE, repos="http://cran.rstudio.com");
        install.packages("Rserve", dependencies=TRUE, repos="http://cran.rstudio.com");
        install.packages("wordcloud", dependencies=TRUE, repos="http://cran.rstudio.com");
        install.packages("textometry", dependencies=TRUE, repos="http://cran.rstudio.com");
        q()
        y
~~~

### Où trouver de l’information sur R&nbsp;?

- Groupe des utilisateurs du logiciel R (CIRAD) :
  <http://forums.cirad.fr/logiciel-R>
- semin-R : <http://rug.mnhn.fr/semin-r>

### Comment mettre à jour le package R ‘textometry’&nbsp;?

Pour les versions antérieures à TXM 0.7.7, il faut passer par
l’environnement R :

- Lancer R
  - Sous Windows, il faut exécuter en tant qu’administrateur (clic droit
    pour afficher le choix) : C:\Program Files\TXM\R\bin\R.exe
  - Sous Mac OS X, il faut exécuter l’application /Applications/R
  - Sous Linux, il faut exécuter l’application R (ou taper “sudo R” dans
    un Terminal)
- rentrer les commandes suivantes
~~~ r
      install.packages("textometry", dependencies=TRUE, repos="http://cran.rstudio.com/");
      q()
      y
~~~
- Enfin redémarrer TXM.

### Quelles instructions R pour (ré-)installer les packages utilisés par TXM&nbsp;?

~~~ r
    # packages directly used by FactoMineR
    install.packages("lme4", dependencies=TRUE, repos="http://cran.rstudio.com/");
    install.packages("pbkrtest", dependencies=TRUE, repos="http://cran.rstudio.com/");
    install.packages("alr4", dependencies=TRUE, repos="http://cran.rstudio.com/");
    install.packages("MASS", dependencies=TRUE, repos="http://cran.rstudio.com/");
    install.packages("nnet", dependencies=TRUE, repos="http://cran.rstudio.com/");
    install.packages("car", dependencies=TRUE, repos="http://cran.rstudio.com/");
    install.packages("ellipse", dependencies=TRUE, repos="http://cran.rstudio.com/");
    install.packages("leaps", dependencies=TRUE, repos="http://cran.rstudio.com/");
    install.packages("lattice", dependencies=TRUE, repos="http://cran.rstudio.com/");
    install.packages("cluster", dependencies=TRUE, repos="http://cran.rstudio.com/");
    install.packages("scatterplot3d", dependencies=TRUE, repos="http://cran.rstudio.com/");
    # packages directly used by TXM
    install.packages("FactoMineR", dependencies=TRUE, repos="http://cran.rstudio.com/");
    install.packages("Rserve", dependencies=TRUE, repos="http://cran.rstudio.com/");
    install.packages("wordcloud", dependencies=TRUE, repos="http://cran.rstudio.com/");
    install.packages("textometry", dependencies=TRUE, repos="http://cran.rstudio.com/");
    q()
    y
~~~

### Comment installer un package R nécessaire au fonctionnement de TXM qui demande une version de R supérieure à celle de TXM&nbsp;?

Suivant les systèmes d’exploitation, le R de TXM correspond à :

- Sous Mac OS X /Applications/TXM/R/bin/R
- Sous Windows C:\Program Files\TXM\R\bin\R
- Sous Ubuntu /usr/lib/TXM/R/bin/R

##### Solution A : à partir du package binaire
{: .no_toc }

Trouver puis installer un package binaire (et ses dépendances) de
version inférieure compatible avec le R de TXM.

Il faut alors :

1.  trouver la date de release du R de TXM : Par exemple, la commande
~~~ r
        R --version
~~~

    donne :
~~~ text
        R version 3.3.2 (2016-10-31) -- "Sincere Pumpkin Patch"
~~~

2.  Aller à la page des binaires pour le système et trouver l’archive du
    package :
    -  Par exemple, avec Mac OS X,
      <https://cran.r-project.org/bin/macosx/mavericks/contrib/3.3>
    -  Par exemple, avec Windows,
      <https://cran.r-project.org/bin/windows/contrib/3.3>
3.  installer le package avec la commande
~~~ r
        R CMD INSTALL package.tar.gz
~~~
    ou
~~~ r
        R CMD INSTALL package.zip
~~~

##### Solution B : à partir du package source
{: .no_toc }

Si le package n’est pas disponible dans sa version binaire ou si vous
utilisez Linux. Il faut alors compiler le package :

1.  installer l’environnement de compilation de package
    - Windows : il faut installer R Tools :
      <https://cran.r-project.org/bin/windows/Rtools>
    - Mac OS X : il faut installer l’environnement :
      <https://cran.r-project.org>
2.  trouver la date de release du R installé : Par exemple, la commande
~~~ r
        R --version
~~~
    donne :
~~~ text
        R version 3.3.2 (2016-10-31) -- "Sincere Pumpkin Patch"
~~~
3.  trouver la liste des anciennes sources du package. Par exemple pour
    FactoMineR :
    <https://cran.r-project.org/src/contrib/Archive/FactoMineR>
4.  puis télécharger la première archive livrée après la date de release
    du R et vérifier dans le contenu de l’archive que la dépendance à R
    correspond. Par exemple, le fichier DESCRIPTION de l’archive
    “FactoMineR_1.31.4.tar.gz” indique la dépendance
~~~ texte
        Depends: R (>= 2.12.0)
~~~
    , donc l’installation de ces sources pourra fonctionner avec un R de
    version 3.3.2.
5.  répéter l’opération pour tous les packages dont il dépend.
6.  installer tous les packages avec la commande
~~~ r
        R CMD INSTALL package.tar.gz
~~~

# Interface<br>

### Comment afficher les messages de debug de TXM&nbsp;?

Pour afficher plus de messages d’exécution dans TXM il faut :

- se rendre dans la page de préférence `TXM > Avancé`
- sélectionner le niveau de détails du journal “WARNING”
- sélectionner l’option “Afficher la stacktrace”
- sélectionner l’option “Ajouter des commentaires techniques”

### Comment afficher encore plus de messages de debug de TXM sous Linux&nbsp;?

Lancer TXM en ligne de commande avec les options suivantes :

~~~ bash
UBUNTU_MENUPROXY=0 exec /usr/lib/TXM/TXM/TXM -run -debug -log -noexit -noredirection --launcher.ini "$HOME/TXM/.txm/TXM.ini" -data "$HOME/TXM/.txm/data" -user "$HOME/TXM/.txm/user" -install "/usr/lib/TXM/TXM"
~~~

### Comment disposer de plus de place pour l’affichage des résultats&nbsp;?

On peut très simplement profiter de tout l’espace de la fenêtre TXM pour
afficher les résultats, en double-cliquant sur le nom de l’onglet : les
zones marginales de navigation et de messages se retirent au profit de
la zone des résultats. Pour revenir à l’affichage des trois zones,
double-cliquer à nouveau sur le nom de l’onglet.

### Peut-on afficher simultanément, côte à côte, plusieurs résultats&nbsp;?

Oui, on utilise pour cela les facilités du gestionnaire de fenêtre (cf.
manuel).

Le principe est de cliquer gauche sur l’onglet d’un des résultats
concernés, de maintenir cliqué tout en déplaçant le curseur vers le bord
de la zone vers lequel on veut afficher ce résultat (par ex. à droite ou
en bas), et de relâcher le bouton de la souris quand le curseur devient
une flèche épaisse touchant le bord et que se dessine le contour d’une
nouvelle zone occupant une moitié de la zone actuelle. La taille des
deux zones ainsi obtenues peut être ajustée en déplaçant la frontière
(cliquer sur la frontière pour la saisir et la déplacer). La manoeuvre
peut être répétée jusqu’à obtention du nombre et de la disposition des
zones voulus.

### La fenêtre de mon TXM est encombrée de zones grises vides que je ne peux pas fermer, comment m’en débarrasser&nbsp;?

Si l’on quitte TXM avec une fenêtre de résultats divisée en plusieurs
zones, au démarrage suivant TXM présente la fenêtre résultats déjà
divisée en plusieurs zones vides, et il n’y a pas de petite croix en
haut à droite ni de commande pour les fermer.

Pour se débarrasser de ces zones vides :

- lancer n’importe quelle commande qui affiche un résultat (par exemple
  un Lexique sur un corpus), qui occupera dans l’une des zones
  disponibles.
- cliquer sur l’onglet du haut de la zone de ce résultat et le
  glisser-déposer vers le haut d’une zone voisine vide (à la place pour
  les onglets) de façon à le transférer dans cette autre zone. En se
  libérant de son contenu, la zone de départ se ferme, on a donc une
  zone de moins.
- répéter la manoeuvre en déplaçant le résultat de zone en zone pour que
  se ferme successivement chaque zone quittée, jusqu’à ce que toutes les
  zones vides soient fermées.
- on peut bien sûr alors fermer ou supprimer le résultat lui-même si on
  n’en a plus besoin.  
  &nbsp;

Deux remarques :

- C’est donc une bonne habitude, avant de quitter TXM, de fermer tous
  les onglets affichant des résultats (on peut le faire de façon
  groupée, par exemple avec la commande `Affichage > Fermer toutes les fenêtres de résultats` ; ou clic droit sur un onglet et commande
  “Fermer tout” / “Close all”, pour tous les onglets superposés dans une
  zone). C’est plus simple en fin de session qu’au début.
- Si l’on préfère que le double-clic sur une ligne de concordance ne
  scinde pas automatiquement la fenêtre de résultats mais ouvre
  simplement un nouvel onglet à la suite, au premier plan, cela est
  possible en réglant une préférence de l’Édition : menu `Édition > Préférences > TXM > Utilisateur > Édition` (à la fin de la liste des
  commandes), régler le paramètre “Position de la nouvelle fenêtre à
  ”OVER“, puis valider (bouton ”Appliquer et fermer” / “Apply and
  Close”). Ce réglage est disponible à partir de TXM 0.8.3.  
  &nbsp;

### Mac OS X et le message d’erreur “LookupViewService”

Depuis la version 10.10.4, une boite de dialogue “Rapport de problème
pour LookupViewService” peut s’afficher en cas de clic sur l’entête d’un
tableau de TXM.

<img
src="https://forge.cbp.ens-lyon.fr/redmine/attachments/download/1241"
data-query="?width=500px"
alt="https://forge.cbp.ens-lyon.fr/redmine/attachments/download/1241" />

En attendant qu’Apple fournisse une correction pour ce bug, on peut :

- soit ne pas déclenché le mécanisme “Lookup” en appuyant moins fort sur
  le trackpad quand on sélectionne une entête de tableau TXM
- soit désactiver manuellement le mécanisme “Lookup” qui provoque cette
  erreur dans les préférences systèmes de Mac OS X : dans la page
  “Trackpad”, section “Pointer & cliquer”, il faut décocher “Rechercher”
  (en : “Look up”).

### Comment récupérer une zone de la fenêtre TXM, par exemple la zone Console si on l’a fermée&nbsp;?

En général, en bas à gauche de la fenêtre TXM on a une petite icône avec
un ‘+’ (plus) jaune, qui permet d’ouvrir d’autres vues. Après avoir
cliqué sur l’icône, une fenêtre s’ouvre avec une arborescence où
sélectionner les vues à afficher (celles qui sont déjà affichées sont en
grisé). La Console se trouve dans la rubrique “General”.

Sinon le sous-menu “Affichage” / “Vues” donne directement accès à
l’ouverture de différentes vues fermées. Le sous-menu “Affichage” /
“Vues” / “Autres…” donne accès à toutes les vues disponibles. Par
exemple, l’affichage de la Console est accessible par le sous-menu
“Affichage” / “Vues” / “Autres…” / “General” / “Console”.

# Exports<br>

### Comment ouvrir dans un tableur l’export d’un tableau de résultats de TXM&nbsp;?

Des réglages peuvent être nécessaires du fait de la diversité des
environnements logiciels, qui ont des conventions différentes. En gros,
il s’agit que TXM et le tableur s’entendent sur les choix de codage,
pour les caractères (notamment les accents) et pour la délimitation des
colonnes. Il y a donc deux stratégies possibles : 1) on règle TXM pour
qu’il produise des résultats sous une forme à laquelle l’environnement
est habitué, ou 2) on indique au tableur utilisé les caractéristiques du
fichier produit par TXM, s’il ne le traite pas automatiquement
correctement.

**Première stratégie : adaptation de TXM à l’environnement**

La question ne se pose généralement pas pour linux, où tout se passe
bien :-).

Pour Windows et mac, régler les paramètres de l’export :

- dans le Menu Outils sélectionner Préférences, puis naviguer dans
  l’arborescence : TXM / Utilisateur / Export, et régler les différents
  champs :
- encodage des fichiers d’export : c’est UTF-8 par défaut, on peut
  tenter windows-1252 pour Windows, et pour mac l’encodage x-MacRoman
  (ou x-MacRomania ?)
- laisser les autres réglages en l’état (séparateur de colonne
  point-virgule, séparateur de texte guillemet) et n’essayer d’autres
  valeurs que si cela paraît nécessaire après un test d’export, cf.
  ci-après.

Faire un test : lancer un nouvel export d’un résultat, et ouvrir le
fichier dans un tableur :

- si les caractères accentués passent mal, soit régler sur un autre
  encodage et refaire le test, soit revenir sur UTF-8 (c’est l’encodage
  le plus puissant) et passer à la stratégie n°2 (indiquer au tableur
  les caractéristiques du fichier produit par TXM) ;
- si tous les résultats sont dans la première colonne c’est que le
  séparateur de colonne n’a pas été reconnu, essayer alors un autre
  séparateur de colonnes (la virgule).

**Deuxième stratégie : faire que le tableur prenne en compte les
caractéristiques du fichier produit par TXM**

Si vous aviez fait des essais de réglage des paramètres de l’export,
commencer par rétablir les valeurs par défaut :

- dans le Menu Outils sélectionner Préférences, puis naviguer dans
  l’arborescence : TXM / Utilisateur / Export
- Encodage des fichiers d’export : UTF-8
- Colonnes séparées par : point-virgule
- Séparateur de texte : guillemet

Ensuite, vous exportez un résultat, ce qui crée un fichier.

1\) Pour ouvrir le fichier dans openoffice Calc :

- nommer le fichier avec l‘extension .csv ;
- double-cliquer dessus
  pour l’ouvrir avec Calc ;
- régler si besoin les paramètres d’import :
  - Encodage des caractères : Unicode (UTF-8) (ou en tout cas la même
  chose que ce qui a été réglé dans les préférences de l’export si vous
  l’avez modifié) ;
  - Langue : Anglais (USA) (ou toute langue qui
  utilise le point ’.’ pour représenter les nombres décimaux ou réels -
  comme ‘3.1415’)
  - séparé par : point-virgule ; séparateur de texte : ”
  - type de colonne : texte pour les colonnes avec des mots, anglais US
    pour les colonnes avec des nombres (pour éviter que Calc ne
    reconnaisse abusivement des dates).

2\) Pour ouvrir le fichier dans microsoft office Excel :

*stratégie n°1* (testée dans un office 2003 sous windows)

- commencer par lancer excel.
- Passer par les menus pour ouvrir le fichier résultats de TXM : `Données
  > Données externes > Importer des données, et là naviguer jusqu’au
  fichier .csv résultat de TXM et valider ;
  - Cela lance un assistant d’importation de texte qui me permet de
    faire tous les bons réglages (encodage, séparateur de colonne,
    etc.), cf. étapes décrites dans la stratégie n°2 ci-après.

*stratégie n°2*

- nommer le fichier avec l’extension .txt ;
- lancer excel ; dans excel, faire Fichier/Ouvrir et sélectionner le
  fichier (régler “types de fichier” à “Tous les fichiers \*.\*” pour
  pouvoir le voir).
- on peut alors régler les paramètres d’import :
  - étape 1 : fichier “délimité”
  - étape 2 : séparateurs : point-virgule pour TXM 0.6 ; identificateur
    de texte : ”
  - étape 3 : format des données en colonne : choisir Texte pour la
    première, et dans les réglages “Avancé” choisir le point comme
    séparateur de décimales et décocher le “signe moins à la fin des
    nombres négatifs”.

### Dans mon export de spécificités, je n’ai pas les mêmes valeurs d’indice que dans le résultat affiché, est-ce normal&nbsp;?

Les valeurs des indices de spécificités sont arrondies pour l’affichage
dans TXM, et plus détaillées (décimales et non plus entières) dans
l’export.

### Je ne comprends pas le tableau obtenu par l’export des spécificités (TXM 0.5-)

En TXM 0.5 il y a effectivement plusieurs différences importantes entre
le tableau résultat affiché dans TXM et le tableau résultat exporté :

      * l'ordre des colonnes est l'ordre alphabétique pour le résultat dans TXM, mais pour l'export il est arbitraire et est identique à celui des colonnes dans toutes les autres fonctions sur la même partition (résultat d'index, ou affichage de diagramme en bâtons, ou table lexicale, etc.) ;
      * dans le tableau exporté, il n'y a plus la fréquence totale de chaque mot, en revanche il y a l'indication de la fréquence locale dans chaque partie. La fréquence globale peut être rétablie en utilisant les fonctions du tableur pour insérer une colonne additionnant toutes les colonnes des fréquences locales.

Une fois ces difficultés gérées, il peut être pratique aussi de chercher
toutes les valeurs “Infinity” et les remplacer par 1000 pour avoir des
colonnes homogènes, tout en nombres.

Mais l’export des spécificités est beaucoup plus clair avec la version
0.6 de TXM (on retrouve les mêmes colonnes que dans l’affichage, avec le
même ordre, et avec leur nom en entête), cela peut valoir le coup de
changer de version de TXM !

### Comment récupérer mes corpus au format TXM binaire pour pouvoir les communiquer à un collègue&nbsp;?

Dans TXM 0.5 ou versions précédentes, il s’agit simplement de copier le
répertoire créé dans le répertoire personnel de TXM quand le corpus a
été importé. D’après le manuel ([section 3.4.2 Importer, exporter,
charger des
corpus](http://textometrie.sourceforge.net/doc/refman/ManueldeReferenceTXM0.5_FR14.xhtml#toc36)-)
le répertoire en question est situé à \$HOME/TXM/corpora/\<nom du
corpus\>, où \$HOME représente le chemin du répertoire personnel de
l’utilisateur.

Il suffit de transmettre tel quel ce répertoire (dans une archive
compressée souvent) pour pouvoir être re-chargé directement.

A partir de la version 0.6, c’est plus direct : il suffira d’utiliser la
commande d’export de corpus, qui génère directement une archive
compressée.

### Comment alléger un corpus au format TXM binaire&nbsp;?

On peut alléger un corpus binaire en n’y laissant que les répertoires
suivants :

- data
- HTML
- registry
- src
- tiger

C’est à dire en supprimant les sous-répertoires suivants :

- txm
- wtc
- ptokenized
- tokenized
- stokenized
- treetagger
- annotations

# Sauvegardes<br>

### Comment faire pour re-importer un tableau .csv exporté de TXM ? Autrement dit, puis-je reprendre mes travaux après avoir quitté TXM&nbsp;?

D’une façon générale les résultats de calcul sont perdus quand on quitte
TXM, seuls les sous-corpus et les partitions sont conservés. On
reconstruit dont les résultats qu’on veut retrouver, en notant les
paramètres du calcul pour le relancer à l’identique et reconstruire le
même tableau.

Les **tables lexicales** font exception en bénéficiant d’une possibilité
de sauvegarde depuis la version 0.7, mais en l’état actuel cette
fonctionnalité est encore insuffisamment documentée et peu intuitive. Le
principe est de conserver la table dans un fichier en l’exportant, puis
de la récupérer dans une nouvelle session de TXM en l’important.

Voici la procédure de sauvegarde de la table lexicale :

1\) <u>Exporter une table lexicale</u> : Attention, ce n’est pas la
commande export habituelle qu’il faut utiliser -celle qui sert à
récupérer la table pour l’afficher dans un tableur comme Calc et Excel.
C’est un autre export (au format R), qui est accessible en faisant un
clic droit sur une des cellules de la table elle-même : commande
“exporter la table”. Vous enregistrez alors votre fichier (comme
extension vous pouvez lui mettre .csv).

2\) <u>Importer une table lexicale</u> : Il vous faut préalablement
créer une table lexicale quelconque sous la même partition. Ensuite, par
un clic droit sur une cellule de cette table, vous retrouvez dans le
menu contextuel qui s’ouvre la commande “Importer une table”, vous
indiquez alors le fichier contenant la table que vous aviez exportée :
celle-ci va remplacer le contenu de la table courante.

{% include warning.html content="Les modifications que vous effectuez sur la table dans TXM
ne sont pas enregistrées dans le fichier depuis lequel vous avez importé
la table. Donc si vous voulez enregistrer les modifications faites lors
d’une session il vous faut faire un nouvel export de la table." %}

Remarque : Dans les anciennes versions de TXM (0.7, 0.7.1), il y a un
bug sur les entêtes de colonnes : le premier caractère de chaque nom est
modifié ou retiré. Solution pour contourner ce bug : au moment où vous
exportez la table, celle-ci s’affiche dans un nouvel onglet. Vous pouvez
alors ajouter une \*lettre\* au début de chaque intitulé de colonne. Par
exemple, si la première ligne du fichier est :

~~~ text
    "DeGaulle" "Pompidou"
~~~

vous la modifiez en

~~~ text
    "XDeGaulle" "XPompidou"
~~~

et vous sauvegardez le fichier (icône habituelle dans la barre d’outils,
ou ctrl-s). L’import de la table devrait alors restituer les intitulés
de colonne attendus.

# Autres<br>

### D’où viennent les membres de la liste txm-users&nbsp;?

Origine géographique des membres de la liste txm-users en Novembre 2013
:  

<iframe src="https://mapsengine.google.com/map/embed?mid=zaEksEdIvdqU.kMfs4qW98e9U" width="640" height="480"></iframe>

[^1]: Les délais d’attente et le nombre total d’essais successifs ont
    été réduits pour aider au réglage d’accès par PROXY.

[^2]: depuis TXM 0.7.7, il n’est plus nécessaire d’installer Java avant
    TXM. TXM utilise son propre Java

[^3]: depuis TXM 0.7.7, il n’est plus nécessaire d’installer Java avant
    TXM. TXM utilise son propre Java

[^4]: depuis TXM 0.7.7, il n’est plus nécessaire d’installer Java avant
    TXM. TXM utilise son propre Java.

[^5]: L’option “Deviner” \[le système d’encodage des caractères
    utilisé\] ne fonctionne pas à tous les coups. Il vaut mieux
    travailler avec un système d’encodage connu et unique, et indiqué au
    moment de l’import.

[^6]: cette façon de faire est préférable parce que le format CSV est un
    format de transit pas très bien défini et manipulé de façon variable
    selon les tableurs et les réglages de langues dans les systèmes
    d’exploitation - typiquement à cause des différences de convention
    d’écriture des nombres à virgule flottante entre le français et
    l’anglais. Cela permet également de profiter des services de Calc
    pour aider l’édition du tableau comme le formatage des lignes, le
    stylage gras, etc., la colorisation de l’arrière plan des cellules
    ainsi que la numérotation automatique

[^7]: Certains en font même une compétition, voir le [PDF Liberation
    Hackathon](https://pdfliberation.wordpress.com).

[^8]: Tous les formats supportés : CBZ, CBR, CBC, CHM, DJVU, EPUB, FB2,
    HTML, HTMLZ, LIT, LRF, MOBI, ODT, PDF, PRC, PDB, PML, RB, RTF, SNB,
    TCR, TXT, TXTZ

[^9]: Il y a 15 formats PDF différents correspondants à autant de types
    de documents métier qui n’ont pas forcément un contenu textuel :
    bureautique - documents de traitement de texte, archivage - images
    de scans, plans d’architecture, etc.

[^10]: Équivalences terminologiques : une structure A contenant une
    structure B est “de niveau supérieur à“ la structure B ou “contient“
    la structure B. Une structure B contenue par une structure A est “de
    niveau inférieur à“ la structure A ou “est contenue“ par la
    structure A
