---
ititle: Documentation TXM complémentaire
layout: page
lang: fr
ref: documentation-dir
---

# Documentation complémentaire
{: style="text-align: center; padding-bottom: 30px" .no_toc}

Archive de la documentation hors-ligne de TXM.

Voir aussi la page de [documentation générale](https://txm.gitpages.huma-num.fr/textometrie/Documentation/).

<table>
  <tr><th scope="col">Nom du fichier</th><th scope="col">Date de dernière modification</th></tr>
{% assign files = site.static_files | sort_natural:'name' %}
{% for file in files %}

  {% assign pathParts = file.path | split: "/" %}
  {% assign length = pathParts.size | minus: 2 %}
  {% assign path = "" %}
  {% for c in (0..length) %}
    {% capture path %}{{ path }}/{{pathParts[c]}}{% endcapture %}
  {% endfor %}

    {% if path == '//files/documentation' %}
        <tr>
	        <td>
	          <a href="{{ file.path | relative_url }}">{{ file.name }}</a>
	        </td>
	        <td>
	          {% include translated_date.html date=file.modified_time format="%A %e %B %Y %k:%M" lang="fr" %}
	        </td>
	      </tr>
    {% endif %}
{% endfor %}
</table>
