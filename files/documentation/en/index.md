---
ititle: Additional TXM documentation
layout: page
lang: en
ref: documentation-dir
---

# Additional documentation
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<table>
{% assign files = site.static_files | sort_natural:'name' %}
{% for file in files %}
    {% if file.path contains 'documentation' %}
        <tr>
	  <td>
	    <a href="{{ file.path | relative_url }}">{{ file.name }}</a>
	  </td>
	  <td>
	    {% include translated_date.html date=file.modified_time format="%A %e %B %Y %k:%M" lang="en" %}
	  </td>
	</tr>
    {% endif %}
{% endfor %}
</table>

