---
ititle: Ressources TXM
layout: page
lang: fr
ref: ressources
---

# Ressources téléchargeables

-   Logiciels
    - [TXM pour poste (Windows, Mac OS X & Linux)](software/TXM)
    - [portail TXM](software/TXM portal) (J2EE / Tomcat & Glassfish)
    - [macros TXM](software/TXM macros) (Groovy)
-   [Documentation complémentaire](documentation)
    - tous les manuels et tutoriels publics
-   [Corpus](corpora)
    - corpus exemples chargeables directement dans TXM pour les analyser (aucun import de fichiers sources n'est nécessaire)
-   [Matériel de cours](course materials)
    - diapos, exempliers, fichiers source exemples, etc.
-   Bibliothèques de feuilles de style
    - [feuilles XSLT](library/xsl) : utiles pour pré-traitement de sources ou pour l'import dans TXM
    - [feuilles CSS](library/css) : utiles pour les éditions HTML construites par TXM


