---
ititle: TXM Resources
layout: page
lang: en
ref: ressources
---

# Downloadable Resources

- Software
     - [TXM for desktop (Windows, Mac OS X & Linux)]({{"/files/software/TXM/en" | absolute_url}})
     - [TXM portal]({{"/files/software/TXM portal/en" | absolute_url}}) (J2EE / Tomcat & Glassfish)
     - [TXM macros]({{"/files/software/TXM macros/en" | absolute_url}}) (Groovy)
- [Documentation]({{"/files/documentation/en" | absolute_url}})
     - all textbooks and public tutorials
- [Corpora]({{"/files/corpora/en" | absolute_url}})
     - sample corpora loadable directly into TXM for analysis (no import of source files is needed)
- [Course material]({{"/files/course materials/en" | absolute_url}})
     - slides, exempliers, sample source files, etc.
- Libraries
     - [XSLT sheets]({{"/files/library/xsl/en" | absolute_url}}): useful for preprocessing sources or for import into TXM
     - [CSS sheets]({{"/files/library/css/en" | absolute_url}}): useful for HTML editions built by TXM

