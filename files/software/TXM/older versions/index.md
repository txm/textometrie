---
ititle: Anciennes versions de TXM
layout: page
lang: fr
ref: txm-old
---

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

# Anciennes versions de TXM
{: style="text-align: center; padding-bottom: 30px" .no_toc}

| 0.7.1 | [28 mai 2013](https://groupes.renater.fr/sympa/arc/txm-users/2013-05/msg00016.html) | {::nomarkdown}<ul><li>vue interne</li><li>macros</li></ul>{:/}
| 0.6 | [6 avril 2012](https://groupes.renater.fr/sympa/arc/txm-users/2012-04/msg00006.html) | {::nomarkdown}<ul><li>modules d'import xml-pps, xml-tei frantext</li><li>CAH (FactoMineR avec AFC)</li><li>envoyer vers R</li><li>xsl front</li><li>export de corpus</li><li>export de graphiques (jpg, png...)</li></ul>{:/}
| 0.5 | [17 mars 2011](https://groupes.renater.fr/sympa/arc/txm-users/2011-03/msg00012.html) | {::nomarkdown}<ul><li>modules d'import transcriber, xml-txm</li><li>3 architectures (Windows, Mac OS X, Linux)</li><li>édition de tables lexicales</li><li>index de partition, AFC, cooccurrences, progression</li><li>scriptage Groovy et R</li><li>assistants (partition, sous-corpus, CQL)</li><li>liens hypertextuels entre outils</li></ul>{:/}
| 0.4.7 | [3 août 2010](https://groupes.renater.fr/sympa/arc/txm-users/2010-08/msg00000.html) | {::nomarkdown}<ul><li>modules d'import presse-papier, txt, xml, xml-tei/bfm</li><li>changer la langue de l'interface</li><li>supprimer corpus ou résultats</li><li>AFC</li></ul>{:/}
| 0.4.6 | [20 avril 2010](https://groupes.renater.fr/sympa/arc/txm-users/2010-04/msg00003.html) | {::nomarkdown}<ul><li>premier module d'import : format CNR (logiciel Cordial)</li><li>retour au texte</li><li>export des résultats</li></ul>{:/}
| 0.4.5 | [22 janvier 2010](https://groupes.renater.fr/sympa/arc/txm-users/2010-02/msg00000.html) | {::nomarkdown}<ul><li>Linux</li></ul>{:/}

