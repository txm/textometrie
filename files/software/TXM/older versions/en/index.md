---
ititle: Old Versions of TXM
layout: page
lang: en
ref: txm-old
---

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

# Old versions of TXM
{: style="text-align: center; padding-bottom: 30px" .no_toc}

| 0.7.1 | [May 28, 2013](https://groupes.renater.fr/sympa/arc/txm-users/2013-05/msg00016.html) | {::nomarkdown}<ul><li>internal view</li><li>macros</li></ul>{:/}
| 0.6 | [April 6, 2012](https://groupes.renater.fr/sympa/arc/txm-users/2012-04/msg00006.html) | {::nomarkdown}<ul><li>xml-pps, xml-tei frantext import modules</li><li>AHC (FactoMineR with CFA)</li><li>send to R</li><li>xsl front</li><li>export of corpus</li><li>export of graphics (jpg, png...)</li></ul>{:/}
| 0.5 | [March 17, 2011](https://groupes.renater.fr/sympa/arc/txm-users/2011-03/msg00012.html) | {::nomarkdown}<ul><li>transcriber, xml-txm import modules</li><li>3 architectures (Windows, Mac OS X, Linux)</li><li>editing lexical tables</li><li>partition index and CFA, cooccurrences, progression</li><li>Groovy and R scripting</li><li>wizards (partition, sous-corpus, CQL)</li><li>hypertext links between tools</li></ul>{:/}
| 0.4.7 | [August 3, 2010](https://groupes.renater.fr/sympa/arc/txm-users/2010-08/msg00000.html) | {::nomarkdown}<ul><li>clipboard, txt, xml, xml-tei/bfm import modules</li><li>change the language of the interface</li><li>delete corpus or results</li><li>CFA</li></ul>{:/}
| 0.4.6 | [April 20, 2010](https://groupes.renater.fr/sympa/arc/txm-users/2010-04/msg00003.html) | {::nomarkdown}<ul><li>first import module: CNR format (Cordial software)</li><li>back to text</li><li>export results</li></ul>{:/}
| 0.4.5 | [January 22, 2010](https://groupes.renater.fr/sympa/arc/txm-users/2010-02/msg00000.html) | {::nomarkdown}<ul><li>Linux</li></ul>{:/}

