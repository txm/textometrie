---
ititle: Bienvenue dans TXM 0.8.3
layout: page
lang: fr
ref: welcome txm-0.8.3
---

# Bienvenue dans TXM 0.8.3

# Nouveautés

En plus de corriger de nombreux bugs, cette nouvelle version comprend quelques éléments nouveaux notables :
{: style="margin-bottom: 5px"}

- Compatibilité Mac (dont processeurs M1 et M2)
- Amélioration
    -   ergonomie des interfaces de certaines commandes
    -   robustesse de l'interface de sauvegarde des annotations CQP et URS
- Simplification
    -   choix de la langue des corpus importés par Presse-Papier
    -   export/import de propriétés de mots d'un corpus
    -   gestion des utilitaires
- Nouvelles fonctionnalités
    -   export/import de corpus
    -   export/import d'appel de calcul avec ses paramètres
- Mise à niveau générale des composants
    -   R [4.2.2](https://cran.r-project.org/bin/windows/base/old/4.2.2/NEWS.R-4.2.2.html)
    -   Java [17.0.7](https://www.oracle.com/java/technologies/javase/17-relnote-issues.html)
    -   Eclipse [2022-09](https://projects.eclipse.org/releases/2022-09)
    -   Saxon [12.2](https://www.saxonica.com/saxon-c/release-notes.xml#saxonc12-2)
    -   Groovy [4.0.3](https://groovy-lang.org/releasenotes/groovy-4.0.html)

Retrouvez la liste exhaustive des changements dans la liste des [tickets de développement de TXM 0.8.3](https://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=83).

## Extensions

- TreeTagger
    - upgrade version 3.2.5
- Annotation Unité-Relation-Schéma (URS)
    - interface de saisie et de sauvegarde plus robuste

## Retours de bugs

1.  Vérifiez que votre retour n'a pas déjà été signalé dans la [liste des tickets de retours de la plateforme TXM](https://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=83) ;
2.  Si ce n'est pas le cas, signalez le problème soit :
    -   dans la [liste de diffusion txm-users](https://groupes.renater.fr/sympa/info/txm-users) ;
    -   en éditant directement le [wiki txm-users](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.3) ;
    -   en tchatant directement avec les développeurs dans le canal IRC \#txm du serveur 'irc.freenode.net' ;
    -   en <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contactant l'équipe TXM par mail</a>.

## Remerciements

Merci aux testeuses et testeurs de cette version :

- Flora Badin
- Ioana Galleron
- Amal Guha
- Philippe Guillet
- Jean-Louis Janin
- Loïc Liégeois
- Giancarlo Luxardo
- Christophe Parisse
- Pierre Ratinaud
- Gilles Toubiana

## Contact

N'hésitez pas à [nous contacter]({{"Nous-contacter" | relative_url}}) pour de plus amples informations.

L'équipe TXM