---
ititle: Téléchargement de TXM 0.8.3
layout: page
lang: fr
ref: txm-0.8.3
---

# Téléchargement de TXM 0.8.3
{: style="text-align: center; padding-bottom: 30px" .no_toc}

## Sommaire
{: .no_toc}

1. TOC
{:toc}

---

# Logiciels d'installation de TXM 0.8.3 pour Windows, Mac et Linux
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

| [![]({{"/img/windows-icon.png" | absolute_url}}){: width="125" .center-image} Windows 7 -> 11](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.3/TXM_0.8.3_2024-01-25_Win64_installer.exe) | [![]({{"/img/macosx-icon.png" | absolute_url}}){: width="125" .center-image} Mac OS X 10.15 - 13.0](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.3/TXM_0.8.3_2024-01-18_MacOSX_installer.pkg) | [![]({{"/img/ubuntu-icon.png" | absolute_url}}){: width="125" .center-image} Ubuntu 20.04 - 22.04](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.3/TXM_0.8.3_2024-01-18_Linux64_installer.deb) |

Version courante :
- Windows : 25 janvier 2024 (bug d'import Windows)
- Mac : 18 janvier 2024
- Linux : 18 janvier 2024

[Versions précédentes](https://gitlab.huma-num.fr/txm/txm-software/-/tree/master/files/software/TXM/0.8.3) :
- 16 janvier 2024 : (installation Windows utilisateur, portée annotation CQP, nouveau corpus VOEUX)
- 7 novembre 2023 (stylage éditions @style et @type, utilitaires PDF)
- 22 juin 2023 (bug R Mac, bug namespace 'txm:' <ana> enregistrement annotations, bug enregistrement corpus renommé)
- 20 juin 2023 (version initiale)

## Nouveautés

En plus de corriger de nombreux bugs, cette nouvelle version comprend quelques éléments nouveaux notables :
{: style="margin-bottom: 5px"}

- Compatibilité Mac (dont processeurs M1 et M2)
- Amélioration
    -   ergonomie des interfaces de certaines commandes
    -   robustesse de l'interface de sauvegarde des annotations CQP et URS
- Simplification
    -   choix de la langue des corpus importés par Presse-Papier
    -   export/import de propriétés de mots d'un corpus
    -   gestion des utilitaires
- Nouvelles fonctionnalités
    -   export/import de corpus
    -   export/import d'appel de calcul avec ses paramètres
- Mise à niveau générale des composants
    -   R [4.2.2](https://cran.r-project.org/bin/windows/base/old/4.2.2/NEWS.R-4.2.2.html)
    -   Java [17.0.7](https://www.oracle.com/java/technologies/javase/17-relnote-issues.html)
    -   Eclipse [2022-09](https://projects.eclipse.org/releases/2022-09)
    -   Saxon [12.2](https://www.saxonica.com/saxon-c/release-notes.xml#saxonc12-2)
    -   Groovy [4.0.3](https://groovy-lang.org/releasenotes/groovy-4.0.html)

Retrouvez la liste exhaustive des changements dans la liste des [tickets de développement de TXM 0.8.3](https://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=83).

### Nouveautés des mises à jour

  - Corpus
      - Documentation :
          - les corpus peuvent désormais intégrer une documentation.
            Celle-ci est composée de fichiers HTML regroupés dans le dossier « doc » du corpus binaire. Le fichier "index.html" en est l’accueil, qui est ouvert par le nouveau bouton « Documentation » de la barre d’outils (icone de maison, quand une documentation est présente).
  - Commandes
      - Édition : 
          - ajout d'une barre de navigation dans l’historique de lecture
          - la recherche plein texte met en évidence tous les mots trouvés dans la page
      - Propriétés : 
          - ajout d'un accès rapide aux listes de valeurs des propriétés
            de structures
          - ajout de la liste des annotations en cours
            (non-sauvegardées) 
          - accès au journal de cycle de vie du corpus depuis l'onglet
            "Général". Le journal contient les dates et les actions
            effectuées sur le corpus (imports initiaux, sauvegardes
            d'annotation, mises à jour, etc.) 
      - AFC : 
          - tri décroissant par défaut des indicateurs des points lignes
            et colonnes
      - Annotation CQP :
          - choix de la portée de l'annotation de séquences de mots
            (premier mot ou mot visé par le label @, premier mot, mot
            visé par le label @, tous les mots)
  - Imports 
      - XML/w et XTZ : 
          - les structures sans propriétés sont automatiquement
            numérotées avec la propriété « n » à partir de 1 
      - XTZ :
          - possibilité de rendu de mots collés dans les éditions. Les
            mots encodés avec l'attribut w@join="left" ne seront pas
            précédés d'un espace
  - Macros et scriptage de TXM
      - Mise à jour des macros « annotation/CQLList\* » : nouveau
        paramètre pour préciser la portée de l'annotation (premier mot ou mot visé par le label @, premier mot, etc.)
  - Interface
      - affichage des tableaux de résultats des commandes Index, Table
        Lexicale et Spécificités :
          - nouveau formatage des titres de colonnes pour limiter leur
            largeur (sauf pour Windows)
          - accès au réglage du facteur de largeur des colonnes dans les
            préférences « TXM \> Utilisateur »
      - lancement sous Windows : la fenêtre de lancement au fond noir
        est désormais fermée après lancement
  - Installation sous Windows
      - il n’est plus obligatoire d’avoir les droits d’administrateur
        pour installer TXM (sauf pour installer dans les dossiers
        système comme « C:\\Program Files » bien sûr)
      - nouvelle option d'installation « portable » : dans ce mode, TXM
        organise ses dossiers dans son dossier d'installation

### Nouveautés des corpus

  - une nouvelle version du corpus VOEUX est disponible, avec de nouveaux discours jusqu'à l'année 2021 inclue
    - disponibilité selon les versions de TXM :
      - la nouvelle version du logiciel d'installation de TXM 0.8.3 contient cette nouvelle version
      - les mises à jour de TXM 0.8.3 ne mettent pas à jour le corpus. Dans ce cas vous pouvez télécharger le nouveau corpus depuis son entrepôt de référence (fichier VOEUX-2023-11-28.txm dans le dossier "bin") et le charger vous-même : <https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/VOEUX>

## Extensions

- TreeTagger
    - upgrade version 3.2.5
- Annotation Unité-Relation-Schéma (URS)
    - interface de saisie et de sauvegarde plus robuste

## Prérequis d'installation

-   Vous aurez besoin des droits d'installation sur votre machine pour pouvoir installer TXM ;
-   Vous aurez besoin d'un accès à Internet ;
-   TXM fonctionne sur des machines **64-bit** ;
-   Windows
    -   TXM est supporté pour Windows 7 et Windows 10
-   Mac OS X
    -   TXM est supporté pour Mac OS X 10.14
        -   Réglages de sécurité : il faut modifier les paramètres de « sécurité » du
            système pour pouvoir installer correctement TXM. Aller dans
            « Préférences Système &gt; Personnel &gt; Sécurité » et
            autoriser les applications téléchargées depuis Internet. Si
            on ne fait pas ce réglage, le problème ne se manifeste pas
            au moment de l'installation, mais au premier lancement.
-   Linux
    -   TXM est supporté pour Ubuntu 20.04 et 22.04, et ses variantes (Xubuntu, Kubuntu, Lubuntu, etc.)
    -   TXM fonctionne également sur des variantes du système Debian
    -   L'installation de TXM provoque l'installation des packages
        suivants : zenity, po-debconf, libwebkit2gtk-4.0-37, debconf, libc6 (>= 2.15), libgtk2-perl|libgtk3-perl, libblas3, libbz2-1.0, libcairo2, libgfortran5, libglib2.0-0, libgomp1, libjpeg8, liblapack3, liblzma5, libpango-1.0-0, libpangocairo-1.0-0, libpaper-utils, libpcre3, libpng16-16, libreadline7|libreadline8, libegl1-mesa, libtinfo5, libtiff5, libx11-6, libxt6, tcl8.6, ucf, unzip, xdg-utils, zip, zlib1g

## Instructions d'installation

-   vérifier les pré-requis d'installation en fonction de votre système d'exploitation, voir ci-dessus
-   télécharger le logiciel d'installation de TXM correspondant à votre système et à l'architecture de votre machine
-   double-cliquer sur l'icone du logiciel d'installation de TXM
-   suivre les instructions du logiciel: accepter la licence, etc.
    - remarque concernant les utilisateurs de **Linux** : la **première fois**
      que vous installez TXM sur une machine, il est également nécessaire,
      après l'installation et avant le premier lancement, de **quitter votre
      session de travail (déconnexion) puis de vous reconnecter** pour
      finaliser l'installation.
-   l'installation se finalise lors du premier lancement de TXM (voir les instructions de lancement ci-dessous)
-   vous pouvez alors [installer TreeTagger]({{"InstallTreeTagger" | relative_url}}) (vous pouvez bien sûr utiliser TXM sans annotation linguistique automatique des mots, mais pour utiliser tout le potentiel de l'outil, il est vivement recommandé d'installer cette extension pour une exploitation optimale)

## En cas de problème d'installation

L'assistance pour l'installation et l'utilisation de TXM repose sur la communauté de ses utilisateurs et de ses développeurs.
{: style="margin-bottom: -10px"}

Pour plus d'aide concernant l'installation de TXM, vous pouvez :
{: style="margin-bottom: 5px"}

-   consulter les sections « Installer TXM sur sa machine » et « En cas de problème avec le logiciel » du [Manuel de TXM](https://pages.textometrie.org/txm-manual)
-   consulter la section « Installation et mises à jour » de la [FAQ des utilisateurs de TXM](https://groupes.renater.fr/wiki/txm-users/public/faq) qui liste
    les problèmes d'installation les plus communs
-   vous inscrire à la liste de diffusion des utilisateurs de TXM ['txm-users'](https://groupes.renater.fr/sympa/info/txm-users) et y demander de l'aide
-   converser en ligne via [IRC sur le canal '\#txm' du serveur](irc.freenode.net)
-   contacter l'équipe TXM par mail à l'addresse <a href="mailto:{{ "textometrie@groupes.renater.fr" | encode_email }}" title="textometrie AT groupes.renater.fr">textometrie AT groupes.renater.fr</a>

## Lancer TXM

-   Windows
    -   menu « Démarrer / TXM-0.8.3 / TXM-0.8.3 » (faire glisser l'icone de
        l'application TXM dans la barre de lancement rapide pour
        rajouter un accès direct)
-   Mac OS X
    -   Naviguer dans le répertoire « Applications / TXM-0.8.3 » avec le Finder
        et double-cliquer sur l'icone de l'application TXM (faire
        glisser l'icone de l'application TXM dans le dock pour rajouter
        un accès direct)
-   Linux
    -   Naviguer dans la section « Applications installées » du Launchpad
        d'Unity et double-cliquer sur l'icone de l'application de TXM-0.8.3
        (pour rajouter un accès direct, faire un clic droit sur l'icone
        de TXM du Lanceur et sélectionner « Conserver dans le Lanceur »)

## Utiliser TXM

Voir les rubriques du menu Aide ainsi que les ressources en ligne suivantes :
{: style="margin-bottom: 5px"}

-   Lire le [manuel en ligne de TXM](https://txm.gitpages.huma-num.fr/txm-manual) ;
-   Télécharger le [manuel PDF de TXM](https://txm.gitpages.huma-num.fr/txm-manual/txm-manual.pdf) pour impression.
-   Regarder la [vidéo d'un atelier TXM](https://txm.gitpages.huma-num.fr/textometrie/html/enregistrement_atelier_initiation_TXM_fr.html)
-   Participer à un [atelier TXM](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
-   Participer au [wiki de la communauté des utilisateurs de TXM](https://groupes.renater.fr/wiki/txm-users)

## Retours de bugs

1.  Vérifiez que votre retour n'a pas déjà été signalé dans la [liste des tickets de retours de la plateforme TXM](https://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=83) ;
2.  Si ce n'est pas le cas, signalez le problème soit :
    -   dans la [liste de diffusion txm-users](https://groupes.renater.fr/sympa/info/txm-users) ;
    -   en éditant directement le [wiki txm-users](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.3) ;
    -   en tchatant directement avec les développeurs dans le canal IRC \#txm du serveur 'irc.freenode.net' ;
    -   en <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contactant l'équipe TXM par mail</a>.

## Remerciements

Merci aux testeuses et testeurs de cette version :

- Flora Badin
- Ioana Galleron
- Amal Guha
- Philippe Guillet
- Jean-Louis Janin
- Loïc Liégeois
- Giancarlo Luxardo
- Christophe Parisse
- Pierre Ratinaud
- Gilles Toubiana

## Contact

N'hésitez pas à [nous contacter]({{"Nous-contacter" | relative_url}}) pour de plus amples informations.

L'équipe TXM
