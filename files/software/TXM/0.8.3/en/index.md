---
ititle: TXM 0.8.3 Download
layout: page
lang: en
ref: txm-0.8.3
---
# TXM 0.8.3 Download
{: style="text-align:center;padding-bottom:30px" .no_toc}

## Content
{: .no_toc}

1. TOC
{:toc}
---
# TXM 0.8.3 Download
{: style="text-align:center;padding-bottom:30px".no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: top-right;" markdown="1">

| [![]({{"/img/windows-icon.png" | absolute_url}}){: width="125" .center-image} Windows 7 -> 11](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.3/TXM_0.8.3_2024-01-25_Win64_installer.exe) | [![]({{"/img/macosx-icon.png" | absolute_url}}){: width="125" .center-image} Mac OS X 10.15 - 13.0](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.3/TXM_0.8.3_2024-01-18_MacOSX_installer.pkg) | [![]({{"/img/ubuntu-icon.png" | absolute_url}}){: width="125" .center-image} Ubuntu 20.04 - 22.04](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.3/TXM_0.8.3_2024-01-18_Linux64_installer.deb) |

[Previous versions](https://gitlab.huma-num.fr/txm/txm-software/-/tree/master/files/software/TXM/0.8.3): 
- november 7, 2023
- june 22, 2023
- june 20, 2023

## News

In addition to fixing many bugs, this new version includes some notable new features:
{: style="margin-bottom:5px"}

- Mac compatibility (including M1 and M2 processors)
- Improved
    - interface ergonomy for some commands
    - interface robustness for saving CQP and URS annotations
- Simplified
    - choice of language for corpora imported via clipboard
    - export/import of corpus word properties
    - utility management
- New functions
    - export/import corpus
    - export/import calculus call with parameters
- General component upgrades
    - R [4.2.2](https://cran.r-project.org/bin/windows/base/old/4.2.2/NEWS.R-4.2.2.html)
    - Java [17.0.7](https://www.oracle.com/java/technologies/javase/17-relnote-issues.html)
    - Eclipse [2022-09](https://projects.eclipse.org/releases/2022-09)
    - Saxon [12.2](https://www.saxonica.com/saxon-c/release-notes.xml#saxonc12-2)
    - Groovy [4.0.3](https://groovy-lang.org/releasenotes/groovy-4.0.html)

For a complete list of changes, see the list of [TXM 0.8.3 development tickets](https://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=83).

### What's new in updates

   - Corpus
       - Documentation:
           - corpora can now integrate documentation.
             This is composed of HTML files grouped in the
             “doc” folder of the binary corpus. The “index.html” file
             is the home page, which is opened by the new button
             “Documentation” in the toolbar (house icon,
             when documentation is present).
   - Commands
       - Editions:
           - added navigation bar in reading history
           - full text search now highlights all words
             found in the page
       - Properties:
           - added quick access to property value lists
             of structures
           - addition of the list of current annotations
             (unsaved)
           - access to the corpus lifecycle log from the tab
             "General". The log contains dates and actions
             carried out on the corpus (initial imports,
             annotations saving, updates, etc.)
       - CA:
           - descending sorting by default of lines and columns points indicators
       - CQP annotation:
           - choice of the scope of the annotation of sequences of words
             (first word or word targeted by the label @, first word, word
             targeted by the label @, all words)
   - Imports
       - XML/w and XTZ:
           - structures without properties are automatically
             numbered with the property “n” starting from 1
       - XTZ:
           - possibility of rendering adjacent words in editions (with no whitespace in between).
             words encoded with the w@join="left" attribute will not be
             preceded by a whitespace
   - Macros and TXM scripting
       - Updated “annotation/CQLList\*” macros: new
         parameter to specify the scope of the annotation (first word or word targeted by the label @, first word, etc.)
   - Interface
       - display of result tables of Index, Lexical Table
          and Specificities commands:
           - new formatting of column titles to limit their
             width (except for Windows)
           - access to setting the column width factor in the
             preferences “TXM \> User”
       - launch under Windows: the launch window with a black background
         is now closed after launch
   - Installation under Windows
       - it is no longer mandatory to have administrator rights
         to install TXM (except for installing in system folders
          like “C:\\Program Files” of course)
         
       - new “portable” installation option: in this mode, TXM
         organizes its files in its installation folder

### What's new in corpora

   - a new version of the VOEUX corpus is available, with new speeches up to and including the year 2021
     - availability depending on the TXM versions:
       - the new version of the TXM 0.8.3 installation software contains this new version
       - TXM 0.8.3 updates do not update the corpus. In this case you can download the new corpus from its reference repository (VOEUX-2023-11-28.txm file in the "bin" folder) and load it yourself: <https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/VOEUX>

## Extensions

- TreeTagger
    - upgrade to version 3.2.5
- URS (Unit-Relation-Schema) annotation
    - more robust input and save interface

## Installation Prerequisites
{: .no_toc}

-   you need to have 'new software installation privilege' on your machine to install TXM
-   you need Internet access
-   TXM works on **64-bit** machines
-   Windows
    -   TXM is supported for Windows 7 and Windows 10
-   Mac OS X
    -   TXM is supported for 10.14
        -   Security settings: from Mac OS X 10.9 and upper, you need to
            change the system security settings to install TXM properly.
            Go to "System Preferences &gt; Personal &gt; Security" and
            allow applications downloaded from the Internet. If that
            preference is not set, the problem doesn't occur at
            installation time, but at the first launch.
-   Linux
    -   TXM is supported for Ubuntu 20.04 and 22.04, and its variants (Xubuntu, Kubuntu, Lubuntu, etc.)
    -   TXM installation process installs the following dependencies:
        zenity, po-debconf, libwebkit2gtk-4.0-37, debconf, libc6 (>= 2.15), libgtk2-perl|libgtk3-perl, libblas3, libbz2-1.0, libcairo2, libgfortran5, libglib2.0-0, libgomp1, libjpeg8, liblapack3, liblzma5, libpango-1.0-0, libpangocairo-1.0-0, libpaper-utils, libpcre3, libpng16-16, libreadline7|libreadline8, libegl1-mesa, libtinfo5, libtiff5, libx11-6, libxt6, tcl8.6, ucf, unzip, xdg-utils, zip, zlib1g.

## Installation Instructions

-   verify the 'Installation Prerequisites' for your machine, see above
-   download the TXM setup file for your system and machine architecture (64-bit)
-   double-click on the TXM setup file icon
-   follow the instructions on screen: accept licence, etc.
    - note concerning **Linux** users: the **first time** you install TXM on a machine, you also need to **quit your working session (logout) and login again** to finalize the installation process.
-   run TXM once to finalize the setup process (see running instructions below)
-   you can now [install TreeTagger]({{"en/InstallTreeTagger" | relative_url}}) (optional but recommended)
-   The End

## Troubleshooting installation problems

TXM support is community based.
{: style="margin-bottom: -10px"}

For further help concerning TXM installation:
{: style="margin-bottom: 5px"}

-   consult the "Installer TXM sur sa machine" and the "En cas de problème avec le logiciel" sections of the [TXM manual](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/manual/0.7.9/fr/manual1.xhtml) (in French)
-   consult the "Installation et mises à jour" section of the [TXM usersFAQ](https://groupes.renater.fr/wiki/txm-users/public/faq) which lists some of the common installation problems people run into.
-   subscribe to the ['txm-users'](https://groupes.renater.fr/sympa/info/txm-users) mailing list and ask for help
-   chat online via IRC on the '\#txm' channel of the irc.freenode.net server
-   contact the TXM team by email at <a href="mailto:{{ "textometrie@groupes.renater.fr" | encode_email }}" title="textometrie AT groupes.renater.fr">textometrie AT groupes.renater.fr</a>

## Running TXM

-   Windows
    -   Menu 'Start / TXM-0.8.3 / TXM-0.8.3' (drag TXM application icon to the Quick Access Toolbar to add direct access)
-   Mac OS X
    -   Navigate to the 'Applications / TXM-0.8.3' folder in the Finder and double-click on TXM application icon (drag TXM application icon to your Dock to add direct access)
-   Linux
    -   Navigate to the 'Installed Applications' section in the Launchpad and double-click on TXM-0.8.3 application icon (right-click on TXM icon in the Dock and select 'Keep in Dock' to add direct access)

## Using TXM

See the 'Help' menu entries and the following online resources:
{: style="margin-bottom: 5px"}

-   Read [TXM manual online](https://txm.gitpages.huma-num.fr/txm-manual)
-   Download [TXM PDF manual](http://textometrie.ens-lyon.fr/files/documentation/TXM%20Manual%200.7.pdf) for printing
-   View the [TXM workshop video](https://txm.gitpages.huma-num.fr/textometrie/html/enregistrement_atelier_initiation_TXM_fr.html)
-   Participate to a [TXM workshop](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
-   Participate to [TXM users community wiki](https://groupes.renater.fr/wiki/txm-users)

## Feedback and bug reports

1.  Check if your feedback is not already reported in the [TXM platform bug tracker](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=70)
2.  If not, send your feedback either:
    -   in the [txm-open mailing list](https://lists.sourceforge.net/lists/listinfo/txm-open)
    -   by directly editing the [txm-users wiki](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.3)
    -   by chating directly with the developers on the \#txm IRC channel of the 'irc.freenode.net' server
    -   by <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contacting the TXM team by mail</a>

## Acknowledgements

Thank you to the testers of this version:

- Flora Badin
- Ioana Galleron
- Amal Guha
- Philippe Guillet
- Jean-Louis Janin
- Loïc Liégeois
- Giancarlo Luxardo
- Christophe Parisse
- Pierre Ratinaud
- Gilles Toubiana

## Contact

Please [contact us]({{"en/Nous-contacter" | relative_url}}) for further information.

The TXM team
