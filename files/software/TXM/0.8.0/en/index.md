---
ititle: TXM 0.8.0 Download
layout: page
lang: en
ref: txm-0.8.0
---

## <This version has been replaced by [TXM 0.8.2]({{"/files/software/TXM/0.8.2" | absolute_url}})>

# TXM 0.8.0 Download
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

| [![]({{"/img/windows-icon.png" | absolute_url}}){: width="125" .center-image} Windows 7 and up (64 bit)](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/TXM_0.8.0_2019-09-03_Win64_installer.exe) | [![]({{"/img/macosx-icon.png" | absolute_url}}){: width="125" .center-image} Mac OS X 10.10 and up](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/TXM_0.8.0_2019-09-03_MacOSX_installer.pkg) | [![]({{"/img/ubuntu-icon.png" | absolute_url}}){: width="125" .center-image} Ubuntu 16.04 and up](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/TXM_0.8.0_2019-09-03_Linux64_installer.deb) |

<p>&nbsp;</p>

## Summary
{: .no_toc}

1. TOC
{:toc}

### Previous TXM 0.8.0 versions
{: .no_toc}

- 15th july 2019: [Windows](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/TXM_0.8.0_2019-07-15_Win64_installer.exe) -
    [Ubuntu](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/TXM_0.8.0_2019-07-15_Linux64_installer.deb) -
    [Mac OS X](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/TXM_0.8.0_2019-07-15_MacOSX_installer.pkg)

- 6th june 2019: [Windows](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/TXM_0.8.0_2019-06-06_Win64_installer.exe) -
    [Ubuntu](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/TXM_0.8.0_2019-06-06_Linux64_installer.deb) -
    [Mac OS X](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/TXM_0.8.0_2019-06-06_MacOSX_installer.pkg)

</div>

---

## News

In addition to fixing many bugs, this new version includes some notable
new elements,

### Improved installation
{: .no_toc}

-   TXM is now installed in a directory dedicated to its version. This
    makes it possible to use different versions of TXM at the same time,
    for example TXM 0.7.9 and TXM 0.8.0;
-   it is no longer necessary to painstakingly install TreeTagger
    yourself then link to TXM: two new TXM extensions now allow you to
    download and install in a few clicks both TreeTagger and two default
    language models (en and fr).

### Improved user session
{: .no_toc}

- all calculation results can now be kept between two TXM sessions, either
on demand (default operating mode) or systematically;
- all commands now include the setting of all their parameters from the
results window in a dedicated area. This makes it much easier to refine
the results. For example, you can easily adjust the queries to use or
the structure to display in a Progression, without having to relaunch
the command and enter all its parameters.<br/>
Consequences :
    -   for most commands, you must first specify one or two mandatory
    parameters in the dedicated area before you can obtain a result by
    pressing the Calculate button in the results window;
    -   the re-calculation of a result causes the re-calculation of all the
    results which depend on it (its descendants in the Corpus view): for
    example, recalculating a lexical table recalculates the tables of
    specificities or the AFCs built from it ;
    -   the results, tables or graphs, can be updated either as soon as one
    of the parameters is modified (operating mode called "electric",
    deactivated by default), or on demand by pressing the
    "\[re-\]calculate" button (in this operating mode, an "asterisk"
    mark is displayed in the window tab as soon as one of its parameters
    is modified, to indicate that the display of the results can be
    refreshed) ;
    -   New preferences:
        -   TXM &gt; User &gt; Automatic recalculation when changing a
        parameter: a result is recalculated as soon as one of the
        parameters of the command is modified;
        -   TXM &gt; User &gt; Backup all default results (persistence): all
        new results are kept between TXM sessions;
- a result can be cloned, with or without its descendants, for example to
compare variants of calculations;
- graphic display variants (font type or font size, background color,
etc.) can be adjusted directly from the results window in a dedicated
toolbar.

### Improved annotation tools
{: .no_toc}

-   You can now correct or add simple word properties from a
    concordance, such as TreeTagger's "enpos" and "enlemma" properties.
    This annotation mode has been added to the initial annotation modes
    of word sequences;

### Improved import
{: .no_toc}

-   the metadata file "metadata.\*" can now be read either in .ods
    (LibreOffice Calc), .xlsx (MS Excel) or .csv format;
-   the import and the loading of corpora are faster because TXM does
    not restart the engines during these treatments anymore.

## Installation Prerequisites
{: .no_toc}

-   you need to have 'new software installation privilege' on your
    machine to install TXM
-   you need Internet access
-   TXM works on **64-bit** machines
-   Windows
    -   TXM is supported for Windows 7 (Seven)
    -   TXM is known to work on Windows 10, Windows 8 and Windows Vista
-   Mac OS X
    -   TXM is supported for 10.12
    -   TXM is known to work on 10.11, 10.10 and 10.09
        -   Security settings: from Mac OS X 10.9 and upper, you need to
            change the system security settings to install TXM properly.
            Go to "System Preferences &gt; Personal &gt; Security" and
            allow applications downloaded from the Internet. If that
            preference is not set, the problem doesn't occur at
            installation time, but at the first launch.
-   Linux
    -   TXM is supported for Ubuntu 16.04+ and its variants (Xubuntu,
        Kubuntu, Lubuntu, etc.)
    -   TXM installation process installs the following dependencies:
        zenity, po-debconf, libwebkitgtk-1.0-0, debconf, libc6 (&gt;=
        2.15), libgtk2-perl, libblas3, libbz2-1.0, libcairo2,
        libgfortran3, libglib2.0-0, libgomp1, libjpeg8, liblapack3,
        liblzma5, libpango-1.0-0, libpangocairo-1.0-0, libpaper-utils,
        libpcre3, libpng12-0, libreadline6, libtiff5, libx11-6, libxt6,
        tcl8.5, tk8.5, ucf, unzip, xdg-utils, zip, zlib1g.

## Installation Instructions

-   verify the 'Installation Prerequisites' for your machine, see above
-   download the TXM setup file for your system and machine architecture
    (64-bit)
-   double-click on the TXM setup file icon
-   follow the instructions on screen: accept licence, etc.
    - note concerning **Linux** users: the **first time** you install TXM on a
      machine, you also need to **quit your working session (logout) and login
      again** to finalize the installation process.
-   run TXM once to finalize the setup process (see running instructions
    below)
-   you can now [install TreeTagger]({{"en/InstallTreeTagger" | relative_url}}) (optional but recommended)
-   The End

## Troubleshooting installation problems

TXM support is community based.
{: style="margin-bottom: -10px"}

For further help concerning TXM installation:
{: style="margin-bottom: 5px"}

-   consult the "Installer TXM sur sa machine" and the "En cas de
    problème avec le logiciel" sections of the [TXM
    manual](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/manual/0.7.9/fr/manual1.xhtml)
    (in French)
-   consult the "Installation et mises à jour" section of the [TXM users
    FAQ](https://groupes.renater.fr/wiki/txm-users/public/faq) which
    lists some of the common installation problems people run into.
-   subscribe to the
    ['txm-users'](https://groupes.renater.fr/sympa/info/txm-users)
    mailing list and ask for help
-   chat online via IRC on the '\#txm' channel of the irc.freenode.net
    server
-   contact the TXM team by email at <a href="mailto:{{ "textometrie@groupes.renater.fr" | encode_email }}" title="textometrie AT groupes.renater.fr">textometrie AT groupes.renater.fr</a>

## Running TXM

-   Windows
    -   Menu 'Start / TXM / TXM' (drag TXM application icon to the Quick
        Access Toolbar to add direct access)
-   Mac OS X
    -   Navigate to the 'Applications / TXM' folder in the Finder and
        double-click on TXM application icon (drag TXM application icon
        to your Dock to add direct access)
-   Linux
    -   Navigate to the 'Installed Applications' section in the
        Launchpad and double-click on TXM application icon (right-click
        on TXM icon in the Dock and select 'Keep in Dock' to add direct
        access)

## Using TXM

For further information about using TXM:
{: style="margin-bottom: 5px"}

-   Read [TXM manual
    online](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/manual/0.7.9/fr/https://txm.gitpages.huma-num.fr/textometrie/files/documentation/manual/0.7.9/fr/manual1.xhtml)
-   Download [TXM PDF
    manual](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/Manuel%20de%20TXM%200.7%20FR.pdf)
    for printing
-   View the [TXM workshop
    video](http://txm.sourceforge.net/enregistrement_atelier_initiation_TXM_fr.html)
-   Participate to a [TXM
    workshop](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
-   Participate to [TXM users community
    wiki](https://groupes.renater.fr/wiki/txm-users)

## Feedback and bug reports

1.  Check if your feedback is not already reported in the [TXM platform
    bug
    tracker](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=70)
2.  If not, send your feedback either:
    -   in the [txm-open mailing
        list](https://lists.sourceforge.net/lists/listinfo/txm-open)
    -   by directly editing the [txm-users
        wiki](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.0)
    -   by chating directly with the developers on the \#txm IRC channel
        of the 'irc.freenode.net' server
    -   by <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contacting the TXM team by mail</a>

Please [contact us]({{"en/Nous-contacter" | relative_url}}) for further information.

The TXM team

