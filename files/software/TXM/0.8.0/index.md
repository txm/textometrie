---
ititle: Téléchargement de TXM 0.8.0
layout: page
lang: fr
ref: txm-0.8.0
---

## <Cette version a été remplacée par [TXM 0.8.2]({{"/files/software/TXM/0.8.2" | absolute_url}})>

# Téléchargement de TXM 0.8.0
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

| [![]({{"/img/windows-icon.png" | absolute_url}}){: width="125" .center-image} Windows 7 et plus (64 bit)](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.0/TXM_0.8.0_2019-09-03_Win64_installer.exe) | [![]({{"/img/macosx-icon.png" | absolute_url}}){: width="125" .center-image} Mac OS X 10.10 et plus](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.0/TXM_0.8.0_2019-09-03_MacOSX_installer.pkg) | [![]({{"/img/ubuntu-icon.png" | absolute_url}}){: width="125" .center-image} Ubuntu 16.04 et plus](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.0/TXM_0.8.0_2020-04-10_Linux64_installer.deb) |

<p>&nbsp;</p>

## Sommaire
{: .no_toc}

1. TOC
{:toc}

### Versions antérieures de TXM 0.8.0
{: .no_toc}

- 15 juillet 2019: [Windows](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/TXM_0.8.0_2019-07-15_Win64_installer.exe) -
    [Ubuntu](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/TXM_0.8.0_2019-07-15_Linux64_installer.deb) -
    [Mac OS X](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/TXM_0.8.0_2019-07-15_MacOSX_installer.pkg)

-   6 juin 2019 : [Windows](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/TXM_0.8.0_2019-06-06_Win64_installer.exe) -
    [Ubuntu](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/TXM_0.8.0_2019-06-06_Linux64_installer.deb) -
    [Mac OS X](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/TXM_0.8.0_2019-06-06_MacOSX_installer.pkg)

</div>

---

## Nouveautés

En plus de corriger de nombreux bugs, cette nouvelle version comprend
quelques éléments nouveaux notables,

### Amélioration de l'installation
{: .no_toc}

-   TXM s’installe désormais dans un répertoire dédié à sa version. Cela
    permet d’utiliser différentes versions de TXM en même temps, par
    exemple TXM 0.7.9 et TXM 0.8.0 ;
-   il n’est plus nécessaire d’installer laborieusement TreeTagger
    soi-même puis de le lier à TXM : deux nouvelles extensions TXM
    permettent désormais de télécharger et d’installer en quelques clics
    à la fois TreeTagger et deux modèles de langues par défaut (fr et
    en).

### Amélioration de la session utilisateur
{: .no_toc}

-   on peut désormais conserver tous les résultats de calculs entre deux
sessions de TXM, soit à la demande (mode de fonctionnement par
défaut), soit systématiquement ;
-   toutes les commandes intègrent désormais le réglage de tous leurs
paramètres depuis la fenêtre de résultats dans une zone dédiée. Cela
permet d'affiner beaucoup plus facilement les résultats. Par
exemple, on peut facilement régler les requêtes à utiliser ou bien
la structure à afficher dans une Progression, sans avoir à la
relancer la commande et saisir tous ses paramètres.<br/>
    Conséquences :
    -   pour la majorité des commandes, il faut commencer par préciser
        un ou deux paramètres obligatoires dans la zone dédiée avant de
        pouvoir obtenir un résultat en appuyant sur le bouton Calculer
        de la fenêtre de résultats ;
    -   le re-calcul d'un résultat provoque le re-calcul de tous les résultats qui en dépendent (ses descendants dans la vue Corpus)
        : par exemple, recalculer une table lexicale recalcule les tableaux de spécificités ou les AFC construits à partir d’elle ;
    -   les résultats, tableaux ou graphiques, peuvent être mis à jour 
        soit dès que l’on modifie un des paramètres (mode de
        fonctionnement appelé « électrique », désactivé par défaut),
        soit à la demande en appuyant sur le bouton « \[re-\]calculer »
        (dans ce mode de fonctionnement, une marque « astérisque »
        s’affiche dans l’onglet de la fenêtre dès que l’on modifie un de
        ses paramètres, pour indiquer que l’on peut rafraîchir
        l'affichage des résultats) ;
    -   Nouvelles préférences :
        -   TXM &gt; Utilisateur &gt; Recalcul automatique lors du
            changement d'un paramètre : un résultat est recalculé dès
            qu'un des paramètres de la commande est modifié ;
        -   TXM &gt; Utilisateur &gt; Sauvegarde de tous les résultats
            par défaut (persistance) : tous les nouveaux résultats sont
            conservés entre les sessions de TXM ;
-   on peut cloner un résultat, avec ou sans ses descendants, par
    exemple pour comparer des variantes de calculs ;
-   les variantes d'affichage graphique (police ou taille des
    caractères, couleur de fond, etc.) sont réglables directement depuis
    la fenêtre de résultats dans une barre d’outils dédiée.

### Amélioration des outils d’annotation
{: .no_toc}

-   on peut désormais corriger ou ajouter des propriétés de mots simples
    depuis une concordance, comme par exemple les propriétés « frpos »
    et « frlemma » de TreeTagger. Ce mode d’annotation a été ajouté aux
    modes d’annotation initiaux de séquences de mots ;

### Amélioration de l’import
{: .no_toc}

-   le fichier de métadonnées « metadata.\* » peut désormais être
    indifféremment lu au format .ods (LibreOffice Calc), .xlsx (MS
    Excel) ou .csv ;
-   l'import et le chargement de corpus sont plus rapides car TXM ne
    redémarre plus les moteurs lors de ces traitements.

## Prérequis d'installation

-   Vous aurez besoin des droits d'installation sur votre machine pour
    pouvoir installer TXM ;
-   Vous aurez besoin d'un accès à Internet ;
-   TXM fonctionne sur des machines **64-bit** ;
-   Windows
    -   TXM est supporté pour Windows 7 (Seven)
    -   TXM fonctionne également avec Windows 10, Windows 8 et Windows
        Vista
-   Mac OS X
    -   TXM est supporté pour Mac OS X 10.12
    -   TXM fonctionne également avec 10.11, 10.10 et 10.09
        -   Réglages de sécurité : à partir de Mac OS X 10.9 (Lion) et
            supérieur, il faut modifier les paramètres de “sécurité” du
            système pour pouvoir installer correctement TXM. Aller dans
            “Préférences Système &gt; Personnel &gt; Sécurité” et
            autoriser les applications téléchargées depuis Internet. Si
            on ne fait pas ce réglage, le problème ne se manifeste pas
            au moment de l'installation, mais au premier lancement.
-   Linux
    -   TXM est supporté pour Ubuntu 16.04 et ses variantes (Xubuntu,
        Kubuntu, Lubuntu, etc.)
    -   TXM fonctionne également sur des variantes du système Debian
    -   L'installation de TXM provoque l'installation des dépendances
        suivantes : zenity, po-debconf, libwebkitgtk-1.0-0, debconf,
        libc6 (&gt;= 2.15), libgtk2-perl, libblas3, libbz2-1.0,
        libcairo2, libgfortran3, libglib2.0-0, libgomp1, libjpeg8,
        liblapack3, liblzma5, libpango-1.0-0, libpangocairo-1.0-0,
        libpaper-utils, libpcre3, libpng12-0, libreadline6, libtiff5,
        libx11-6, libxt6, tcl8.5, tk8.5, ucf, unzip, xdg-utils, zip,
        zlib1g.

## Instructions d'installation

-   vérifier les pré-requis d'installation, voir ci-dessus
-   télécharger l'installeur de TXM correspondant à votre système
    d'exploitation et à l'architecture de votre machine (64-bit ou
    32-bit)
-   double-cliquer sur l'icone de l'installeur de TXM
-   suivre les instructions de l'installeur: accepter la licence, etc.
    - remarque concernant les utilisateurs de **Linux** : la **première fois**
      que vous installez TXM sur une machine, il est également nécessaire,
      après l'installation et avant le premier lancement, de **quitter votre
      session de travail (déconnexion) puis de vous reconnecter** pour
      finaliser l'installation.
-   lancer TXM une première fois pour finaliser le processus
    d'installation (voir les instructions de lancement ci-dessous)
-   vous pouvez maintenant [installer TreeTagger]({{"InstallTreeTagger" | relative_url}}) (optionnel mais recommandé)
-   Fin

## En cas de problème d'installation

L'assistance pour l'installation et l'utilisation de TXM repose sur la
communauté de ses utilisateurs et de ses développeurs.
{: style="margin-bottom: -10px"}

Pour plus d'aide concernant l'installation de TXM, vous pouvez :
{: style="margin-bottom: 5px"}

-   consulter les sections "Installer TXM sur sa machine" et "En cas de
    problème avec le logiciel" du [Manuel de
    TXM](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/manual/0.7.9/fr/manual1.xhtml)
-   consulter la section "Installation et mises à jour" de la [FAQ des
    utilisateurs de
    TXM](https://groupes.renater.fr/wiki/txm-users/public/faq) qui liste
    les problèmes d'installation les plus communs
-   vous inscrire à la liste de diffusion des utilisateurs de TXM
    ['txm-users'](https://groupes.renater.fr/sympa/info/txm-users) et y
    demander de l'aide
-   converser en ligne via IRC sur le canal '\#txm' du serveur
    irc.freenode.net
-   contacter l'équipe TXM par mail à l'addresse <a href="mailto:{{ "textometrie@groupes.renater.fr" | encode_email }}" title="textometrie AT groupes.renater.fr">textometrie AT groupes.renater.fr</a>

## Lancer TXM

-   Windows
    -   menu 'Démarrer / TXM / TXM' (faire glisser l'icone de
        l'application TXM dans la barre de lancement rapide pour
        rajouter un accès direct)
-   Mac OS X
    -   Naviguer dans le répertoire 'Applications / TXM' avec le Finder
        et double-cliquer sur l'icone de l'application TXM (faire
        glisser l'icone de l'application TXM dans le dock pour rajouter
        un accès direct)
-   Linux
    -   Naviguer dans la section 'Applications installées' du Launchpad
        d'Unity et double-cliquer sur l'icone de l'application de TXM
        (pour rajouter un accès direct, faire un clic droit sur l'icone
        de TXM du Lanceur et sélectionner 'Conserver dans le Lanceur')

## Utiliser TXM

Pour plus d'information sur l'usage de TXM :
{: style="margin-bottom: 5px"}

-   Lire [le manuel en ligne de
    TXM](http://txm.sourceforge.net/doc/manual/0.7.9/fr/manual1.xhtml) ;
-   Télécharger [le manuel PDF de
    TXM](http://textometrie.ens-lyon.fr/files/software/TXM/0.8.0/Manuel%20de%20TXM%200.7%20FR.pdf)
    pour impression.
-   Regarder la [vidéo d'un atelier
    TXM](http://txm.sourceforge.net/enregistrement_atelier_initiation_TXM_fr.html)
-   Participer à un [atelier
    TXM](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
-   Participer au [wiki de la communauté des utilisateurs de
    TXM](https://groupes.renater.fr/wiki/txm-users)

## Retours de bugs

1.  Vérifiez que votre retour n'a pas déjà été signalé dans la [liste
    des retours de la plateforme
    TXM](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=70)
    ;
2.  Si ce n'est pas le cas, signalez le problème soit :
    -   dans la [liste de diffusion
        txm-users](https://groupes.renater.fr/sympa//info/txm-users) ;
    -   en éditant directement le [wiki
        txm-users](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.0)
        ;
    -   en tchatant directement avec les développeurs dans le canal IRC
        \#txm du serveur 'irc.freenode.net' ;
    -   en <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contactant l'équipe TXM par mail</a>.

N'hésitez pas à [nous contacter]({{"Nous-contacter" | relative_url}}) pour de plus amples informations.

L'équipe TXM

