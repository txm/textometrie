---
ititle: Download TXM 0.7.9
layout: page
lang: en
ref: txm-0.7.9
---

   Download TXM 0.7.9

* * *

Téléchargement de  TXM 0.7.9  Download  

=========================================

[![](index_files/windows-icon.png)  
Windows 7 et plus (64 bit)](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.7.9/TXM_0.7.9_Win64_installer.exe)  
[Windows 7 et plus (32 bit)](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.7.9/TXM_0.7.9_Win32_installer.exe)

[![](index_files/macosx-icon.png)  
Mac OS X 10.10 et plus](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.7.9/TXM_0.7.9_MacOSX_installer.pkg)  

[![](index_files/ubuntu-icon.png)  
Ubuntu 16.04 et 18.04](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.7.9/TXM_0.7.9_Linux64_installer_18_04.deb)  

Content
-------

*   [News](#News)
*   [Installation Prerequisites](#Installation Prerequisites)
*   [Installation Instructions](#Installation Instructions)
*   [Troubleshooting installation problems](#Troubleshooting installation problems)
*   [Running TXM](#Running TXM)
*   [Using TXM](#Using TXM)
*   [TXM manual](#TXM manual)
*   [Feedback and bug reports](#Feedback and bug reports)
*   [Contribute to TXM](#Contribute to TXM)

Sommaire
--------

*   [Nouveautés](#Nouveautés)
*   [Prérequis d'installation](#Prérequis d'installation)
*   [Instructions d'installation](#Instructions d'installation)
*   [En cas de problème d'installation](#En cas de problème d'installation)
*   [Lancer TXM](#Lancer TXM)
*   [Utiliser TXM](#Utiliser TXM)
*   [Manuel de TXM](#Manuel de TXM)
*   [Retours de bugs](#Retours de bugs)
*   [Contribuer à TXM](#Contribuer à TXM)

* * *

ENGLISH
-------

News
----

*   TXM now hosts its own R for Mac OS X and Linux operating systems: this should greatly help Mac OS X users who are having problems running R with TXM (Specificity, CA, etc. commands);
*   more robust words highlight in text editions when getting back to text according to different versions of Windows;
*   TXM manual update:
    *   documentation of the XML-XTZ + CSV import module, useful in particular to easily produce synoptic editions with facsimiles and to be able to obtain corpora that can be annotated through concordances;
    *   documentation of the Analec extension to annotate within text editions. See the extension project wiki: [Annotation and analysis of coreference chains in TXM](https://groupes.renater.fr/wiki/txm-users/public/umr_lattice#projet_anr_democrat_annotation_et_analyse_de_chaines_de_co-references) (in French).

Installation Prerequisites
--------------------------

*   you need to have 'new software installation privilege' on your machine to install TXM
*   you need Internet access
*   TXM works on **64-bit** machines
*   Windows
    *   TXM is supported for Windows 7 (Seven)
    *   TXM is known to work on Windows 10, Windows 8 and Windows Vista
*   Mac OS X
    *   TXM is supported for 10.12
    *   TXM is known to work on 10.11, 10.10 and 10.09
        *   Security settings: from Mac OS X 10.9 and upper, you need to change the system security settings to install TXM properly. Go to "System Preferences > Personal > Security" and allow applications downloaded from the Internet. If that preference is not set, the problem doesn't occur at installation time, but at the first launch.
*   Linux
    *   TXM is supported for Ubuntu 16.04+ and its variants (Xubuntu, Kubuntu, Lubuntu, etc.)
    *   TXM installation process installs the following dependencies: zenity, po-debconf, libwebkitgtk-1.0-0, debconf, libc6 (>= 2.15), libgtk2-perl, libblas3, libbz2-1.0, libcairo2, libgfortran3, libglib2.0-0, libgomp1, libjpeg8, liblapack3, liblzma5, libpango-1.0-0, libpangocairo-1.0-0, libpaper-utils, libpcre3, libpng12-0, libreadline6, libtiff5, libx11-6, libxt6, tcl8.5, tk8.5, ucf, unzip, xdg-utils, zip, zlib1g.

Installation Instructions
-------------------------

*   verify the 'Installation Prerequisites' for your machine, see above
*   download the TXM setup file for your system and machine architecture (64-bit)
*   double-click on the TXM setup file icon
*   follow the instructions on screen: accept licence, etc.
*   run TXM once to finalize the setup process (see running instructions below)
    *   warning: automatic updates and extensions will only be available from the second run of TXM

Note concerning **Linux** users: the **first time** you install TXM on a machine, you also need to **quit your working session (logout) and login again** to finalize the installation process.

Troubleshooting installation problems
-------------------------------------

TXM support is community based.

For further help concerning TXM installation:

*   consult the "Installer TXM sur sa machine" and the "En cas de problème avec le logiciel" sections of the [TXM manual](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/manual/0.7.9/fr/manual1.xhtml) (in French)
*   consult the "Installation et mises à jour" section of the [TXM users FAQ](https://groupes.renater.fr/wiki/txm-users/public/faq) which lists some of the common installation problems people run into.
*   subscribe to the ['txm-users'](https://groupes.renater.fr/sympa/info/txm-users) mailing list and ask for help
*   chat online via IRC on the '#txm' channel of the irc.freenode.net server
*   contact the TXM team by email at 'textometrie AT groupes.renater.fr'

Running TXM
-----------

*   Windows
    *   Menu 'Start / TXM / TXM' (drag TXM application icon to the Quick Access Toolbar to add direct access)
*   Mac OS X
    *   Navigate to the 'Applications / TXM' folder in the Finder and double-click on TXM application icon (drag TXM application icon to your Dock to add direct access)
*   Linux
    *   Navigate to the 'Installed Applications' section in the Launchpad and double-click on TXM application icon (right-click on TXM icon in the Dock and select 'Keep in Dock' to add direct access)

Using TXM
---------

For further information about using TXM:

*   Read [TXM manual online](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/manual/0.7.9/fr/manual1.xhtml)
*   Download [TXM PDF manual](Manuel%20de%20TXM%200.7%20FR.pdf) for printing
*   View the [TXM workshop video](https://txm.gitpages.huma-num.fr/textometrie/html/enregistrement_atelier_initiation_TXM_fr.html)
*   Participate to a [TXM workshop](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
*   Participate to [TXM users community wiki](https://groupes.renater.fr/wiki/txm-users)

TXM manual
----------

The installer includes the new PDF version of TXM 0.7 manual (in French). That you can also download from here: [Manuel de TXM 0.7 FR.pdf](Manuel%20de%20TXM%200.7%20FR.pdf)

The corresponding TXM binary corpus has been updated (result of the 'Microsoft Word / LibreOffice Writer' TXM import module prototype) [Download link](https://gitlab.huma-num.fr/txm/txm-ressources/-/raw/master/corpora/txm-odt-manual/TXM-MANUAL-2023-06-23.txm?inline=false)

Feedback and bug reports
------------------------

1.  Check if your feedback is not already reported in the [TXM platform bug tracker](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=35)
2.  If not, send your feedback either:
    *   in the [txm-open mailing list](https://lists.sourceforge.net/lists/listinfo/txm-open)
    *   by directly editing the [txm-users wiki](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.7.9)
    *   by chating directly with the developers on the #txm IRC channel of the 'irc.freenode.net' server
    *   by contacting the TXM team at the 'textometrie AT groupes.renater.fr' address

Contribute to TXM
-----------------

TXM is provided to you free of charge. In return, in the spirit of open-source software, you are invited to participate in its improvement. To do so, you do not have to be software developer. For instance, you can:

*   Send us your publications or other papers showing your use of TXM
*   Add a link from your website to the home page of the Textométrie project [textometrie.ens-lyon.fr](http://textometrie.ens-lyon.fr/)
*   Report bugs you might find in TXM (see section 'Feedbacks and bug reports')
*   Help us translate the user interface or documentation in other languages
*   Share with the community of users your course materials, as well as sample corpora
*   Invite your fellow developers to adapt TXM to your needs and share with us these developments. This may concern TXM core (Java and C programming: for professional developers) or its periphery (Groovy language scripting: much more accessible)
*   Set up a research project using TXM (e.g. NEH or DFG funded) for which we can advise you in the customization of the software
*   Make proposals for improving the documentation and TXM dissemination

Please contact us at 'textometrie AT groupes.renater.fr' for further information.

The TXM team

* * *

FRANÇAIS
--------

Nouveautés
----------

*   TXM héberge désormais son propre R pour les systèmes d'exploitation Mac OS X et Linux : ceci devrait considérablement aider les utilisateurs sous Mac OS X qui rencontraient des problèmes de fonctionnement de R avec TXM (commandes Spécificités, AFC, etc.) ;
*   mise en évidence des mots dans les éditions plus robuste lors du retour au texte selon les différentes versions de Windows ;
*   mise à jour du manuel de TXM :
    *   documentation du module d'import XML-XTZ+CSV, utile notamment pour produire facilement des éditions synoptiques avec fac-similés et pour obtenir des corpus annotables en concordance ;
    *   documentation de l'extension Analec d'annotation au sein d’éditions de texte. Voir le wiki du chantier : [Annotation et analyse de chaînes de co-références dans TXM](https://groupes.renater.fr/wiki/txm-users/public/umr_lattice#projet_anr_democrat_annotation_et_analyse_de_chaines_de_co-references).

Prérequis d'installation
------------------------

*   Vous aurez besoin des droits d'installation sur votre machine pour pouvoir installer TXM ;
*   Vous aurez besoin d'un accès à Internet ;
*   TXM fonctionne sur des machines 64-bit ;
*   Windows
    *   TXM est supporté pour Windows 7 (Seven)
    *   TXM fonctionne également avec Windows 10, Windows 8 et Windows Vista
*   Mac OS X
    *   TXM est supporté pour Mac OS X 10.12
    *   TXM fonctionne également avec 10.11, 10.10 et 10.09
        *   Réglages de sécurité : à partir de Mac OS X 10.9 (Lion) et supérieur, il faut modifier les paramètres de “sécurité” du système pour pouvoir installer correctement TXM. Aller dans “Préférences Système > Personnel > Sécurité” et autoriser les applications téléchargées depuis Internet. Si on ne fait pas ce réglage, le problème ne se manifeste pas au moment de l'installation, mais au premier lancement.
*   Linux
    *   TXM est supporté pour Ubuntu 16.04 et ses variantes (Xubuntu, Kubuntu, Lubuntu, etc.)
    *   TXM fonctionne également sur des variantes du système Debian
    *   L'installation de TXM provoque l'installation des dépendances suivantes : zenity, po-debconf, libwebkitgtk-1.0-0, debconf, libc6 (>= 2.15), libgtk2-perl, libblas3, libbz2-1.0, libcairo2, libgfortran3, libglib2.0-0, libgomp1, libjpeg8, liblapack3, liblzma5, libpango-1.0-0, libpangocairo-1.0-0, libpaper-utils, libpcre3, libpng12-0, libreadline6, libtiff5, libx11-6, libxt6, tcl8.5, tk8.5, ucf, unzip, xdg-utils, zip, zlib1g.

Instructions d'installation
---------------------------

*   Vérifier les pré-requis d'installation, voir ci-dessus
*   Télécharger l'installeur de TXM correspondant à votre système d'exploitation et à l'architecture de votre machine (64-bit ou 32-bit)
*   Double-cliquer sur l'icone de l'installeur de TXM
*   Suivre les instructions de l'installeur: accepter la licence, etc.
*   Lancer TXM une première fois pour finaliser le processus d'installation (voir les instructions de lancement ci-dessous)
    *   Attention: les mises à jour automatiques et les extensions ne seront disponibles qu'à partir du deuxième lancement de TXM

Remarque concernant les utilisateurs de **Linux** : la **première fois** que vous installez TXM sur une machine, il est également nécessaire de **quitter votre session de travail (déconnexion) puis de vous reconnecter** pour finaliser l'installation.

En cas de problème d'installation
---------------------------------

L'assistance pour l'installation et l'utilisation de TXM repose sur la communauté de ses utilisateurs et de ses développeurs.

Pour plus d'aide concernant l'installation de TXM, vous pouvez :

*   consulter les sections "Installer TXM sur sa machine" et "En cas de problème avec le logiciel" du [Manuel de TXM](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/manual/0.7.9/fr/manual1.xhtml)
*   consulter la section "Installation et mises à jour" de la [FAQ des utilisateurs de TXM](https://groupes.renater.fr/wiki/txm-users/public/faq) qui liste les problèmes d'installation les plus communs
*   vous inscrire à la liste de diffusion des utilisateurs de TXM ['txm-users'](https://groupes.renater.fr/sympa/info/txm-users) et y demander de l'aide
*   converser en ligne via IRC sur le canal '#txm' du serveur irc.freenode.net
*   contacter l'équipe TXM par mail à l'addresse 'textometrie AT groupes.renater.fr'

Lancer TXM
----------

*   Windows
    *   menu 'Démarrer / TXM / TXM' (faire glisser l'icone de l'application TXM dans la barre de lancement rapide pour rajouter un accès direct)
*   Mac OS X
    *   Naviguer dans le répertoire 'Applications / TXM' avec le Finder et double-cliquer sur l'icone de l'application TXM (faire glisser l'icone de l'application TXM dans le dock pour rajouter un accès direct)
*   Linux
    *   Naviguer dans la section 'Applications installées' du Launchpad d'Unity et double-cliquer sur l'icone de l'application de TXM (pour rajouter un accès direct, faire un clic droit sur l'icone de TXM du Lanceur et sélectionner 'Conserver dans le Lanceur')

Utiliser TXM
------------

Pour plus d'information sur l'usage de TXM :

*   Lire [le manuel en ligne de TXM](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/manual/0.7.9/fr/manual1.xhtml) ;
*   Télécharger [le manuel PDF de TXM](Manuel%20de%20TXM%200.7%20FR.pdf) pour impression.
*   Regarder la [vidéo d'un atelier TXM]https://txm.gitpages.huma-num.fr/textometrie/html/enregistrement_atelier_initiation_TXM_fr.html)
*   Participer à un [atelier TXM](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
*   Participer au [wiki de la communauté des utilisateurs de TXM](https://groupes.renater.fr/wiki/txm-users)

Manuel de TXM
-------------

L'installeur comprend la nouvelle version PDF du manuel de TXM 0.7, que vous pourrez également télécharger à : [Manuel de TXM 0.7 FR.pdf](Manuel%20de%20TXM%200.7%20FR.pdf)

Le corpus TXM binaire correspondant a été mis à jour (résultat du module d'import prototype 'Microsoft Word / LibreOffice Writer') : [lien de téléchargement](https://gitlab.huma-num.fr/txm/txm-ressources/-/raw/master/corpora/txm-odt-manual/TXM-MANUAL-2023-06-23.txm?inline=false)

Retours de bugs
---------------

1.  Vérifiez que votre retour n'a pas déjà été signalé dans la [liste des retours de la plateforme TXM](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=35) ;
2.  Si ce n'est pas le cas, signalez le problème soit :
    *   dans la [liste de diffusion txm-users](https://groupes.renater.fr/sympa//info/txm-users) ;
    *   en éditant directement le [wiki txm-users](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.7.9) ;
    *   en tchatant directement avec les développeurs dans le canal IRC #txm du serveur 'irc.freenode.net' ;
    *   en contactant l'équipe TXM à l'adresse 'textometrie AT groupes.renater.fr'.

Contribuer à TXM
----------------

TXM vous est fourni gracieusement. En contre-partie, dans l'esprit du logiciel open-source, vous êtes invité à participer à son adaptation ou à son amélioration. Pour cela, vous n'êtes pas obligé d'être développeur informatique. Par exemple, vous pouvez :

*   nous transmettre vos publications ou autres documents de travail montrant votre usage de TXM
*   ajouter un lien depuis votre site web vers la page d'accueil du projet Textométrie : [textometrie.ens-lyon.fr](http://textometrie.ens-lyon.fr)
*   nous faire part des dysfonctionnements que vous pourriez constater dans TXM (voir la section 'Retours de bugs')
*   nous aider à traduire son interface utilisateur ou sa documentation dans d'autres langues
*   partager avec la communauté des utilisateurs vos supports de cours, ainsi que des corpus exemples
*   inviter vos collègues développeurs à adapter TXM à vos propres besoins et partager avec nous ces évolutions. Soit en son coeur (langages Java et C pour développeurs professionnels), soit à sa périphérie (langage de script Groovy beaucoup plus accessible)
*   monter un projet mobilisant TXM (e.g. ANR) pour lequel nous pouvons vous conseiller dans la mise en oeuvre ou dans l'adaptation du logiciel
*   nous faire des propositions pour améliorer la documentation et sa diffusion

N'hésitez pas à nous contacter à l'adresse 'textometrie AT groupes.renater.fr' pour de plus amples informations.

L'équipe TXM
