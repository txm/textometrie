---
ititle: Téléchargement de TXM 0.8.3 beta
layout: page
lang: fr
ref: txm-0.8.3 beta
---

# Bienvenue à la campagne de test de TXM 0.8.3 beta
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

{% include important.html content="Cette version de TXM est destinée à tester et à débugger le logiciel en vue de la publication de la version définitive.

Il n'est pas recommandé de l'utiliser dans le cadre de votre travail." %}

{% include note.html content="Cette version s'installe à coté des autres versions de TXM, elle n'interférera pas avec le fonctionnement des versions précédentes installées sur la même machine." %}

<p>&nbsp;</p>


## Sommaire
{: .no_toc}

1. TOC
{:toc}

---



## Nouveautés

En plus de corriger de nombreux bugs, cette nouvelle version comprend quelques éléments nouveaux notables :
{: style="margin-bottom: 5px"}


- Compatibilité avec les Mac de nouvelle génération (processeurs M1, M2 ou M3 et systèmes récents)
- Amélioration de l'ergonomie des interfaces de certaines commandes
- Amélioration de la robustesse de l'interface de sauvegarde des annotations CQP et URS
- Plus de fonctionalités d'import et d'export de corpus
- Simplification du choix de la langue d'un corpus importé par Presse-Papier
- Simplification de l'export et de l'import des propriétés des mots d'un corpus
- Nouvelle possibilité d'exporter et d'importer un appel de calcul
- Simplification de la gestion des utilitaires
- Préparation de l'intégration de l'extension 'Annotation Syntaxique' qui permettra d'exploiter la structure syntaxique des phrases en plus des lemmes et de la morpho-syntaxe
- Mise à niveau générale des technologies aux versions les plus récentes : R 4.2.2, Java 17, Eclipse 2022-09, Saxon 11, Groovy 4.0.3

Retrouvez la liste exhaustive des changements dans la liste des [tickets de développement de TXM 0.8.3](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=83).

<p>&nbsp;</p>

## Procédure de test

- Merci de suivre les indications de la [recette de TXM 0.8.3 beta](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.3beta) dans le wiki txm-users ;
- Vous pouvez également nous adresser directement vos retours par mail à l'adresse textometrie at groupes.renater.fr.


# Téléchargement de TXM 0.8.3 beta
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

| [![]({{"/img/windows-icon.png" | absolute_url}}){: width="125" .center-image} Windows 7, 10 et 11 (64 bit)](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.3beta/TXM_0.8.3beta_2023-06-01_Win64_installer.exe) | [![]({{"/img/macosx-icon.png" | absolute_url}}){: width="125" .center-image} Mac OS X 10.15+](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.3beta/TXM_0.8.3beta_2023-06-01_MacOSX_installer.pkg) | [![]({{"/img/ubuntu-icon.png" | absolute_url}}){: width="125" .center-image} Ubuntu 20.04 et 22.04](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.3beta/TXM_0.8.3beta_2023-06-01_Linux64_installer.deb) |

## Planning

La période de test de TXM 0.8.3 beta se terminera vers la  fin mai 2023, en fonction des retours.

Merci pour vos contributions,  
l'équipe TXM 
