---
ititle: Download TXM 0.8.2
layout: page
lang: en
ref: txm-0.8.2
---

## <This version has been replaced by [TXM 0.8.3]({{"/files/software/TXM/0.8.3" | absolute_url}})>

# TXM 0.8.2
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

{% include note.html content="Cette version s'installe à coté de TXM 0.8.1, les deux versions du logiciel peuvent cohabiter sur la même machine." %}

<p>&nbsp;</p>


## Summary
{: .no_toc}

1. TOC
{:toc}

---

# Download TXM 0.8.2
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

| [![]({{"/img/windows-icon.png" | absolute_url}}){: width="125" .center-image} Windows 7 and 10 (64 bit)](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2/TXM_0.8.2_2022-06-20_Win64_installer.exe) | [![]({{"/img/macosx-icon.png" | absolute_url}}){: width="125" .center-image} Mac OS X 10.15 (*)](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2/TXM_0.8.2_2022-06-20_MacOSX_installer.pkg) | [![]({{"/img/ubuntu-icon.png" | absolute_url}}){: width="125" .center-image} Ubuntu 20.04 and 22.04](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2/TXM_0.8.2_2022-06-20_Linux64_installer.deb) |

<b>(*) Mac compatibility</b>: TXM 0.8.2 is compatible with Macs equipped with an Intel processor (not with those equipped with an M1 processor, available since early 2021):<br>
<table>
  <tbody>
    <tr class="firstRow">
      <td colspan="2" rowspan="2" class="Date-cell"></td>
      <td colspan="4" class="10/2019-cell" style="text-align: center;">Système Mac OS</td>
    </tr>
    <tr class="firstRow">
      <td class="10/2019-cell" style="text-align: center;">10.15 (Catalina)<br/>10/2019</td>
      <td class="11/2020-cell" style="text-align: center;">11.6 (Big&nbsp;Sur)<br/>11/2020</td>
      <td class="06/2021-cell" style="text-align: center;">12.04 (Monterey)<br/>06/2021</td>
      <td class="06/2021-cell" style="text-align: center;">13.0 (Ventura)<br/>10/2022</td>
    </tr>
    <tr class="lastRow">
      <td rowspan="2" class="10/2019-cell" style="text-align: center; vertical-align: middle;">Processeur</td>
      <td class="Date-cell" style="text-align: center;">Intel</td>
      <td style="text-align: center;color: green; font-weight: bold">TXM 0.8.2</td>
      <td style="text-align: center;color: orange; font-weight: bold"><a href="https://txm.gitpages.huma-num.fr/textometrie/files/software/TXM/0.8.1">TXM 0.8.1</a></td>
      <td style="text-align: center;color: orange; font-weight: bold"><a href="https://txm.gitpages.huma-num.fr/textometrie/files/software/TXM/0.8.1">TXM 0.8.1</a></td>
      <td style="text-align: center;color: orange; font-weight: bold"><a href="https://txm.gitpages.huma-num.fr/textometrie/files/software/TXM/0.8.1">TXM 0.8.1</a></td>
    </tr>
    <tr class="lastRow">
      <td style="text-align: center;">M1</td>
      <td style="text-align: center;">X</td>
      <td style="text-align: center;color: red; font-weight: bold"><a href="https://txm.gitpages.huma-num.fr/textometrie/files/software/TXM/0.8.0">TXM 0.8.0</a></td>
      <td style="text-align: center;color: red; font-weight: bold"><a href="https://txm.gitpages.huma-num.fr/textometrie/files/software/TXM/0.8.0">TXM 0.8.0</a></td>
      <td style="text-align: center;color: red; font-weight: bold"><a href="https://txm.gitpages.huma-num.fr/textometrie/files/software/TXM/0.8.0">TXM 0.8.0</a></td>
    </tr>
  </tbody>
</table>

Previous versions : 
- 10 june 2022 : [Windows 7 et 10 (64 bit)](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2/TXM_0.8.2_2022-06-10_Win64_installer.exe), [Mac OS X 10.14](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2/TXM_0.8.2_2022-06-10_MacOSX_installer.pkg),[ Ubuntu 20.04 et 22.04](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2/TXM_0.8.2_2022-06-10_Linux64_installer.deb)


## Nouveautés

En plus de corriger de nombreux bugs, cette nouvelle version comprend quelques éléments nouveaux notables :
{: style="margin-bottom: 5px"}

- General Interface
  - Systematic display of calculation progress
  - Simplified access to utilities (macros) from the main 'Utilities' menu
  - corrected display of result tables (line heights under Mac OS X)
- Search engines
 - Simplification of searchs for words sequences in CQP (you enter the sequence directly)
    - for example, typing 'because that' (without quotes) causes the query `[word="because"] [word="that"]` to be searched
  - Simplification of word search by other engines than CQP, depending on the installed extensions and available annotations:
    - TIGER Search' extension: search in syntax trees ('TIGER' engine)
    - URS Annotation' extension: search in URS units ('URSQL' engine)
- Concordances
  - New possibility to format Concordances references (when importing the corpus or in a concordance parameters)
- Editing
  - New full-text search from the edition pages : (equivalent to the 'Ctrl-F' in applications : Firefox, Thunderbird, Writer, Calc, etc.)
  - Hypertextual link not only from the Concordance to the Edition (double click on a concordance line -> visualization in the Edition), but now also in the other direction, from the Edition to the Concordance (select the words in the page, then launch the concordance).
- Index
  - the hypertext link back to the concordance is now more intuitive. It generates an intermediate sub-corpus, so that the occurrences in the concordance always correspond to those counted in the Index. Thus all the conditions of the initial Index query are taken into account, there is no need to check and possibly complete the concordance query manually.
- Corpus
  - Possibility to integrate a documentation in HTML format into a corpus, which can be consulted in a dedicated tab of the corpus properties and which can refer to websites by hypertext links.
- Annotation
  - Redesign of the CQP annotation bars by concordances
  - Better protection of CQP annotation saving
  - New URS annotation by Concordances (URS extension)
- Export
  - Export of the Dimensions of a partition as a table in CSV format (tabulation column separator)
  - New option to export corpora in 'TXM 0.7.9' format (to facilitate loading corpora in TXM 0.6.3 portals)
- Scripts
  - Call of R scripts under Windows fixed

Translated with www.DeepL.com/Translator (free version)

All new features [in TXM 0.8.2 developpement tickets ](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=70).

## Installation Prerequisites
{: .no_toc}

-   you need to have 'new software installation privilege' on your machine to install TXM
-   you need Internet access
-   TXM works on **64-bit** machines
-   Windows
    -   TXM is supported for Windows 7 and Windows 10
-   Mac OS X
    -   TXM is supported for 10.14
        -   Security settings: from Mac OS X 10.9 and upper, you need to
            change the system security settings to install TXM properly.
            Go to "System Preferences &gt; Personal &gt; Security" and
            allow applications downloaded from the Internet. If that
            preference is not set, the problem doesn't occur at
            installation time, but at the first launch.
-   Linux
    -   TXM is supported for Ubuntu 20.04 and 21.04, and its variants (Xubuntu, Kubuntu, Lubuntu, etc.)
    -   TXM installation process installs the following dependencies:
        zenity, po-debconf, libwebkitgtk-1.0-0, debconf, libc6 (&gt;=
        2.15), libgtk2-perl, libblas3, libbz2-1.0, libcairo2,
        libgfortran3, libglib2.0-0, libgomp1, libjpeg8, liblapack3,
        liblzma5, libpango-1.0-0, libpangocairo-1.0-0, libpaper-utils,
        libpcre3, libpng12-0, libreadline6, libtiff5, libx11-6, libxt6,
        tcl8.5, tk8.5, ucf, unzip, xdg-utils, zip, zlib1g.

## Installation Instructions

-   verify the 'Installation Prerequisites' for your machine, see above
-   download the TXM setup file for your system and machine architecture (64-bit)
-   double-click on the TXM setup file icon
-   follow the instructions on screen: accept licence, etc.
    - note concerning **Linux** users: the **first time** you install TXM on a machine, you also need to **quit your working session (logout) and login again** to finalize the installation process.
-   run TXM once to finalize the setup process (see running instructions below)
-   you can now [install TreeTagger]({{"en/InstallTreeTagger" | relative_url}}) (optional but recommended)
-   The End

## Troubleshooting installation problems

TXM support is community based.
{: style="margin-bottom: -10px"}

For further help concerning TXM installation:
{: style="margin-bottom: 5px"}

-   consult the "Installer TXM sur sa machine" and the "En cas de problème avec le logiciel" sections of the [TXM manual](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/manual/0.7.9/fr/manual1.xhtml) (in French)
-   consult the "Installation et mises à jour" section of the [TXM usersFAQ](https://groupes.renater.fr/wiki/txm-users/public/faq) which lists some of the common installation problems people run into.
-   subscribe to the ['txm-users'](https://groupes.renater.fr/sympa/info/txm-users) mailing list and ask for help
-   chat online via IRC on the '\#txm' channel of the irc.freenode.net server
-   contact the TXM team by email at <a href="mailto:{{ "textometrie@groupes.renater.fr" | encode_email }}" title="textometrie AT groupes.renater.fr">textometrie AT groupes.renater.fr</a>

## Running TXM

-   Windows
    -   Menu 'Start / TXM-0.8.2 / TXM-0.8.2' (drag TXM application icon to the Quick Access Toolbar to add direct access)
-   Mac OS X
    -   Navigate to the 'Applications / TXM-0.8.2' folder in the Finder and double-click on TXM application icon (drag TXM application icon to your Dock to add direct access)
-   Linux
    -   Navigate to the 'Installed Applications' section in the Launchpad and double-click on TXM-0.8.2 application icon (right-click on TXM icon in the Dock and select 'Keep in Dock' to add direct access)

## Using TXM

See the 'Help' menu entries and the following online resources:
{: style="margin-bottom: 5px"}

-   Read [TXM manual online](https://txm.gitpages.huma-num.fr/txm-manual)
-   Download [TXM PDF manual](http://textometrie.ens-lyon.fr/files/documentation/TXM%20Manual%200.7.pdf) for printing
-   View the [TXM workshop video](https://txm.gitpages.huma-num.fr/textometrie/html/enregistrement_atelier_initiation_TXM_fr.html)
-   Participate to a [TXM workshop](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
-   Participate to [TXM users community wiki](https://groupes.renater.fr/wiki/txm-users)

## Feedback and bug reports

1.  Check if your feedback is not already reported in the [TXM platform bug tracker](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=70)
2.  If not, send your feedback either:
    -   in the [txm-open mailing list](https://lists.sourceforge.net/lists/listinfo/txm-open)
    -   by directly editing the [txm-users wiki](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.2)
    -   by chating directly with the developers on the \#txm IRC channel of the 'irc.freenode.net' server
    -   by <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contacting the TXM team by mail</a>

Please [contact us]({{"en/Nous-contacter" | relative_url}}) for further information.

The TXM team
