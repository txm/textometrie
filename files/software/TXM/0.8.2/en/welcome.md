---
ititle: Welcome to TXM 0.8.2
layout: page
lang: en
ref: welcome txm-0.8.2
---

# Welcome to TXM 0.8.2

## News

In addition to fixing many bugs, this new version includes some notable new elements:
{: style="margin-bottom: 5px"}

- fixes and consolidation of TXM 0.8.1 architecture and TXM GUI
  - CQL errors shown when the query syntax is broken
  - Specificities columns layout
  - A propos: display commons paths of TXM
- fonctionnalities:
  - TXT Query : natural language queries. e.g "I'm looking for a word"
  - Edition command : new search button 
  - Concordances command : new pattern option for references. e.g "%s: %s" to format references.
  - Properties command: add a doc/indexl.html file to the corpus to display the corpus documentation 
- Utilities
  - New "Utilities" main menu: direct acces to all utilities
  - Install&UpdateMacros: install or update from TXM SVN sources
  - Table2XML: transform a Table file (Excel, ODS, TSV) into XML-TEI files ready to be imported in TXM
  - Vocapia2TRS: transform Vocapia transcription files into XML-TRS files ready to be imported in TXM
- Import modules
  - new management of w@id in sources
  - XML-XTZ:
    - re-tokenize : tokenizer option to retokenize w words of XML sources
  - XML-TRS
    - specicifiq import options to tune editions
  - XML-TMX
    - the module was broken in TXM 0.8.1

Find out all changes in [TXM 0.8.2 development tickets](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=70).

## Extensions

The following extensions have been updated for TXM 0.8.2:
{: style="margin-bottom: 5px"}

- TreeTagger software: installation on Mac OS X Catalina
- TreeTagger models: en, fr models
- TreeTagger: enable the Train and Tag commands on corpus
- Media Player: restore media player for Ubuntu users
- Unit-Relation-Schema (URS) annotation : 
  - compute Index, Concordance and References using the URSQL (URS query language)
  - auto-save annotations when the annotation interface is up
  - annotate Units in Concordances
  - the properties view value validation process indicates when a new value is input



## Using TXM

See the 'Help' menu entries and the following online resources:
{: style="margin-bottom: 5px"}

-   Read [TXM manual
    online](https://txm.gitpages.huma-num.fr/txm-manual)
-   Download [TXM PDF manual](http://textometrie.ens-lyon.fr/files/documentation/TXM%20Manual%200.7.pdf)
    for printing
-   View the [TXM workshop video](https://txm.gitpages.huma-num.fr/textometrie/html/enregistrement_atelier_initiation_TXM_fr.html)
-   Participate to a [TXM workshop](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
-   Participate to [TXM users community wiki](https://groupes.renater.fr/wiki/txm-users)

## Feedback and bug reports

1.  Check if your feedback is not already reported in the [TXM platform
    bug
    tracker](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=74)
2.  If not, send your feedback either:
    -   in the [txm-open mailing
        list](https://lists.sourceforge.net/lists/listinfo/txm-open)
    -   by directly editing the [txm-users
        wiki](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.0)
    -   by chating directly with the developers on the \#txm IRC channel
        of the 'irc.freenode.net' server
    -   by <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contacting the TXM team by mail</a>

Please [contact us]({{"en/Nous-contacter" | relative_url}}) for further information.

The TXM team
