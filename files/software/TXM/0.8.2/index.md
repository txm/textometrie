---
ititle: Téléchargement de TXM 0.8.2
layout: page
lang: fr
ref: txm-0.8.2
---

## <Cette version a été remplacée par [TXM 0.8.3]({{"/files/software/TXM/0.8.3" | absolute_url}})>

# TXM 0.8.2
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">


{% include note.html content="Cette version s'installe à coté de TXM 0.8.1, les deux versions du logiciel peuvent cohabiter sur la même machine." %}

<p>&nbsp;</p>


## Sommaire
{: .no_toc}

1. TOC
{:toc}

---

# Téléchargement de TXM 0.8.2
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

| [![]({{"/img/windows-icon.png" | absolute_url}}){: width="125" .center-image} Windows 7 et 10 (64 bit)](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2/TXM_0.8.2_2022-06-20_Win64_installer.exe) | [![]({{"/img/macosx-icon.png" | absolute_url}}){: width="125" .center-image} Mac OS X 10.15 (*)](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2/TXM_0.8.2_2022-06-20_MacOSX_installer.pkg) | [![]({{"/img/ubuntu-icon.png" | absolute_url}}){: width="125" .center-image} Ubuntu 20.04 et 22.04](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2/TXM_0.8.2_2022-06-20_Linux64_installer.deb) |

<br><b>(*) Compatibilité Mac :</b> TXM 0.8.2 est compatible avec les Macs équipés d'un processeur Intel (pas avec ceux équipés d'un processeur M1, disponibles depuis début 2021):<br>
<table>
  <tbody>
    <tr class="firstRow">
      <td colspan="2" rowspan="2" class="Date-cell"></td>
      <td colspan="4" class="10/2019-cell" style="text-align: center;">Système Mac OS</td>
    </tr>
    <tr class="firstRow">
      <td class="10/2019-cell" style="text-align: center;">10.15 (Catalina)<br/>10/2019</td>
      <td class="11/2020-cell" style="text-align: center;">11.6 (Big&nbsp;Sur)<br/>11/2020</td>
      <td class="06/2021-cell" style="text-align: center;">12.04 (Monterey)<br/>06/2021</td>
      <td class="06/2021-cell" style="text-align: center;">13.0 (Ventura)<br/>10/2022</td>
    </tr>
    <tr class="lastRow">
      <td rowspan="2" class="10/2019-cell" style="text-align: center; vertical-align: middle;">Processeur</td>
      <td class="Date-cell" style="text-align: center;">Intel</td>
      <td style="text-align: center;color: green; font-weight: bold">TXM 0.8.2</td>
      <td style="text-align: center;color: orange; font-weight: bold"><a href="https://txm.gitpages.huma-num.fr/textometrie/files/software/TXM/0.8.1">TXM 0.8.1</a></td>
      <td style="text-align: center;color: orange; font-weight: bold"><a href="https://txm.gitpages.huma-num.fr/textometrie/files/software/TXM/0.8.1">TXM 0.8.1</a></td>
      <td style="text-align: center;color: orange; font-weight: bold"><a href="https://txm.gitpages.huma-num.fr/textometrie/files/software/TXM/0.8.1">TXM 0.8.1</a></td>
    </tr>
    <tr class="lastRow">
      <td style="text-align: center;">M1</td>
      <td style="text-align: center;">X</td>
      <td style="text-align: center;color: red; font-weight: bold"><a href="https://txm.gitpages.huma-num.fr/textometrie/files/software/TXM/0.8.0">TXM 0.8.0</a></td>
      <td style="text-align: center;color: red; font-weight: bold"><a href="https://txm.gitpages.huma-num.fr/textometrie/files/software/TXM/0.8.0">TXM 0.8.0</a></td>
      <td style="text-align: center;color: red; font-weight: bold"><a href="https://txm.gitpages.huma-num.fr/textometrie/files/software/TXM/0.8.0">TXM 0.8.0</a></td>
    </tr>
  </tbody>
</table>

Versions précédentes : 
- 10 juin 2022 : [Windows 7 et 10 (64 bit)](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2/TXM_0.8.2_2022-06-10_Win64_installer.exe), [Mac OS X 10.14](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2/TXM_0.8.2_2022-06-10_MacOSX_installer.pkg),[ Ubuntu 20.04 et 22.04](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2/TXM_0.8.2_2022-06-10_Linux64_installer.deb)


## Nouveautés

En plus de corriger de nombreux bugs, cette nouvelle version comprend quelques éléments nouveaux notables :
{: style="margin-bottom: 5px"}

- Interface Générale
  - Affichage systématique de la progression des calculs
  - Accès simplifié aux utilitaires (macros) depuis le menu principal 'Utilitaires'
  - Affichage des tableaux de résultats corrigé sous Mac OS X
- Moteurs de recherche
  - Simplification de la recherche de séquences de mots en CQP (on saisit la séquence directement)
    - par exemple la saisie directe de 'parce que' dans le champ de recherche (sans les guillemets) provoque la recherche de la requête `[word="parce"] [word="que"]`
  - Simplification de la recherche de mots par d'autres moteurs que CQP, en fonction des extensions installées et des annotations disponibles :
    - extension 'TIGER Search' : recherche dans les arbres syntaxiques (moteur 'TIGER')
    - extension 'Annotation URS' : recherche dans les unités URS (moteur 'URSQL')
- Édition
  - Nouvelle recherche plein texte depuis les pages d’édition : équivalent du ‘Ctrl-F’ des applications courantes Firefox, Thunderbird, Writer, Calc... mais en exploitant bien sûr CQP
  - Lien hypertextuel non plus seulement de la Concordance vers l'Édition (double clic sur une ligne de concordance → visualisation dans l'Édition), mais aussi maintenant dans l'autre sens, de l'Édition vers la Concordance (on sélectionne les mots dans la page, puis on lance la concordance).
- Concordances
  - Nouvelles possibilités de formater les références de Concordances (à l'import du corpus ou depuis une concordance)
- Index
  - le lien hypertexte de retour à la concordance fait désormais en sorte que les occurrences de la concordance se limitent à celles utilisées pour calculer l'Index.
- Corpus
  - Possibilité d'intégrer à un corpus une documentation au format HTML, consultable dans un onglet dédié des Propriétés du corpus et pouvant renvoyer vers des sites web par liens hypertextes.
- Annotation
  - Redesign des barres d'annotation CQP par concordances
  - Meilleure protection de la sauvegarde d'annotations CQP
  - Nouvelle annotation URS par Concordances (extension URS)
- Export
  - Export des Dimensions d'une partition sous la forme d'un tableau au format CSV (séparateur de colonnes tabulation)
  - Nouvelle option d’export de corpus au format ‘TXM 0.7.9’ (pour faciliter le chargement de corpus dans les portails TXM 0.6.3)
- Scripts
  - Appel de scripts R sous Windows corrigé

Retrouvez la liste exhaustive des changements dans la liste des [tickets de développement de TXM 0.8.2](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=70).

## Extensions

Les extensions suivantes ont été mises à jour pour TXM 0.8.2 : TreeTagger software, TreeTagger en, fr models, Media Player, Annotation Unité-Relation-Schéma (URS).

## Prérequis d'installation

-   Vous aurez besoin des droits d'installation sur votre machine pour pouvoir installer TXM ;
-   Vous aurez besoin d'un accès à Internet ;
-   TXM fonctionne sur des machines **64-bit** ;
-   Windows
    -   TXM est supporté pour Windows 7 et Windows 10
-   Mac OS X
    -   TXM est supporté pour Mac OS X 10.14
        -   Réglages de sécurité : il faut modifier les paramètres de « sécurité » du
            système pour pouvoir installer correctement TXM. Aller dans
            « Préférences Système &gt; Personnel &gt; Sécurité » et
            autoriser les applications téléchargées depuis Internet. Si
            on ne fait pas ce réglage, le problème ne se manifeste pas
            au moment de l'installation, mais au premier lancement.
-   Linux
    -   TXM est supporté pour Ubuntu 20.04 et 22.04, et ses variantes (Xubuntu, Kubuntu, Lubuntu, etc.)
    -   TXM fonctionne également sur des variantes du système Debian
    -   L'installation de TXM provoque l'installation des packages
        suivants : zenity, po-debconf, libwebkit2gtk-4.0-37, debconf, libc6 (>= 2.15), libgtk2-perl|libgtk3-perl, libblas3, libbz2-1.0, libcairo2, libgfortran5, libglib2.0-0, libgomp1, libjpeg8, liblapack3, liblzma5, libpango-1.0-0, libpangocairo-1.0-0, libpaper-utils, libpcre3, libpng16-16, libreadline7|libreadline8, libegl1-mesa, libtinfo5, libtiff5, libx11-6, libxt6, tcl8.6, ucf, unzip, xdg-utils, zip, zlib1g

## Instructions d'installation

-   vérifier les pré-requis d'installation, voir ci-dessus
-   télécharger l'installeur de TXM correspondant à votre système d'exploitation et à l'architecture de votre machine
-   double-cliquer sur l'icone de l'installeur de TXM
-   suivre les instructions de l'installeur: accepter la licence, etc.
    - remarque concernant les utilisateurs de **Linux** : la **première fois**
      que vous installez TXM sur une machine, il est également nécessaire,
      après l'installation et avant le premier lancement, de **quitter votre
      session de travail (déconnexion) puis de vous reconnecter** pour
      finaliser l'installation.
-   lancer TXM une première fois pour finaliser le processus d'installation (voir les instructions de lancement ci-dessous)
-   vous pouvez maintenant [installer TreeTagger]({{"InstallTreeTagger" | relative_url}}) (vous pouvez bien sûr utiliser TXM sans annotation linguistique automatique des mots, mais pour utiliser tout le potentiel de l'outil, il est vivement recommandé d'installer cette extension pour une exploitation optimale)

## En cas de problème d'installation

L'assistance pour l'installation et l'utilisation de TXM repose sur la communauté de ses utilisateurs et de ses développeurs.
{: style="margin-bottom: -10px"}

Pour plus d'aide concernant l'installation de TXM, vous pouvez :
{: style="margin-bottom: 5px"}

-   consulter les sections « Installer TXM sur sa machine » et « En cas de problème avec le logiciel » du [Manuel de TXM](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/manual/0.7.9/fr/manual1.xhtml)
-   consulter la section « Installation et mises à jour » de la [FAQ des
    utilisateurs de TXM](https://groupes.renater.fr/wiki/txm-users/public/faq) qui liste
    les problèmes d'installation les plus communs
-   vous inscrire à la liste de diffusion des utilisateurs de TXM ['txm-users'](https://groupes.renater.fr/sympa/info/txm-users) et y demander de l'aide
-   converser en ligne via [IRC sur le canal '\#txm' du serveur](irc.freenode.net)
-   contacter l'équipe TXM par mail à l'addresse <a href="mailto:{{ "textometrie@groupes.renater.fr" | encode_email }}" title="textometrie AT groupes.renater.fr">textometrie AT groupes.renater.fr</a>

## Lancer TXM

-   Windows
    -   menu « Démarrer / TXM-0.8.2 / TXM-0.8.2 » (faire glisser l'icone de
        l'application TXM dans la barre de lancement rapide pour
        rajouter un accès direct)
-   Mac OS X
    -   Naviguer dans le répertoire « Applications / TXM-0.8.2 » avec le Finder
        et double-cliquer sur l'icone de l'application TXM (faire
        glisser l'icone de l'application TXM dans le dock pour rajouter
        un accès direct)
-   Linux
    -   Naviguer dans la section « Applications installées » du Launchpad
        d'Unity et double-cliquer sur l'icone de l'application de TXM-0.8.2
        (pour rajouter un accès direct, faire un clic droit sur l'icone
        de TXM du Lanceur et sélectionner « Conserver dans le Lanceur »)

## Utiliser TXM

Voir les rubriques du menu Aide ainsi que les ressources en ligne suivantes :
{: style="margin-bottom: 5px"}

-   Lire le [manuel en ligne de TXM](https://txm.gitpages.huma-num.fr/txm-manual) ;
-   Télécharger le [manuel PDF de TXM](http://textometrie.ens-lyon.fr/files/documentation/TXM%20Manual%200.7.pdf) pour impression.
-   Regarder la [vidéo d'un atelier TXM](https://txm.gitpages.huma-num.fr/textometrie/html/enregistrement_atelier_initiation_TXM_fr.html)
-   Participer à un [atelier TXM](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
-   Participer au [wiki de la communauté des utilisateurs de TXM](https://groupes.renater.fr/wiki/txm-users)

## Retours de bugs

1.  Vérifiez que votre retour n'a pas déjà été signalé dans la [liste des tickets de retours de la plateforme TXM](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=70) ;
2.  Si ce n'est pas le cas, signalez le problème soit :
    -   dans la [liste de diffusion txm-users](https://groupes.renater.fr/sympa/info/txm-users) ;
    -   en éditant directement le [wiki
        txm-users](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.2) ;
    -   en tchatant directement avec les développeurs dans le canal IRC
        \#txm du serveur 'irc.freenode.net' ;
    -   en <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contactant l'équipe TXM par mail</a>.

N'hésitez pas à [nous contacter]({{"Nous-contacter" | relative_url}}) pour de plus amples informations.

L'équipe TXM
