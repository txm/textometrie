---
ititle: Bienvenue dans TXM 0.8.2
layout: page
lang: fr
ref: welcome txm-0.8.2
---

# Bienvenue dans TXM 0.8.2

## Nouveautés

En plus de corriger de nombreux bugs, cette nouvelle version comprend quelques éléments nouveaux notables :
{: style="margin-bottom: 5px"}

- Interface Générale
  - Affichage systématique de la progression des calculs
  - Accès simplifié aux utilitaires (macros) depuis le menu principal 'Utilitaires'
  - Affichage des tableaux de résultats corrigé sous Mac OS X
- Moteurs de recherche
  - Simplification de la recherche de séquences de mots en CQP (on saisit la séquence directement)
    - par exemple la saisie directe de 'parce que' dans le champ de recherche (sans les guillemets) provoque la recherche de la requête `[word="parce"] [word="que"]`
  - Simplification de la recherche de mots par d'autres moteurs que CQP, en fonction des extensions installées et des annotations disponibles :
    - extension 'TIGER Search' : recherche dans les arbres syntaxiques (moteur 'TIGER')
    - extension 'Annotation URS' : recherche dans les unités URS (moteur 'URSQL')
- Édition
  - Nouvelle recherche plein texte depuis les pages d’édition : équivalent du ‘Ctrl-F’ des applications courantes Firefox, Thunderbird, Writer, Calc... mais en exploitant bien sûr CQP
  - Lien hypertextuel non plus seulement de la Concordance vers l'Édition (double clic sur une ligne de concordance → visualisation dans l'Édition), mais aussi maintenant dans l'autre sens, de l'Édition vers la Concordance (on sélectionne les mots dans la page, puis on lance la concordance).
- Concordances
  - Nouvelles possibilités de formater les références de Concordances (à l'import du corpus ou depuis une concordance)
- Index
  - le lien hypertexte de retour à la concordance fait désormais en sorte que les occurrences de la concordance se limitent à celles utilisées pour calculer l'Index.
- Corpus
  - Possibilité d'intégrer à un corpus une documentation au format HTML, consultable dans un onglet dédié des Propriétés du corpus et pouvant renvoyer vers des sites web par liens hypertextes.
- Annotation
  - Redesign des barres d'annotation CQP par concordances
  - Meilleure protection de la sauvegarde d'annotations CQP
  - Nouvelle annotation URS par Concordances (extension URS)
- Export
  - Export des Dimensions d'une partition sous la forme d'un tableau au format CSV (séparateur de colonnes tabulation)
  - Nouvelle option d’export de corpus au format ‘TXM 0.7.9’ (pour faciliter le chargement de corpus dans les portails TXM 0.6.3)
- Scripts
  - Appel de scripts R sous Windows corrigé

Retrouvez la liste exhaustive des changements dans la liste des [tickets de développement de TXM 0.8.2](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=70).

## Extensions

Les extensions suivantes ont été mises à jour pour TXM 0.8.2 : TreeTagger software, TreeTagger en, fr models, Media Player, Annotation Unité-Relation-Schéma (URS).

## Utiliser TXM

Voir les rubriques du menu Aide ainsi que les ressources en ligne suivantes :
{: style="margin-bottom: 5px"}

-   Lire le [manuel en ligne de TXM](https://txm.gitpages.huma-num.fr/txm-manual) ;
-   Télécharger le [manuel PDF de TXM](http://textometrie.ens-lyon.fr/files/documentation/TXM%20Manual%200.7.pdf) pour impression.
-   Regarder la [vidéo d'un atelier TXM](https://txm.gitpages.huma-num.fr/textometrie/html/enregistrement_atelier_initiation_TXM_fr.html)
-   Participer à un [atelier TXM](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
-   Participer au [wiki de la communauté des utilisateurs de TXM](https://groupes.renater.fr/wiki/txm-users)

## Retours de bugs

1.  Vérifiez que votre retour n'a pas déjà été signalé dans la [liste des tickets de retours de la plateforme TXM](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=74) ;
2.  Si ce n'est pas le cas, signalez le problème soit :
    -   dans la [liste de diffusion txm-users](https://groupes.renater.fr/sympa//info/txm-users) ;
    -   en éditant directement le [wiki
        txm-users](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.2beta) ;
    -   en tchatant directement avec les développeurs dans le canal IRC
        \#txm du serveur 'irc.freenode.net' ;
    -   en <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contactant l'équipe TXM par mail</a>.

N'hésitez pas à [nous contacter]({{"Nous-contacter" | relative_url}}) pour de plus amples informations.

L'équipe TXM
