---
ititle: Campagne de tests de TXM 0.8.1 BETA
layout: page
lang: fr
ref: txm-0.8.1beta
redirect_from: /files/software/TXM/0.8.1beta/fr
---

# Bienvenue à la campagne de tests de TXM 0.8.1 BETA
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

{% include important.html content="Merci de ne pas installer cette version BETA sur votre machine si vous ne souhaitez pas nous aider à valider cette version de test." %}

{% include note.html content="Cette version s'installe à coté de TXM 0.8.0, les deux versions du logiciel peuvent cohabiter sur la même machine." %}

<p>&nbsp;</p>

## Sommaire
{: .no_toc}

1. TOC
{:toc}

---

## Nouveautés

En plus de corriger de nombreux bugs, cette nouvelle version comprend quelques éléments nouveaux notables :

- rétablissement du système de mises à jour
  - Attention : sous Windows, il faut lancer TXM en mode administrateur pour pouvoir faire des mises à jour
- consolidation du comportement et amélioration générale de l'interface de TXM
- fonctionnalités :
  - Spécificités : correction de l'export
  - Table Lexicale : nouveau paramètre Fmax
  - Visualisations graphiques :
    - raccourci copier « Ctrl-C » de graphiques
    - rétablissement des graphiques SVG pour les utilisateurs Ubuntu
  - Cooccurrences :
    - amélioration du décompte et du calcul de distance entre le pivot et les cooccurrents
    - amélioration du lien vers les concordances
  - Retour au texte : nouvelle préférence pour choisir l'emplacement de l'édition dans l'interface de TXM
  - Annotations : optimisation du temps de sauvegarde des annotations
  - Corpus : nouvelles options d'export

## Téléchargements

| [![]({{"/img/windows-icon.png" | absolute_url}}){: width="60" .center-image} Windows 7 et 10 (64 bit)](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.1beta/TXM_0.8.1beta2_2020-06-05_Win64_installer.exe) | [![]({{"/img/macosx-icon.png" | absolute_url}}){: width="60" .center-image} Mac OS X 10.15](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.1beta/TXM_0.8.1beta2_2020-06-05_MacOSX_installer.pkg) | [![]({{"/img/ubuntu-icon.png" | absolute_url}}){: width="60" .center-image} Ubuntu 16.04 et 18.04](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.1beta/TXM_0.8.1beta2_2020-06-05_Linux64_installer.deb) |

## Procédure de tests

- Merci de suivre la [recette de TXM 0.8.1 beta](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.1beta) dans le wiki txm-users ;
- Vous pouvez également nous adresser directement vos retours par mail à l'adresse textometrie at groupes.renater.fr.

## Planning

Nous envisageons de publier la version définitive de TXM 0.8.1 courant mai 2020, en fonction des retours.

Merci pour vos contributions,  
l'équipe TXM 


