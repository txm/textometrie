---
ititle: Téléchargements TXM
layout: page
lang: fr
ref: txm
---

# Téléchargements de TXM
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

## Version la plus récente

La dernière version de TXM est la [0.8.3]({{"/files/software/TXM/0.8.3" | absolute_url}}) du 20 juin 2023.

Versions
--
| [0.8.4]({{"/files/software/TXM/0.8.4" | absolute_url}}) | [12 février 2025](https://groupes.renater.fr/sympa/arc/txm-users/2023-06/msg00017.html) | installeur | {::nomarkdown}<ul><li>Texts, AFC, CA, ...</li></ul>{:/}
| [0.8.3]({{"/files/software/TXM/0.8.3" | absolute_url}}) | [20 juin 2023](https://groupes.renater.fr/sympa/arc/txm-users/2023-06/msg00017.html) | installeur | {::nomarkdown}<ul><li>compatibilité Mac</li><li>export/import de propriétés de mots</li><li>export/import d’appel de calcul</li></ul>{:/}
| [0.8.2]({{"/files/software/TXM/0.8.2" | absolute_url}}) | [10 juin 2022](https://groupes.renater.fr/sympa/arc/txm-users/2022-06) | installeur | {::nomarkdown}<ul><li>recherche de séquences de mots simplifiée</li><li>recherche plein texte depuis les pages d’édition</li><li>formatage des références de Concordances</li><li>documentation de corpus</li></ul>{:/}
| [0.8.1]({{"/files/software/TXM/0.8.1" | absolute_url}}) | [29 juin 2020](https://groupes.renater.fr/sympa/arc/txm-users/2020-06/msg00003.html) | installeur | {::nomarkdown}<ul><li>consolidation de l'architecture</li><li>consolidation de l'interface utilisateur</li><li>rétablissement des mises à jour</li></ul>{:/}
| [0.8.0]({{"/files/software/TXM/0.8.0" | absolute_url}}) | [29 mai 2019](https://groupes.renater.fr/sympa/arc/txm-users/2019-05/msg00016.html) | installeur | {::nomarkdown}<ul><li>persistance des résultats</li><li>réglage des paramètres de commandes depuis la fenêtre de résultats</li><li>annotation de mots par concordance</li><li>métadonnées au format Excel/Calc</li><li>extension TreeTagger</li><li>possibilité d'avoir plusieurs versions de TXM en même temps</li></ul>{:/}
| [0.7.9]({{"/files/software/TXM/0.7.9" | absolute_url}}) | [9 janvier 2018](https://groupes.renater.fr/sympa/arc/txm-users/2018-01/msg00000.html) | installeur | {::nomarkdown}<ul><li>meilleure intégration de R</li><li>compatibilité avec l'extension "Annotation URS"</li></ul>{:/}
| [0.7.8](http://textometrie.ens-lyon.fr/files/software/TXM/0.7.8) | [20 avril 2017](https://groupes.renater.fr/sympa/arc/txm-users/2017-04/msg00004.html) | mise à jour, puis installeur | {::nomarkdown}<ul><li>installation windows pour salles de cours</li><li>installation de R plus nécessaire pour Mac OS X et Linux</li><li>annotation par concordances</li><li>import xtz</li><li>import docx/odt</li><li>navigation progression ↔ concordance ↔ édition</li></ul>{:/}
| [0.7.7](http://textometrie.ens-lyon.fr/files/software/TXM/0.7.7) | [31 juillet 2015](https://groupes.renater.fr/sympa/arc/txm-users/2015-07/msg00016.html) | mise à jour | {::nomarkdown}<ul><li>installation de Java plus nécessaire</li><li>graphiques interactifs</li></ul>{:/}
| [0.7.6](http://textometrie.ens-lyon.fr/files/software/TXM/0.7.6) | [21 juillet 2014](https://groupes.renater.fr/sympa/arc/txm-users/2014-07/msg00008.html) | mise à jour | {::nomarkdown}<ul><li>nouvelles visualisations graphiques (AFC, Spécificités...)</li></ul>{:/}
| [0.7.5](http://textometrie.ens-lyon.fr/files/software/TXM/0.7.5) | [12 février 2014](https://groupes.renater.fr/sympa/arc/txm-users/2014-02/msg00001.html) | installeur | {::nomarkdown}<ul><li>mise à jour automatique</li><li>extensions</li><li>calculs interruptibles</li></ul>{:/}

- [Versions plus anciennes]({{"/files/software/TXM/older versions" | absolute_url}})
{: style="padding-top: 30px"}

</div>

