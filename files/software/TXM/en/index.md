---
ititle: TXM Download
layout: page
lang: en
ref: txm
---

# TXM Download
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

## Latest

The latest version of TXM is [0.8.1]({{"/files/software/TXM/0.8.2/en" | absolute_url}}) June 10, 2022.

Versions
--
| [0.8.4]({{"/files/software/TXM/0.8.4/en" | absolute_url}}) | [February 12, 2025](https://groupes.renater.fr/sympa/arc/txm-users/2023-06/msg00017.html) | installer | {::nomarkdown} <ul> <li>Texts, CA, AHC, ...</li><li></li><li></li></ul> {:/}
| [0.8.3]({{"/files/software/TXM/0.8.3/en" | absolute_url}}) | [June 20, 2023](https://groupes.renater.fr/sympa/arc/txm-users/2023-06/msg00017.html) | installer | {::nomarkdown} <ul> <li></li><li></li><li></li></ul> {:/}
| [0.8.2]({{"/files/software/TXM/0.8.2/en" | absolute_url}}) | [June 10, 2022](https://groupes.renater.fr/sympa/arc/txm-users/2022-06) | installer | {::nomarkdown} <ul> <li></li><li></li><li></li></ul> {:/}
| [0.8.1]({{"/files/software/TXM/0.8.1/en" | absolute_url}}) | [June 29, 2020](https://groupes.renater.fr/sympa/arc/txm-users/2020-06/msg00003.html) | installer | {::nomarkdown} <ul> <li>architecture consolidation </li><li>GUI consolidation</li><li>restoration of updates service</li></ul> {:/}
| [0.8.0]({{"/files/software/TXM/0.8.0/en" | absolute_url}}) | [May 29, 2019](https://groupes.renater.fr/sympa/arc/txm-users/2019-05/msg00016.html) | installer | {::nomarkdown} <ul> <li> persistence of results </li> <li> command parameters tuning in results window </li> <li> word annotation by concordance </li> <li> metadata in Excel/Calc format </li> <li> TreeTagger extension </li> <li> separate installations </li> </ul> {:/}
| [0.7.9]({{"/files/software/TXM/0.7.9/en" | absolute_url}}) | [January 9, 2018](https://groupes.renater.fr/sympa/arc/txm-users/2018-01/msg00000.html) | installer | {::nomarkdown} <ul> <li> better integration of R </li> <li> compatibility with "URS Annotation" extension</li> </ul> {:/}
| [0.7.8](http://textometrie.ens-lyon.fr/files/software/TXM/0.7.8) | [April 20, 2017](https://groupes.renater.fr/sympa/arc/txm-users/2017-04/msg00004.html) | installer | {::nomarkdown} <ul> <li> windows installation for classrooms </li> <li> no more R installation needed for Mac OS X and Linux </li> <li> annotation by concordance </li> <li> xtz import module </li> <li> docx/odt import module</li> <li> navigation between progression ↔ concordance ↔ edition </li> </ul> {:/}
| [0.7.7](http://textometrie.ens-lyon.fr/files/software/TXM/0.7.7) | [July 31, 2015](https://groupes.renater.fr/sympa/arc/txm-users/2015-07/msg00016.html) | update | {::nomarkdown} <ul> <li> no more Java installation needed </li> <li> interactive graphics </li> </ul> {:/}
| [0.7.6](http://textometrie.ens-lyon.fr/files/software/TXM/0.7.6) | [July 21, 2014](https://groupes.renater.fr/sympa/arc/txm-users/2014-07/msg00008.html) | update | {::nomarkdown} <ul> <li> new graphic visualizations (CA, Specificities...) </li> </ul> {:/}
| [0.7.5](http://textometrie.ens-lyon.fr/files/software/TXM/0.7.5) | [February 12, 2014](https://groupes.renater.fr/sympa/arc/txm-users/2014-02/msg00001.html) | installer | {::nomarkdown} <ul> <li> automatic updates </li> <li> extensions </li> <li> interruptible calculations </li> </ul> {:/}

- [Older versions]({{"/files/software/TXM/older versions/en" | absolute_url}})
{: style="padding-top: 30px"}

