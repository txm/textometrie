---
ititle: Welcome to TXM 0.8.1
layout: page
lang: en
ref: welcome txm-0.8.1
---

# Welcome to TXM 0.8.1

## News

In addition to fixing many bugs, this new version includes some notable new elements:
{: style="margin-bottom: 5px"}

- consolidation of TXM 0.8.0 architecture and TXM GUI
- restoration of TXM update service to benefit from improvements more regularly
  - Warning: under Windows, TXM must be launched in administrator mode to be able to do an update
- fonctionnalities:
  - Annotation by concordances: consolidation and optimization of backup time
  - back to text: new preference to choose where the text edition is opened
  - Lexicale Table: new Fmax parameter 
  - charts:
    - Copy chart keyboard shortcut: Ctrl-C
    - restore graphics to SVG format for Ubuntu users
  - Co-occurrences:
    - better accuracy of co-occurrents count and mean distance computing
    - better links to concordances
  - Specificities: export correction
  - Transcriber Import: new hyperlinks between transcription edition and media playback
  - Corpus: new export options

Find out all changes in [TXM 0.8.1 development tickets](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=73).

## Extensions

The following extensions have been updated for TXM 0.8.1:
{: style="margin-bottom: 5px"}

- TreeTagger software: installation on Mac OS X Catalina
- TreeTagger en, fr models
- Media Player: new synchronization parameters to the media files, remote playback of media files with access control
- Unit-Relation-Schema (URS) annotation

## Using TXM

See the 'Help' menu entries and the following online resources:
{: style="margin-bottom: 5px"}

-   Read [TXM manual
    online](https://txm.gitpages.huma-num.fr/txm-manual)
-   Download [TXM PDF manual](http://textometrie.ens-lyon.fr/files/documentation/TXM%20Manual%200.7.pdf)
    for printing
-   View the [TXM workshop video](https://txm.gitpages.huma-num.fr/textometrie/html/enregistrement_atelier_initiation_TXM_fr.html)
-   Participate to a [TXM workshop](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
-   Participate to [TXM users community wiki](https://groupes.renater.fr/wiki/txm-users)

## Feedback and bug reports

1.  Check if your feedback is not already reported in the [TXM platform
    bug
    tracker](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=74)
2.  If not, send your feedback either:
    -   in the [txm-open mailing
        list](https://lists.sourceforge.net/lists/listinfo/txm-open)
    -   by directly editing the [txm-users
        wiki](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.0)
    -   by chating directly with the developers on the \#txm IRC channel
        of the 'irc.freenode.net' server
    -   by <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contacting the TXM team by mail</a>

Please [contact us]({{"en/Nous-contacter" | relative_url}}) for further information.

The TXM team
