---
ititle: Téléchargement de TXM 0.8.1
layout: page
lang: fr
ref: txm-0.8.1
---

## <Cette version a été remplacée par [TXM 0.8.2]({{"/files/software/TXM/0.8.2" | absolute_url}})>

# Téléchargement de TXM 0.8.1
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

| [![]({{"/img/windows-icon.png" | absolute_url}}){: width="125" .center-image} Windows 7 et 10 (64 bit)](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.1/TXM_0.8.1_2020-06-29_Win64_installer.exe) | [![]({{"/img/macosx-icon.png" | absolute_url}}){: width="125" .center-image} Mac OS X 10.14](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.1/TXM_0.8.1_2020-06-29_MacOSX_installer.pkg) | [![]({{"/img/ubuntu-icon.png" | absolute_url}}){: width="125" .center-image} Ubuntu 16.04, 18.04 et 20.04](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.1/TXM_0.8.1_2020-12-16_Linux64_installer.deb)<a href="#note"><sup>(1)</sup></a> |

<p>&nbsp;</p>

## Sommaire
{: .no_toc}

1. TOC
{:toc}

---

### Versions antérieures de TXM 0.8.1
{: .no_toc}

- 29 juin 2020: [Ubuntu](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.1/TXM_0.8.1_2020-06-29_Linux64_installer.deb) l'installeur de TXM-0.8.1 pour Ubuntu a été mis à jour le 17 décembre 2020 pour corriger le fonctionnement des calculs d'AFC et de CAH sous Ubuntu 20.04 et 20.10. 

## Nouveautés

En plus de corriger de nombreux bugs, cette nouvelle version comprend quelques éléments nouveaux notables :
{: style="margin-bottom: 5px"}

- consolidation de l'architecture de TXM 0.8.0 et du comportement de l’interface utilisateur de TXM
- rétablissement du système de mises à jour pour bénéficier d'améliorations plus régulièrement
  - Attention : sous Windows, il faut lancer TXM en mode administrateur pour pouvoir faire des mises à jour
- fonctionnalités :
  - Annotation par concordances : consolidation et optimisation du temps de sauvegarde
  - retour au texte : nouvelle préférence pour choisir où s'ouvrira l'édition dans l'interface de TXM
  - Table Lexicale : nouveau paramètre Fmax
  - visualisations graphiques :
    - nouveau raccourci copier « Ctrl-C » de graphiques
    - rétablissement des graphiques au format SVG pour les utilisateurs Ubuntu
  - Spécificités : correction de l'export
  - Cooccurrences :
    - amélioration du décompte et du calcul de distance entre le pivot et ses cooccurrents
    - amélioration du lien vers les concordances
  - Import Transcriber : nouveaux hyperliens entre l'édition de la transcription et la lecture du média
  - Corpus : nouvelles options d'export

Retrouvez la liste exhaustive des changements dans la liste des [tickets de développement de TXM 0.8.1](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=73).

## Extensions

Les extensions suivantes ont été mises à jour pour TXM 0.8.1 :
{: style="margin-bottom: 5px"}

- TreeTagger software : installation sur Mac OS X Catalina
- TreeTagger en, fr models
- Media Player : nouveaux paramètres de synchronisation aux fichiers média, lecture distante des fichiers média avec contrôle d'accès
- Annotation Unité-Relation-Schéma (URS)

## Prérequis d'installation

-   Vous aurez besoin des droits d'installation sur votre machine pour pouvoir installer TXM ;
-   Vous aurez besoin d'un accès à Internet ;
-   TXM fonctionne sur des machines **64-bit** ;
-   Windows
    -   TXM est supporté pour Windows 7 et Windows 10
-   Mac OS X
    -   TXM est supporté pour Mac OS X 10.14
        -   Réglages de sécurité : il faut modifier les paramètres de « sécurité » du
            système pour pouvoir installer correctement TXM. Aller dans
            « Préférences Système &gt; Personnel &gt; Sécurité » et
            autoriser les applications téléchargées depuis Internet. Si
            on ne fait pas ce réglage, le problème ne se manifeste pas
            au moment de l'installation, mais au premier lancement.
-   Linux
    -   TXM est supporté pour Ubuntu 16.04, 18.04 et 20.04<a href="#note"><sup>(*)</sup></a>, et ses variantes (Xubuntu, Kubuntu, Lubuntu, etc.)
    -   TXM fonctionne également sur des variantes du système Debian
    -   L'installation de TXM provoque l'installation des packages
        suivants : zenity, po-debconf, libwebkitgtk-1.0-0, debconf,
        libc6 (&gt;= 2.15), libgtk2-perl, libblas3, libbz2-1.0,
        libcairo2, libgfortran3, libglib2.0-0, libgomp1, libjpeg8,
        liblapack3, liblzma5, libpango-1.0-0, libpangocairo-1.0-0,
        libpaper-utils, libpcre3, libpng12-0, libreadline6, libtiff5,
        libx11-6, libxt6, tcl8.5, tk8.5, ucf, unzip, xdg-utils, zip,
        zlib1g.

## Instructions d'installation

-   vérifier les pré-requis d'installation, voir ci-dessus
-   télécharger l'installeur de TXM correspondant à votre système d'exploitation et à l'architecture de votre machine
-   double-cliquer sur l'icone de l'installeur de TXM
-   suivre les instructions de l'installeur: accepter la licence, etc.
    - remarque concernant les utilisateurs de **Linux** : la **première fois**
      que vous installez TXM sur une machine, il est également nécessaire,
      après l'installation et avant le premier lancement, de **quitter votre
      session de travail (déconnexion) puis de vous reconnecter** pour
      finaliser l'installation.
-   lancer TXM une première fois pour finaliser le processus d'installation (voir les instructions de lancement ci-dessous)
-   vous pouvez maintenant [installer TreeTagger]({{"InstallTreeTagger" | relative_url}}) (optionnel mais recommandé)

## En cas de problème d'installation

L'assistance pour l'installation et l'utilisation de TXM repose sur la communauté de ses utilisateurs et de ses développeurs.
{: style="margin-bottom: -10px"}

Pour plus d'aide concernant l'installation de TXM, vous pouvez :
{: style="margin-bottom: 5px"}

-   consulter les sections « Installer TXM sur sa machine » et « En cas de problème avec le logiciel » du [Manuel de TXM](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/manual/0.7.9/fr/manual1.xhtml)
-   consulter la section « Installation et mises à jour » de la [FAQ des
    utilisateurs de TXM](https://groupes.renater.fr/wiki/txm-users/public/faq) qui liste
    les problèmes d'installation les plus communs
-   vous inscrire à la liste de diffusion des utilisateurs de TXM ['txm-users'](https://groupes.renater.fr/sympa/info/txm-users) et y demander de l'aide
-   converser en ligne via [IRC sur le canal '\#txm' du serveur](irc.freenode.net)
-   contacter l'équipe TXM par mail à l'addresse <a href="mailto:{{ "textometrie@groupes.renater.fr" | encode_email }}" title="textometrie AT groupes.renater.fr">textometrie AT groupes.renater.fr</a>

## Lancer TXM

-   Windows
    -   menu « Démarrer / TXM-0.8.1 / TXM-0.8.1 » (faire glisser l'icone de
        l'application TXM dans la barre de lancement rapide pour
        rajouter un accès direct)
-   Mac OS X
    -   Naviguer dans le répertoire « Applications / TXM-0.8.1 » avec le Finder
        et double-cliquer sur l'icone de l'application TXM (faire
        glisser l'icone de l'application TXM dans le dock pour rajouter
        un accès direct)
-   Linux
    -   Naviguer dans la section « Applications installées » du Launchpad
        d'Unity et double-cliquer sur l'icone de l'application de TXM
        (pour rajouter un accès direct, faire un clic droit sur l'icone
        de TXM du Lanceur et sélectionner « Conserver dans le Lanceur »)

## Utiliser TXM

Voir les rubriques du menu Aide ainsi que les ressources en ligne suivantes :
{: style="margin-bottom: 5px"}

-   Lire le [manuel en ligne de TXM](https://txm.gitpages.huma-num.fr/txm-manual) ;
-   Télécharger le [manuel PDF de TXM](http://textometrie.ens-lyon.fr/files/documentation/TXM%20Manual%200.7.pdf) pour impression.
-   Regarder la [vidéo d'un atelier TXM](https://txm.gitpages.huma-num.fr/textometrie/html/enregistrement_atelier_initiation_TXM_fr.html)
-   Participer à un [atelier TXM](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
-   Participer au [wiki de la communauté des utilisateurs de TXM](https://groupes.renater.fr/wiki/txm-users)

## Retours de bugs

1.  Vérifiez que votre retour n'a pas déjà été signalé dans la [liste des tickets de retours de la plateforme TXM](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=74) ;
2.  Si ce n'est pas le cas, signalez le problème soit :
    -   dans la [liste de diffusion txm-users](https://groupes.renater.fr/sympa//info/txm-users) ;
    -   en éditant directement le [wiki
        txm-users](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.1) ;
    -   en tchatant directement avec les développeurs dans le canal IRC
        \#txm du serveur 'irc.freenode.net' ;
    -   en <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contactant l'équipe TXM par mail</a>.

N'hésitez pas à [nous contacter]({{"Nous-contacter" | relative_url}}) pour de plus amples informations.

L'équipe TXM

<hr/>
Notes :
{: style="margin-bottom: 5px"}

1. <a id="note"></a>Attention, sous Ubuntu 20.04, le navigateur interne de TXM ne pourra pas afficher les pages de sites web utilisant le protocole HTTPS avec un certificat de serveur auto-signé (comme, par exemple, les pages du nouveau site web du projet textometrie).
