---
ititle: Bienvenue dans TXM 0.8.1
layout: page
lang: fr
ref: welcome txm-0.8.1
---

# Bienvenue dans TXM 0.8.1

## Nouveautés

En plus de corriger de nombreux bugs, cette nouvelle version comprend quelques éléments nouveaux notables :
{: style="margin-bottom: 5px"}

- consolidation de l'architecture de TXM 0.8.0 et du comportement de l’interface utilisateur de TXM
- rétablissement du système de mises à jour pour bénéficier d'améliorations plus régulièrement
  - Attention : sous Windows, il faut lancer TXM en mode administrateur pour pouvoir faire des mises à jour
- fonctionnalités :
  - Annotation par concordances : consolidation et optimisation du temps de sauvegarde
  - retour au texte : nouvelle préférence pour choisir où s'ouvrira l'édition dans l'interface de TXM
  - Table Lexicale : nouveau paramètre Fmax
  - visualisations graphiques :
    - nouveau raccourci copier « Ctrl-C » de graphiques
    - rétablissement des graphiques au format SVG pour les utilisateurs Ubuntu
  - Spécificités : correction de l'export
  - Cooccurrences :
    - amélioration du décompte et du calcul de distance entre le pivot et ses cooccurrents
    - amélioration du lien vers les concordances
  - Import Transcriber : nouveaux hyperliens entre l'édition de la transcription et la lecture du média
  - Corpus : nouvelles options d'export

Retrouvez la liste exhaustive des changements dans la liste des [tickets de développement de TXM 0.8.1](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=73).

## Extensions

Les extensions suivantes ont été mises à jour pour TXM 0.8.1 :
{: style="margin-bottom: 5px"}

- TreeTagger software : installation sur Mac OS X Catalina
- TreeTagger en, fr models
- Media Player : nouveaux paramètres de synchronisation aux fichiers média, lecture distante des fichiers média avec contrôle d'accès
- Annotation Unité-Relation-Schéma (URS)

## Utiliser TXM

Voir les rubriques du menu Aide ainsi que les ressources en ligne suivantes :
{: style="margin-bottom: 5px"}

-   Lire le [manuel en ligne de TXM](https://txm.gitpages.huma-num.fr/txm-manual) ;
-   Télécharger le [manuel PDF de TXM](http://textometrie.ens-lyon.fr/files/documentation/TXM%20Manual%200.7.pdf) pour impression.
-   Regarder la [vidéo d'un atelier TXM](https://txm.gitpages.huma-num.fr/textometrie/html/enregistrement_atelier_initiation_TXM_fr.html)
-   Participer à un [atelier TXM](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
-   Participer au [wiki de la communauté des utilisateurs de TXM](https://groupes.renater.fr/wiki/txm-users)

## Retours de bugs

1.  Vérifiez que votre retour n'a pas déjà été signalé dans la [liste des tickets de retours de la plateforme TXM](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=74) ;
2.  Si ce n'est pas le cas, signalez le problème soit :
    -   dans la [liste de diffusion txm-users](https://groupes.renater.fr/sympa//info/txm-users) ;
    -   en éditant directement le [wiki
        txm-users](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.1) ;
    -   en tchatant directement avec les développeurs dans le canal IRC
        \#txm du serveur 'irc.freenode.net' ;
    -   en <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contactant l'équipe TXM par mail</a>.

N'hésitez pas à [nous contacter]({{"Nous-contacter" | relative_url}}) pour de plus amples informations.

L'équipe TXM
