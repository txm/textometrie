---
ititle: TXM 0.8.2 beta Download
layout: page
lang: en
ref: txm-0.8.2 beta
---

#  TXM 0.8.2 beta Download
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

| [![]({{"/img/windows-icon.png" | absolute_url}}){: width="125" .center-image} Windows 7 and 10 (64 bit)](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2beta/TXM_0.8.2beta_2022-01-24_Win64_installer.exe) | [![]({{"/img/macosx-icon.png" | absolute_url}}){: width="125" .center-image} Mac OS X 10.14](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2beta/TXM_0.8.2beta_2022-01-24_MacOSX_installer.pkg) | [![]({{"/img/ubuntu-icon.png" | absolute_url}}){: width="125" .center-image} Ubuntu 18.04 and 20.04](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2beta/TXM_0.8.2beta_2022-01-24_Linux64_installer.deb) |

## News

In addition to fixing many bugs, this new version includes some notable new elements:
{: style="margin-bottom: 5px"}

- General Interface
  - Systematic display of calculation progress
  - Simplified access to utilities (macros) from the main menu
  - Corrected display of results tables (row heights under Mac OS X)
- Search engines
  - New full text search from edition pages:  
  (equivalent to the 'Ctrl-F' of common applications: Firefox, Thunderbird, Writer, Calc, etc.)
  - Simplification of the search for sequences of words in CQP (you enter the sequence directly)
    - for example, typing 'parce que' (without quotation marks) searches for the query `[word="parce"] [word="que"]`
  - Simplification of word search by other engines than CQP, depending on the installed extensions and available annotations:
    - TIGER Search' extension: search in syntax trees ('TIGER' engine)
    - URS Annotation' extension: search in URS units ('URSQL' engine)
- Concordances
  - New possibility to format Concordances references
  - Use of words from an edition page to launch a concordance (select the words in the page, then launch the concordance)
- Annotation
  - Redesign of CQP annotation bars by concordances
  - Better protection of CQP annotations saving
  - New URS annotation by Concordances (URS extension)
- Calling R scripts in Windows fixed
- New corpus export option in 'TXM 0.7.9' format (for TXM 0.6.3 portals)

Find out all changes in [TXM 0.8.2 development tickets](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=70).

## Extensions

The following extensions have been updated for TXM 0.8.2:
{: style="margin-bottom: 5px"}

- TreeTagger software: installation on Mac OS X Catalina
- TreeTagger en, fr models
- Media Player: new synchronization parameters to the media files, remote playback of media files with access control
- Unit-Relation-Schema (URS) annotation

## Installation Prerequisites
{: .no_toc}

-   you need to have 'new software installation privilege' on your
    machine to install TXM
-   you need Internet access
-   TXM works on **64-bit** machines
-   Windows
    -   TXM is supported for Windows 7 and Windows 10
-   Mac OS X
    -   TXM is supported for 10.14
        -   Security settings: from Mac OS X 10.9 and upper, you need to
            change the system security settings to install TXM properly.
            Go to "System Preferences &gt; Personal &gt; Security" and
            allow applications downloaded from the Internet. If that
            preference is not set, the problem doesn't occur at
            installation time, but at the first launch.
-   Linux
    -   TXM is supported for Ubuntu 16.04, 18.04 and 20.04, and its variants (Xubuntu,
        Kubuntu, Lubuntu, etc.)
    -   TXM installation process installs the following dependencies:
        zenity, po-debconf, libwebkitgtk-1.0-0, debconf, libc6 (&gt;=
        2.15), libgtk2-perl, libblas3, libbz2-1.0, libcairo2,
        libgfortran3, libglib2.0-0, libgomp1, libjpeg8, liblapack3,
        liblzma5, libpango-1.0-0, libpangocairo-1.0-0, libpaper-utils,
        libpcre3, libpng12-0, libreadline6, libtiff5, libx11-6, libxt6,
        tcl8.5, tk8.5, ucf, unzip, xdg-utils, zip, zlib1g.

## Installation Instructions

-   verify the 'Installation Prerequisites' for your machine, see above
-   download the TXM setup file for your system and machine architecture
    (64-bit)
-   double-click on the TXM setup file icon
-   follow the instructions on screen: accept licence, etc.
    - note concerning **Linux** users: the **first time** you install TXM on a
      machine, you also need to **quit your working session (logout) and login
      again** to finalize the installation process.
-   run TXM once to finalize the setup process (see running instructions
    below)
-   you can now [install TreeTagger]({{"en/InstallTreeTagger" | relative_url}}) (optional but recommended)
-   The End

## Troubleshooting installation problems

TXM support is community based.
{: style="margin-bottom: -10px"}

For further help concerning TXM installation:
{: style="margin-bottom: 5px"}

-   consult the "Installer TXM sur sa machine" and the "En cas de
    problème avec le logiciel" sections of the [TXM
    manual](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/manual/0.7.9/fr/manual1.xhtml)
    (in French)
-   consult the "Installation et mises à jour" section of the [TXM users
    FAQ](https://groupes.renater.fr/wiki/txm-users/public/faq) which
    lists some of the common installation problems people run into.
-   subscribe to the
    ['txm-users'](https://groupes.renater.fr/sympa/info/txm-users)
    mailing list and ask for help
-   chat online via IRC on the '\#txm' channel of the irc.freenode.net
    server
-   contact the TXM team by email at <a href="mailto:{{ "textometrie@groupes.renater.fr" | encode_email }}" title="textometrie AT groupes.renater.fr">textometrie AT groupes.renater.fr</a>

## Running TXM

-   Windows
    -   Menu 'Start / TXM-0.8.2 / TXM-0.8.2' (drag TXM application icon to the Quick
        Access Toolbar to add direct access)
-   Mac OS X
    -   Navigate to the 'Applications / TXM-0.8.2' folder in the Finder and
        double-click on TXM application icon (drag TXM application icon
        to your Dock to add direct access)
-   Linux
    -   Navigate to the 'Installed Applications' section in the
        Launchpad and double-click on TXM application icon (right-click
        on TXM icon in the Dock and select 'Keep in Dock' to add direct
        access)

## Using TXM

See the 'Help' menu entries and the following online resources:
{: style="margin-bottom: 5px"}

-   Read [TXM manual
    online](https://txm.gitpages.huma-num.fr/txm-manual)
-   Download [TXM PDF manual](http://textometrie.ens-lyon.fr/files/documentation/TXM%20Manual%200.7.pdf)
    for printing
-   View the [TXM workshop video](http://txm.sourceforge.net/enregistrement_atelier_initiation_TXM_fr.html)
-   Participate to a [TXM workshop](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
-   Participate to [TXM users community wiki](https://groupes.renater.fr/wiki/txm-users)

## Feedback and bug reports

1.  Check if your feedback is not already reported in the [TXM platform
    bug
    tracker](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=74)
2.  If not, send your feedback either:
    -   in the [txm-open mailing
        list](https://lists.sourceforge.net/lists/listinfo/txm-open)
    -   by directly editing the [txm-users
        wiki](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.2)
    -   by chating directly with the developers on the \#txm IRC channel
        of the 'irc.freenode.net' server
    -   by <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contacting the TXM team by mail</a>

Please [contact us]({{"en/Nous-contacter" | relative_url}}) for further information.

The TXM team

<hr/>
Notes:
{: style="margin-bottom: 5px"}

1. <a id="note"></a>Please note, under Ubuntu 20.04, the internal browser of TXM will not be able to display the pages of websites using the HTTPS protocol with a self-signed server certificate (like, for example, the pages of the new textometry project website).
