---
ititle: Téléchargement de TXM 0.8.2 beta
layout: page
lang: fr
ref: txm-0.8.2 beta
---

# Bienvenue à la campagne de tests de TXM 0.8.2 beta
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

{% include important.html content="Merci de ne pas installer cette version BETA sur votre machine si vous ne souhaitez pas nous aider à valider cette version de test." %}

{% include note.html content="Cette version s'installe à coté de TXM 0.8.1, les deux versions du logiciel peuvent cohabiter sur la même machine." %}

<p>&nbsp;</p>


## Sommaire
{: .no_toc}

1. TOC
{:toc}

---



## Nouveautés

En plus de corriger de nombreux bugs, cette nouvelle version comprend quelques éléments nouveaux notables :
{: style="margin-bottom: 5px"}

- Interface Générale
  - Affichage systématique de la progression des calculs
  - Accès simplifié aux utilitaires (macros) depuis le menu principal
  - Affichage des tableaux de résultats corrigé (hauteurs de lignes sous Mac OS X)
- Moteurs de recherche
  - Nouvelle recherche plein texte depuis les pages d'édition :  
  (équivalent du 'Ctrl-F' des applications courantes : Firefox, Thunderbird, Writer, Calc, etc.)
  - Simplification de la recherche de séquences de mots en CQP (on saisit la séquence directement)
    - par exemple la saisie de 'parce que' (sans les guillemets) provoque la recherche de la requête `[word="parce"] [word="que"]`
  - Simplification de la recherche de mots par d'autres moteurs que CQP, en fonction des extensions installées et des annotations disponibles :
    - extension 'TIGER Search' : recherche dans les arbres syntaxiques (moteur 'TIGER')
    - extension 'Annotation URS' : recherche dans les unités URS (moteur 'URSQL')
- Concordances
  - Nouvelle possibilité de formater les références de Concordances
  - Utilisation des mots d'une page d'édition pour lancer une concordance (on sélectionne les mots dans la page, puis on lance la concordance)
- Annotation
  - Redesign des barres d'annotation CQP par concordances
  - Meilleure protection de la sauvegarde d'annotations CQP
  - Nouvelle annotation URS par Concordances (extension URS)
- Appel de scripts R sous Windows corrigé
- Nouvelle option d'export de corpus au format 'TXM 0.7.9' (pour les portails TXM 0.6.3)

Retrouvez la liste exhaustive des changements dans la liste des [tickets de développement de TXM 0.8.2](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=70).

<p>&nbsp;</p>

## Procédure de tests

- Merci de suivre la [recette de TXM 0.8.2 beta](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.2beta) dans le wiki txm-users ;
- Vous pouvez également nous adresser directement vos retours par mail à l'adresse textometrie at groupes.renater.fr.


# Téléchargement de TXM 0.8.2 beta
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

| [![]({{"/img/windows-icon.png" | absolute_url}}){: width="125" .center-image} Windows 7 et 10 (64 bit)](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2beta/TXM_0.8.2beta_2022-01-24_Win64_installer.exe) | [![]({{"/img/macosx-icon.png" | absolute_url}}){: width="125" .center-image} Mac OS X 10.14](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2beta/TXM_0.8.2beta_2022-01-24_MacOSX_installer.pkg) | [![]({{"/img/ubuntu-icon.png" | absolute_url}}){: width="125" .center-image} Ubuntu 18.04 et 20.04](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.2beta/TXM_0.8.2beta_2022-01-24_Linux64_installer.deb) |

## Planning

Nous envisageons de publier la version définitive de TXM 0.8.2 courant février 2022, en fonction des retours.

Merci pour vos contributions,  
l'équipe TXM 
