---
ititle: Téléchargement de TXM 0.8.4
layout: page
lang: fr
ref: txm-0.8.4
---

# Téléchargement de TXM 0.8.4
{: style="text-align: center; padding-bottom: 30px" .no_toc}

## Sommaire
{: .no_toc}

1. TOC
{:toc}

---

# Logiciels d'installation de TXM 0.8.4 pour Windows, Mac et Linux
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

| [![]({{"/img/windows-icon.png" | absolute_url}}){: width="125" .center-image} Windows 10 - 11](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.4/TXM_0.8.4_2025-02-12_Win64_installer.exe) | [![]({{"/img/macosx-icon.png" | absolute_url}}){: width="125" .center-image} Mac OS X 12.0 - 13.0.0](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.4/TXM_0.8.4_2025-02-12_MacOSX_installer.pkg) | [![]({{"/img/ubuntu-icon.png" | absolute_url}}){: width="125" .center-image} Ubuntu 20.04 - 24.04](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.4/TXM_0.8.4_2025-02-12_Linux64_installer.deb) |

Version courante : 12 février 2025

[Versions précédentes](https://gitlab.huma-num.fr/txm/txm-software/-/tree/master/files/software/TXM/0.8.4) : (aucune)

## Nouveautés

En plus de corriger de nombreux bugs, cette nouvelle version comprend quelques éléments nouveaux notables :
{: style="margin-bottom: 5px"}

- Outils
  - Table lexicale
    - Désormais une Table Lexicale peut être **enregistrée dans un fichier tableur** au format Libre Office Calc (.ods) ou bien être remplie à partir du contenu d'un fichier tableur au format Libre Office Calc
    - Un corpus peut désormais **importer directement** une table lexicale depuis un fichier indépendamment d'une partition
  - AFC
    - On peut désormais **visualiser tous les plans** de l'AFC [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#choix-des-axes)
    - **inverser tous les axes** de l'AFC à la demande [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#inversion-des-axes)
    - **supprimer des lignes et des colonnes de la table lexicale** source depuis la visualisation de l'AFC [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#%C3%A9dition-de-la-table-lexicale-source-%C3%A0-partir-des-points-du-plan-de-lafc)
    - **filtrer les point-lignes et les point-colonnes** à partir des données d'aide à l'interprétation pour *focaliser la visualisation sur les éléments les plus pertinents* [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#filtrage-des-point-lignes-et-des-point-colonnes)
    - **styler par feuilles de styles les point-lignes et les point-colonnes** à partir des données d'aide à l'interprétation pour *caractériser visuellement les éléments les plus pertinents* [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#stylage-des-point-lignes-et-des-point-colonnes)
  - CAH
    - La visualisation du **dendrogramme** de la classification est désormais totalement intégrée à l'interface avec possibilités de Zoom et de Panoramiques [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#affichage-du-dendrogramme-des-classes)
    - Le diagramme à barre des inerties des noeuds de regroupement est affiché à droite du dendrogramme
    - La CAH donne désormais accès aux tableaux d'aide à l'interprétation fournis par FactoMineR [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#affichage-des-aides-%C3%A0-linterpr%C3%A9tation-1)
    - de nouveaux paramètres de FactoMineR sont disponibles dans TXM [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#param%C3%A8tres-suppl%C3%A9mentaires-2)
  - Moteur de recherche CQP
    - Sous Mac et Linux, on peut désormais choisir la **stratégie de résolution** du moteur CQP au niveau de chaque requête CQL
  - Import Presse-Papier
    - On peut désormais choisir le **nom du corpus** en plus de la langue d'annotation
- Vue Corpus
  - Une nouvelle commande `Corpus > Textes` liste tous les textes d'un corpus avec leurs métadonnées ainsi que des diagrammes à barres de l'équilibre d'une sélection de ces textes par nombre de mots ou par nombre de textes par métadonnées [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#sec:texts)
  - Une nouvelle commande `Corpus > Documentation` permet d'afficher la documentation d'un corpus sous la forme de pages .html.
- Interface Utilisateur
  - Il n'y a plus de **fenêtres fantômes**
  - Les premières colonnes des tableaux de résultats ('word', 'F', etc.) sont désormais **figées** pour faciliter la lecture des grands tableaux
- Préférences
  - La nouvelle préférence 'TXM > Avancé > Memory size' permet de régler la **taille de la mémoire** de TXM [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/pr%C3%A9f%C3%A9rences.html#section-txm-avanc%C3%A9)
- Utilitaires
  - Une nouvelle **calculette** est disponible depuis la barre d'outils pour calculer des expressions mathématiques directement depuis TXM

Retrouvez la liste exhaustive des changements dans la liste des [tickets de développement de TXM 0.8.4](https://gitlab.huma-num.fr/txm/txm-src/-/issues/?sort=label_priority&state=all&milestone_title=TXM%200.8.4&first_page_size=100).

## Extensions

- Media Player : l'extension est désormais installée par défaut

## Prérequis d'installation

-   Vous aurez besoin des droits d'installation sur votre machine pour pouvoir installer TXM ;
-   Vous aurez besoin d'un accès à Internet ;
-   TXM fonctionne sur des machines **64-bit** ;
-   Windows
    -   TXM est supporté pour Windows 10 et Windows 11
-   Mac OS X
    -   TXM est supporté pour Mac OS X 12.0 et 13.0
        -   Réglages de sécurité : il faut modifier les paramètres de « sécurité » du
            système pour pouvoir installer correctement TXM. Aller dans
            « Préférences Système &gt; Personnel &gt; Sécurité » et
            autoriser les applications téléchargées depuis Internet. Si
            on ne fait pas ce réglage, le problème ne se manifeste pas
            au moment de l'installation, mais au premier lancement.
-   Linux
    -   TXM est supporté pour Ubuntu 20.04 à 24.04, et ses variantes (Xubuntu, Kubuntu, Lubuntu, etc.)
    -   TXM fonctionne également sur des variantes du système Debian
    -   L'installation de TXM provoque l'installation des packages
        suivants : zenity, po-debconf, libwebkit2gtk-4.0-37, debconf, libc6 (>= 2.15), libgtk2-perl|libgtk3-perl, libblas3, libbz2-1.0, libcairo2, libgfortran5, libglib2.0-0, libgomp1, libjpeg8, liblapack3, liblzma5, libpango-1.0-0, libpangocairo-1.0-0, libpaper-utils, libpcre3, libpng16-16, libreadline7|libreadline8, libegl1-mesa, libtinfo5, libtiff5, libx11-6, libxt6, tcl8.6, ucf, unzip, xdg-utils, zip, zlib1g

## Instructions d'installation

-   vérifier les pré-requis d'installation en fonction de votre système d'exploitation, voir ci-dessus
-   télécharger le logiciel d'installation de TXM correspondant à votre système et à l'architecture de votre machine
-   double-cliquer sur l'icone du logiciel d'installation de TXM
-   suivre les instructions du logiciel: accepter la licence, etc.
    - remarque concernant les utilisateurs de **Linux** : la **première fois**
      que vous installez TXM sur une machine, il est également nécessaire,
      après l'installation et avant le premier lancement, de **quitter votre
      session de travail (déconnexion) puis de vous reconnecter** pour
      finaliser l'installation.
-   l'installation se finalise lors du premier lancement de TXM (voir les instructions de lancement ci-dessous)
-   vous pouvez alors [installer TreeTagger]({{"InstallTreeTagger" | relative_url}}) (vous pouvez bien sûr utiliser TXM sans annotation linguistique automatique des mots, mais pour utiliser tout le potentiel de l'outil, il est vivement recommandé d'installer cette extension pour une exploitation optimale)

## Installation sur parc de machines Windows avec domaine (salles de cours)

Pour installer TXM dans un parc de machines Windows avec domaine, il faut utiliser l'installeur dédié : [TXM 0.8.4 pour administrateur Windows](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.4/TXM_0.8.4_2025-02-12_Win64_installer_admin.exe).

Cette version installe TXM dans le dossier `Program Files` et ajoute les raccourcis de lancement dans la zone partagée du menu Démarrer de Windows.

## En cas de problème d'installation

L'assistance pour l'installation et l'utilisation de TXM repose sur la communauté de ses utilisateurs et de ses développeurs.
{: style="margin-bottom: -10px"}

Pour plus d'aide concernant l'installation de TXM, vous pouvez :
{: style="margin-bottom: 5px"}

-   consulter les sections « Installer TXM sur sa machine » et « En cas de problème avec le logiciel » du [Manuel de TXM](https://pages.textometrie.org/txm-manual)
-   consulter la section « Installation et mises à jour » de la [FAQ des utilisateurs de TXM](https://groupes.renater.fr/wiki/txm-users/public/faq) qui liste
    les problèmes d'installation les plus communs
-   vous inscrire à la liste de diffusion des utilisateurs de TXM ['txm-users'](https://groupes.renater.fr/sympa/info/txm-users) et y demander de l'aide
-   contacter l'équipe TXM par mail à l'addresse <a href="mailto:{{ "textometrie@groupes.renater.fr" | encode_email }}" title="textometrie AT groupes.renater.fr">textometrie AT groupes.renater.fr</a>

## Lancer TXM

-   Windows
    -   menu « Démarrer», puis aller au répertoire TXM-0.8.4 et sélectioner TXM-0.8.4 » (faire glisser l'icone de l'application TXM dans la barre de lancement rapide pour rajouter un accès direct)
-   Mac OS X
    -   Naviguer dans le répertoire « Applications / TXM-0.8.4 » avec le Finder
        et double-cliquer sur l'icone de l'application TXM (faire
        glisser l'icone de l'application TXM dans le dock pour rajouter
        un accès direct)
-   Linux
    -   Naviguer dans la section « Applications installées » du Launchpad
        d'Unity et double-cliquer sur l'icone de l'application de TXM-0.8.4
        (pour rajouter un accès direct, faire un clic droit sur l'icone
        de TXM du Lanceur et sélectionner « Conserver dans le Lanceur »)

## Utiliser TXM

Voir les rubriques du menu Aide ainsi que les ressources en ligne suivantes :
{: style="margin-bottom: 5px"}

-   Lire le [manuel en ligne de TXM](https://txm.gitpages.huma-num.fr/txm-manual) ;
-   Télécharger le [manuel PDF de TXM](https://txm.gitpages.huma-num.fr/txm-manual/txm-manual.pdf) pour impression.
-   Regarder la [vidéo d'un atelier TXM](https://txm.gitpages.huma-num.fr/textometrie/html/enregistrement_atelier_initiation_TXM_fr.html)
-   Participer à un [atelier TXM](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
-   Participer au [wiki de la communauté des utilisateurs de TXM](https://groupes.renater.fr/wiki/txm-users)

## Retours de bugs

1.  Vérifiez que votre retour n'a pas déjà été signalé dans la liste des [tickets de développement de TXM 0.8.4](https://gitlab.huma-num.fr/txm/txm-src/-/issues/?sort=label_priority&state=all&milestone_title=TXM%200.8.4&first_page_size=100) ;
2.  Si ce n'est pas le cas, signalez le problème soit :
    -   par mail à l'<a href="mailto:{{ site.social-network-links['email'] | encode_email }}?subject=retours TXM 0.8.4" title="{{ email }}">équipe TXM</a> ;
    -   en éditant directement le [wiki txm-users](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.4) ;
    -   en tchatant directement avec les développeurs dans le canal IRC \#txm du serveur 'irc.freenode.net'.

## Remerciements

Merci aux testeuses et testeurs de cette version :
- Flora Badin (Université d'Orléans)
- Philippe Évrard (Université de Strasbourg)
- Virginie Léthier (Université de Franche-Comté)

## Contact

N'hésitez pas à [nous contacter]({{"Nous-contacter" | relative_url}}) pour de plus amples informations.

L'équipe TXM
