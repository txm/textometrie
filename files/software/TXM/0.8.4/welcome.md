---
ititle: Bienvenue dans TXM 0.8.4
layout: page
lang: fr
ref: welcome txm-0.8.4
---

# Bienvenue dans TXM 0.8.4

# Nouveautés

En plus de corriger de nombreux bugs, cette nouvelle version comprend quelques éléments nouveaux notables :
{: style="margin-bottom: 5px"}

- Outils
  - Table lexicale
    - Désormais une Table Lexicale peut être **enregistrée dans un fichier tableur** au format Libre Office Calc (.ods) ou bien être remplie à partir du contenu d'un fichier tableur au format Libre Office Calc
    - Un corpus peut désormais **importer directement** une table lexicale depuis un fichier indépendamment d'une partition
  - AFC
    - On peut désormais **visualiser tous les plans** de l'AFC [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#choix-des-axes)
    - **inverser tous les axes** de l'AFC à la demande [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#inversion-des-axes)
    - **supprimer des lignes et des colonnes de la table lexicale** source depuis la visualisation de l'AFC [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#%C3%A9dition-de-la-table-lexicale-source-%C3%A0-partir-des-points-du-plan-de-lafc)
    - **filtrer les point-lignes et les point-colonnes** à partir des données d'aide à l'interprétation pour *focaliser la visualisation sur les éléments les plus pertinents* [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#filtrage-des-point-lignes-et-des-point-colonnes)
    - **styler par feuilles de styles les point-lignes et les point-colonnes** à partir des données d'aide à l'interprétation pour *caractériser visuellement les éléments les plus pertinents* [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#stylage-des-point-lignes-et-des-point-colonnes)
  - CAH
    - La visualisation du **dendrogramme** de la classification est désormais totalement intégrée à l'interface avec possibilités de Zoom et de Panoramiques [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#affichage-du-dendrogramme-des-classes)
    - Le diagramme à barre des inerties des noeuds de regroupement est affiché à droite du dendrogramme
    - La CAH donne désormais accès aux tableaux d'aide à l'interprétation fournis par FactoMineR [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#affichage-des-aides-%C3%A0-linterpr%C3%A9tation-1)
    - de nouveaux paramètres de FactoMineR sont disponibles dans TXM [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#param%C3%A8tres-suppl%C3%A9mentaires-2)
  - Moteur de recherche CQP
    - Sous Mac et Linux, on peut désormais choisir la **stratégie de résolution** du moteur CQP au niveau de chaque requête CQL
  - Import Presse-Papier
    - On peut désormais choisir le **nom du corpus** en plus de la langue d'annotation
- Vue Corpus
  - Une nouvelle commande `Corpus > Textes` liste tous les textes d'un corpus avec leurs métadonnées ainsi que des diagrammes à barres de l'équilibre d'une sélection de ces textes par nombre de mots ou par nombre de textes par métadonnées [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#sec:texts)
  - Une nouvelle commande `Corpus > Documentation` permet d'afficher la documentation d'un corpus sous la forme de pages .html.
- Interface Utilisateur
  - Il n'y a plus de **fenêtres fantômes**
  - Les premières colonnes des tableaux de résultats ('word', 'F', etc.) sont désormais **figées** pour faciliter la lecture des grands tableaux
- Préférences
  - La nouvelle préférence 'TXM > Avancé > Memory size' permet de régler la **taille de la mémoire** de TXM [ⓘ](https://txm.gitpages.huma-num.fr/txm-manual/pr%C3%A9f%C3%A9rences.html#section-txm-avanc%C3%A9)
- Utilitaires
  - Une nouvelle **calculette** est disponible depuis la barre d'outils pour calculer des expressions mathématiques directement depuis TXM
- Extensions
  - Media Player : l’extension est désormais installée par défaut

Retrouvez la liste exhaustive des changements dans la liste des [tickets de développement de TXM 0.8.4](https://gitlab.huma-num.fr/txm/txm-src/-/issues/?sort=label_priority&state=all&milestone_title=TXM%200.8.4&first_page_size=100).

\[L'infrastructure de développement de TXM a déménagé de la forge du Centre Blaise Pascal (CBP) de l'ENS de Lyon au Gitlab d'Huma-Num à l'occasion de TXM 0.8.4. Nous profitons de cette annonce pour remercier Emmanuel Quéméner de nous avoir hébergé à l'ENS de Lyon pendant toutes ces années ainsi que l'équipe Huma-Num pour le nouvel hébergement\]

## Retours de bugs

1.  Vérifiez que votre retour n'a pas déjà été signalé dans la liste des [tickets de développement de TXM 0.8.4](https://gitlab.huma-num.fr/txm/txm-src/-/issues/?sort=label_priority&state=all&milestone_title=TXM%200.8.4&first_page_size=100) ;
2.  Si ce n'est pas le cas, signalez le problème soit :
    -   par mail à l'<a href="mailto:{{ site.social-network-links['email'] | encode_email }}?subject=retours TXM 0.8.4" title="{{ email }}">équipe TXM</a> ;
    -   en éditant directement le [wiki txm-users](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.4) ;
    -   en tchatant directement avec les développeurs dans le canal IRC \#txm du serveur 'irc.freenode.net'.

## Remerciements

Merci aux testeuses et testeurs de cette version :
- Flora Badin (Université d'Orléans)
- Philippe Évrard (Université de Strasbourg)
- Virginie Léthier (Université de Franche-Comté)

## Contact

N'hésitez pas à [nous contacter]({{"Nous-contacter" | relative_url}}) pour de plus amples informations.

L'équipe TXM

