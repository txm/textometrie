---
ititle: TXM 0.8.4 Download
layout: page
lang: en
ref: txm-0.8.4
---
# TXM 0.8.4 Download
{: style="text-align:center;padding-bottom:30px" .no_toc}

## Content
{: .no_toc}

1. TOC
{:toc}
---
# TXM 0.8.4 Download
{: style="text-align:center;padding-bottom:30px".no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: top-right;" markdown="1">

| [![]({{"/img/windows-icon.png" | absolute_url}}){: width="125" .center-image} Windows 10 - 11](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.4/TXM_0.8.4_2025-02-12_Win64_installer.exe) | [![]({{"/img/macosx-icon.png" | absolute_url}}){: width="125" .center-image} Mac OS X 12.0 - 13.0.0](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.4/TXM_0.8.4_2025-02-12_MacOSX_installer.pkg) | [![]({{"/img/ubuntu-icon.png" | absolute_url}}){: width="125" .center-image} Ubuntu 20.04 - 24.04](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.4/TXM_0.8.4_2025-02-12_Linux64_installer.deb) |

[Previous versions](https://gitlab.huma-num.fr/txm/txm-software/-/tree/master/files/software/TXM/0.8.4): (none)

## News

(to be completed)

For a complete list of changes, see the list of [TXM 0.8.4 development tickets](https://gitlab.huma-num.fr/txm/txm-src/-/milestones/58).

### What's new in updates

(no update yet)

### What's new in corpora

(no corpus update yet)

## Extensions

- MediaPlayer : installed

## Installation Prerequisites
{: .no_toc}

-   you need to have 'new software installation privilege' on your machine to install TXM
-   you need Internet access
-   TXM works on **64-bit** machines
-   Windows
    -   TXM is supported for Windows 10 and Windows 11
-   Mac OS X
    -   TXM is supported for 12.0
        -   Security settings: you need to
            change the system security settings to install TXM properly.
            Go to "System Preferences &gt; Personal &gt; Security" and
            allow applications downloaded from the Internet. If that
            preference is not set, the problem doesn't occur at
            installation time, but at the first launch.
-   Linux
    -   TXM is supported for Ubuntu 20.04 to 24.04, and its variants (Xubuntu, Kubuntu, Lubuntu, etc.)
    -   TXM installation process installs the following dependencies:
        zenity, po-debconf, libwebkit2gtk-4.0-37, debconf, libc6 (>= 2.15), libgtk2-perl|libgtk3-perl, libblas3, libbz2-1.0, libcairo2, libgfortran5, libglib2.0-0, libgomp1, libjpeg8, liblapack3, liblzma5, libpango-1.0-0, libpangocairo-1.0-0, libpaper-utils, libpcre3, libpng16-16, libreadline7|libreadline8, libegl1-mesa, libtinfo5, libtiff5, libx11-6, libxt6, tcl8.6, ucf, unzip, xdg-utils, zip, zlib1g.

## Installation Instructions

-   verify the 'Installation Prerequisites' for your machine, see above
-   download the TXM setup file for your system and machine architecture (64-bit)
-   double-click on the TXM setup file icon
-   follow the instructions on screen: accept licence, etc.
    - note concerning **Linux** users: the **first time** you install TXM on a machine, you also need to **quit your working session (logout) and login again** to finalize the installation process.
-   run TXM once to finalize the setup process (see running instructions below)
-   you can now [install TreeTagger]({{"en/InstallTreeTagger" | relative_url}}) (optional but recommended)
-   The End


## Installing TXM in a Windows machine park with domain

To install TXM in a Windows machine park with domain, you must use a dedicated installer:  [TXM 0.8.4 Windows administrator version](https://gitlab.huma-num.fr/txm/txm-software/-/raw/master/files/software/TXM/0.8.4/TXM_0.8.4_2025-02-12_Win64_installer_admin.exe). 

This setup installs TXM in the Program Files directory and adds the launch shortcuts to the shared area of ​​the Windows Start menu.

## Troubleshooting installation problems

TXM support is community based.
{: style="margin-bottom: -10px"}

For further help concerning TXM installation:
{: style="margin-bottom: 5px"}

-   consult the "Installer TXM sur sa machine" and the "En cas de problème avec le logiciel" sections of the [TXM manual](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/manual/0.7.9/fr/manual1.xhtml) (in French)
-   consult the "Installation et mises à jour" section of the [TXM usersFAQ](https://groupes.renater.fr/wiki/txm-users/public/faq) which lists some of the common installation problems people run into.
-   subscribe to the ['txm-users'](https://groupes.renater.fr/sympa/info/txm-users) mailing list and ask for help
-   chat online via IRC on the '\#txm' channel of the irc.freenode.net server
-   contact the TXM team by email at <a href="mailto:{{ "textometrie@groupes.renater.fr" | encode_email }}" title="textometrie AT groupes.renater.fr">textometrie AT groupes.renater.fr</a>

## Running TXM

-   Windows
    -   Menu 'Start', directory 'TXM-0.8.4' and item 'TXM-0.8.4' (drag TXM application icon to the Quick Access Toolbar to add direct access)
-   Mac OS X
    -   Navigate to the 'Applications / TXM-0.8.4' folder in the Finder and double-click on TXM application icon (drag TXM application icon to your Dock to add direct access)
-   Linux
    -   Navigate to the 'Installed Applications' section in the Launchpad and double-click on TXM-0.8.4 application icon (right-click on TXM icon in the Dock and select 'Keep in Dock' to add direct access)

## Using TXM

See the 'Help' menu entries and the following online resources:
{: style="margin-bottom: 5px"}

-   Read [TXM manual online](https://txm.gitpages.huma-num.fr/txm-manual)
-   Download [TXM PDF manual](http://textometrie.ens-lyon.fr/files/documentation/TXM%20Manual%200.7.pdf) for printing
-   View the [TXM workshop video](https://txm.gitpages.huma-num.fr/textometrie/html/enregistrement_atelier_initiation_TXM_fr.html)
-   Participate to a [TXM workshop](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm)
-   Participate to [TXM users community wiki](https://groupes.renater.fr/wiki/txm-users)

## Feedback and bug reports

1.  Check if your feedback is not already reported in the [TXM platform bug tracker](http://forge.cbp.ens-lyon.fr/redmine/projects/txm/issues?query_id=70)
2.  If not, send your feedback either:
    -   in the [txm-open mailing list](https://lists.sourceforge.net/lists/listinfo/txm-open)
    -   by directly editing the [txm-users wiki](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel/txm_0.8.4)
    -   by chating directly with the developers on the \#txm IRC channel of the 'irc.freenode.net' server
    -   by <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contacting the TXM team by mail</a>

## Acknowledgements

Thank you to the testers of this version:

- Flora Badin
- Philippe Evrard

## Contact

Please [contact us]({{"en/Nous-contacter" | relative_url}}) for further information.

The TXM team
