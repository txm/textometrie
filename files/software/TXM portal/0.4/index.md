---
ititle: Portail TXM 0.4
layout: page
lang: fr
ref: txm-portal-0.4
---

Portail TXM 0.4
===

Nous avons le très grand plaisir de vous annoncer la disponibilité
d'une nouvelle application issue de la plateforme TXM appelée
'portail TXM'.

Il s'agit d'une nouvelle variante, par rapport au logiciel habituel
pour Windows, Mac et Linux, qui s'installe sur Linux en mode serveur
pour Internet, c'est à dire que les utilisateurs de TXM peuvent y
accèder en ligne - ou sur le réseau local d'une équipe - à partir de
n'importe quelle machine équipée d'un simple navigateur web (comme
Firefox).

Cette variante offre donc la possibilité de mettre en ligne tout corpus
manipulé par TXM et d'y accéder instantanément en groupe (entre
collègues ou avec des étudiants) à travers des contrôles d'accès
paramétrables.

Elle s'adresse principalement aux personnes ayant un minimum de
compétences pour installer un logiciel sur Linux et procéder à
certains réglages de fichiers.

Nous remercions chaleureusement le projet ANR CORPTEF
(<http://corptef.ens-lyon.fr>) qui a financé l'essentiel du
développement de cette nouvelle version (1 ETP sur 6 mois), ainsi que
les Presses Universitaires de Caen (PUC : <http://www.unicaen.fr/puc>)
et l'UMR GREYC (<https://www.greyc.fr>) pour le développement
communautaire, dans l'esprit open-source correspondant à la licence
GPL V3.0 de la plateforme TXM, de l'interface des fonctions
statistiques de TXM WEB.

Installer un portail TXM
---

Vous trouverez sur le site logiciel Sourceforge habituel de TXM une
archive contenant le logiciel ainsi que les matériaux nécessaires pour
installer et tester rapidement TXM WEB dans votre propre serveur
d'application Tomcat ou équivalent sur une machine Linux :
<https://sourceforge.net/projects/txm/files/software/TXM%20WEB>

La procédure d'installation est décrite dans le fichier INSTALL de l'archive ainsi qu'à la page <https://sourceforge.net/apps/mediawiki/txm/index.php?title=TXM_WEB:_Quick_Install>

[Wiki instructions d'installation](https://groupes.renater.fr/wiki/txm-info/public/install_a_txm_portal).

Tester un portail TXM
---

Vous pouvez d'ores et déjà tester un exemple de portail TXM WEB
(sans avoir à en installer un vous-même) en accèdant à l'adresse suivante :
<http://portal.textometrie.org/demo>
Ce portail comprend les corpus de test habituels.

Contactez nous si vous souhaitez pouvoir y tester, en accès restreint,
un extrait de vos corpus (que vous manipulez déjà dans TXM en version
poste local).

Présentation rapide des fonctionnalités
---

  * contrôles d'accès : pour répondre à certaines exigences en termes de
confidentialité et de maîtrise de la propriété intellectuelle, il est
possible de contrôler au texte près les fonctionnalités et leurs
paramètres pour chaque utilisateur
  * gestion de comptes utilisateurs : pour accéder à des ressources à
accès restreint, les utilisateurs accèdent à un portail TXM en se connectant
grâce à un compte utilisateur. Ce compte est créé soit par
auto-inscription et validation, soit par un utilisateur particulier
appelé 'administrateur du portail'. Ce dernier dispose de différents
outils lui permettant de créer facilement les comptes et de
communiquer à l'aide de messages et de mails
  * gestion des corpus : un portail TXM peut charger tout corpus TXM au format binaire
  * Un portail TXM offre un accès ergonomique à toutes les fonctionnalités
habituelles de TXM suivantes : édition, concordances, index, lexique,
références
  * Le portail TXM offre également des fonctionnalités originales :
    * sélection de textes par facettage (croisement de métadonnées) pour
   construire un sous-corpus
    * contextes : une autre présentation des concordances avec de larges
   contextes.
    * bibliographies des textes : chaque texte d'un corpus dispose d'une
   fiche bibliographique complète
    * TIGER Search : Le portail TXM est la première application à offrir une
interface d'accès au moteur de recherche d'annotations syntaxiques
TIGER Search (qui a été intégré à TXM)

Retours de bug de cette version beta
---

Merci de faire vos retours via la liste de diffusion 'txm-users' :
<https://listes.cru.fr/sympa/info/txm-users>

  * bugs connus :
    * les messages de l'application en FR et EN sont en cours de finalisation

Participez
---

Le portail TXM est diffusé avec la même licence open-source que TXM. Vous
pouvez donc à tout moment participer à son développement. Cela
est réalisable de différentes façons :

  * en documentant les fonctionnalités ou en traduisant l'interface ou la documentation
  * en développant de nouvelles fonctionnalités ou de nouvelles interfaces
