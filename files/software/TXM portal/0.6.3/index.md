---
ititle: Portail TXM 0.6.3
layout: page
lang: fr
ref: txm-portal-0.6.3
---

Portail TXM 0.6.3
===

# Nouveautés

Pour l'utilisateur, cette version rend disponible les commandes statistiques **Analyse Factorielle** (fig 1) et **Spécificités** (fig 2) et ajoute l'export de résultats pour les Index et Lexique de partitions et pour les Cooccurrences.

Pour l'administrateur de portail, cette version simplifie le **chargement de nouveaux corpus**, améliore l'interface d'administration, complète l'API des liens internes et externes, et ajoute la possibilité de créer des **formulaires dynamiques** d'appel de commandes (comme le formulaire d'appel de la concordance de 'jeune' dans la page d'accueil du corpus VOEUX du [portail de démo](https://txm-demo.huma-num.fr/txm/?command=documentation&path=VOEUX), fig 3).

![Premier plan factoriel des présidents représentés par leurs 200 lemmes les plus fréquents](./afc-presidents.png "Premier plan factoriel des présidents représentés par leurs 200 lemmes les plus fréquents")
*Fig 1. Premier plan factoriel des présidents représentés par leurs 200 lemmes les plus fréquents*

![Lemmes spécifiques des discours des présidents](./presidents-lemmes-specificites.png#center "Lemmes spécifiques des discours des présidents")
*Fig 2. Lemmes spécifiques des discours des présidents*

![Formulaire de concordance simplifié](./formulaire-recherche-jeune-portail.png#center "Formulaire de concordance simplifié")
*Fig 3. Formulaire de concordance simplifié*

## Autres

**Fonctionalités Utilisateur**

  - **Propriétés** (anciennement Dimensions) sur :
    - corpus : en plus du nombre de mots et de textes, la commande affiche la liste des propriétés de mots et de structures du corpus 
    - partition : en plus du nombre de mots et de parties, la commande affiche un diagramme en bâtons des tailles de parties
  - **Édition** : 
    - on peut choisir l'ordre d'affichage des facettes d'édition par glisser-déposer dans la liste déroulante située en bas à droite de l'éditeur
    - le changement des facettes affichées ne perturbe plus la navigation courante
  - **Textes** : ouverture de l'édition du texte en double-cliquant sur sa ligne
  - **Assistant de requêtes CQL**
    - nouvel assistant pour les commandes Concordance et Index (cliquer sur le bouton "baguette magique" pour ouvrir l'assistant)<br> L'assistant fonctionne de façon analogue à celui de TXM pour poste (voir section 8.4.1.1 de la [documentation de TXM 0.8](https://txm.gitpages.huma-num.fr/txm-manual/analyser-un-corpus.html#concordances))

**Fonctionalités Administrateur**

  - Nouvelle mise en page de l'interface Administrateur
  - Suppression des utilisateurs directement depuis le tableau des utilisateurs
  - Téléchargement et chargement du corpus dans la même opération
  - API
    - Accès complet à toutes les commandes et leurs paramètres
    - Nouvelle API dynamique en Javascript

# Historique

Cette version du portail est le résultat de 3 campagnes de développements : 
  - [0.6.3.0](https://gitlab.huma-num.fr/txm/txm-src/-/milestones/52) : Corrections et améliorations globale de l'interface ;
  - [0.6.3.1](https://gitlab.huma-num.fr/txm/txm-src/-/milestones/60) : Rétablissement des commandes AFC et Spécificités ;
  - [0.6.3.2](https://gitlab.huma-num.fr/txm/txm-src/-/milestones/77) : Corrections pour les commandes Édition et Documentation.

# Installation

- Télécharger l'[archive de la release 0.6.3 du portail](https://gitlab.huma-num.fr/txm/txm-vm/-/archive/v0.6.3/txm-vm-v0.6.3.tar.gz) ;
- Suivre les instructions de la section [Install](https://gitlab.huma-num.fr/txm/txm-vm/-/blob/master/README.md#install) du README.

# Mise à jour

- Suivre les instructions de la section [Mise à jour](https://gitlab.huma-num.fr/txm/txm-vm/-/blob/master/README.md#update) du README.

## En cas de problème d'installation

L'assistance pour l'installation et l'utilisation de TXM portail repose sur la communauté de ses utilisateurs et de ses développeurs.

Pour plus d'aide concernant l'installation de TXM portail, vous pouvez :

  -   vous inscrire à la liste de diffusion des admins de portail TXM ['txm-admin'](https://groupes.renater.fr/sympa/info/txm-admin) et y demander de l'aide
  -   contacter l'équipe TXM par mail à l'addresse <a href="mailto:{{ site.social-network-links['email'] | encode_email }}?subject=Support portail" title="{{ email }}">de l'équipe TXM</a>

# Retours de bugs

1.  Vérifiez que votre retour n'a pas déjà été signalé [0.6.3.0](https://gitlab.huma-num.fr/txm/txm-src/-/milestones/52), [0.6.3.1](https://gitlab.huma-num.fr/txm/txm-src/-/milestones/60), [0.6.3.2](https://gitlab.huma-num.fr/txm/txm-src/-/milestones/77) ;  
2.  Si ce n'est pas le cas, signalez le problème soit :
    -   dans la [liste de diffusion txm-admin](https://groupes.renater.fr/sympa/info/txm-admin) ;
    -   en éditant directement le [wiki
        txm-users](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_web/txmweb_0.6.3) ;
    -   en <a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contactant l'équipe TXM par mail</a>.

N'hésitez pas à [nous contacter]({{"Nous-contacter" | relative_url}}) pour de plus amples informations.

L'équipe TXM
