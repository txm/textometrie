---
ititle: TXM portal
layout: page
lang: en
ref: txm-portal
---

TXM portal
===
{: style="text-align: center; padding-bottom: 30px" .no_toc}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

The TXM portal software is a J2EE compliant web application: [installation instructions](https://groupes.renater.fr/wiki/txm-info/public/install_a_txm_portal).

- Current release:
  - TXM_Portal_0.6.1beta[^1]
- Older releases (obsolete):
  - TXM_Portal_0.6alpha[^2]
  - TXM_Portal_0.5beta1[^1]
  - TXM_WEB_0.4beta1_Linux32
  - TXM_WEB_0.4beta1_Linux64

Files
==

<table>
{% assign files = site.static_files | sort_natural:'name' %}
{% for file in files %}
    {% if file.path contains 'TXM portal/' %}
        <tr>
	  <td>
	    <a href="{{ file.path | relative_url }}">{{ file.name }}</a>
	  </td>
	  <td>
	    {% include translated_date.html date=file.modified_time format="%A %e %B %Y %k:%M" lang="en" %}
	  </td>
	</tr>
    {% endif %}
{% endfor %}
</table>

Please contact us at 'textometrie AT ens-lyon.fr' for further information.

The TXM team

Notes
=

[^1]: warning: beta level release is for experienced users testing purpose.
[^2]: warning: alpha level release is not fully documented and should only be considered for development team testing purpose.

