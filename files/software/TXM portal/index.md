---
ititle: Portail TXM
layout: page
lang: fr
ref: txm-portal
---

Portail TXM
===
{: style="text-align: center; padding-bottom: 30px"}

<div style="background-image:url({{'/img/TXM-background.jpg' | absolute_url}});background-repeat: no-repeat;background-position: right top;" markdown="1">

Le logiciel portail TXM est une application web compatible J2EE.

- Version courante :
  - [0.6.3]({{"/files/software/TXM portal/0.6.3" | absolute_url}})
- Version développement :
  - [repository](https://gitlab.huma-num.fr/txm/txm-vm)
- Anciennes versions (obsolètes):
  - TXM_Portal_0.6.1beta[^1]
  - TXM_Portal_0.6alpha[^2]
  - TXM_Portal_0.5beta1[^1]
  - [0.4]({{"/files/software/TXM portal/0.4" | absolute_url}})

Veuillez nous contacter à 'textometrie AT ens-lyon.fr' pour plus d'informations.

L'équipe TXM

Captures d'écran
==

L'accueil du portail txm-demo :
![image.png](./image.png) 

Une concordance et un retour au texte du mot "monde" : 
![image-1.png](./image-1.png)


Notes
=

[^1]: attention: la version bêta est destinée aux utilisateurs expérimentés.
[^2]: avertissement: la publication du niveau alpha n'est pas entièrement documentée et ne doit être prise en compte que pour les tests en équipe de développement.
