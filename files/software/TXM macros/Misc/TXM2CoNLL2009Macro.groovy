package org.txm.macro
import org.txm.export.conll2009.ToCoNLL2009

import java.io.File;
import org.txm.searchengine.cqp.corpus.*
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*

if (!(corpusViewSelection instanceof Corpus)) {
	println "Select a corpus or a sub-corpus"
	return;
}
def corpus = corpusViewSelection

@Field @Option(name="outfile",usage="an example file", widget="File", required=true, def="/home/mdecorde/Bureau/conn_export.tsv")
File outfile = new File("/home/mdecorde/Bureau/conn_export.tsv")
@Field @Option(name="encoding",usage="sentenceProperty", widget="String", required=true, def="UTF-8")
String encoding = "UTF-8"

@Field @Option(name="sentenceProperty",usage="sentenceProperty", widget="String", required=true, def="s_id")
String sentenceProperty = "s_id"
@Field @Option(name="posProperty",usage="sentenceProperty", widget="String", required=true, def="pos")
String posProperty = "frpos"
@Field @Option(name="lemmaProperty",usage="sentenceProperty", widget="String", required=true, def="lemma")
String lemmaProperty = "frlemma"

if (!ParametersDialog.open(this)) return;

def split = sentenceProperty.split("_", 2)
StructuralUnitProperty s = corpus.getStructuralUnit(split[0]).getProperty(split[1])
Property word = corpus.getProperty("word")
Property lemma = corpus.getProperty(lemmaProperty)
Property pos = corpus.getProperty(posProperty)

if (s == null) { println "Error sentence property: $sentenceProperty"; return}
if (word == null) { println "Error no word property"; return}
if (pos == null) { println "Error pos property: $posProperty"; return}
if (lemma == null) { println "Error lemma property: $lemmaProperty"; return}


ToCoNLL2009 processor = new ToCoNLL2009()
processor.process(outfile, corpus, s, word, lemma, pos, encoding)