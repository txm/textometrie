import org.txm.Toolbox
import org.txm.importer.DomUtils
import org.txm.objects.BaseParameters
import org.txm.rcpapplication.commands.ExecuteImportScript
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*

// PARAMETERS
@Field @Option(name="inputDirectory",usage="Dossier qui contient les fichiers à importer", widget="Folder", required=true, def='')
File inputDirectory

@Field @Option(name="importName",usage="Nom du module d'import", widget="String", required=true, def='TXT+CSV | XML/w+CSV | XML-Factiva | XML-TEI-BFM | XML-TEI-Frantext | XML-TEI-TXM | XML-Transcriber+CSV | XML-TMX | CNR+CSV | Hyperbase | Alceste | CWB')
// choose between : 
def importName

if (!ParametersDialog.open(this)) return;
// END OF PARAMETERS
// TXT+CSV | XML/w+CSV | XML-Factiva | XML-TEI-BFM | XML-TEI-Frantext | XML-TEI-TXM | XML-Transcriber+CSV | XML-TMX | CNR+CSV | Hyperbase | Alceste | CWB

// fix possible miss write
importName = importName.toUpperCase()
importName = importName.replaceAll(" ", "")
importName = importName.trim()
importName = importName.replaceAll("\\+", "")
importName = importName.replaceAll("-", "")
importName = importName.replaceAll("/", "")

def scripts = [:]; // 12
scripts["TXTCSV"] = "quickLoader.groovy"
scripts["XMLWCSV"] = "xmlLoader.groovy"
scripts["XMLFACTIVA"] = "factivaLoader.groovy"
scripts["XMLTEIBFM"] = "bfmLoader.groovy"
scripts["XMLTEIFrantex"] = "frantextLoader.groovy"
scripts["XMLTEITXM"] = "xmltxmLoader.groovy"
scripts["XMLTranscriberCSV"] = "transcriberLoader.groovy"
scripts["XMLTMX"] = "tmxLoader.groovy"
scripts["CNRCSV"] = "discoursLoader.groovy"
scripts["HYPERBASE"] = "hyperbaseLoader.groovy"
scripts["ALCESTE"] = "alcesteLoader.groovy"
scripts["CWB"] = "wtcLoader.groovy"

String txmhome = Toolbox.getParam(Toolbox.USER_TXM_HOME);
File scriptsdir = new File(txmhome, "scripts/import")
File script = new File(scriptsdir, scripts.get(importName))

if (!script.exists()) {
	println "Error: no script found for import name ${importName}: $script"
	return
}

if (inputDirectory.exists()) {
	File paramFile = new File(inputDirectory, "import.xml")
	if (!paramFile.exists()) { // create, if it does not exists
		BaseParameters.createEmptyParams(paramFile, inputDirectory.getName())
	}

	final BaseParameters params = new BaseParameters(paramFile);
	params.load();
	params.rootDir = inputDirectory.getAbsolutePath(); // fix
	params.saveToElement();

	if (!params.save()) {
		System.out.println("ERROR: could not save import configuration: "+ params.paramFile);
		return;
	}

	monitor.syncExec(new Runnable() {
		public void run() {
			new ExecuteImportScript().executeScript(script.getAbsolutePath(),params);
		}
	});
} else {
	System.out.println("Warning: Missing inputDirectory directory: "+ inputDirectory);
}

	