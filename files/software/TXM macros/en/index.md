---
ititle: TXM macros
layout: page
lang: en
ref: txm-macro
---

TXM macros
===
{: style="text-align: center;"}

<div style = "background-image: url ({{'/ img / TXM-background.jpg' | absolute_url}}); background-repeat: aucune répétition; background-position: droite en haut;" markdown = "1">

TXM comes with small, exchangeable and adaptable utilities, which take the form of scripts written in the Groovy language.

The reference documentation for the macros can be found in the [TXM user wiki](https://groupes.renater.fr/wiki/txm-users/public/macros).

<!--
TXM desktop user editable script utilities organized in categories:
{: style="margin-bottom: 5px"}

  - [Annotation](../Annotation): utilities to edit and correct word level annotations
  - [Commands](../Commands): complementary utilities that may be fully integrated in TXM at some point
  - [CQP](../CQP): utilities to access CQP search engine directly
  - [CSV](../CSV): utilities to convert CSV tables prior to import into TXM
  - [Edition](../Edition): complementary utilities to patch corpus editions, that may be fully integrated in TXM at some point
  - [Office](../Office): utilities to convert Excel files prior to import into TXM
  - [R](../R): utilities to access R statistical environment directly
  - [Stats](../Stats): complementary statistics utilities
  - [Syntax](../Syntax): PennTreebank to TIGER-XML format conversion
  - [Text](../Text): utilities to process word processor files prior to import into TXM
  - [Transcription](../Transcription): utilities to process audio/video record transcription files prior to import into TXM
  - [TXT](../TXT): utilities to process raw text files prior to import into TXM
  - [XML](../XML): utilities to process XML files prior to import into TXM
  - [XSL](../XSL): utilities to apply XSL transformation stylesheets
  - [Misc](../Misc): other utilities

See also:
{: style="margin-bottom: 5px"}

- the macros section of the [manuel de TXM](https://txm.gitpages.huma-num.fr/txm-manual/piloter-la-plateforme-par-scripts.html#biblioth%C3%A8que-de-macros-pr%C3%A9d%C3%A9finies)
- the [macros reference documentation](https://groupes.renater.fr/wiki/txm-users/public/macros)

-->

Please contact us at 'textometrie AT ens-lyon DOT fr' for further information.

The TXM team

