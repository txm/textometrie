// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden

/* --------------   Specif2Throw    --------------

FR:

Macro affichant la probabilité a priori (avant de faire les lancés ou les tirages) d'obtenir N succès consécutifs
dans des jeux de lancés ou de tirages simples en regard avec la valeur de spécificité de probabilité équivalente.
Il s'agit de jeux de lancés ou tirages connus de tous : le lancé de pièce pour obtenir pile ou face, le lancé de dé
à 6 faces, tirer une carte dans un jeu de 32 ou 52 cartes, etc.
On considère :
- qu'une pièce a 50% de chances (1 chance sur 2) de tomber sur la face 'pile' à chaque lancé ;
- qu'un dé a 16,6% de chances (1 chance sur 6) de tomber sur '6' à chaque lancé ;
- qu'on a 3,1% de chances (1 chance sur 32) de tirer un as de trèfle à chaque tirage dans un jeu de 32 cartes ;
- qu'on a 1,9% de chances (1 chance sur 52) de tirer un as de trèfle à chaque tirage dans un jeu de 52 cartes.
- etc.
On considère que les lancés ou les tirages sont indépendants et que les objets ne sont pas biaisés (eg le dé n'est pas pipé).

La probabilité de succès au jeu est mise en regard avec la spécificité équivalente pour mettre en relation
l'intuition que l'on a des chances de succès dans un jeu de lancé ou de tirage avec l'étonnement de constater
un nombre d'apparitions donné calculé par le modèle des spécificités de TXM.
Il s'agit uniquement de s'appuyer sur l'expérience du 'risque à jouer' pour donner une idée
de l'ordre de grandeur des probabilités mises en oeuvre dans le modèle des spécificités, sachant que l'on
utilise de toute façon essentiellement les classements entre mots obtenus à partir des spécificités plutôt
que les valeurs de spécificité elles-mêmes pour l'interprétation.
Et il ne s'agit bien sûr en aucun cas d'illustrer une quelconque assimilation de l'apparition des mots dans
un texte à un jeu de hasard.

Exemples de résultats pour le lancé de dé (cardOmega = 6) :
-   (1) = 0,1666666667           = 16,666666669999998%    =  1,67e-01 =   [-1]
     n     p                         %                        pe            s
     ▲     ▲                         ▲                        ▲             ▲
     │     │                         │                        │             └ la spécificité équivalente (l'exposant de la probabilité, soit son logarithme en base 10)
     │     │                         │                        └ la probabilité exprimée en notation avec exposant en base 10
     │     │                         └ le pourcentage correspondant
     │     └ la probabilité correspondante
     └ le nombre de lancés ou tirages avec succès consécutifs
    glose : une spécificité de [-1] est du même ordre de grandeur que la probabilité d'obtenir (un) '6' au lancé de dé
-   2 = 0,02777777778888889    =  2,777777778888889%    =  2,78e-02 =   -2
    glose : une spécificité de -2 est du même ordre de grandeur que la probabilité d'obtenir deux '6' consécutifs au lancé de dé
-   3 = 0,004629629632407407   =  0,4629629632407407%   =  4,63e-03 =   -3
    glose : une spécificité de -3 est du même ordre de grandeur que la probabilité d'obtenir trois '6' consécutifs au lancé de dé
-   4 = 0,0007716049388888889  =  0,07716049388888889%  =  7,72e-04 =   -4
    glose : une spécificité de -4 est du même ordre de grandeur que la probabilité d'obtenir entre quatre et cinq '6' consécutifs au lancé de dé
-   5 = 0,00012860082317386833 =  0,012860082317386832% =  1,29e-04 =   -4
-   6 = 0,000021433470533264746=  0,0021433470533264746%=  2,14e-05 =   -5
    glose : une spécificité de -5 est du même ordre de grandeur que la probabilité d'obtenir six '6' consécutifs au lancé de dé
-   7 = 0,000003572245089591907=  0,00035722450895919066%         3,57e-06 =   -6
    glose : une spécificité de -6 est du même ordre de grandeur que la probabilité d'obtenir sept '6' consécutifs au lancé de dé
-   8 = 0,0000005953741817177259         0,000059537418171772596%        5,95e-07 =   -7
    glose : une spécificité de -7 est du même ordre de grandeur que la probabilité d'obtenir huit '6' consécutifs au lancé de dé
-   9 = 0,00000009922903030613347        0,000009922903030613347%        9,92e-08 =   -8
   glose : une spécificité de -8 est du même ordre de grandeur que la probabilité d'obtenir entre neuf et dix '6' consécutifs au lancé de dé
-  10 = 0,000000016538171720996546       0,0000016538171720996547%       1,65e-08 =   -8
-  11 = 0,0000000027563619540506964      0,00000027563619540506964%      2,76e-09 =   -9
    glose : une spécificité de -9 est du même ordre de grandeur que la probabilité d'obtenir onze '6' consécutifs au lancé de dé
-  12 = 0,00000000045939365910032814     0,000000045939365910032815%     4,59e-10 =  -10
     glose : une spécificité de -10 est du même ordre de grandeur que la probabilité d'obtenir douze '6' consécutifs au lancé de dé
- etc.

La macro prend deux paramètres :

* cardOmega : le nombre d'issues possibles pour le jeu de lancé ou de tirage considéré
              (Omega est l'univers de l'expérience aléatoire, cardOmega son cardinal)
 - pour un lancé de pièce : choisir 2 pour cardOmega
   (Omega = {pile, face})
 - pour un lancé de dé : choisir 6 pour cardOmega
   (Omega = {1, 2, 3, 4, 5, 6})
 - pour un tirage de carte dans un jeu de 32 cartes : choisir 32 pour cardOmega
   (Omega = {1 de trèfle, 2 de trèfle, 3 de trèfle, 4 de trèfle, 5 de trèfle, 6 de trèfle, 7 de trèfle,
   8 de trèfle, 9 de trèfle, 10 de trèfle, valet de trèfle, dame de trèfle, roi de trèfle, 1 de carreau,
   2 de carreau, etc.})
 - pour un tirage de carte dans un jeu de 52 cartes : choisir 52 pour cardOmega
 - etc.
* nthrows : le nombre total de lancés ou de tirages avec succès consécutifs
 - pour un lancé de pièce : nthrows 'pile' consécutifs
 - pour un lancé de dé : nthrows '6' consécutifs
 - etc.
 
La macro affiche - selon les options :

* displayProba : toutes les probabilités de 1 à nthrows lancés ou tirages en détaillant :
-  n : le nombre de succès consécutifs
-  p : la probabilité correspondante
-  % : le pourcentage correspondant
- pe : la probabilité exprimée en notation avec exposant en base 10
-  s : la spécificité équivalente (l'exposant de la probabilité, soit son logarithme en base 10)

* onlyMainRanks : s'utilise avec l'option displayProba pour n'afficher que les probabilités des rangs décimaux principaux (1, 2..., 10, 20..., 100, 200...)

* displayThrows : plutôt que le détail de la probabilité, liste pour une spécificité S- donnée, le nombre équivalent de lancés ou de tirages avec succès consécutifs correspondants exprimé sous la forme d'un intervalle : nombre de lancés ou tirages minimum - nombre de lancés ou tirages maximum
Par exemple '-10 = 30-33' signifie : une spécificité S- de '-10' équivaut à entre 30 et 33 lancés ou tirages avec succès consécutifs (cas du lancé de pièce, cardOmega = 2)

Les limites du calcul sont liées au modèle de la mémoire de la machine. Une machine 64-bit peut typiquement calculer la probabilité de 1023 lancés de pièce avec succès consécutifs.

*/

// STANDARD DECLARATIONS
package org.txm.macro

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.eclipse.ui.console.*

// BEGINNING OF PARAMETERS

@Field @Option(name="cardOmega", usage="card(Ω): 2=coin, 6=dice, 32=32-card deck, 52=52-card deck, etc.", widget="Integer", required=true, def="2")
def cardOmega

@Field @Option(name="nthrows", usage="maximum number of successful throws or draws", widget="Integer", required=true, def="99")
def nthrows

@Field @Option(name="displayProba", usage="display probabilities", widget="Boolean", required=false, def="true")
def displayProba

@Field @Option(name="onlyMainRanks", usage="only display main rank lines", widget="Boolean", required=false, def="false")
def onlyMainRanks

@Field @Option(name="displayThrows", usage="display equivalent specificity value for each number or interval number of throws or draws", widget="Boolean", required=false, def="true")
def displayThrows

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

def clearConsole = { ->
	// clear the console
	(ConsolePlugin.getDefault().getConsoleManager().getConsoles())[0].clearConsole()
}

clearConsole()

min = [:]
max = [:]

pow = cardOmega**(nthrows)
powd = pow.doubleValue()

if (powd == Double.POSITIVE_INFINITY || powd == Double.NEGATIVE_INFINITY) {
	println sprintf("** Impossible to calculate probabilities for this value, try a lower value (impossible to represent cardOmega to the power of %d with this machine). Aborting.", nthrows)
	return
}

d = 1/powd

significandSize = ((-java.lang.Math.log10(d) as double)-1 as int)+4

format = sprintf("%%4d = %%%d.%df = %%0%d.%df%%%% = %%9.2e = %%4d", significandSize, significandSize-2, significandSize-1, significandSize-4)

unit = 1
dec = 1

println "cardOmega = $cardOmega (1 chance sur $cardOmega de réussir)"

if (displayProba) {
	println """Légende :
	
   n    p        %        pe    s
   ▲    ▲        ▲        ▲     ▲
   │    │        │        │     └ la spécificité équivalente (l'exposant de la probabilité, soit son logarithme en base 10)
   │    │        │        └ la probabilité exprimée en notation avec exposant en base 10
   │    │        └ le pourcentage correspondant
   │    └ la probabilité correspondante
   └ le nombre de lancés ou tirages avec succès consécutifs
"""

/*
   n    p        %        pe    s
   ▲    ▲        ▲        ▲     ▲
   │    │        │        │     │
   └ le nombre de lancés ou tirages avec succès consécutifs
        │        │        │     │
        └ la probabilité correspondante
                 │        │     │
                 └ le pourcentage correspondant
                          │     │
                          └ la probabilité exprimée en notation avec exposant en base 10
                                │
                                └ la spécificité équivalente (l'exposant de la probabilité, soit son logarithme en base 10)   
*/

	spc = ' ' * (significandSize+2)
	println "   n    p"+spc+" %"+spc+"  pe         s"
}

nthrows.times {
    n = it+1
	p = (1/cardOmega)**(n) as double
	s = java.lang.Math.log10(p)-1 as int
	if (min[s]) { if (min[s] > n) min[s] = n } else min[s] = n
	if (max[s]) { if (max[s] < n) max[s] = n } else max[s] = n
	if (displayProba) {
		s = sprintf(format, n, p, p*100, p, s).replaceAll(/ = 0([^,])/, ' =  $1')
		m0 = s =~ /(....)0+ = ([^-])/
		if (m0.count > 0) {
			spc0 = m0[0][1] + (' ' * (m0[0][0].size()-8)) + ' = ' + m0[0][2]
			s = m0.replaceFirst(spc0)
		}
		m0p = s =~ /(....)0+% = /
		if (m0p.count > 0) {
			spc0p = m0p[0][1] + '%' + (' ' * (m0p[0][0].size()-8)) + ' = '
			s = m0p.replaceFirst(spc0p)
		}

		if (!onlyMainRanks || ((n % dec) == 0) || (n == nthrows)) {
			println s
		}
		
		unit = unit+1
		if (unit >= dec * 10) {
			dec = dec * 10
			unit = 1
		}
	}
}

if (displayThrows) {

	if (displayProba) {
		println ""
	}

	def x = []
	def y = []
	
	println """Légende :
- -S      : spécificité équivalente
-  lancés : intervalle des nombres de lancés ou tirages avec succès consécutifs correspondant
"""
	println " -S   lancés"
	min.each { key, value ->
	 if (min[key] != max[key]) println sprintf("%3d = %d-%d", key, min[key], max[key])
	 else println sprintf("%3d = %d", key, min[key])
	 x.push(key)
	 y.push(min[key])
	}
	
//	println "x = "+x
//	println "y = "+y

}
