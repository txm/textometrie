package org.txm.macro.txt;
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*

// BEGINNING OF PARAMETERS

// Declare each parameter here
// (available widget types: Query, File, Folder, String, Text, Boolean, Integer, Float and Date)

@Field @Option(name="inputFile", usage="The file to read", widget="File", required=true, def="C:/Temp/foo.txt")
def inputFile

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

String s = inputFile.getText()

def dic = [:]

for (char c : s) {
	if (!dic.containsKey(c)) { dic[c] = 0 }
	dic[c]++
}

for (def e : dic) { println(sprintf("%c\t%d", [e.getKey(), e.getValue()])) }
