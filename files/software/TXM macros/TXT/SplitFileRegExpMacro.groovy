// Copyright © 2019 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden
// @author mdecorde

// splits a TXT file into several sub files based on a texts separating line pattern

package org.txm.macro.txt

// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*

// PARAMETERS //

@Field @Option(name="inputFile", usage="input file", widget="File", required=true, def="")
def inputFile // the file to process

@Field @Option(name="fileEncoding", usage="input and output file character encoding ('UTF-8' recommended. But possibly 'cp1252' on Windows, 'MacRoman' on Mac)", widget="String", required=true, def="UTF-8")
def fileEncoding

@Field @Option(name="outputDir", usage="output folder", widget="Folder", required=true, def="")
def outputDir // the directory which received result files

@Field @Option(name="separatorLineRegex", usage="separator line search regex (must include parenthesized groups to extract data for file name building)", widget="String", required=true,
               def="^\\*\\*\\*\\* \\*([^_\\n]+)_([^*\\n]+) \\*([^_\\n]+)_([^*\\n]+)")
def separatorLineRegex // matching line regex

@Field @Option(name="includeSeparatorLine", usage="include separator line content at the beginning of result files",
			   widget="Boolean", required=true, def="false")
def includeSeparatorLine // should include separator line in result files

@Field @Option(name="outputFilenamePrefix", usage="optional result files outputFilenamePrefix (example 'file-')", widget="String", required=false, def="")
def outputFilenamePrefix // result files outputFilenamePrefix

@Field @Option(name="outputFilePattern", usage="optional sub file name pattern, using parenthesized groups content (example \$1 = content of the first parenthesized group of separatorLineRegex, \$2 = content of the second parenthesized group of separatorLineRegex, etc.",
			   widget="String", required=false, def="\$4_\$2")
def outputFilePattern // use the matching line to build the filename

@Field @Option(name="outputFileExtension", usage="optional result files extension", widget="String", required=false, def=".txt")
def outputFileExtension // result files extension

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

separatorLineRegex = /$separatorLineRegex/ // create a pattern object

// CALL THE SPLIT METHOD //

splitByseparatorLineRegexp(inputFile, outputDir, separatorLineRegex, outputFilenamePrefix, outputFilePattern, outputFileExtension, includeSeparatorLine)


// PUBLIC METHOD DECLARATION //

def splitByseparatorLineRegexp(def inputFile, def outputDir, def separatorLineRegex, def outputFilenamePrefix, def outputFilePattern, def outputFileExtension, def includeSeparatorLine) {

	outputDir.deleteDir()
	outputDir.mkdirs()
	if (!outputDir.exists()) return false
	if (!inputFile.isFile()) return false

	def outfile = null
	def writer = null
	def count = 1

	inputFile.eachLine(fileEncoding) { line ->
		if (line ==~ separatorLineRegex) { // new file
			if (outputFilePattern == null || outputFilePattern == "") {
				outfile = new File(outputDir, outputFilenamePrefix+(count++)+outputFileExtension)
			} else {
				String name = outputFilenamePrefix+line.replaceAll(separatorLineRegex, outputFilePattern)+outputFileExtension
				outfile = new File(outputDir, name)
			}
			if (writer != null)	writer.close() // close previous writer
			writer = outfile.newWriter(fileEncoding)
			println "creating $outfile"
			if (includeSeparatorLine) {
				writer.println line
			}
		} else {
			if (writer != null)
				writer.println line
		}
	}
	if (writer != null)	writer.close()
}

