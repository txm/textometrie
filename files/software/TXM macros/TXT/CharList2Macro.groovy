package org.txm.macro.txt;
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.utils.CharsetDetector

// BEGINNING OF PARAMETERS

// Declare each parameter here
// (available widget types: Query, File, Folder, String, Text, Boolean, Integer, Float and Date)

@Field @Option(name="inputFile", usage="The file to read", widget="File", required=true, def="C:/Temp/foo.txt")
def inputFile

@Field @Option(name="inputEncoding", usage="Input encoding", widget="String", required=false, def="UTF8")
String inputEncoding

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

enc = inputEncoding
if (inputEncoding == null || inputEncoding.length() == 0) {
	CharsetDetector detector = new CharsetDetector(inputFile)
	enc = detector.getEncoding()
	println sprintf("Guessed '%s' character encoding.", enc)
}

s = inputFile.getText(enc)

def dic = [:]

for (char c : s) {
	if (!dic.containsKey(c)) { dic[c] = 0 }
	dic[c]++
}

dic = dic.sort()

line = 1
def first=true
for (def e : dic) {
	if (!first) {
		print ", "
	} else first = false
	c = e.getKey()
	if (java.lang.Character.isWhitespace(c)) {
		String s = c
		print(sprintf("<%s> %d", java.lang.Character.getName(s.codePointAt(0)), e.getValue()))
	} else {
		print(sprintf("%c %d", c, e.getValue()))
	}
	if ((line % 10) == 0) println ""
	line++
}
println ""
