package org.txm.macro.txt
// Copyright © - ENS de Lyon - http://textometrie.ens-lyon.fr
// @author sheiden
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2013-05-02 11:28:42 +0200 (jeu., 02 mai 2013) $
// $LastChangedRevision: 2378 $
// $LastChangedBy: sheiden $
//

// STANDARD DECLARATIONS

// imports
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*

// PARAMETERS

// **change this parameter**
@Field @Option(name="inputDirectory", usage="Dossier contenant les fichiers à modifier", widget="Folder", required=true, def='/home')
		inputDirectory = new File(System.getProperty("user.home"), "Bureau/voeux")

// **change this parameter**
@Field @Option(name="extension", usage="extension des fichiers à modifier (expression régulière)", widget="String", required=true, def='\\.txt')
		extension = "\\.txt"

// **change this parameter**
@Field @Option(name="find", usage="Expression régulière à chercher", widget="String", required=true, def='’')
		find = "’"

// **change this parameter**
@Field @Option(name="replaceWith", usage="Chaîne de remplacement", widget="String", required=false, def='\'')
		replaceWith = "'"

// **change this parameter**
@Field @Option(name="encoding", usage="Encodage des caractères des fichiers", widget="String", required=true, def='UTF-8')
		encoding = "utf-8"

// PARAMETERS DIALOG

if (!ParametersDialog.open(this)) return;

// SANITY CHECK

if (inputDirectory==null || find==null || replaceWith==null) { println "** MultiLineSearchReplaceInDirectoryMacro: the input directory, the regular expression to search and replacement must be specified."; return}

if (!inputDirectory.exists()) { println "** MultiLineSearchReplaceInDirectoryMacro: impossible to access the '$inputDirectory' input directory."; return}

// MAIN BODY

println "-- working in $inputDirectory directory with files of '$extension' extension"
println "-- replacing '$find' with '$replaceWith'"

// find = /date="([0-9]+)-([0-9]+-[0-9]+)"/
// **change this parameter**
// replaceWith = 'date="$1-$2" year="$1"'
// **change this parameter** (warning: '$1', '$2'... can be interpreted by Groovy in "..." strings)
//
// RECETTE
// <tei:pb [^>]+>\n   <tei:p>[0-9]+</tei:p>\n   <tei:p>[^ ]+ 18[5-6][0-9]\.</tei:p>\n

def p = /$find/

inputDirectory.eachFileMatch(~/.*$extension/) { file ->               // for each file matching extension
	println "\n-- processing: "+file.getName()
	def tmp = File.createTempFile("SearchReplaceInDirectoryTemp", ".tmp", file.getParentFile()) // create temporary file
	tmp.withWriter(encoding) { writer ->
		writer.print(file.getText(encoding).replaceAll(p, replaceWith))
	}
	file.delete()
	tmp.renameTo(file)
}
