// Copyright © 2012, HerongYang.com, All Rights Reserved.
// Copyright © 2017 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden
// from http://www.herongyang.com/Unicode/Java-charset-Supported-Character-Encodings-in-JDK.html

package org.txm.macro.txt

import java.nio.charset.Charset

println "Name = Aliases:"

m = Charset.availableCharsets()

m.keySet().each { key ->

	e = m.get(key)
	print key+" = "
	first = true
        e.aliases().each { alias ->
		if (first) {
			first = false
		} else {
			print ", "
		}
		print alias
	}
	println ""
}

