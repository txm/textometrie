// Copyright © 2019 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License version 3 or any later version (https://www.gnu.org/licenses/gpl.html)
// @author mdecorde
// @author sheiden

// STANDARD DECLARATIONS
package org.txm.macro.xml

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.importer.StaxIdentityParser
import javax.xml.stream.XMLStreamException

@Field @Option(name="inputFile", usage="input file (XML)", widget="FileOpen", required=true, def="input.xml")
def inputFile

@Field @Option(name="outputFile", usage="output file", widget="FileSave", required=true, def="output.xml")
def outputFile

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

if (!(inputFile.exists() && inputFile.canRead())) {
	println "** ForceWordIDs: cannot find $inputFile"
	return false
}

print "Processing "+inputFile+"..."

outputFile.getParentFile().mkdirs()

// build text id
textid = inputFile.getName()
if (textid.indexOf(".xml") > 0) {
	textid = textid.substring(0, textid.indexOf(".xml"))
}

def parser = new StaxIdentityParser(inputFile) {
	
	int wordnumber = 1
	
	/* ne fonctionne pas
	protected void writeStartElement() throws XMLStreamException {
		println localname
		if (localname ==~ "w|pc") { // a word
			localname = "w"
		} 
		
		super.writeStartElement()
	}
	*/
	
	protected void writeAttributes() throws XMLStreamException {
		if (localname ==~ "w|pc") { 																								// a word
			boolean idwritten = false
			attCount = parser.getAttributeCount()
			attCount.times { i ->
				if (parser.getAttributeLocalName(i) == "id") { // update & backup
					writeAttribute(parser.getAttributePrefix(i), parser.getAttributeLocalName(i), "w_"+textid+"_"+(wordnumber++)) 	// force id
					writeAttribute(parser.getAttributePrefix(i), "foreign-id", parser.getAttributeValue(i)) 						// backup
					idwritten = true
				} else if (parser.getAttributeLocalName(i) != "foreign-id") {
					writeAttribute(parser.getAttributePrefix(i), parser.getAttributeLocalName(i), parser.getAttributeValue(i))
				}
			}
			
			if (!idwritten) { 																										// create id
				writeAttribute(null, "id", "w_"+textid+"_"+(wordnumber++))
			}
		} else {
			super.writeAttributes()
		}
	}
}

res = parser.process(outputFile)

println " done."

return res
