package org.txm.macro

import org.txm.scripts.*
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*

@Field @Option(name="inputDirectory",usage="Dossier qui contient les fichiers à parser", widget="Folder", required=true, def="")
File inputDirectory;

@Field @Option(name="tsvFile",usage="FichierTSV résultat", widget="File", required=true, def="file.tsv")
File tsvFile;

@Field @Option(name="usePaths",usage="FichierTSV résultat", widget="Boolean", required=true, def="true")
def usePaths = true
@Field @Option(name="useAttributes",usage="FichierTSV résultat", widget="Boolean", required=true, def="false")
def useAttributes = true
@Field @Option(name="useAttributeValues",usage="FichierTSV résultat", widget="Boolean", required=true, def="false")
def useAttributeValues = true

if (!ParametersDialog.open(this)) return;
println "Input directory: "+inputDirectory
println "TSV file: "+tsvFile
println "use paths $usePaths"
println "use attributes $useAttributes"
println "use attributes values $useAttributeValues"

XMLStatistics.processDirectory(inputDirectory, tsvFile, ".xml", usePaths, useAttributes, useAttributeValues);
