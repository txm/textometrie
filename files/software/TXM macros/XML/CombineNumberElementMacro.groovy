// Copyright © 2018 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden

/*

This macro calls NumberElement successively

*/

package org.txm.macro.xml

res = gse.run(NumberElementMacro, [
	"args":[
		"inputDirectory":new File("/home/sheiden/Documents/projet-susana-mars-reers/corpus/tei"),
		"outputDirectory":new File("/home/sheiden/Documents/projet-susana-mars-reers/corpus/tei/out"),
		"elementName":"pb",
		"elementAttribute":"n",
		"countStart":94,
		"valuePrefix":"",
		"valueSuffix":"",
		"debug":"true"
	],
				"selection":selection,
				"selections":selections,
				"corpusViewSelection":corpusViewSelection,
				"corpusViewSelections":corpusViewSelections,
				"monitor":monitor])
if (!res) println "** problem calling NumberElementMacro."

res = gse.run(NumberElementMacro, [
	"args":["inputDirectory":new File("/home/sheiden/Documents/projet-susana-mars-reers/corpus/tei/out"),
	"outputDirectory":new File("/home/sheiden/Documents/projet-susana-mars-reers/corpus/tei/out/out"),
	"elementName":"pb",
	"elementAttribute":"facs",
	"countStart":102,
	"valuePrefix":"https://gallica.bnf.fr/iiif/ark:/12148/bpt6k35936/f",
	"valueSuffix":"/full/full/0/native.jpg",
	"debug":"true"
	],
				"selection":selection,
				"selections":selections,
				"corpusViewSelection":corpusViewSelection,
				"corpusViewSelections":corpusViewSelections,
				"monitor":monitor])
if (!res) println "** problem calling NumberElementMacro."

