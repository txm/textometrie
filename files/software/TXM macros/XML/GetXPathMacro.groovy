package org.txm.macro
// Copyright © 2015 - ENS de Lyon - http://textometrie.ens-lyon.fr
// @author sheiden
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2013-05-02 11:28:42 +0200 (jeu., 02 mai 2013) $
// $LastChangedRevision: 2378 $
// $LastChangedBy: sheiden $
//

// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*

import org.txm.importer.*

// BEGINNING OF PARAMETERS

// Declare each parameter here
// (available widget types: Query, File, Folder, String, Text, Boolean, Integer, Float and Date)

@Field @Option(name="srcDirectory", usage="XML source directory", widget="Folder", required=false, def="")
def srcDirectory

@Field @Option(name="filter", usage="filter by file extension", widget="Boolean", required=false, def="true")
def filter

@Field @Option(name="extension", usage="file extension to filter", widget="String", required=false, def=".xml")
def extension

@Field @Option(name="srcFile", usage="XML source file", widget="File", required=false, def="")
def srcFile

@Field @Option(name="XPath", usage="XPath expression", widget="String", required=true, def="//tei:title/text()")
def XPath

@Field @Option(name="sort", usage="sort the results", widget="Boolean", required=false, def="false")
def sort

@Field @Option(name="uniq", usage="remove duplicates", widget="Boolean", required=false, def="false")
def uniq

@Field @Option(name="count", usage="count duplicates", widget="Boolean", required=false, def="false")
def count

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) {
	println "** Abandon de la macro."
	return
}

// END OF PARAMETERS

if ((srcDirectory==null || srcDirectory.size() == 0) && (srcFile==null || srcFile.size() == 0)) { println "** GetXPathMacro: at least a source file or a source directory must be specified."; return}

if (count!=null && count) { sort=true; uniq=true }

if (uniq!=null && uniq) { sort=true }

if (srcDirectory!=null && srcDirectory.size() > 0 && srcDirectory.exists()) {

	def files = srcDirectory.listFiles()
	if (files == null || files.size() == 0) {
		println "** GetXPathMacro: No files in $srcDirectory"
		return
	}
	files.sort()

	def noFileSearched = true
	
	for (File xmlFile : files) {
	
		String name = xmlFile.getName()
		
		if (filter && !name.endsWith(extension)) { continue }
	
		println "** GetXPath(input directory = '$srcDirectory', input file = '$name', XPath = $XPath)"
		noFileSearched = false
		
		XPathResult xpathProcessor = new XPathResult(xmlFile)
		def value = xpathProcessor.getXpathResponses(XPath)
		if (value.size() == 0) { println "No result." } else {
				if (sort!=null && sort) {
					value = value.sort()
					if (count!=null && count) {
						counts = value.countBy { it }
						counts.each { s, f -> println "$s\t$f" }
					} else if (uniq!=null && uniq) {
						value.unique().each { println it }
					} else { value.each { println it } }
				} else { value.each { println it } }
		}
	}
	
	if (noFileSearched) { println "** GetXPath: no file searched." }

 } else if (srcFile!=null && srcFile.exists()) {
			def xmlFile = srcFile
			String name = xmlFile.getName()
			
			println "** GetXPath(input file = '$xmlFile', XPath = $XPath)"

			XPathResult xpathProcessor = new XPathResult(xmlFile)
			def value = xpathProcessor.getXpathResponses(XPath)
			if (value.size() == 0) { println "No result." } else {
				if (sort!=null && sort) {
					value = value.sort()
					if (count!=null && count) {
						counts = value.countBy { it }
						counts.each { s, f -> println "$s\t$f" }
					} else if (uniq!=null && uniq) {
						value.unique().each { println it }
					} else { value.each { println it } }
				} else { value.each { println it } }
			}
		}
