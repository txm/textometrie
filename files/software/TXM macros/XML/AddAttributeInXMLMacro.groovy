// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Arrays;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

// BEGINNING OF PARAMETERS
@Field @Option(name="xmlFile",usage="an example file", widget="File", required=true, def="/home/mdecorde/TEMP/nchurlet/limo.xml")
def xmlFile

@Field @Option(name="xpath",usage="an example string", widget="String", required=false, def="//pop")
def xpath

@Field @Option(name="nomAttributARajouter",usage="an example string", widget="String", required=false, def="n")
def nomAttributARajouter

@Field @Option(name="valeur",usage="an example string", widget="String", required=false, def="lol")
def valeur

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

println "Paramètres :"
println "xmlFile=$xmlFile"
println "xpath=$xpath"
println "nomAttributARajouter=$nomAttributARajouter"
println "valeur=$valeur"

		try {
			//			Scanner sc = new Scanner(System.in);
			//			System.out.println("Veuillez saisir un chemin absolu de fichier :");
			//			String path = sc.nextLine();
			//			System.out.println("Vous avez saisi : " + path);

			File f = xmlFile
			if (!testerFichier(f)) {
				System.out.println("Abandon.");
				return;
			}

			// Créé un Stream
			FileInputStream fileInputStream = new FileInputStream(f);

			// Récupère les noeuds à l'aide de l'xpath
			NodeList list = evaluer(fileInputStream, xpath);

			// modifie les noeuds et récupère le DOM modifié
			Document doc = modifierDOM(list, nomAttributARajouter, valeur);

			//Ecrire le DOM dans un autre fichier
			File outfile = new File(f.getParentFile(), "resultat-"+f.getName());

			// on écrit le DOM modifié dans une String
			String str = convertDomToString(doc);

			// on écrit la String dans le fichier de sortie
			ecrireString(str, outfile);
		} catch (TransformerException e) {
			System.out.println("Erreur lors de la convertion du DOM modifié en String : "+e);
		} catch (XPathExpressionException e) {
			System.out.println("Erreur lors de l'application de l'XPath '"+xpath +"' : "+e);
		} catch (IOException e) {
			System.out.println("Erreur lors de l'écriture du fichier résultat : "+e);
		}
	

	/**
	 * Prend une chaine de caractère et l'écrit dans un fichier
	 * @param str paramètre chaine de caractère
	 * @param outfile Fichier résultat
	 * @throws IOException
	 */
	private static void ecrireString(String str, File outfile) throws IOException {
		//Ouvre le fichier argument
		FileWriter filewriter = new FileWriter(outfile);
		//Prend le string pour l'écrire dans le fichier
		filewriter.write(str);
		//Ferme le fichier
		filewriter.close();
	}


	/**
	 * Prend un fichier et vérifie qu'il existe et qu'il est lisible
	 * @param f paramètre fichier
	 * @return un boolean
	 */
	private static boolean testerFichier(File f) {
		if (!f.exists()) { // verifie l'existance du fichier
			System.out.println("Le fichier "+f.getAbsolutePath()+" n'existe pas");
			return false;
		}

		if (!f.canRead()) {// verifie la lisibilité du fichier
			System.out.println("Le fichier "+f.getAbsolutePath()+" n'est pas lisible (droits insuffisants).");
			return false;
		}
		return true;
	}

	public static String convertDomToString(Document document) throws TransformerException {
		DOMSource domSource = new DOMSource(document);

		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = null;

		transformer = tf.newTransformer();
		transformer.transform(domSource, result);

		String stringResult = writer.toString();
		return stringResult;
	}	 

	/**
	 * Permet de modifier un document XML  en lui ajoutant un attribut auquel on associe une valeur (de type : attribut = valeur)
	 * @param list Paramètre liste de noeuds
	 * @param nomAttributARajouter Paramètre string chaine de caractère (le nom de l'attribut à ajouter)
	 * @param valeur Paramètre string chaine de caractère (la valeur de l'attribut à ajouter)
	 * @return DOM document
	 */
	public static Document modifierDOM(NodeList list, String nomAttributARajouter, String valeur ) {
		Document doc = null; // null pour l'instant
		//incrémentation de i avec la longueur de la liste de noeud comme limite.
		for (int i = 0 ; i < list.getLength() ; i++) 
		{ 

			Element n = (Element)list.item(i); //A chaque boucle on récupère  le ieme élément
			if (valeur == null) {
				//S'il n'y a pas de valeur renseignée on numérote les éléments à partir de 1 
				n.setAttribute(nomAttributARajouter, Integer.toString(i+1));
			} else {
				//Sinon on donne la valeur renseignée à l'attribut qu'on ajoute.
				n.setAttribute(nomAttributARajouter, valeur);
			}
			
			if (doc == null) 	doc = n.getOwnerDocument(); 
			//Donne le conteneur racine de l'arbre XML du noeud 'n'
		}
		return doc;
	}

	/**
	 * 
	 * @param stream Paramètre un fichier entrant
	 * @param expression Paramètre string chaine de caractère
	 * @return Une liste de noeuds
	 * @throws XPathExpressionException
	 */
	public static NodeList evaluer(InputStream stream, String expression) throws XPathExpressionException{
		NodeList liste = null; 
		//On initialise une liste de noeud nommée "liste"
		InputSource source = new InputSource(stream);
		//Création d'une source entrante(fichier) que l'on nomme source.

		XPathFactory fabrique = XPathFactory.newInstance();
		//On crée une fabrique d'XPath(XPathFactory) nommée fabrique
		XPath xpath = fabrique.newXPath();
		//On crée un Xpath nommé "xpath" à partir de de la fabrique d'XPath nommée "fabrique"
		
		//évaluation de l'expression XPath
		XPathExpression exp = xpath.compile(expression);
		//Lance une méthode super balèse pq'elle va retourner des noeuds à partir de l'expression exp
		liste = (NodeList)exp.evaluate(source,XPathConstants.NODESET);

		return liste;
	}