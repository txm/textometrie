import org.txm.Toolbox
import org.txm.searchengine.cqp.*

def CQI = Toolbox.getCqiClient();
if ((CQI instanceof NetCqiClient)) {
	println "Error: CQP eval only available in CQP memory mode"
	return;
}

CQI.query("set da enpos;")


/////////////////////////////////////
//// CQP variables documentation ////
/////////////////////////////////////

//// Useful CQP variables ///
// [da]	DefaultNonbrackAttr - implicit word property in queries ('word' by default)
// [ms]	MatchingStrategy    - shortest | standard | longest | traditional
// [sr]	StrictRegions       - single structure match

//// Maybe useful variables ////
// [o]	Optimize - simple regular expressions infixe optimizer
// [hf]	HistoryFile
// [wh]	WriteHistory

//// Useless variables ////
// [p]	Paging
// [pg]	Pager
// [h]	Highlighting
// [col] Colour
// [pb]	ProgressBar
// [pp]	PrettyPrint
// [c]	Context
// [lc]	LeftContext
// [rc]	RightContext
// [ld]	LeftKWICDelim
// [rd]	RightKWICDelim
// [pm]	PrintMode
// [po]	PrintOptions
// [ps]	PrintStructures
// [sta] ShowTagAttributes
// [st]	ShowTargets
// [as]	AutoShow
// [es]	ExternalSort
// [esc] ExternalSortCommand
// 	AutoSave
// 	SaveOnExit
// 	Timing

//// Must not be changed ////
// [r]	Registry
// [dd]	DataDirectory
// [sub] AutoSubquery
