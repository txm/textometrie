import org.txm.Toolbox
import org.txm.searchengine.cqp.*

def CQI = Toolbox.getCqiClient();
if ((CQI instanceof NetCqiClient)) {
	println "Error: only available in CQP memory mode"
	return;
}

// week days list
def list_name = "week"
def list_value = "Monday Tuesday Wednesday Thursday Friday"

def cqp_statement = "define \$$list_name = \"$list_value\";"
println "CQP executes : $cqp_statement"
CQI.query(cqp_statement)


/*
 * Other examples (commented)
 */

/*
CQI.query("define \$week = \"Monday Tuesday Wednesday Thursday Friday\";")
*/

/*
// week days list in file
def list_name = "weekfile"
def list_path = "/home/sheiden/TXM/cqp/week.txt"

def cqp_statement = "define \$$list_name < \"$list_path\";"
println "CQP executes : $cqp_statement"
CQI.query(cqp_statement)
*/

/*
// pos stop list
def stoplistcategories = "ABR ADV DET:ART DET:POS INT KON NUM PRO PRO:DEM PRO:IND PRO:PER PRO:POS PRO:REL PRP PRP:det PUN PUN:cit SENT SYM"
def query = "define \$stoplistcategories =\"$stoplistcategories\";"
println "CQP executes : $query"
CQI.query(query)
*/
