---
ititle: Macros TXM
layout: page
lang: fr
ref: txm-macro
---

Macros TXM
===
{: style="text-align: center;"}

<div style = "background-image: url ({{'/ img / TXM-background.jpg' | absolute_url}}); background-repeat: aucune répétition; background-position: droite en haut;" markdown = "1">

TXM est livré avec des petits utilitaires échangeables et adaptables, qui prennent la forme de scripts écrits en langage Groovy.

La documentation de référence des macros se trouve dans le
[wiki des utilisateurs de TXM](https://groupes.renater.fr/wiki/txm-users/public/macros).

<!--
Scripts utilitaires pour TXM pour poste, éditables par l'utilisateur, organisés par catégories:
{: style="margin-bottom: 5px"}

- [Annotation](Annotation) : utilitaires pour éditer et corriger les annotations au niveau des mots
- [Commands](Commands) : services complémentaires pouvant éventuellement être intégrés à TXM à terme
- [CQP](CQP) : utilitaires pour accéder au moteur de recherche CQP directement
- [CSV](CSV) : utilitaires pour traiter des tables CSV avant importation dans TXM
- [Edition](Edition) : utilitaires complémentaires pour modifier des éditions de corpus, pouvant éventuellement être intégrés à TXM à terme
- [Office](Office) : utilitaires de conversion de fichiers Excel avant import dans TXM
- [R](R) : utilitaires pour accéder à l'environnement statistique R directement
- [Stats](Stats) : outils statistiques complémentaires
- [Syntax](Syntax) : conversion de format de PennTreebank vers TIGER-XML
- [Texte](Texte) : utilitaires pour traiter des fichiers de traitement de texte avant importation dans TXM
- [Transcription](Transcription) : utilitaires pour traiter des fichiers de transcriptions d'enregistrements audio/vidéo avant importation dans TXM
- [TXT](TXT) : utilitaires pour traiter des fichiers en texte brut avant importation dans TXM
- [XML](XML) : utilitaires pour traiter des fichiers XML avant importation dans TXM
- [XSL](XSL) : utilitaires pour appliquer des feuilles de transformation XSL
- [Divers](Divers) : autres utilitaires

Voir aussi:
{: style="margin-bottom: 5px"}

- La section sur les macros du [manuel de TXM](https://txm.gitpages.huma-num.fr/txm-manual/piloter-la-plateforme-par-scripts.html#biblioth%C3%A8que-de-macros-pr%C3%A9d%C3%A9finies)
- La [documentation de référence des macros](https://groupes.renater.fr/wiki/txm-users/public/macros)

-->

Veuillez nous contacter à 'textometrie AT ens-lyon.fr' pour plus d'informations.

L'équipe TXM

