package org.txm.macro;
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.rcpapplication.commands.*
import org.txm.Toolbox
import org.txm.stat.engine.r.RWorkspace

@Field @Option(name="f",usage="La fréquence de la forme dans la partie", widget="Integer", required=true, def="14")
def f;

@Field @Option(name="F",usage="La fréquence totale de la forme dans le corpus", widget="Integer", required=true, def="200")
def F;

@Field @Option(name="t",usage="Le nombre d'occurrences de la partie", widget="Integer", required=true, def="500")
def t;

@Field @Option(name="T",usage="Le nombre total d'occurrences du corpus", widget="Integer", required=true, def="1000")
def T;

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

def r = RWorkspace.getRWorkspaceInstance()
def file = File.createTempFile("txm", ".svg", new File(Toolbox.getParam(Toolbox.USER_TXM_HOME), "results"))

/// BEGIN SCRIPTS
def script ="""
x=$f
F=$F
T=$T
t=$t

m=((F+1)*(t+1))/(T+2)
xlimit=(F/4)
if (xlimit < m) xlimit = (F/4) + m
f=0:xlimit

m=((F+1)*(t+1))/(T+2)
pf=dhyper(f,F,T-F,t)
pfsum=pf
for (i in 1:m) {
	pfsum[i+1] = pfsum[i+1] + pfsum[i]
}

for (i in (xlimit-1):m) {
	pfsum[i+1] = pfsum[i+1] + pfsum[i+2]
}

px <- pf[x+1]
pfsumx <- pfsum[x+1]

plot(f,pfsum, xlab="f'", ylab="P", type="l", col="blue")
lines(f, pf, type="l", col="green")

al=max(pfsum)/10
ad=1
tpos=4
if (m > x) {ad = -ad}
if (m > x) {tpos = 2}
arrows(m, al*0.7,m,0, length=0.1)
arrows(x+ad, al*0.7+px,x,px, length=0.1)
arrows(x+ad, al*1.4+pfsumx,x,pfsumx, length=0.1)

smode <- sprintf("mode = %d", round(m))
spf <- sprintf("p(f'=%d) = %s", x, format.default(px, sci = TRUE, digits = 4))
sintpf <- sprintf("p(f'>=%d) = %s", x, format.default(pfsumx, sci = TRUE, digits = 4))

text(m,al,smode,xpd=TRUE)
text(x, (al*0.7)+px,spf, pos=tpos,xpd=TRUE)
text(x, (al*1.4)+pfsumx,sintpf, pos=tpos,xpd=TRUE)

s1 <- sprintf("P(f' = f)")
s2 <- sprintf("integral P(f' >= f)")
legend("topright", c(s1, s2),lty = 1, col=c("green", "blue"), inset = .02, cex=0.9)
"""
/// END SCRIPTS
r.plot(file, script)

def px = r.eval("px").asDouble()
def mode = r.eval("m").asInteger()
println """P(f'=f) et P(f' >= f)
f	F	t	T
$f	$F	$t $T
P(f' >= $f) = $px
mode = $mode"""
println "Result saved in: "+file.getAbsolutePath()

//display the graphic
monitor.syncExec(new Runnable() {
	@Override
	public void run() {	OpenSVGGraph.OpenSVGFile(file.getAbsolutePath(), "Specificity distribution") }
});