package org.txm.macro;
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.importer.ApplyXsl2;

// BEGINNING OF PARAMETERS
@Field @Option(name="XSLFile",usage="an example file", widget="File", required=true, def="")
def XSLFile = new File("/home/mdecorde/TXM/xsl/identity.xsl")

@Field @Option(name="intputDirectory",usage="an example folder", widget="Folder", required=true, def="")
def intputDirectory = new File("/home/mdecorde/xml/TESTS2/xml")

@Field @Option(name="outputDirectory",usage="an example folder", widget="Folder", required=true, def="")
def outputDirectory = new File("/tmp/xsltest")

//@Field @Option(name="parameters",usage="an example folder", widget="Text", required=false, def="")
def parameters = ""

@Field @Option(name="debug",usage="an example folder", widget="String", required=true, def="false")
def debug

if (!ParametersDialog.open(this)) return;
// END OF PARAMETERS

debug = "true" == debug

outputDirectory.mkdir()

parameters = parameters.trim()
if (parameters.length() == 0) {
	parameters = null
} else {
	def split = parameters.split("\n")
	parameters = []
	for (def str : split) {
		def split2 = str.split("=", 2)
		if (split2.size() == 2) {
			parameters << split2
		}
	}
}

println "Use XSL $XSLFile with parameters $parameters"
println "Processed directory: $intputDirectory"

def files = [] 
ApplyXsl2 a = new ApplyXsl2(XSLFile.getAbsolutePath());
intputDirectory.eachFileMatch(~/.+\.(xml|XML)/) { XMLFile ->
	String name = XMLFile.getName()
	try {
		
		def outFile = new File(outputDirectory, name)
		if (parameters != null) {
			for(def param : parameters) {
				a.SetParam(param[0], param[1])
			}
		}
		a.process(XMLFile, outFile);
		a.resetParams()
		files << XMLFile
	} catch (Exception e) {
		println "Warning: XSL transformation of '$name' failed with error=$e with "
		if (debug) e.printStackTrace(); 
	}
}