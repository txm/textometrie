---
ititle: TXM Documentation
layout: page
lang: en
ref: documentation
---

## Summary
{: .no_toc}

1. TOC
{:toc}

## TXM desktop software user documentation (Windows, Mac OS X, Linux)

### Video tutorial
{: .no_toc}

- [Videocast of **TXM 0.6 initiation Workshop** taught by Bénédicte Pincemin on September 27th 2012](/html/enregistrement_atelier_initiation_TXM_fr.html) (in French) [^1]
- Videocasts of the training workshop **Import of corpus into TXM 0.8** by Alexey Lavrentev in March 2020, as part of the course *Digital edition of textual sources* (LAF4204) at ENS de Lyon:
   1. [TXM Import 1. Introduction](https://www.youtube.com/watch?v=pX_qwr_NxOs&list=PL_weW0aPormgLdNb0fSyS5lwaXU3it6WN&index=1&pp=iAQB) (10:55)
   1. [TXM Import 2. Clipboard and TXT+CSV](https://www.youtube.com/watch?v=TE4hxNOl2eA&list=PL_weW0aPormgLdNb0fSyS5lwaXU3it6WN&index=2&pp=iAQB) (23:22)
   1. [TXM import 3. XML and 'XML TEI Zero+CSV' XML import module - simple](https://www.youtube.com/watch?v=aqXAmJnJIRI&list=PL_weW0aPormgLdNb0fSyS5lwaXU3it6WN&index=3&pp=iAQB) (23:12)
   1. [TXM import 4. TEI Oxgarage and advanced 'XML TEI Zero+CSV XML import module - avancé (CSS and XSLT)](https://www.youtube.com/watch?v=unzbuwpfq00&list=PL_weW0aPormgLdNb0fSyS5lwaXU3it6WN&index=4&pp=iAQB) (18:54)

[^1]: Feel free to adjust the video quality settings in Youtube - use 720p HD resolution if your connection allows it.

## Documentation from community
{: .no_toc}

- [Explore, play, analyse your corpus with TXM: A short introduction of TXM](http://dhd-blog.org/?p=3384) by José Calvo and Silvia Gutiérrez, 17. April 2014.
- in German: [TXM Kurzreferenz](https://www.penflip.com/c.schoech/txm-kurzreferenz), C. Schoech,  9. November 2014.

### TXM Manual
{: id="txm-manual"}
{: .no_toc}
{: style="margin-bottom: 20px"}

#### TXM 0.8 Manual

- [to read online](https://pages.textometrie.org/txm-manual) (in French, being drafted) (HTML)

#### TXM 0.7 Manual

- [to print]({{"/files/documentation/TXM%20Manual%200.7.pdf" | absolute_url}}) (PDF), [reference version]({{"/files/documentation/Manuel%20de%20TXM%200.7%20FR.pdf" | absolute_url}}) (in French) (PDF)
- [to read online](/html/doc/manual/0.7.9/fr/manual1.xhtml) (in French) (HTML)

### Very fast introduction to TXM key concepts in 90 minutes
{: .no_toc}

- [PDF]({{"/files/documentation/Tutorial%20A%20Gentle%20Introduction%20To%20TXM%20Key%20Concepts%20In%20One%20And%20An%20Half%20Hour.pdf" | absolute_url}})

### Leaflet presenting TXM
{: .no_toc}

- [PDF]({{"/files/documentation/TXM%20Leaftlet%20EN.pdf" | absolute_url}})

### Tutorials
{: .no_toc}

- [Import of Europresse articles in XML/w+CSV to TXM](https://groupes.renater.fr/wiki/txm-users/public/tutoriel_europresse)
- [R tutorial](http://zoonek2.free.fr/UNIX/48_R/all.html)
- Other tutorials are available [on the wiki page of txm-users list](https://groupes.renater.fr/wiki/txm-users/public/tutoriels)

### FAQ
{: .no_toc}

- [Frequently Asked Questions](https://groupes.renater.fr/wiki/txm-users/public/faq) on the txm-users wiki (in French)

### Participate
{: .no_toc}

- report [bugs](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_logiciel)
- ask for [new features](https://groupes.renater.fr/wiki/txm-users/public/demande_de_fonctionnalites)

## Additional User Documentation
{: .no_toc}

- [Speech transcription Guidelines with Transcriber for TXM import and analysis]({{"/files/documentation/Guide%20de%20Transcription%20d_entretiens%20Transcriber-TXM%200.3_FR.pdf" | absolute_url}})
-[Weblex Manual]({{"/files/documentation/references/weblex.pdf" | absolute_url}}): : Weblex is a textometry software developed in Lyon before TXM; it is no longer online, but its documentation can still be instructive, for example Chapter 9 "CQP Expressions Reference Manual".

### Diagnostic
{: .no_toc}

- [TXM diagnostics by command line](https://groupes.renater.fr/wiki/txm-users/public/tutoriel_ligne_de_commande)

## TXM portal software user documentation

[BFM portal tutorial](http://txm.ish-lyon.cnrs.fr/bfm/html/Tutoriel_TXM_BFM_V1.pdf) (Medieval French Database) is the main user-documentation currently available for the portal version of TXM (in French). It is available throught the [BFM portal](http://txm.bfm-corpus.org/).

Furthermore, you might be interested in:
{: style="margin-bottom: 5px"}

- [Known bugs of current version](https://groupes.renater.fr/wiki/txm-users/public/retours_de_bugs_web/txmweb_0.4)
- [Feature requests and possible evolutions](https://groupes.renater.fr/wiki/txm-users/public/demande_de_fonctionnalites_du_portail)

## TXM portal software administrator documentation

- [Getting started with a TXM portal (Huma-Num)]({{"/files/documentation/Portail%20TXM/Manuel%20du%20portail%20TXM%200.6.3%20-%20prise%20en%20main%20rapide.pdf" | absolute_url}})
- TXM Portal Administration Manual
    - [Administrator manual for the TXM portal 0.6.3]({{"/files/documentation/Portail%20TXM/Manuel%20du%20portail%20TXM%200.6.3%20-%20admin.pdf" | absolute_url}})
    - [Administrator manual for the TXM portal 0.6.1]({{"/files/documentation/Portail%20TXM/Manuel%20du%20portail%20TXM%200.6.1%20-%20admin.pdf" | absolute_url}})
- [Diagnostics and restart of a TXM portal TXM](https://groupes.renater.fr/wiki/txm-info/private/diagnostic_portail_txm)
- [Update of the software of a TXM portal](https://groupes.renater.fr/wiki/txm-info/public/update_txm_portal_software)
- [List of public TXM portals](https://groupes.renater.fr/wiki/txm-users/public/references_portails)

## Documentation for TXM Platform Developers

The [TXM developers's wiki](https://groupes.renater.fr/wiki/txm-info) is the reference site for the development of the TXM platform. Contact us to participate.

We find in particular:
{: style="margin-bottom: 5px"}

- how to [install the development environment](https://groupes.renater.fr/wiki/txm-info/public/preparation_env_dev_manuel) (in French)
    - [automatic translation in English](https://translate.google.com/translate?sl=fr&tl=en&js=y&prev=_t&hl=fr&ie=UTF-8&u=https%3A%2F%2Fgroupes.renater.fr%2Fwiki%2Ftxm-info%2Fpublic%2Fpreparation_env_dev_manuel&edit-text=&act=url)
    - [automatic translation in Russian](https://translate.google.com/translate?sl=fr&tl=ru&js=y&prev=_t&hl=fr&ie=UTF-8&u=https%3A%2F%2Fgroupes.renater.fr%2Fwiki%2Ftxm-info%2Fpublic%2Fpreparation_env_dev_manuel&edit-text=&act=url)
    - [automatic translation in German](https://translate.google.com/translate?sl=fr&tl=de&js=y&prev=_t&hl=fr&ie=UTF-8&u=https%3A%2F%2Fgroupes.renater.fr%2Fwiki%2Ftxm-info%2Fpublic%2Fpreparation_env_dev_manuel&edit-text=&act=url)
- how to [get the sources](https://groupes.renater.fr/wiki/txm-info/public/preparation_env_dev_manuel#recuperer_les_projets_txm_depuis_svn)
- the [Javadoc]({{"/Javadocs" | absolute_url}})
- the [R textometry package documentation](https://cran.r-project.org/web/packages/textometry/textometry.pdf)
- development tutorials concerning the Toolbox and the desktop version, the architecture of import modules, the GUI translation procedure, the procedure for building setups for a release, etc.
- the [roadmap](https://groupes.renater.fr/wiki/txm-info/#chantiers) of the successive versions of TXM
- some design elements of future components
- the list of developers participating in the project
- a list of key open source technologies that are or can be used in TXM
- links to reference documents of TXM components

All documentation files available at: [documentation section](https://txm.gitpages.huma-num.fr/textometrie/files/documentation/).

## TXM Platform Components

- CQP Search Engine
     - [CQP Manual](http://corpora.dslo.unibo.it/TCORIS/cqpman.pdf) (1999) 
     - [CQP Tutorial](http://cwb.sourceforge.net/files/CQP_Tutorial.pdf) (2019)
     - [CQP Corpus Administrator's Manual]({{"/files/documentation/cqp-technical-manual.pdf" | absolute_url}}) (1994)
     - [Corpus Encoding Tutorial](http://cwb.sourceforge.net/files/CWB_Encoding_Tutorial.pdf) (2002)
     - [Open CWB project documentation](http://cwb.sourceforge.net/documentation.php)
     - [CQI Tutorial](http://cwb.sourceforge.net/files/cqi_tutorial.pdf) (2000)
- R Statistics Engine
     - [An Introduction to R](http://cran.r-project.org/doc/manuals/R-intro.html)
     - [Introduction à R et au tidyverse](https://juba.github.io/tidyverse) (in French)
     - Richard A. Becker, John M. Chambers, Allan R. Wilks (1988) *The New S Language, A programming environment for data analysis and graphics*, Pacific Grove, Wadsworth & Brooks/Cole
     - Brian S. Everitt & Torsten Hothorn (2006) *A Handbook of Statistical Analyses Using R*, Boca Raton, Chapman & Hall/CRC
     - Pierre-André Cornillon, Arnaud Guyader, François Husson, Nicolas Jégou, Julie Josse, Maela Kloareg, Éric Matzner-Løber, Laurent Rouvière (2008), *Statistiques avec R*, Rennes, Presses Universitaires de Rennes
     - R. Harald Baayen (2008) *[Analyzing Linguistic Data, A practical introduction to statistics](http://www.sfs.uni-tuebingen.de/~hbaayen/publications/baayenCUPstats.pdf)*, Cambridge, Cambridge University Press.
     - Gries (2009) *Quantitative Corpus Linguistics With R: A Practical Introduction*
     - [official R site](http://www.r-project.org)
     - [Writing R Extensions](http://cran.r-project.org/doc/manuals/R-exts.html)
- Groovy Script Engine
    - [Official Groovy website](https://groovy-lang.org)
       - [Learning section](https://groovy-lang.org/learn.html)
    - Dierk König, Andrew Glover, Paul King, Guillaume Laforge et al., *Groovy in action*, Greenwich : Manning, 2007.
    - Barclay, Kenneth A. and Savage, W. J., *Groovy programming: an introduction for Java developers*, Morgan Kaufmann Publishers, 2007.
    - Venkat Subramaniam, *Programming Groovy : dynamic productivity for the Java developer*, Daniel H. Steinberg ed., Raleigh, N.C. : Pragmatic Bookshelf, 2008.

## Textometry: Corpus Encoding

- Habert Benoît, Fabre Cécile, Issac Fabrice. (1998) De l'écrit au numérique : constituer, normaliser, exploiter les corpus électroniques. Paris, InterÉditions/Masson, Informatiques
- Habert Benoît. (2005) Instruments et ressources électroniques pour le français, Paris, Orphys 
- XML & TEI
    - [A gentle introduction to XML](http://www.tei-c.org/release/doc/tei-p5-doc/en/html/SG.html)
    - [What is the Text Encoding Initiative?](https://books.openedition.org/oep/426)
    - [Main structural elements](http://www.tei-c.org/release/doc/tei-p5-doc/en/html/DS.html)
    - [The TEI header](http://www.tei-c.org/release/doc/tei-p5-doc/en/html/HD.html)
    - [Documentation of all elements](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/REF-ELEMENTS.html)
    - [Elements available for all TEI documents](http://www.tei-c.org/release/doc/tei-p5-doc/en/html/CO.html)
    - [TEI infrastructure](http://www.tei-c.org/release/doc/tei-p5-doc/en/html/ST.html)
    - [Introduction to ODD](https://tei-c.org/guidelines/customization/getting-started-with-p5-odds)
        - [page ODD du wiki TEI](https://wiki.tei-c.org/index.php?title=ODD)
    - [Roma parameterization interface](http://www.tei-c.org/Roma)
    - [Customizing the TEI with Roma](https://tei-c.org/guidelines/customization/customizing-the-tei-with-roma)
- NLP (Natural Language Processing)
    - [NLP journals](http://www.derczynski.com/sheffield/journals.html)
    - [TAL journal](http://www.atala.org/revuetal)
    - [TreeTagger](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/)
        - Helmut, Schmid (1994) - [Probabilistic Part-of-Speech Tagging Using Decision Trees](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tree-tagger1.pdf)

