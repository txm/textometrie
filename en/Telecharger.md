---
ititle: TXM Download
layout: page
lang: en
ref: télécharger
---

### Download latest release of TXM for desktop<br/>(Windows, Mac OS X, Linux)
{: style="text-align: center;"}

{:refdef: style="text-align: center;"}
[![Download TXM]({{"/img/download_100.png" | absolute_url}})]({{"/files/software/TXM/0.8.3/en" | absolute_url}})
{: refdef}

### All TXM versions to Download

- [TXM desktop]({{"/files/software/TXM/en" | absolute_url}}) : The TXM desktop software allows you to import your own corpora and analyze them on your own machine.
- [TXM web portal]({{"/files/software/TXM portal/en" | absolute_url}}) : TXM web portal software makes it possible to access online corpora via simple web browsers without having to install TXM on its own machine. It installs on a web server.

### TXM desktop installation tutorials

- [Installing TXM on your own computer](https://groupes.renater.fr/wiki/txm-users/public/preparation_ordinateur) (in French)
- [Installing TXM in a classroom](https://groupes.renater.fr/wiki/txm-users/public/installer_txm_dans_une_salle_de_cours) (in French)

### TXM web portal installation tutorial

- [TXM portal installation tutorial](https://groupes.renater.fr/wiki/txm-info/public/install_a_txm_portal)

### Download example corpora

- [Library of sample corpora for TXM]({{"/files/corpora/en" | absolute_url}}).

## Citing TXM

**<font color="green">If you use TXM for your research, please quote one of the following two references in your publications. This is important for the sustainability of the platform development:</font><br/>**

### A. Reference in English

| {% reference heiden_serge_txm_2010-1 --file Publications de l’équipe %} |

Bibtex[^1]:
~~~
{% raw %}@inproceedings{heiden:halshs-00549764,
  TITLE = {{The TXM Platform: Building Open-Source Textual Analysis Software Compatible with the TEI Encoding Scheme}},
  AUTHOR = {Heiden, Serge},
  URL = {https://halshs.archives-ouvertes.fr/halshs-00549764},
  BOOKTITLE = {{24th Pacific Asia Conference on Language, Information and Computation}},
  ADDRESS = {Sendai, Japan},
  EDITOR = {Ryo Otoguro, Kiyoshi Ishikawa, Hiroshi Umemoto, Kei Yoshimoto and Yasunari Harada},
  PUBLISHER = {{Institute for Digital Enhancement of Cognitive Development, Waseda University}},
  PAGES = {389-398},
  YEAR = {2010},
  MONTH = Nov,
  KEYWORDS = {xml-tei corpora ; search engine ; statistical analysis ; textometry ; open-source},
  PDF = {https://halshs.archives-ouvertes.fr/halshs-00549764/file/paclic24_sheiden.pdf},
  HAL_ID = {halshs-00549764},
  HAL_VERSION = {v1},
}{% endraw %}
~~~

### B. Reference in French

| {% reference heiden_serge_txm_2010 --file Publications de l’équipe %} |

Bibtex:
~~~
{% raw %}@inproceedings{heiden:halshs-00549779,
  TITLE = {{TXM : Une plateforme logicielle open-source pour la textom{'e}trie - conception et d{'e}veloppement}},
  AUTHOR = {Heiden, Serge and Magu{'e}, Jean-Philippe and Pincemin, B{'e}n{'e}dicte},
  URL = {https://halshs.archives-ouvertes.fr/halshs-00549779},
  BOOKTITLE = {{10th International Conference on the Statistical Analysis of Textual Data - JADT 2010}},
  ADDRESS = {Rome, Italy},
  EDITOR = {Sergio Bolasco, Isabella Chiari, Luca Giuliano},
  PUBLISHER = {{Edizioni Universitarie di Lettere Economia Diritto}},
  VOLUME = {2},
  NUMBER = {3},
  PAGES = {1021-1032},
  YEAR = {2010},
  MONTH = Jun,
  KEYWORDS = {textometry ; full text search engine ; statistical analysis ; natural language processing ; grails framework ; textom{'e}trie ; open-source ; moteur de recherche plein texte ; analyse statistique ; xml-tei ; traitement automatique de la langue ; tal ; eclipse rcp ; framework grails},
  PDF = {https://halshs.archives-ouvertes.fr/halshs-00549779/file/Heiden_al_jadt2010.pdf},
  HAL_ID = {halshs-00549779},
  HAL_VERSION = {v1},
}{% endraw %}
~~~

Notes :

[^1]: for import into Zotero.

