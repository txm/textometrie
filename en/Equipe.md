---
ititle: TXM Team
layout: page
lang: en
ref: équipe
---

##  TXM Team - Who's Who? 
{: style="text-align: center;"}

| **Firstname** | **Name** | **Responsibility** | **Phone number** | **e-mail** | **jabber** |
| Matthieu  | DECORDE                                                        | TXM main developer | +33.(0)4.37.37.62.16 | matthieu *POINT* decorde *AT* ens-lyon.fr | mdecorde *AT* jabber.ens-lyon.fr |
| Serge     | [HEIDEN](http://ihrim.ens-lyon.fr/auteur/heiden-serge)         | Textometry project lead, TXM development lead, digital philology | +33.(0)6.22.00.38.83 | slh *AT* ens-lyon.fr | sheiden *AT* jabber.ens-lyon.fr |
| Sébastien | JACQUOT                                                        | TXM developer | +33.(0)3.81.66.54.22 | sebastien *POINT* jacquot *AT* univ-fcomte.fr | |
| Alexei    | [LAVRENTIEV](http://ihrim.ens-lyon.fr/auteur/lavrentev-alexey) | digital philology, XML-TEI corpus encoding and processing | +33.(0)4.37.37.63.10 | alexei *POINT* lavrentev *AT* ens-lyon.fr | |
| Bénédicte | [PINCEMIN](http://ihrim.ens-lyon.fr/auteur/pincemin-benedicte) | Expert user, Textometry | +33.(0)4.37.37.66.42 | benedicte *POINT* pincemin *AT* ens-lyon.fr | |

<br/>

![]({{ "/img/equipe-top.jpg" | absolute_url }})

From left to right: Serge Heiden, Matthieu Decorde, Bénédicte Pincemin, Alexis Lavrentiev & Sébastien Jacquot

