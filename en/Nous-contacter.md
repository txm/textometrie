---
ititle: TXM Contact
layout: page
lang: en
ref: nous-contacter
---

# Contact us

- By email: <a href="mailto:{{ "textometrie@groupes.renater.fr" | encode_email }}" title="textometrie AT groupes.renater.fr">textometrie AT groupes.renater.fr</a>
- TXM users website: <https://groupes.renater.fr/wiki/txm-users>
- TXM developers website: <https://groupes.renater.fr/wiki/txm-info>
- Research project website: <http://http://www.textometrie.org>
- Postal address:  
  Serge Heiden - groupe Cactus  
  Laboratoire IHRIM - ENS de Lyon  
  15 parvis René Descartes - BP7000  
  69342 Lyon Cedex 07  
  FRANCE  
- Phone: +33 (0)6 22 00 38 83

