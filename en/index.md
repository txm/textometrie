---
layout: page
categories: txm textométrie welcome
lang: en
ref: index
---

<!--
<div style="position: -webkit-sticky; position: -moz-sticky; position: -ms-sticky; position: -o-sticky; position: sticky; top: 70px; float: right;">
-->
<div style="position: fixed; top: 100px; right: 8px;">
<table border="0" width="280" CELLPADDING="3px" BGCOLOR="#F2F2F2" style="margin-left:auto;margin-right:auto;">
	<tr>
		<td width="280" BGCOLOR="#F2F2F2">
			<font color="green"
				style="font-family:arial, sans-serif; font-size:12pt;">
				Quick Links
			</font>
		</td>
	</tr>
	<tr VALIGN="TOP">
		<td width="280" VALIGN="TOP" BGCOLOR="#F2F2F2">
			<font style="font-family:arial, sans-serif; font-size:12pt; color:#2582A4;">
				<a href="https://groupes.renater.fr/wiki/txm-users/index"
					target="blank">TXM Users's wiki (French)</a>
				<br />
				<a href="https://groupes.renater.fr/sympa//info/txm-users/"
					target="blank">TXM-Users's mailing list</a>
				<br />
				<a href="../files/en">Library of files for TXM
				</a>
				<br />
				<a href="https://groupes.renater.fr/wiki/txm-info" target="blank">TXM developers's wiki
				</a>
				<br />
				<a href="https://lists.sourceforge.net/lists/listinfo/txm-open/"
					target="blank">TXM-Open mailing list (English)</a>
			</font>
		</td>
	</tr>
</table>

<table border="0" width="280" CELLPADDING="3px" BGCOLOR="#F2F2F2" style="margin-left:auto;margin-right:auto;">
	<tr>
		<td width="270" VALIGN="TOP" BGCOLOR="#F2F2F2">
			<font
				style="font-family:arial, sans-serif; font-size:12pt; color:#FE642E">   &nbsp;&nbsp;News
			</font>
		</td>
	</tr>
	<tr VALIGN="TOP">
		<td width="270" VALIGN="TOP"
			style="padding-left:10px;padding-right:20px;padding-top:10px" BGCOLOR="#F2F2F2">
			<marquee direction="up" scrollamount="2" HEIGHT="80px">
					<font color="red" style="font-family:arial, sans-serif; font-size:12pt;">
							The last version of the TXM platform (0.8.3) has been released on 20th June 2023.
							<br />
							<br />
					</font>
					<font color="red" style="font-family:arial, sans-serif; font-size:12pt;">
							The extension « Annotation URS (Unit-Relation-Schema) » version 1.0 has been released on 4th July 2019.
							<br />
							<br />
					</font>
			</marquee>
		</td>
	</tr>
</table>
</div>

## Download the latest version of TXM
{: style="padding-top: 30px"}

| [![Download TXM]({{"/img/download_100.png" | absolute_url}})]({{"/files/software/TXM/0.8.3/en" | absolute_url}}) | <font color="red">ersion 0.8.3 of the TXM desktop software has been updated on 7th November 2023<br/>Version 0.8.3 of the TXM desktop software has been released on 20th June 2023<br/>Version 0.6.3 of the TXM portal software has been released on 19th September 2023</font> |

- [Other versions](Telecharger)

### Access the demonstration site of the TXM portal software

- <http://portal.textometrie.org/demo>

### Access the TXM Workshops main page (page in French & workshops in French)

- <https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm>

## The project in a few lines

[Textometry](Introduction), born in France in the 80's, has developed powerful techniques for the analysis of large body of texts. Following lexicometry and text statistical analysis, it offers tools and methods tested in multiple branches of the humanities and is statistically well founded.

The project brings together open-source Textometry software developments to set up a [modular platform called TXM](Presentation). It is both an heritage of international influence and the launch of a new generation of textometrical research, in synergy with existing corpus technologies (Unicode, XML, TEI, NLP tools, CQP, R).

The TXM platform is provided for free under an open-source license <[GNU General Public License, version 2 and more](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)> in two different forms,

## TXM Desktop Software
For the following operating systems:
{: style="margin-bottom: 5px"}

- Windows 64 bits
- Mac OS X
- Linux 64 bits

The TXM desktop software allows you to import your own corpora and analyze them on your own machine.

See the [software download page](Telecharger) for the desktop version.

## TXM Portal Software

The TXM web portal software allows access to online corpora through a web browser. It includes configurable access controls.
It installs itself on a web server in a similar way to the Wordpress blog software: articles are replaced by text corpora and navigation in articles by text analysis and exploration tools.

See a [list of public portals](https://groupes.renater.fr/wiki/txm-users/public/references_portails).

See the [software download page](Telecharger) for the portal version.

## How to Cite the TXM software and the Textometry project

**<font color="green">If you use TXM for your research, please quote one of the following two references in your publications. This is important for the sustainability of the platform development:</font><br/>**

### A. Reference in English

| {% reference heiden_serge_txm_2010-1 --file Publications de l’équipe %} |

### B. Reference in French

| {% reference heiden_serge_txm_2010 --file Publications de l’équipe %} |

## TXM in social networks

Come and dialog with us on:
{: style="margin-bottom: 5px"}

- Twitter : <https://twitter.com/txm_>
- Facebook : <https://www.facebook.com/groups/148388185204997>
- The #txm IRC channel on the irc.freenode.net server
    - or directly on [webchat](http://webchat.freenode.net?channels=txm)

## Sponsors

The development of [TXM is supported](Sponsors) by a community of partners.

## Contact us

We are always pleased to hear from users of the platform or to respond to questions about it, [our contact information is here](Nous-contacter).

*[XML]: eXtensible Markup Language
*[TEI]: Text Encoding Initiative
*[TAL]: Natural Language Processing (Traitement Automatique des Langues)
*[CQP]: Corpus Query Processor
*[IRC]: Internet Relay Chat

