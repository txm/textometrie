---
ititle: TXM Community
layout: page
lang: en
ref: communauté
---

# TXM Community
{: style="text-align: center;"}

## Users

TXM Users exchange information between them and with developers through the [txm-open *AT* lists.sourceforge.net](http://lists.sourceforge.net/mailman/listinfo/txm-open) **international mailing list**.

French speaking TXM Users exchange information between them and with developers through the [txm-users *AT* groupes.renater.fr](https://groupes.renater.fr/sympa/info/txm-users) **mailing list** and the **[TXM Users wiki](https://groupes.renater.fr/wiki/txm-users)**.

 
## Developers

TXM Developers exchange information through the [txm-info *AT* groupes.renater.fr](https://groupes.renater.fr/sympa/info/txm-info) **mailing list** and the **[TXM Developers wiki](https://groupes.renater.fr/wiki/txm-info)**.

 
## Contribute

[Contribute to the improvement of TXM](https://groupes.renater.fr/wiki/txm-users/public/contribuer#en)

 
## Public TXM portals

Several TXM portals publish corpora on line, some of which are public: <https://groupes.renater.fr/wiki/txm-users/public/references_portails>

