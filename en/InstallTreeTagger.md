---
ititle: TreeTagger installation into TXM tutorial
layout: page
lang: en
ref: install-treetagger
---

# TreeTagger installation into TXM tutorial [^1]
{: style="text-align: center; padding-bottom: 30px" .no_toc}

To be able to automatically lemmatize your corpora during the import
process into TXM, follow one of the two tutorials A or B below.

## A. TreeTagger installation into TXM 0.8.0 and versions above

Starting from TXM 0.8.0, two extensions dedicated to TreeTagger install
automatically the TreeTagger software and the French and English models:
{: style="margin-bottom: 5px"}

1.  Call the "File &gt; Add an Extension" command
2.  Select the "TreeTagger software" and the "TreeTagger models" lines to install the TreeTagger software and French and
     English models
3.  Validate the next steps
4.  After TXM has restarted, TreeTagger is ready to be used
5.  To install more language models, follow the [4th section](#step-4) of the
    TreeTagger manual installation below
6.  The End

## B. TreeTagger "manual" installation into TXM 0.7.9 and previous versions

This tutorial will guide you to:
{: style="margin-bottom: 5px"}

1.  Download the TreeTagger software and some language specific model
    files
2.  Tell the TXM platform where TreeTagger and its model files are
    installed on your machine

### B.1 Download files from the web and prepare them

While connected to the Internet:
{: style="margin-bottom: 5px"}

1.  Download the TreeTagger software archive from the TreeTagger web
    site:
    -   [Windows (32bit et 64bit)](https://www.cis.uni-muenchen.de/%7Eschmid/tools/TreeTagger/data/tree-tagger-windows-3.2.zip)
    -   [Mac OS X](https://www.cis.uni-muenchen.de/%7Eschmid/tools/TreeTagger/data/tree-tagger-MacOSX-3.2.tar.gz)
    -   [Linux 64bit](https://www.cis.uni-muenchen.de/%7Eschmid/tools/TreeTagger/data/tree-tagger-linux-3.2.tar.gz)
    -   [Linux 32bit](https://www.cis.uni-muenchen.de/%7Eschmid/tools/TreeTagger/data/tree-tagger-linux-3.2-old.tar.gz)
2.  Extract the content (bin, cmd, doc, FILES, LICENSE and README) to a
    folder named "**treetagger**" located in your applications folder [^2].
    Depending on your system, in:

    | Windows | **`C:\Programs\treetagger`**
    | Windows XP | **`C:\Program Files\treetagger`**
    | Mac OS X | **`/Applications/treetagger`**
    | Linux | **`/usr/lib/treetagger`**

    **Check**: After extraction, the treetagger folder must contain
    the following files and directories : bin, cmd, doc, FILES and
    README.

    **Note**: This way of installing TreeTagger is specific to TXM. You
    really just need to extract the contents of the TreeTagger archive.
    You don't need to follow any additionnal instructions found in any
    INSTALL.txt file that could be found in the archive.

3.  Create a "**treetagger-models**" folder in your 'TXM-0.8.1' folder [^3].
    It will contain all the language specific model files.
4.  Download from TreeTagger website a language model file (compressed file: '\*.gz')
    for each language in which you may need to tag a text:
    -   English<sub>1</sub> \[en\]: [english.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/english.par.gz) model built from Penn treebank ([Penn treebank tagset](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/Penn-Treebank-Tagset.pdf))
    -   English<sub>2</sub> \[en\]:
        [english-bnc.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/english-bnc.par.gz)
        model built from British National Corpus ([BNC
        tagset](https://www.natcorp.ox.ac.uk/docs/c5spec.html))
    -   French \[fr\]:
        [french.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/french.par.gz)
        model ([French
        tagset](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/french-tagset.html))
    -   Spoken French \[frp\]: [perceo](https://cnrtl.fr/corpus/perceo)
        to get from the CNRTL web site
    -   Old French \[fro\]:
        [fro](https://bfm.ens-lyon.fr/spip.php?article324) model
        ([tagset](https://bfm.ens-lyon.fr/IMG/pdf/Cattex2009_2.0.pdf))
    -   German \[de\]:
        [german.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/german.par.gz)
        model
        ([tagset](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/stts_guide.pdf))
    -   Italian \[it\]:
        [italian.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/italian.par.gz)
        model
        ([tagset](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/italian-tagset.txt))
    -   Spanish<sub>1</sub> \[es\]:
        [spanish.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/spanish.par.gz)
        model
        ([tagset](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/spanish-tagset.txt))
    -   Spanish<sub>2</sub> \[es\]:
        [spanish-ancora.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/spanish-ancora.par.gz)
        model built from Ancora corpus
        ([tagset](https://clic.ub.edu/corpus/webfm_send/18))
    -   Russian \[ru\]:
        [russian.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/russian.par.gz)
        model ([tagset](https://corpus.leeds.ac.uk/mocky/ru-table.tab))
    -   Latin<sub>1</sub> \[la\]:
        [latin.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/latin.par.gz)
        model built by Gabriele
        Brandolini([tagset](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/Lamap-Tagset.pdf))
    -   Latin<sub>2</sub> \[la\]:
        [latinIT.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/latinIT.par.gz)
        model built from Index Thomisticus Treebank
        ([tagset](https://itreebank.marginalia.it/doc/Tagset_IT.pdf))
    -   Greek \[el\]:
        [greek.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/greek.par.gz)
        model
        ([tagset](https://nlp.ilsp.gr/nlp/tagset_examples/tagset_en))
    -   Ancient Greek \[grc\]:
        [ancient-greek.par.gz](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/ancient-greek.par.gz)
        model
        ([tagset](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tagsetdocs.txt))
    -   Other languages: see the list of all the available language models in the '[Parameter files](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger)' section of the TreeTagger website
{: id="step-4"}
5.  Extract the downloaded model(s) archive(s) into the "treetagger-models"
    folder.  
    Under Windows, if you don't know how to extract '\*.gz' files, we
    recommend to use the [7-zip](https://www.7-zip.org) open-source
    software.
6.  Rename each model file according to the 2-letter [ISO
    639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)
    language code standard. For instance:
    -   'french.par' to 'fr.par'
    -   'english.par' to 'en.par'
    -   etc.  
    **With Windows and Mac OS X** : The default behavior of these sytems
    is to hide file extensions they think they can manage. This may
    mislead the user when he rename a file (the name displayed is
    "fr.par" but the real file name is "fr.par.bin"  
    In that case, you need to display and check the real file names in
    your Explorer/Finder:
        -   Under Windows :
            1.  Follow the official tutorial: [Show or hide file name
                extensions](https://windows.microsoft.com/en-us/windows/show-hide-file-name-extensions#show-hide-file-name-extensions=windows-7)
            2.  You can now choose the appropriate file name.
        -   Under Mac OS X :
            1.  Double click on the file icon (Ctrl-click mouse or
                double-finger tap in the trackpad)
            2.  Select the 'Get Info' menu entry
            3.  Edit the 'Name and Extension' field : delete the '.bin'
                extension.
            4.  Close the "Info" window.
    **Check**: the 'treetagger-models' folder must contain some model files like
    the 'fr.par' file of size about 18 Mo or the 'en.par' file of size
    about 14.4 Mo.

### B.2 In TXM

#### 1. Set the TreeTagger preferences

1.  Select the 'Edit / Preferences' main menu entry
2.  Go to the 'TXM / Advanced / NLP / TreeTagger' page (see figure 1)
3.  Set the 'Path to the install folder' preference to the 'treetagger'
        folder path
4.  Set the 'Path to the linguistic models folder' preference to the 'treetagger-models'
        folder path
5.  Finish with the 'OK' button to save the preferences
<figure markdown="1">
{: style="margin-top: 1px"}
![]({{"/img/preferences-treetagger-en.png" | absolute_url}} "Figure 1: TreeTagger preferences in TXM")
<figcaption style="text-align: center;">
 Figure 1: TreeTagger preferences in TXM
 </figcaption>
</figure>

####  2. Check the installation

1. Copy the following text:

        Running SearchEngine in memory mode.
        Statistical Engine launched.connected.
        Reloading subcorpora and partitions...Done.
        No update available.

2. In TXM launch the File &gt; Import &gt; Clipboard command

3. Check in the console that the last lines are:

        pAttrs : [id, lbid, enpos, enlemma]
        sAttrs : [text:+id+path+base+project, s:+n, p:+id, txmcorpus:+lang]
        -- EDITION - Building edition
        .
        Import done:3sec (3265 ms)
        Running SearchEngine in memory mode.
        Statistical Engine launched.connected.
        Reloading subcorpora and partitions...Done.
        TXM is ready.

    (Note that the first above line should contain **enpos** and
    **enlemma**. But the indication of time after "Import done" can of
    course be different.)

In case of difficulty you can find further help in the [FAQ](https://groupes.renater.fr/wiki/txm-users/public/faq#treetagger_ne_fonctionne_pas_comment_bien_regler_treetagger_pour_txm) (in French).

If you can't manage the installation process, please send your
enquiries to the TXM users mailing list (txm-users AT groupes.renater.fr)
after [subscribing](https://groupes.renater.fr/sympa/subscribe/txm-users) to the mailing list, or
<a href="mailto:{{ site.social-network-links['email'] | encode_email }}" title="{{ email }}">contact the TXM team by mail</a>.

------------------------------------------------------------------------
Notes:

[^1]: TreeTagger licence prohibits the delivery of TreeTagger embedded in a commercial software. As TXM licence doesn't prevent anyone to do business with TXM, we can not include TreeTagger in the TXM distribution. See [TreeTagger web site](https://www.cis.uni-muenchen.de/%7Eschmid/tools/TreeTagger)

[^2]: If you don't have access rights to create the folder in the applications folder, you can create it in your home folder.

[^3]: The 'TXM-0.8.1' folder is in your home directory.

