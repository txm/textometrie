---
ititle: TXM Sponsors
layout: page
lang: en
ref: sponsors
---

## TXM Development Sponsors

### Research units

- [IHRIM UMR 5317](http://ihrim.ens-lyon.fr) (Institute of History of Representations and Ideas in Modernities), CNRS, ENS de Lyon, Université Lumière Lyon 2, Université Jean Monnet Saint-Etienne, Université Jean Moulin Lyon 3, Université Clermont Auvergne.
- [ELLIADD EA 4661](http://elliadd.univ-fcomte.fr) (Edition, Litteratures, Langages, Computer Science, Arts, Didactics, Discourse), Université de Franche-Comté Besançon.
- [ICAR UMR 5191](http://icar.cnrs.fr) (Interactions, Corpus, Apprentissages, Représentations), CNRS, ENS de Lyon, Université Lyon 2  (2007-2015)

### Equipex

- [Matrice Memory](http://www.matricememory.fr/?lang=en): study of relations between personnal memory and collective memory, between psychological field and social field.

### Grants

- **ANR**
    - ANR-17-CE38-0010, Déc 2017 - Nov 2020, [ANTRACT project](https://anr.fr/Project-ANR-17-CE38-0010): Transdisciplinary Analysis of French Newsreels (1945-1969);
    - ANR-16-CE38-0010, 2017 - 2020, [Profiterole project](https://anr.fr/Project-ANR-16-CE38-0010) : PRocessing Old French Instrumented TExts for the Representation Of Language Evolution;
    - ANR-15-CE38-0008, Mar 2016 - Fév 2020, [Democrat project](https://anr.fr/Project-ANR-15-CE38-0008) : Describing and Modelling Reference Chains: Tools for Corpus Annotation (including diachronic and comparative language studies) and Automatic Processing;
    - ANR-12-CORP-0010, Fév 2013 - Déc 2016, [Oriflamms project](https://anr.fr/Project-ANR-12-CORP-0010) : Ontology Research, Image Features, Letterform Analysis on Multilingual Medieval Scripts;
    - ANR-11-IDEX-0007-02/10-LABX-0081, Mar 2013 - Fév 2015, ASLAN Labex;
    - ANR-10-EQPX-0021/10-EQPX-0021, Jan 2012 - Déc 2014, [Matrice Memory Équipex](http://www.matricememory.fr/?lang=en): Analysis Tools For Research Through International Cooperation and Experimentations;
    - ANR-07-CORP-0015, Sept 2009 - Juil 2010, CORPTEF project: development of the second version of the TXM web portal software for the online distribution of the CORPTEF corpus;
    - ANR-06-CORP-029, Jan 2007 - Déc 2010, Textométrie project: design and development of the first version of the TXM platform;
- **ANR-DFG** :
    - ANR-14-FRAL-0006, Jan 2015 - Déc 2018, [PaLaFra project](https://anr.fr/Project-ANR-14-FRAL-0006) : PAssage du LAtin au FRAnçais - building and analysis of a Latino-French corpus;
    - ANR-08-FRAL-0006, Juin - Juil 2012, projet SRCMF : TIGER Search component, syntactic annotations import & analysis;
- **DGLFLF**, 2011, projet GGHF : processing and import of the GGHF corpus into TXM;
- **Université de Lyon 3**, Jan - Mar 2011, Laboratoire EVS : XML Transcriber import module, graphical user interface to R;
- **Région Rhône-Alpes Cluster 13**, Juin - Août 2009 : development of the first version of the TXM web portal for the online distribution of the XML-TEI P5 encoded Holy Grail edition.

### Other Sponsors from the TXM developers community

- [EVS UMR 5600](http://umr5600.cnrs.fr/en/homepage) research unit, 2013 -: improvement of the 'textometry' R package;
- [GREYC UMR 6072](https://www.greyc.fr/?page_id=1342&lang=en) research unit (Caen Basse-Normandie University & CNRS) & Pôle Document Numérique of the [MRSH](http://www.unicaen.fr/recherche/mrsh/home_gb.html) center from the Caen Basse-Normandie University, with support from European Union FEDER program and from Basse-Normandie region CPER, May - Jul 2011: PUC corpora import, implementation of statistical commands into the web portal version and Glassfish hosting;
- PhD in micro-finance, 2011: Factiva and Calibre import module.

*[ANR]: French National Research Agency (Agence Nationale pour la Recherche)
*[DFG]: Deutsche Forschungsgemeinschaft
*[DGLFLF]: General Delegation for the French language and the languages of France (Délégation générale à la langue française et aux langues de France)
*[XML]: eXtensible Markup Language
*[TEI]: Text Encoding Initiative
*[TAL]: Natural Language Processing (Traitement Automatique des Langues)
*[CQP]: Corpus Query Processor
*[IRC]: Internet Relay Chat
*[Labex]: Laboratoire d'excellence
*[Équipex]: Équipement d'excellence
*[GGHF]: Great Historical Grammar of French (Grande Grammaire Historique du Français)
*[EVS]: Environnement Ville Société
*[CNRS]: French National Centre for Scientific Research (Centre National de la Recherche Scientifique)
*[EA]: Équipe d'Accueil
*[UMR]: Mixed Research Unit (Unité Mixte de Recherche)
*[MRSH]: Maison de la Recherche en Sciences Humaines
*[FEDER]: European Regional Development Fund (Fonds européen de développement régional)
*[CPER]: Contrat de Projet État-Région

