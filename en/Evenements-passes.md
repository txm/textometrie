---
ititle: Past Events
layout: page
lang: en
ref: événements-passés
---

# Past Events
{: style="text-align: center;"}

## TXM Team

- {: style="list-style: none"} &nbsp;
    - jeudi 19 - vendredi 20 novembre 2015, Novossibirsk, Russie : école thématique [El’Manuscript-2015 "Textual Heritage and Digital Technologies"](http://textualheritage.org/content/view/43/238/lang,english/), "Introduction to textual data analysis with textometry" (S. Heiden, A. Lavrentiev).
- november
    - jeudi 29 octobre 2015, Lyon : [TEI Members’ Meeting and Conference 2015]( http://tei2015.huma-num.fr), "Demonstration of the Analysis of a corpus of Akkadian Tablets with the TXM tool" (M. Béranger)
    - jeudi 29 octobre 2015, Lyon : [TEI Members’ Meeting and Conference 2015]( http://tei2015.huma-num.fr), "The TXM-compatible TEI-encoded synaptic edition of the Excerptum Roberti de Chronica Mariani Scotti" (G. Schmidt)
    - mercredi 28 octobre 2015, Lyon : [TEI Members’ Meeting and Conference 2015]( http://tei2015.huma-num.fr), "Reengineering Akkadian Tablets with TEI and TXM for linguistic Analysis" (M. Béranger, S. Heiden & A. Lavrentiev)
    - mardi 27 octobre 2015, Lyon : [TEI Members’ Meeting and Conference 2015]( http://tei2015.huma-num.fr), workshop [Introduction to the TXM Content Analysis Software](http://tei2015.huma-num.fr/en/workshops/#item5), (S. Heiden  & A. Lavrentiev)
- october
    - mardi 22 septembre 2015, Lyon : atelier TXM, ["Préparation de corpus et import dans TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (S. Heiden)
    - lundi 21 septembre 2015, Lyon : atelier TXM, ["Initiation à TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
    - mardi 1er septembre 2015, Strasbourg : [10e journées internationales Lexicologie, Terminologie, Traduction]( http://ltt2015.sciencesconf.org/), "Initiation à l’analyse de corpus de textes avec le logiciel libre TXM" (S. Heiden)
- september
    - mardi 7 juillet 2015, Strasbourg : [école d'été doctorale "Le doctorat mode(s) d'emploi"](http://ed.humanites.unistra.fr/ecole-dete-2015/presentation/) "Le doctorat mode(s) d'emploi", "Atelier TXM - Que peut-on faire avec TXM ?" (B. Pincemin)
    - jeudi 2 juillet 2015, Sydney :   [Digital Humanities Conference (DH2015)]( http://dh2015.org),  Conference "Progressive Philology with TXM: From 'Raw Text' to 'TEI-encoded Text' Analysis and Mining" (S. Heiden)
    - mercredi 1er juillet 2015, Sydney :   [Digital Humanities Conference (DH2015)]( http://dh2015.org), Poster "From KWIC Concordance to Video Excerpt or Folio Facsimile: Demonstration of Multimodal and Multimedia Corpora in TXM" (S. Heiden)
- july
    - lundi 29 juin 2015, Sydney : [Digital Humanities Conference (DH2015)]( http://dh2015.org), TXM workshop "Introduction To The TXM Content Analysis Software" (S. Heiden)
    - lundi 29 juin 2015, Lille : [Atelier de formation annuel du consortium CAHIER "Préserver, éditer et exploiter les sources de la recherche"]( http://cahier.hypotheses.org/1892#more-1892)), "L'édition des dossiers documentaires de Bouvard & Pécuchet : enrichir les transcriptions grâce à la TEI" (S. Dord-Crouslé) puis
"Analyser les transcriptions TEI et exploiter les métadonnées grâce à la plateforme TXM" (A. Lavrentiev)
    - jeudi 11 et vendredi 12 juin 2015, Lyon : [Journée d'étude et atelier TXM corpus russes]( http://www.bibliotheque-diderot.fr/txm-corpus-russes-266968.kjsp) (A. Lavrentiev et S. Heiden pour la formation TXM)
    - mardi 2 juin 2015, Lyon : atelier TXM, ["Préparation de corpus et import dans TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (S. Heiden)
    - lundi 1er juin 2015, Lyon : atelier TXM, ["Initiation à TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
- june
    - mercredi 27 mai 2015, Tours : [Atelier méthodologique "Analyse de discours, analyse de contenu, quels enjeux, quels outils ? ou comment s’y retrouver dans les techniques qualitatives informatisées en SHS"]( http://msh.univ-tours.fr/article/atelier-methodologique-analyse-de-discours-analyse-de-contenu-quels-enjeux-quels-outils-ou), "Exemples d'utilisation de TXM" - démonstration de 30 minutes (F. Badin et L. Bertrand)
    - lundi 11 et mardi 12 mai 2015, Bordeaux : [URFIST](http://weburfist.univ-bordeaux.fr/) , ["Initiation à l'analyse de corpus de textes avec le logiciel libre TXM"]( http://weburfist.univ-bordeaux.fr/initiation-a-lanalyse-de-corpus-de-textes-avec-le-logiciel-libre-txm/) (S. Heiden)
    - lundi 11 mai 2015, Paris : programme [E-Philologie ENS / EPHE ](http://ephilolog.hypotheses.org), Cours pratique "Textométrie et Plateforme TXM" (A. Lavrentiev)
- may
    - mardi 28 avril 2015, Lyon : atelier TXM, ["Préparation de corpus et import dans TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (S. Heiden)
    - lundi 27 avril 2015, Lyon : atelier TXM, ["Initiation à TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
- april
    - jeudi 12 mars 2015, Lyon : programme [DEMM "Digital Editing of Medieval Manuscripts"](http://tei.it.ox.ac.uk/Talks/2015-03-lyon/), Atelier ["Discovering the TXM platform for textual analysis"](http://goo.gl/RFRRsu) (A. Lavrentiev et S. Heiden)
    - mardi 3 mars 2015, Lyon : atelier TXM, ["Préparation de corpus et import dans TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (S. Heiden)
    - lundi 2 mars 2015, Lyon : atelier TXM, ["Initiation à TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
- march
    - vendredi 23 janvier 2015, Lyon : séminaire de linguistique allemande (contact [Emmanuelle Prak-Derrington](http://icar.univ-lyon2.fr/membres/eprakderrington/)), "Présentation de TXM" (B. Pincemin)
    - mercredi 21 janvier 2015, Lyon : atelier TXM, ["Préparation de corpus et import dans TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (S. Heiden)
    - mardi 20 janvier 2015, Lyon : atelier TXM, ["Initiation à TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
- january
## 2015
    - samedi 20 décembre 2014, Varsovie, Pologne : intervention à l'atelier Digital Humanities du [DELab](http://delab.uw.edu.pl), invitation de l'ambassade de France en Pologne, "Philologie numérique pour l'analyse textométrique du texte 'Actions and Products' de Twardowski en Polonais, Anglais et Français avec TXM" (S. Heiden)
    - mardi 9 décembre 2014, Genève : invitation par le [groupe de recherche Théorie, Action, Langage et Savoirs (TALES)](http://www.unige.ch/fapse/tales/TALES.html) de l'université de Genève, "Atelier de formation à TXM" (S. Heiden)
    - mercredi 3 décembre 2014, Lyon : atelier TXM, ["Préparation de corpus et import dans TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (S. Heiden)
    - mardi 2 décembre 2014, Lyon : atelier TXM, ["Initiation à TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
- december
    - vendredi 28 novembre 2014, Strasbourg : invitation de l'[équipe Fonctionnements discursifs et traduction (FDT)](http://lilpa.unistra.fr/fdt), "Atelier d'initiation à TXM et préparation de corpus" (S. Heiden)
    - jeudi 27 novembre 2014, Strasbourg : invitation de l'[École doctorale des Humanités](http://ed.humanites.unistra.fr) de l'Université de Strasbourg, "Atelier d'initiation à TXM et préparation de corpus" (S. Heiden)
    - jeudi 20 novembre 2014, Paris : Ateliers de formation du Consortium Corpus Ecrits du TGIR HumaNum, ["TXM avancé : comprendre et utiliser les spécificités"](http://corpusecrits.huma-num.fr/2014/10/28/ateliers-de-formation-19-20-novembre-2014/),  (B. Pincemin)
    - jeudi 20 novembre 2014, Paris : "Base de français médiéval : une ressource libre et ouverte au service de la communauté des médiévistes" (Céline Guillot-Barbance, Serge Heiden, Alexei Lavrentiev), [Colloque Crealscience](http://www.crealscience.fr/index.php?langue=fr&type=Colloque) "Les états anciens de langues à l'heure du numérique", Paris, 20-21 novembre 2014.
- november
    - vendredi 26 septembre 2014, Lyon : atelier TXM, ["Préparation de corpus et import dans TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (S. Heiden)
    - jeudi 25 septembre 2014, Lyon : atelier TXM, ["Initiation à TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
    - samedi 20 septembre 2014, Minorque : Ecole d'été [Création et utilisation de corpus de textes médiévaux](http://www.glossaria.eu/minorque/), "Analyse de corpus avec TXM" (B. Pincemin)
    - 15-20 septembre 2014, Varna, Bulgarie :  Colloque [El’Manuscript-2014 "Textual Heritage and Information Technologies"](http://textualheritage.org/content/view/486/209/lang,english/), un atelier avec TXM (A. Lavrentiev).
- september
    - lundi 30 juin - vendredi 4 juillet 2014, Lyon : [atelier Cahier "Edition analytique"](http://www.cahier.paris-sorbonne.fr/2014/05/atelier-edition-analytique/).
    - lundi 23 juin 2014, Lyon : atelier ["TXM avancé"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
    - jeudi 19 juin 2014, Paris : [groupe Cortext, Séminaire de l'axe Traces digitales](http://www.inra-ifris.org/axes-de-recherche-thematique/groupe-plateforme-cortext/seminaire-de-l-axe-traces-digitales-groupe-cortext.html), "Philologie numérique et analyse textométrique  : analyse de corpus textuels XML structurés et lemmatisés avec le logiciel open-source TXM" (S. Heiden) [reporté en raison des grèves SNCF]
    - mercredi 18 juin 2014, Lyon : Séminaire [ASLAN WP1 thinking language complexity](http://aslan.universite-lyon.fr/recherche/wp1-thinking-language-complexity-173649.kjsp), "Dimensions de complexité dans la synergie BFM / TXM pour l'analyse linguistique empirique par corpus" (S. Heiden)
    - mardi 10 juin 2014, Neuchâtel : Journée d'étude ANR Presto & Centre de linguistique française de l'université de Neuchâtel, "Présentation de TXM" (B. Pincemin)
    - vendredi 6 juin 2014, Paris : 12e Journées internationales d'Analyse statistique des Données Textuelles ([JADT 2014](http://www.aftal.fr/jadt2014/?page_id=140)), Démonstration de TXM (B. Pincemin)
    - mardi 3 juin 2014, Rome : Atelier doctoral [Histoire et informatique : textométrie des sources médiévales](https://lamop.univ-paris1.fr/spip.php?article827#.U0e81dvR6fg), "Introduction to the TXM content analysis platform" (S. Heiden)
- june
    - vendredi 23 mai 2014, Lyon : atelier TXM, ["Préparation de corpus et import dans TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (S. Heiden)
    - jeudi 22 mai 2014, Lyon : atelier TXM, ["Initiation à TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
    - lundi 12 et mardi 13 mai 2014, Orléans : "Atelier de formation à TXM" (S. Heiden)
- may
    - vendredi 11 avril 2014, Moscou : Université HSE, "Atelier Textométrie & TXM" (A. Lavrentiev)
    - mardi 8 avril 2014, Lyon : atelier TXM, ["Préparation de corpus et import dans TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (S. Heiden)
    - lundi 7 avril 2014, Lyon : atelier TXM, ["Initiation à TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
- april
    - jeudi 27 mars 2014, Paris : Atelier PALM/MEDITEXT, présentation de TXM (S. Heiden)
    - jeudi 20 mars 2014, Lyon : Labex ASLAN, laboratoire ICAR, Cellule Corpus Complexe, Atelier logiciel "Présentation du logiciel TXM" (S. Heiden & B. Pincemin)
- march
    - vendredi 28 février 2014, Lyon : atelier TXM, ["Préparation de corpus et import dans TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (S. Heiden)
    - jeudi 27 février 2014, Lyon : atelier TXM, ["Initiation à TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
    - Jeudi 20 et vendredi 21 février 2014, Université de La Manouba, Tunis, Tunisie : Atelier de formation en textométrie et analyse numérique des textes, "Initiation à TXM" et "TXM avancé : préparer et importer son corpus dans TXM" (S. Heiden)
    - mardi 18 février 2014, Paris : Séminaire pratique du master Technologies numériques appliquées à l'histoire, Ecole Nationale des Chartes, "Base de Français Médiéval, Philologie numérique et Plateforme TXM" (A. Lavrentiev)
    - jeudi 6 et vendredi 7 février 2014, Würzburg, Allemagne : DARIAH-DE Workshop "[Introduction to the TXM Content Analysis Platform](http://www.germanistik.uni-wuerzburg.de/lehrstuehle/computerphilologie/aktuelles/veranstaltungen/workshop_txm)" (S. Heiden)
    - mercredi 5 février 2014, Paris : Séminaire Recherches linguistiques et corpus, Sorbonne, "Etude de la variation dans l'histoire du français : l'apport de la Base de français médiéval" puis "Atelier Portail BFM et Logiciel TXM" (C. Guillot & Alexei Lavrentiev)
- février
    - mardi 21 janvier 2014, Lyon : atelier TXM, ["TXM avancé"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
    - jeudi 9 janvier 2014, Besançon : Laboratoire ELLIADD (contact : [Marion Bendinelli](http://elliadd.univ-fcomte.fr/fiches/bendinellimarion)), "Formation TXM" (S. Heiden)
- january
## 2014
    - mardi 10 décembre 2013, Paris : atelier "Les Nouvelles formes de communication. Adapter TXM pour travailler sur les corpus NouvCom (SMS, tchat, forum, etc.)", dans le cadre de la Formation aux outils d'exploration de corpus organisée par le GT10 de l'IR-Corpus les 9 et 10 décembre 2013 (S. Heiden)
    - lundi 9 décembre 2013, Paris : formation "La concordance pour tous (corpus brut, annoté, structuré)", dans le cadre de la Formation aux outils d'exploration de corpus organisée par le GT10 de l'IR-Corpus les 9 et 10 décembre 2013 (B. Pincemin)
- december
    - vendredi 29 novembre 2013, Lyon : atelier TXM, ["TXM avancé, spécial Diachronie du français / français médiéval"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin & A. Lavrentiev)
    - mercredi 6 novembre 2013, Lyon : atelier TXM, ["Préparation de corpus et import dans TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (S. Heiden)
    - mardi 5 novembre 2013, Lyon : atelier TXM, ["Initiation à TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
- november
    - vendredi 11 octobre 2013, Lyon : atelier TXM, ["Préparation de corpus et import dans TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (S. Heiden)
    - jeudi 10 octobre 2013, Lyon : atelier TXM, ["Initiation à TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
- october
    - mercredi 18 septembre 2013, Padoue, Italie : atelier ["LAB - Introduction to TXM"](http://www.giat.org/?page_id=11&lang=en) IQLA-GIAT Summer School in
    Quantitative Analysis of Textual Data, University of Padua, 16-20 September 2013 (S. Heiden)
- september
    - vendredi 19 juillet 2013, Lincoln - Nebraska, États-Unis : colloque [Digital Humanities 2013](http://dh2013.unl.edu), "TXM Platform for analysis of TEI encoded textual sources" (S. Heiden)
    - mardi 16 juillet 2013, Lincoln - Nebraska, États-Unis : atelier ["Introduction to the TXM content analysis platform"](http://dh2013.unl.edu/schedule-and-events/workshops/#TXM) pré-conférence [Digital Humanities 2013](http://dh2013.unl.edu) (S. Heiden)
- july
    - mardi 18 juin 2013, Lyon : colloque ["Corpus de textes : composer, mesurer, interpréter"](http://nombresetmots.ens-lyon.fr/spip.php?rubrique12) du [laboratoire junior N&M’s](http://nombresetmots.ens-lyon.fr) : "Création d'un logiciel open-source : l'exemple de TXM" (S. Heiden)
    - lundi 10 juin 2013, Lyon : atelier TXM spécial ["Initiation à Iramuteq"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (P. Ratinaud)
    - mardi 4 juin 2013, Lyon : atelier TXM, ["Préparation de corpus et import dans TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (S. Heiden)
    - mardi 4 juin 2013, Caen : journée d'étude *Que faire des corpus (une fois) numérisés ?*, "Fonctionnalités textométriques pour l'analyse littéraire : possibilités offertes par le logiciel libre TXM" (B. Pincemin)
- june
    - vendredi 31 mai 2013, Toulouse : atelier méthodologique Analyses textuelles, ["Préparation de corpus et import dans TXM"](http://sms.univ-tlse2.fr/accueil-sms/ateliers-methodologiques/analyses-textuelles/) (S. Heiden)
    - lundi 27 mai 2013, Toulouse : atelier méthodologique Analyses textuelles, ["Initiation à TXM"](http://sms.univ-tlse2.fr/accueil-sms/ateliers-methodologiques/analyses-textuelles/) (B. Pincemin)
    - vendredi 24 mai 2013, Lyon : atelier TXM, ["Initiation à TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
    - mardi 21 mai 2013, Lyon : atelier TXM, ["TXM avancé"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
- may
    - jeudi 18 avril 2013, Liège : Séminaire interdisciplinaire d'analyse de textes, ["Comprendre et utiliser les concordances. Méthodologie textométrique avec le logiciel libre TXM"](http://www.squash.ulg.ac.be/events/psyceduc_2013_6/) (B. Pincemin)
    - lundi 15 avril 2013, Paris : Atelier INA Matrice, ["Mémoire de la Shoah. 
    Parcours textométrique dans le corpus *Témoignages*"](http://matricememory.fr/matrice/dossiertelechargements/matrice%20ateliers%20Ina%202012_2013.pdf) (D. Mayaffre, B. Pincemin)
    - vendredi 5 avril 2013, Lyon : atelier TXM, ["Préparation de corpus et import dans TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (S. Heiden)
    - jeudi 4 avril 2013, Lyon : atelier TXM, ["Initiation à TXM"](https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm) (B. Pincemin)
- april
## 2013

## Summer Schools

- MISAT 2011 : [école thématique CNRS « Méthodes Informatiques et Statistiques en Analyse de Textes » (MISAT)](http://laseldi.univ-fcomte.fr/ecole/PAGES/2011/index.php),  Besançon – Centre Sainte-Anne, 18-24 Juin 2011. 
- MISAT 2010 : [école thématique CNRS « Méthodes Informatiques et Statistiques en Analyse de Textes » (MISAT)](http://laseldi.univ-fcomte.fr/ecole/PAGES/2010/index.php), Fréjus – Villa Clythia, 18-24 Juin 2010.
- MISAT 2009 : [école thématique CNRS « Méthodes Informatiques et Statistiques en Analyse de Textes » (MISAT)](http://laseldi.univ-fcomte.fr/ecole/PAGES/2009/index.php?page=programme),  Besançon – Centre Sainte-Anne, 14-19 Juin 2009. 

## International Conferences

- JADT 2012 : [11ièmes journées internationales d’analyse statistique des données textuelles](http://www.jadt2012.ulg.ac.be), Liège, 13-15 juin 2012.
- JADT 2010 : [10ièmes journées internationales d’analyse statistique des données textuelles](http://jadt2010.uniroma1.it/fr), Rome, 9-11 juin 2010.
- JADT 2008 : [9ièmes journées internationales d’analyse statistique des données textuelles](http://jadt2008.ens-lsh.fr/?lang=en), Lyon, 12-14 mars 2008. Actes en ligne à [Lexicometrica](http://www.cavi.univ-paris3.fr/lexicometrica/jadt/jadt2008/tocJADT2008.htm).
            
## Other manifestations

- [Documents, textes et oeuvres : autour de François Rastier](http://www.ccic-cerisy.asso.fr/rastier12.html), Cerisy-la-Salle, du 6 au 9 juillet 2012

