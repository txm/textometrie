---
ititle: TXM Reference articles
layout: page
lang: en
ref: références
---

# Works using TXM

> List in progress

[Note: if you use TXM yourself, please send us your bibliographic references - if possible in BibTeX format - at our [contact address]({{"en/Nous-contacter" | relative_url}})]

{% bibliography --file Travaux utilisant TXM %}

