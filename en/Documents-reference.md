---
ititle: TXM Reference Documents
layout: page
categories: txm
lang: en
ref: documents-référence
---

## General Monographs on Methodology

Lebart L., Salem A., Berry L. (1998) - Exploring Textual Data. Kluwer academic pub., Boston, 222 p.<br/>
[english translation and adaptation of « Statistique Textuelle » – [consult](http://books.google.com/books?id=24nvLSkVcJsC&printsec=frontcover&hl=fr&cd=1&source=gbs_ViewAPI) – [borrow](http://www.sudoc.abes.fr/DB=2.1/LNG=EN/SRCH?IKT=12&TRM=049397265)]

Lebart L., Salem A. (1994) - Statistique Textuelle. Dunod, Paris, 342 p.<br/>
[[consult](http://books.google.com/books?id=_kNrGwAACAAJ&hl=fr&cd=1&source=gbs_ViewAPI) – [borrow](http://www.sudoc.abes.fr/DB=2.1/LNG=EN/SRCH?IKT=12&TRM=003289990)] [^1] – [author versions available online: [at ENST](http://ses.telecom-paristech.fr/lebart/ST.html), [at Paris 3](http://www.cavi.univ-paris3.fr/lexicometrica/livre/st94/st94-tdm.html) ]  

## Reference Article for each Text Analysis Model

Guilbaud G-Th. (1980) - Zipf et les fréquences, Mots N° 1, p. 97-126<br/>
[zipf law –[online access on the Persée portal](http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1980_num_1_1_1007)]

Lafon P. (1980) - Sur la variabilité de la fréquence des formes dans un corpus, Mots N°1 , p. 127-165<br/>
[specificity model – [online access on the Persée portal](http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1980_num_1_1_1008)]

Tournier M. (1980) - D’où viennent les fréquences de vocabulaire ?, Mots N°1, p 189-212.<br/>
[lexicon, usages, vocabulary and probabilities – [online access on the Persée portal](http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1980_num_1_1_1010)] [^2]

Lafon P. (1981) - Statistiques des localisations des formes d’un texte, Mots, N° 2, mars 1981, p. 157-188<br/>
[bursts model – [online access on the Persée portal](http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1981_num_2_1_1026)] [^2]

Lafon P. (1981) - Analyse lexicométrique et recherche des cooccurrences, Mots N°3 , p. 95-148<br/>
[cooccurrency model – [online access on the Persée portal](http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1981_num_3_1_1041)] [^2]

Lafon P., Salem A. (1983) - L’inventaire des segments répétés d’un texte, Mots N°6, p. 161-177<br/>
[repeated segments model – [online access on the Persée portal](http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1983_num_6_1_1101)] [^2]

Salem A. (1988) - Approches du temps lexical. Statistique textuelle et séries chronologiques, Mots, 17, octobre 1988, p. 105-143<br/>
[chronological series model – [online access on the Persée portal](http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1988_num_17_1_1401)] [^2]
  
## JADT Conference proceedings

Electronic edition of the [JADT Conference proceedings](http://lexicometrica.univ-paris3.fr/jadt/) available on the Lexicometrica website.

## Other References

Brunet, Etienne (1986) - Actes du Colloque international CNRS "Méthodes quantitatives et informatiques dans l’étude des textes" (en hommage à Charles Muller), Genève, Editions Slatkine, XIV- 948 pages

Brunet, Etienne (2006) - "Le corpus conçu comme une boule",
Corpus en Lettres et Sciences sociales : des documents numériques à l’interprétation, Actes du XVIIe Colloque d’Albi Langages et Signification, Albi, 10-14 juillet 2006, Carine Duteil-Mougel & Baptiste Foulquié (éds), ISBN 2-907955-12-18, pp. 69-78<br/>[[online access in Texto!](http://www.revue-texto.net/1996-2007/Parutions/Livres-E/Albi-2006/Brunet.pdf), ISSN 1773-0120, juin 2006, vol. XI, n°2]

Brunet, Etienne (2009) - Comptes d’auteurs. Etudes statistiques de Rabelais à Gracq, Textes édités par Damon Mayaffre, préface d’Henri Béhar, Paris, Champion, 2009, 396 pages, ISBN 274532019X (reedition of selected papers)

Brunet, Etienne (2011) - Ce qui compte. Ecrits choisis, Textes édités par Céline Poudat, préface de Ludovic Lebart, Paris, Champion, 2011, 396 pages, ISBN 2745322257 (reedition of selected papers)

Fénelon J.P. (1981) - Qu’est-ce-que l’analyse des données ?, Lefonen, Paris. Réédité en 1999 par SEISAM, Paris<br/>
[teaching aid – [consult](http://books.google.com/books?id=SBpwGQAACAAJ) – [borrow (first edition)](http://www.sudoc.abes.fr/DB=2.1/LNG=EN/SRCH?IKT=12&TRM=022282033)  ([2nd edition](http://www.sudoc.abes.fr/DB=2.1/LNG=EN/SRCH?IKT=12&TRM=053617657))]

Geffroy Annie, Lafon Pierre, Tournier Maurice (1974) - L’indexation minimale, Plaidoyer pour une non-lemmatisation, Colloque sur l’analyse des corpus linguistiques : "Problèmes et méthodes de l’indexation minimale", Strasbourg 21-23 mai 1973

Husson François, Lê Sébastien & Pagès Jérôme (2009) - Analyse de données avec R, Presse Universitaires de Rennes (ISBN 978-2-7535-0938-2, 15 euros).

Lafon Pierre (1984) - Dépouillements et statistiques en lexicométrie, Paris : Slatkine-Champion. Avec la préface "De la lemmatisation" de Charles Muller, pp.I-XII

Luong Xuan (éd.) (2003) - La distance intertextuelle, Corpus, 2<br/>
[[online access on the Revues.org portal](https://journals.openedition.org/corpus/52?lang=en)]

Muller Charles (1973) - Initiation aux méthodes de la statistique linguistique, Champion, coll. Unichamp, 1992 (reprint of 1973 Hachette edition)

Muller C. (1977) - Principes et méthodes de statistique lexicale, Hachette, Paris

Tournier Maurice (1985a) - « Sur quoi pouvons-nous compter ? Réponse à Charles Muller », Verbum, hommage à Hélène Nais, pp. 481-492

Viprey J.-M. (2002) - Analyses textuelles et hypertextuelles des Fleurs du mal, Champion, Paris

Volle M. (1980) - Analyse des données, Economica, Paris

## User's Manual of Software from ANR Textometrie project participants

-  [DTM](http://www.dtmvic.com/06_TutorialF.html)
-  [Hyperbase](ftp://ancilla.unice.fr/manuel.pdf)
-  [Lexico 3](http://lexi-co.com/ressources/manuel-3.41.pdf)
-  [SATO](http://www.ling.uqam.ca/sato/satoman-fr.html)
-  [Trameur](http://tal.univ-paris3.fr/trameur/leMetierLexicometrique.pdf)
-  [Weblex](http://textometrie.ens-lyon.fr/IMG/pdf/weblex.pdf)
-  [Xaira](http://projects.oucs.ox.ac.uk/xaira/Doc/oldrefman.xml?ID=body.1_div.2)
  
## Not so old PhD Thesis

Beaudouin Valérie (2002) – Mètre et rythmes du vers classique. Corneille et Racine, Paris : Champion, coll. « Lettres numériques »

Bourion E. (2001). [L’aide à l’interprétation des textes électroniques](http://www.revue-texto.net/1996-2007/Corpus/Publications/Bourion/Bourion_Aide.html). Thèse de doctorat, Sciences du langage, Université de Nancy II

Martinez William (2003) - Contribution à une méthodologie de l’analyse des cooccurrences lexicales multiples dans les corpus textuels, Thèse de troisième cycle, Université de Paris 3 Sorbonne Nouvelle, décembre 2003

Zimina-Poirot Maria (2004) - [Approches quantitatives de l’extraction de ressources traductionnelles à partir de corpus parallèles](http://www.cavi.univ-paris3.fr/ilpga/ed/student/stmz/ED268-PagePersoMZ_fichiers/stmz/page8.htm), Thèse pour le Doctorat en Sciences du langage, Université de la Sorbonne nouvelle - Paris 3, soutenue le 26 novembre 2004, 328 p.


## Notes

[^1]: Click on "Where to find this document ?" to get the list of libraries where to borrow the reference in France.
[^2]: Not implemented in TXM as of 30 Avril 2010.

