---
ititle: Communauté TXM
layout: page
categories: txm
lang: fr
ref: communauté
---

# Communauté TXM
{: style="text-align: center;"}

## Utilisateurs

Les utilisateurs francophones de TXM dialoguent entre eux et avec les développeurs de la plateforme avec la **liste de diffusion des utilisateurs** - [txm-users *À* groupes.renater.fr](https://groupes.renater.fr/sympa/info/txm-users) - et par le biais du **[wiki des utilisateurs](https://groupes.renater.fr/wiki/txm-users)**.

Les utilisateurs anglophones de TXM dialoguent entre eux et avec les développeurs de la plateforme avec la **liste de diffusion des utilisateurs internationale** - [txm-open *À* lists.sourceforge.net](http://lists.sourceforge.net/mailman/listinfo/txm-open).

## Développeurs

Les développeurs de la plateforme TXM dialoguent entre eux et avec les développeurs de la plateforme avec la **liste de diffusion des développeurs** - [txm-info *À* groupes.renater.fr](https://groupes.renater.fr/sympa/info/txm-info) - et par le biais du **[wiki des développeurs](https://groupes.renater.fr/wiki/txm-info)**.

## Contribuer

[Pour participer à l'amélioration de TXM](https://groupes.renater.fr/wiki/txm-users/public/contribuer)

## Portails TXM publics

Un certain nombre de portails TXM donnent accès à leurs corpus en ligne, dont certains sont public. En voici une liste : <https://groupes.renater.fr/wiki/txm-users/public/references_portails>

