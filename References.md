---
ititle: Références
layout: page
lang: fr
ref: références
---

# Travaux utilisant TXM

> Liste en cours d'établissement

[Note : si vous utilisez TXM vous-même, envoyez-nous vos références bibliographiques - si possible au format BibTeX - à notre [adresse de contact]({{"Nous-contacter" | relative_url}})]

{% bibliography --file Travaux utilisant TXM %}

