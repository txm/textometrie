---
layout: page
lang: fr
ref: index
---

<!--
<div style="position: -webkit-sticky; position: -moz-sticky; position: -ms-sticky; position: -o-sticky; position: sticky; top: 70px; float: right;">
-->
<div style="position: fixed; top: 100px; right: 8px;" class="rapid-access">
<table border="0" width="280" CELLPADDING="3px" BGCOLOR="#F2F2F2" style="margin-left:auto;margin-right:auto;">
	<tr>
		<td width="280" BGCOLOR="#F2F2F2">
			<font color="green"
				style="font-family:arial, sans-serif; font-size:12pt;">
				Accès rapides
			</font>
		</td>
	</tr>
	<tr VALIGN="TOP">
		<td width="280" VALIGN="TOP" BGCOLOR="#F2F2F2">
			<font style="font-family:arial, sans-serif; font-size:12pt; color:#2582A4;">
				<a href="https://groupes.renater.fr/wiki/txm-users/index"
					target="blank">Wiki des utilisateurs de TXM</a>
				<br />
				<a href="https://groupes.renater.fr/sympa/info/txm-users/"
					target="blank">Liste txm-users</a>
				<br />
				<a href="files">Bibliothèque
					de fichiers pour TXM
				</a>
				<br />
				<a href="https://groupes.renater.fr/wiki/txm-info" target="blank">Wiki des
					développeurs de TXM
				</a>
				<br />
				<a href="https://lists.sourceforge.net/lists/listinfo/txm-open/"
					target="blank">Liste anglophone txm-open</a>
			</font>
		</td>
	</tr>
</table>

<table border="0" width="280" CELLPADDING="3px" BGCOLOR="#F2F2F2" style="margin-left:auto;margin-right:auto;">
	<tr>
		<td width="270" VALIGN="TOP" BGCOLOR="#F2F2F2">
			<font
				style="font-family:arial, sans-serif; font-size:12pt; color:#FE642E">   &nbsp;&nbsp;Actualités
			</font>
		</td>
	</tr>
	<tr VALIGN="TOP">
		<td width="270" VALIGN="TOP"
			style="padding-left:10px;padding-right:20px;padding-top:10px" BGCOLOR="#F2F2F2">
			<marquee direction="up" scrollamount="2" HEIGHT="80px">
					<font color="red" style="font-family:arial, sans-serif; font-size:12pt;">
							La dernière version de TXM (0.8.4) est sortie le 12 février 2025.
							<br />
							<br />
					</font>
					<font color="red" style="font-family:arial, sans-serif; font-size:12pt;">
							La dernière version du logiciel portail TXM (0.6.3.2) est sortie le 13 février 2025.
							<br />
							<br />
					</font>
			</marquee>
		</td>
	</tr>
</table>
</div>

## Télécharger la dernière version de TXM
{: style="padding-top: 30px"}

| [![Télécharger TXM]({{"/img/download_100.png" | absolute_url}})]({{"/files/software/TXM/0.8.4" | absolute_url}}) | <font color="red">La version 0.8.4 du logiciel TXM pour poste a été livrée le 12 février 2025<br/>La version 0.6.3.2 du logiciel portail TXM a été livrée le 13 février 2025</font> |

- [Autres versions](Telecharger)

### Accéder au site de démonstration du logiciel portail TXM hébergé à Huma-num

- <https://txm-demo.huma-num.fr>

### Accéder à la page des Ateliers de formation TXM

- <https://groupes.renater.fr/wiki/txm-users/public/ateliers_txm>

## Le projet en quelques lignes

La [textométrie](Introduction), née en France dans les années 80, a développé des techniques puissantes et originales pour l'analyse de grands corpus de textes. Reprenant les acquis de la lexicométrie et de la statistique textuelle, elle propose des outils et des méthodes éprouvés dans de multiples branches des SHS et statistiquement solidement fondés.

Le projet Textométrie fédère les développements logiciels open-source du domaine pour mettre en place une [plateforme modulaire appelée TXM](Presentation). Il s'agit à la fois d'une opération patrimoniale au rayonnement international et du lancement d'une nouvelle génération de recherche textométrique, en synergie avec les technologies de corpus actuelles (Unicode, XML, TEI, outils de TAL, CQP, R).

La plateforme TXM est diffusée gratuitement sous licence open-source <[Licence publique générale GNU, version 2 et plus](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)> par le biais de deux applications différentes,

## Le logiciel TXM pour poste de travail

Pour les systèmes d'exploitation :
{: style="margin-bottom: 5px"}

- Windows 64 bits ;
- Mac OS X ;
- Linux 64 bits.

La logiciel TXM pour poste vous permet d'importer vos propres corpus et de les analyser sur votre propre machine.

Voir la [page de téléchargement du logiciel](Telecharger) pour la version pour poste.

## Le logiciel portail web TXM pour serveur

Le logiciel portail web de TXM permet de donner accès à des corpus en ligne par le biais de simples navigateurs web sans avoir à installer TXM sur sa propre machine. Il permet des contrôles d'accès paramétrables par comptes utilisateurs.
Il s'installe sur un serveur web de façon analogue au logiciel de blog Wordpress: les articles étant remplacés par des corpus de textes et la navigation dans les articles par des outils d'exploration et d'analyse textométriques.

Voir des [exemples de portails publics en ligne](https://groupes.renater.fr/wiki/txm-users/public/references_portails).

Voir la [page de téléchargement du logiciel](Telecharger) pour la version portail web.

## Citer le logiciel TXM et le projet Textométrie

**<font color="green">Si vous utilisez TXM pour vos travaux de recherche, merci de citer l'une des deux références suivantes dans vos publications. Ceci est important pour la pérennisation du développement de la plateforme :</font><br/>**

### A. Référence en Français

| {% reference heiden_serge_txm_2010 --file Publications de l’équipe %} |

### B. Référence en Anglais

| {% reference heiden_serge_txm_2010-1 --file Publications de l’équipe %} |

## TXM dans les réseaux sociaux

Venez dialoguer sur :
{: style="margin-bottom: 5px"}

- Twitter : <https://twitter.com/txm_>
- Facebook : <https://www.facebook.com/groups/148388185204997>
- Le canal IRC #txm sur le serveur irc.freenode.net
    - ou directement sur [webchat](http://webchat.freenode.net?channels=txm)

## Sponsors

Le développement de [TXM est soutenu](Sponsors) par une communauté de partenaires.

## Nous contacter

Nous sommes toujours ravis de recevoir des nouvelles de l'utilisation de la plateforme ou de répondre à des questions la concernant, [nos coordonnées sont ici](Nous-contacter).

*[XML]: eXtensible Markup Language
*[TEI]: Text Encoding Initiative
*[TAL]: Traitement Automatique des Langues
*[CQP]: Corpus Query Processor
*[IRC]: Internet Relay Chat

