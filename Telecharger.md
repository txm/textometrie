---
ititle: Télécharger TXM
layout: page
lang: fr
ref: télécharger
---

### Télécharger la dernière version du logiciel TXM pour poste<br/>(Windows, Mac OS X, Linux)
{: style="text-align: center;"}

{:refdef: style="text-align: center;"}
[![Télécharger TXM]({{"/img/download_100.png" | absolute_url}})]({{"/files/software/TXM/0.8.3" | absolute_url}})
{: refdef}

### Toutes les versions du logiciel TXM disponibles en téléchargement

- [TXM pour poste]({{"/files/software/TXM" | absolute_url}}) : Le logiciel TXM pour poste vous permet d’importer vos propres corpus et de les analyser sur votre propre machine.
- [portail web TXM]({{"/files/software/TXM portal" | absolute_url}}) : Le logiciel portail web de TXM permet de donner accès à des corpus en ligne par le biais de simples navigateurs web sans avoir à installer TXM sur sa propre machine. Il s’installe sur un serveur web.

### Tutoriels d'installation de la version pour poste

- [Installer TXM sur son ordinateur personnel](https://groupes.renater.fr/wiki/txm-users/public/preparation_ordinateur)
- [Installer TXM dans une salle de cours](https://groupes.renater.fr/wiki/txm-users/public/installer_txm_dans_une_salle_de_cours)

### Tutoriels d'installation de la version portail web

- [installer le portail TXM sous Linux](https://groupes.renater.fr/wiki/txm-info/public/install_a_txm_portal)

### Télécharger des Corpus exemple

- [Bibliothèque de corpus exemples pour TXM]({{"/files/corpora/en" | absolute_url}})

## Citer TXM

**<font color="green">Si vous utilisez TXM pour vos travaux de recherche, merci de citer l'une des deux références suivantes dans vos publications. Ceci est important pour la pérennisation du développement de la plateforme :</font>**

### A. Référence en Français

| {% reference heiden_serge_txm_2010 --file Publications de l’équipe %} |

Bibtex[^1]:
~~~
{% raw %}@inproceedings{heiden:halshs-00549779,
  TITLE = {{TXM : Une plateforme logicielle open-source pour la textom{'e}trie - conception et d{'e}veloppement}},
  AUTHOR = {Heiden, Serge and Magu{'e}, Jean-Philippe and Pincemin, B{'e}n{'e}dicte},
  URL = {https://halshs.archives-ouvertes.fr/halshs-00549779},
  BOOKTITLE = {{10th International Conference on the Statistical Analysis of Textual Data - JADT 2010}},
  ADDRESS = {Rome, Italy},
  EDITOR = {Sergio Bolasco, Isabella Chiari, Luca Giuliano},
  PUBLISHER = {{Edizioni Universitarie di Lettere Economia Diritto}},
  VOLUME = {2},
  NUMBER = {3},
  PAGES = {1021-1032},
  YEAR = {2010},
  MONTH = Jun,
  KEYWORDS = {textometry ; full text search engine ; statistical analysis ; natural language processing ; grails framework ; textom{'e}trie ; open-source ; moteur de recherche plein texte ; analyse statistique ; xml-tei ; traitement automatique de la langue ; tal ; eclipse rcp ; framework grails},
  PDF = {https://halshs.archives-ouvertes.fr/halshs-00549779/file/Heiden_al_jadt2010.pdf},
  HAL_ID = {halshs-00549779},
  HAL_VERSION = {v1},
}{% endraw %}
~~~

### B. Référence en Anglais

| {% reference heiden_serge_txm_2010-1 --file Publications de l’équipe %} |

Bibtex: 
~~~
{% raw %}@inproceedings{heiden:halshs-00549764,
  TITLE = {{The TXM Platform: Building Open-Source Textual Analysis Software Compatible with the TEI Encoding Scheme}},
  AUTHOR = {Heiden, Serge},
  URL = {https://halshs.archives-ouvertes.fr/halshs-00549764},
  BOOKTITLE = {{24th Pacific Asia Conference on Language, Information and Computation}},
  ADDRESS = {Sendai, Japan},
  EDITOR = {Ryo Otoguro, Kiyoshi Ishikawa, Hiroshi Umemoto, Kei Yoshimoto and Yasunari Harada},
  PUBLISHER = {{Institute for Digital Enhancement of Cognitive Development, Waseda University}},
  PAGES = {389-398},
  YEAR = {2010},
  MONTH = Nov,
  KEYWORDS = {xml-tei corpora ; search engine ; statistical analysis ; textometry ; open-source},
  PDF = {https://halshs.archives-ouvertes.fr/halshs-00549764/file/paclic24_sheiden.pdf},
  HAL_ID = {halshs-00549764},
  HAL_VERSION = {v1},
}{% endraw %}
~~~

Notes :

[^1]: pour l'import dans Zotero.

