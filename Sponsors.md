---
ititle: Soutiens au développement de TXM
layout: page
lang: fr
ref: sponsors
---

## Soutiens au développement de TXM

### Laboratoires

- [IHRIM UMR 5317](http://ihrim.ens-lyon.fr) (Institut d’Histoire des Représentations et des Idées dans les Modernités), CNRS, ENS de Lyon, Université Lumière Lyon 2, Université Jean Monnet Saint-Etienne, Université Jean Moulin Lyon 3, Université Clermont Auvergne (2016- )
- [Laboratoire ELLIADD EA 4661](http://elliadd.univ-fcomte.fr) (Édition, Littératures, Langages, Informatique, Arts, Didactique, Discours), Université de Franche-Comté Besançon  (2013- )
- [Laboratoire ICAR UMR 5191](http://icar.cnrs.fr) (Interactions, Corpus, Apprentissages, Représentations), CNRS, ENS de Lyon, Université Lyon 2  (2007-2015)

### Équipex

- [Matrice Memory](http://www.matricememory.fr): étudier les rapports entre mémoire individuelle et mémoire collective, entre le psychique et le social.

### Contrats

- **ANR**
    - ANR-17-CE38-0010, Déc 2017 - Nov 2020, [projet ANTRACT](https://anr.fr/Project-ANR-17-CE38-0010) : Analyse Transdisciplinaire des Actualités filmées (1945-1969) ;
    - ANR-16-CE38-0010, 2017 - 2020, [projet Profiterole](https://anr.fr/Project-ANR-16-CE38-0010) : PRocessing Old French Instrumented TExts for the Representation Of Language Evolution ;
    - ANR-15-CE38-0008, Mar 2016 - Fév 2020, [projet Democrat](https://anr.fr/Project-ANR-15-CE38-0008) : DEscription et MOdélisation des Chaînes de Référence : outils pour l’Annotation de corpus (en diachronie et en langues comparées) et le Traitement automatique ;
    - ANR-12-CORP-0010, Fév 2013 - Déc 2016, [projet Oriflamms](https://anr.fr/Project-ANR-12-CORP-0010) : Recherche en ontologie, Descripteurs d'images, Analyse des formes et lettres des écritures médiévales multilingues ;
    - ANR-11-IDEX-0007-02/10-LABX-0081, Mar 2013 - Fév 2015, Labex ASLAN ;
    - ANR-10-EQPX-0021/10-EQPX-0021, Jan 2012 - Déc 2014, [Équipex Matrice](http://www.matricememory.fr/?lang=en) : développement de TXM dans une infrastructure pour les historiens du contemporain ;
    - ANR-07-CORP-0015, Sept 2009 - Juil 2010, projet CORPTEF : développement de la deuxième version du portail pour la diffusion du corpus CORPTEF ;
    - ANR-06-CORP-029, Jan 2007 - Déc 2010, projet Textométrie : conception et développement de la première version de la plateforme TXM ;
- **ANR-DFG** :
    - ANR-14-FRAL-0006, Jan 2015 - Déc 2018, [projet PaLaFra](https://anr.fr/Project-ANR-14-FRAL-0006) : PAssage du LAtin au FRAnçais - constitution et analyse d’un corpus numérique latino-français ;
    - ANR-08-FRAL-0006, Juin - Juil 2012, projet SRCMF : module Tiger Search, import & concordances syntaxiques ;
- **DGLFLF**, 2011, projet GGHF : traitement et import du corpus GGHF dans TXM ;
- **Université de Lyon 3**, Jan - Mar 2011, Laboratoire EVS : module d'import XML-Transcriber, interface graphique utilisateur à R ;
- **Région Rhône-Alpes Cluster 13**, Juin - Août 2009 : développement d'une première version du portail web pour la mise en ligne de la Queste del Saint Graal encodée en XML-TEI P5.

### Autres soutiens de la communauté de développeurs TXM

- Laboratoire [EVS UMR 5600](http://umr5600.cnrs.fr/fr/accueil), 2013 - : participation au chantier Statistiques - amélioration du package R 'textometry' ;
- Laboratoire [GREYC UMR 6072](https://www.greyc.fr) (Université de Caen Basse-Normandie & CNRS) & Pôle Document Numérique de la [MRSH](http://www.unicaen.fr/recherche/mrsh) de l'Université de Caen Basse-Normandie, avec le soutien du fond FEDER de l'Union Européenne, du CPER de la région Basse-Normandie, Mai - Juil 2011 : participation au chantier Portail - implémentation des commandes statistiques dans la version portail et hébergement dans Glassfish ;
- Thèse de micro-finance, 2011 : module d'import Factiva et Calibre.

*[ANR]: Agence Nationale pour la Recherche
*[DFG]: Deutsche Forschungsgemeinschaft
*[DGLFLF]: Délégation générale à la langue française et aux langues de France
*[XML]: eXtensible Markup Language
*[TEI]: Text Encoding Initiative
*[TAL]: Traitement Automatique des Langues
*[CQP]: Corpus Query Processor
*[IRC]: Internet Relay Chat
*[Labex]: Laboratoire d'excellence
*[Équipex]: Équipement d'excellence
*[GGHF]: Grande Grammaire Historique du Français
*[EVS]: Environnement Ville Société
*[CNRS]: Centre National de la Recherche Scientifique
*[EA]: Équipe d'Accueil
*[UMR]: Unité Mixte de Recherche
*[MRSH]: Maison de la Recherche en Sciences Humaines
*[FEDER]: European Regional Development Fund
*[CPER]: Contrat de Projet État-Région

