---
layout: default
title:  "Accès rapides"
categories: liens
---

## Accès rapides

- [Wiki des utilisateurs de TXM](https://groupes.renater.fr/wiki/txm-users/index)
- [Liste txm-users](https://groupes.renater.fr/sympa//info/txm-users/)
- [Ressources pour TXM](https://gitlab.huma-num.fr/txm/txm-ressources)
- [Wiki des développeurs de TXM](https://groupes.renater.fr/wiki/txm-info)
- [Liste anglophone txm-open](https://lists.sourceforge.net/lists/listinfo/txm-open)

