---
ititle: Javadocs TXM
layout: page
lang: fr
ref: javadocs
---

# Javadocs TXM
{: style="text-align: center; padding-bottom: 30px" .no_toc}

- [latest](https://txm.gitpages.huma-num.fr/txm-javadoc/Latest%20from%20sources)
- [0.8.2](https://txm.gitpages.huma-num.fr/txm-javadoc/0.8.2)
- [0.8.1](https://txm.gitpages.huma-num.fr/txm-javadoc/0.8.1)
- [0.8.0](http://textometrie.ens-lyon.fr/html/javadoc/0.8.0)
- 0.7.9
  - [Toolbox](http://textometrie.ens-lyon.fr/html/javadoc/TXM/TBX)
  - [RCP UI](http://textometrie.ens-lyon.fr/html/javadoc/TXM/RCP)

