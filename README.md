# Textométrie.org

Rédaction du site web [textometrie.org](https://txm.gitpages.huma-num.fr/textometrie) ([https://txm.gitpages.huma-num.fr/textometrie](https://txm.gitpages.huma-num.fr/textometrie))

## Using Jekyll locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install](https://jekyllrb.com/docs/installation) Jekyll
1. Download dependencies: `bundle`
1. Build and preview: `bundle exec jekyll serve`
1. Add content

The above commands should be executed from the root directory of this project.

## Documentation

- [Jekyll](http://jekyllrb.com)
  - [install](https://jekyllrb.com/docs/installation)
  - [documentation](https://jekyllrb.com/docs/home)
- [gitlab pages](https://docs.gitlab.com/ce/user/project/pages/introduction.html)
- [gitlab ci](https://about.gitlab.com/gitlab-ci)

