---
ititle: Présentation TXM
layout: page
lang: fr
ref: présentation
---

# Qu’est-ce que TXM ?
{: .no_toc}

La plateforme TXM combine des techniques puissantes et originales pour l’analyse de corpus de textes structurés et annotés au moyen de composants modulaires et open-source (Heiden, 2010; Heiden et al., 2010; Pincemin et al., 2010). Elle a été initiée par le projet ANR Textométrie[^1] qui a lancé une nouvelle génération de recherches [textométriques]({{"/Introduction" | absolute_url}}), en synergie avec les technologies de corpus et de statistique actuelles (Unicode, XML, TEI, TAL, CQP et R).

Elle aide les utilisateurs à construire et à analyser tout type de corpus textuel numérique éventuellement étiqueté et structuré en XML. Elle est diffusée sous forme d'un logiciel pour poste Windows, Mac ou Linux appelé "TXM" (basé sur la technologie [Eclipse RCP](http://wiki.eclipse.org/index.php/Rich_Client_Platform)) et sous la forme d’un logiciel portail web (basé sur la technologie [GWT](http://www.gwtproject.org)) pour les accès aux corpus en ligne.
 
Elle est couramment utilisée par des projets de recherche de différentes disciplines de lettres, sciences humaines et sociales comme l'histoire, la littérature, la géographie, la linguistique, la sociologie et les sciences politiques. Les publications scientifiques de textométrie sont présentées pendant la conférence internationale des [Journées d'Analyse statistique des Données Textuelles](http://jadt.org/jadt) (JADT), voir aussi (Heiden and Pincemin, 2008).[^6]

## Sommaire
{: .no_toc}

1. TOC
{:toc}

## Points forts de TXM

### Utilisable progressivement du débutant à l'expert
{: .no_toc}

Le débutant profite d'une interface intuitive qui comprend tous les éléments modernes se trouvant dans les applications de bureau les plus courantes (traitement de texte, tableur, messagerie...) :
{: style="margin-bottom: 5px"}
- multi-fenêtrage
- déclenchement d'actions par menu général, barre d'outils, menu contextuel ou liens hypertextes
- organisation hiérarchique des objets manipulés (corpus, sous-corpus, partitions) et des résultats (concordances, index, afc...)
- affichage des graphiques vectoriels : quel que soit le niveau de zoom, les informations sont affichées avec la meilleure définition
- sauvegarde de la disposition des fenêtres entre sessions de travail

L'expert peut utiliser les raccourcis clavier et surtout les possibilités de pilotage par scripts écrits dans des langages de programmation dynamique :
{: style="margin-bottom: 5px"}
- en langage Groovy pour accéder à toute la plateforme TXM (développée en Java)
- en langage R pour accéder à tous les packages R installés et aux objets créés par TXM (corpus, listes de mots...)

Les scripts permettent de reproduire automatiquement des sessions de travail complexes (pour lesquelles les manipulations dans l'interface sont contraignantes) et répétitives (les scripts sont très profitables quand on doit répéter un calcul en ne changeant que quelques paramètres ou quand un calcul dépend du résultat d'un autre). On peut les transmettre à des collègues pour qu'ils puissent à leur tour les exécuter eux-mêmes pour obtenir des résultats ou pour comprendre et vérifier leur fonctionnement. Enfin, ils permettent d'étendre la plateforme TXM elle-même (sous la forme de macros).

### Combine des outils d'analyse de texte simples et accessibles à des outils plus évolués
{: .no_toc}

TXM peut s'utiliser pour produire de simples listes de mots (le vocabulaire d'un texte) ou comme un simple concordancier (la liste des apparitions d'un mot donné en contexte). Mais il offre également des outils d'analyse plus évolués comme le vocabulaire statistiquement plus spécifique d'un texte du corpus ou les mots statistiquement les plus présents dans le voisinage d'un mot donné.
L'utilisateur choisit sa stratégie de travail avec TXM en fonction de ses préférences et de son expérience et peut choisir d'utiliser progressivement des outils plus évolués si cela s'avère pertinent.

Par exemple, si TXM est seulement utilisé comme un concordancier, l'utilisateur pourra approfondir progressivement sa maitrise des expressions de requêtes du moteur de rercherche plein texte en fonction des limites d'expression qu'il cherchera à dépasser pour son travail. Par exemple, en développant la recherche d'une séquence de mots variable (un mot donné suivi d'un adjectif par exemple) plutôt que la recherche d'un mot simple.

### Traite tout texte ou extrait de texte facilement
{: .no_toc}

TXM permet de travailler directement sur du texte ayant été préalablement copié dans le presse-papier du système par autre application - par sélection + copier (traitement de texte, visionneur PDF, navigateur, messagerie...).
TXM travaille également sur des textes organisés dans des répertoires de fichiers textes suivants divers formats.

### Utilisable progressivement du simple corpus en texte brut aux corpus richement structurés et annotés en XML-TEI
{: .no_toc}

TXM offre une gamme continue de modules d'importation couvrant les formats standard les plus fréquemment utilisés :
{: style="margin-bottom: 5px"}
- TXT : pour tout texte brut venant de traitements de texte, de PDF, de sites web, etc. ;
- XML : pour les textes structurés légèrement (seulement les phrases ou les paragraphes) voire enrichis (avec des balises XML qui encodent certains mots avec des propriétés lexicales) ;
- TEI : pour les textes encodés selon les recommandations du consortium TEI et qui ont vocation à être capitalisés pour des projets à long terme, mutualisés avec d'autres initiatives ou compatibles avec des systèmes d'archivage.

Un projet peut appliquer TXM sur ses données encodées progressivement de la façon la plus simple (et la plus limitée à l'usage) à la façon la plus complexe (et la plus riche). TXM permet donc d'adapter les coûts d'encodage de corpus en fonction des besoins réels de l'étude, en particulier quand ces besoins se découvrent au fur et à mesure du dépouillement du corpus. Dans ces conditions, TXM assiste à la fois l'activité d'encodage et celle d'exploitation des corpus.

### Gère une très grande diversité de formats et de configuration de corpus
{: .no_toc}

TXM ne se limite pas aux corpus textuels. Il permet de travailler sur des corpus de transcriptions de l'oral (où la transcription encode notamment les locuteurs et des points de synchronisation temporelle avec la vidéo ou l'audio), sur des corpus parallèles où des textes sont en relation de traduction (corpus multilingues) ou en relation de version ainsi que sur des corpus enrichis d'annotations linguistiques comme des structures syntaxiques (dans le cas du format TigerSearch). Cette diversité est une garantie de la robustesse du modèle de corpus interne à la plateforme.

### Gère les formats les plus standard
{: .no_toc}

TXM importe le texte brut encodé en [Unicode](http://www.unicode.org) : standard international pour l'encodage des caractères de tous les systèmes d'écriture utilisés dans le monde.
TXM importe les textes encodés en [XML](http://www.w3.org/XML) : standard international du W3C pour l'encodage des données textuelles.
TXM importe déjà plus d'une dizaine d'applications des recommandations du consortium [TEI](http://www.tei-c.org/index.xml).
Ce faisant, TXM accompagne les évolutions de ces standards ce qui est une garantie de stabilité de ses capacités de prise en charge des corpus dans le temps.

### Son moteur de recherche travaille sur les mots et pas seulement sur des chaînes de caractères
{: .no_toc}

Son moteur de recherche intégré, appelé « Corpus Query Processor » ([CQP](http://cwb.sourceforge.net)), permet d'exprimer la recherche - pour l'affichage ou pour le décompte - de séquences de mots plutôt que de caractères. Il est donc particulièrement adapté au travail phraséologique et à la recherche de collocations. Les mots sont par ailleurs accessibles non seulement à partir de leur forme graphique (ou de surface) mais également par le biais de toutes les informations qui leurs sont associées comme leur lemme ou leur catégorie grammaticale. Ceci offre une grande richesse de construction d'observables textuels sur lesquels appliquer tous les outils de textométrie.

### Son modèle de corpus est riche car structuré
{: .no_toc}

Tous les textes des corpus TXM peuvent être structurés selon une arborescence de structures ayant diverses propriétés. Ces structures peuvent être mobilisées par le moteur de recherche (pour borner une recherche de séquences de longueur variable de mots par exemple) ou pour construire des configurations de sous-corpus interne à un texte donné par exemple.

### Permet d'exporter tous ses résultats vers d'autres logiciels
{: .no_toc}

Tous les résultats de calculs sous forme de tableaux sont exportables en CSV pour être manipulés par d'autres logiciels (comme des tableurs) et tous les graphiques produits sont exportables sous forme d'image vectorielle ou bitmap (notamment pour les publications).

### Il est facile à modifier par un informaticien
{: .no_toc}

Tous les informaticiens ont appris la programmation avec le langage [Java](<https://fr.wikipedia.org/wiki/Java_(langage)#Liens_externes>) et l'environnement de développement [Eclipse](https://www.eclipse.org),  utilisé pour développer la plateforme TXM, rend l'intervention dans les sources extrêmement rapide. L'équipe de développement fait beaucoup d'efforts pour documenter sa méthode de travail, produire des manuels pour les développeurs et la documentation du code.

### Son architecture logicielle est robuste
{: .no_toc}

TXM est développé avec les deux langages de programmation les plus utilisés de l'industrie informatique : Java et C. Dans le contexte du langage Java et de son écosystème, TXM est développé avec le framework « Rich Client Platform » ([RCP](<http://wiki.eclipse.org/RCP_FAQ>)) qui obéit au standard d'architecture industriel [OSGi Alliance](<http://www.osgi.org>).

### Il est installable sur toutes les plateformes
{: .no_toc}

TXM est disponible sous la forme d'une application de bureau pour les 3 systèmes d'exploitation les plus utilisés par les chercheurs et le grand public : Windows, Mac OS X et Linux.

### Il permet de mettre en ligne des corpus et de les analyser
{: .no_toc}

TXM est disponible sous la forme d'un serveur web de portail.  Un portail TXM permet de mettre en ligne des corpus manipulés par la version bureau et de donner accès à leur analyse au moyen d'un simple navigateur web. La personne qui accède au portail n'a besoin d'installer ni TXM ni les corpus sur son poste.

### Il est transparent
{: .no_toc}

En donnant systématiquement accès à ses sources, sa licence open-source offre :
{: style="margin-bottom: 5px"}
- une transparence totale aux procédés de calcul utilisés (garantie scientifique de vérifiabilité et de reproductibilité) ;
- la possibilité à chacun de l'améliorer au bénéfice de la communauté de ses utilisateurs.

### Il est accessible
{: .no_toc}

Diffusé gratuitement, il est facilement installable et remplaçable n'importe où, notamment là où les moyens sont limités ou simplement pour être testé en cas de découverte.

## Fonctionnalités de TXM

- construire des sous-corpus à partir de différentes métadonnées (propriétés) des textes (eg. : date de publication, auteur, type de texte, thème) ;
- construire des partitions à partir de ces propriétés permettant d'appliquer des calculs de contraste entre les textes ou entre groupes de textes ;
- produire des concordances kwic à partir de recherches de motifs lexicaux complexes - construires à partir des propriétés des mots (eg. : "un mot de lemme 'aimer' suivi à au plus de 2 mots d'un mot commençant par 'pouv'). Depuis chaque ligne de concordance, vous pouvez accéder à la lecture de la page de l'édition HTML correspondante ;
- calculer le vocabulaire d'ensemble d'un corpus ou la liste des valeurs attestées d'une propriété de mot donnée ;
- construire une édition HTML de base pour chaque texte du corpus ;
- construire différents tableaux de contingence croisant les mots, les textes et leurs structures ;
- calculer la liste des mots apparaissant de façon préférentielle dans les mêmes contextes qu'un motif lexical complexe (cooccurrents statistiques) ;
- calculer les mots, ou les propriétés de mots, particulièrement présents dans une partie du corpus (spécificités statistiques) ;
- calculer des visualisations du corpus sous forme de cartographie de mots, de propriétés ou de textes  (analyse factorides correspondances avec le package FactoMineR de R) ;
- construire un corpus à partir de diverses sources textuelles. 16 modules d'importation sont disponibles dont[^2] : texte brut combiné à des métadonnées plates (CSV ou Excel ou Calc), XML/w brut+métadonnées[^8], XML-TEI Zéro+métadonnées[^7], XML-TEI BFM[^3] , XML-TXM[^4] , Transcriber+métadonnées[^5] , Hyperbase, Alceste, Cordial ainsi que des prototypes (TMX, Factiva...) ;
- intègre l'application automatique d'outils de traitement automatique de la langue (TAL) sur les textes. Livré avec un plugin de l'étiqueteur morphosyntaxique et lemmatiseur TreeTagger pour différentes langues (TreeTagger est à installer séparément pour des raisons de licence). Les résultats de cet outil sont accessibles dans la plateforme sous forme de propriétés de mots (eg. : mot "aime", étiquette morphosyntaxique "VER:pres" - verbe au présent de l'indicatif, lemme "aimer") ;
- exporter tous les résultats au format CSV pour les listes et au format SVG pour les graphiques ;
- peut être piloté par des scripts Groovy ou R.

## Voir aussi

- [la fiche TXM sur le wiki de la TEI](http://wiki.tei-c.org/index.php/TXM)
- [la fiche TAPoR 2 de TXM](http://tapor.ca/tools/194)
- [la fiche Bamboo DIRT de TXM](http://dirtdirectory.org/resources/txm)
- [la fiche PLUME de TXM](http://www.projet-plume.org/relier/txm)

Notes:

[^1]: Financé par le contrat ANR-06-CORP-029, 2007-2010 - voir l'[Accueil](http://textometrie.ens-lyon.fr) de ce site
[^2]: Voir leur description respective dans la [documentation en ligne](http://textometrie.ens-lyon.fr/html/doc/manual/0.7.9/fr/manual94.xhtml)
[^3]: Tel que défini par le guide d'encodage XML de textes compatible avec la TEI du [projet BFM](http://bfm.ens-lyon.fr)
[^4]: Un format pivot interne orienté TAL compatible XML-TEI
[^5]: Tel que défini par le logiciel [Transcriber](<http://trans.sourceforge.net>)
[^6]: Autres informations concernant le projet Textométrie et la plateforme TXM : [Publications du projet]({{ "Publications-equipe" | relative_url }}), [Manuel de référence](https://gitlab.huma-num.fr/txm/textometrie/-/raw/master/files/documentation/TXM%20Reference%20Manual%200.5_EN.pdf?inline=false), [Wiki des utilisateurs](https://groupes.renater.fr/wiki/txm-users), [Wiki des développeurs](https://groupes.renater.fr/wiki/txm-info)
[^7]: format XML compatible TEI, composé d'un jeu minimal de balises : `cell`, `emph`, `graphic`, `hi`, `head`, `item`, `lb`, `list`, `note`, `p`, `pb`, `ref`, `row`, `table`, `text`, `w`.
[^8]: n'importe quel XML où la balise `w` peut encoder des mots
